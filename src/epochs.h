#pragma once

#include <cinttypes>

constexpr int64_t rd_epoch = 0;
constexpr int64_t gregorian_epoch = 1;
constexpr int64_t egyptian_epoch = -272787;
constexpr int64_t armenian_epoch = 201443;
constexpr int64_t akan_epoch = 37;
constexpr int64_t julian_epoch = -1;
constexpr int64_t balinese_pawukon_epoch = -1721279;
constexpr int64_t coptic_epoch = 103605;
constexpr int64_t ethiopic_epoch = 2796;
constexpr int64_t hebrew_epoch = -1373427;
constexpr int64_t icelandic_epoch = 109;
constexpr int64_t islamic_epoch = 227015;
constexpr double  julian_day_epoch = -1721424.5;
constexpr int64_t mayan_long_date_epoch = -1137142;
constexpr int64_t mayan_haab_epoch = mayan_long_date_epoch - 348;
constexpr int64_t mayan_tzolkin_epoch = mayan_long_date_epoch - 159;
constexpr int64_t unix_timestamp_epoch = 719163;
constexpr double  modified_julian_day_epoch = 678576;
constexpr int64_t zoroastrian_epoch = 230638;

// Correlation is based on the fall of Mexico City to Hernán Cortés on August 13, 1521 (Julian)
constexpr int64_t aztec_correlation = 555403;
constexpr int64_t tonalpohualli_correlation = aztec_correlation - 104;
constexpr int64_t xihuitl_correlation = aztec_correlation - 201;

constexpr double  arya_lunar_month                 = 1577917500.0 / 53433336.0;
constexpr double  arya_lunar_day                   = arya_lunar_month / 30.0;
constexpr int64_t old_hindu_solar_epoch            = -1132959;
constexpr double  arya_solar_year                  = 15779175.0 / 43200.0;
constexpr double  arya_solar_month                 = arya_solar_year / 12.0;
constexpr double  arya_jovian_period               = 1577917500.0 / 364224.0;
