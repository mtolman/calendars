#include <calendars/c/calendars/iso.h>
#include <calendars/cxx/calendars/gregorian.h>
#include <calendars/cxx/calendars/iso.h>

#include "epochs.h"
#include "utils.h"

CalendarIsoDate calendar_iso_create(int64_t year, int16_t week, int16_t day) { return CalendarIsoDate{year, week, day}; }

void calendar_iso_init(CalendarIsoDate *date, int64_t year, int16_t week, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_iso_create(year, week, day);
}

int calendar_iso_compare(const CalendarIsoDate *left, const CalendarIsoDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->year, right->year, left->week, right->week, left->day, right->day);
}

bool calendar_iso_is_long_year(int64_t year) {
  const auto [ jan1, dec31 ] = calendars::gregorian::Date::year_boundaries(year);
  return calendars::RdDate::from(jan1).day_of_week() == calendars::DAY_OF_WEEK::THURSDAY
         || calendars::RdDate::from(dec31).day_of_week() == calendars::DAY_OF_WEEK::THURSDAY;
}

CalendarIsoDate calendar_iso_from_rd_date(CalendarRdDate rdDateInput) {
  namespace math       = calendars::math;
  const auto rdDate    = calendars::RdDate{rdDateInput};
  const auto approx    = rdDate.sub_days(3).to<calendars::gregorian::Date>().year();
  const auto approxIso = calendar_iso_create(approx + 1, 1, 1);
  const auto isoYear   = (rdDateInput >= calendar_iso_to_rd_date(&approxIso) ? approx + 1 : approx);
  const auto isoStart  = calendar_iso_create(isoYear, 1, 1);
  const auto isoWeek   = static_cast<int16_t>(math::floor(static_cast<double>(rdDateInput - calendar_iso_to_rd_date(&isoStart)) / 7.0) + 1);
  const auto isoDay    = static_cast<int16_t>(math::amod(rdDateInput, 7));
  return calendar_iso_create(isoYear, isoWeek, isoDay);
}

CalendarRdDate calendar_iso_to_rd_date(const CalendarIsoDate *date) {
  if (date == nullptr) {
    return 0;
  }

  const auto reference     = calendar_gregorian_create(date->year - 1, MONTH_DECEMBER, 28);
  const auto gregReference = calendar_gregorian_to_rd_date(&reference);
  return calendar_nth_week_day(gregReference, date->week, DAY_OF_WEEK_SUNDAY) + date->day;
}
