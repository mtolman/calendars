#include <calendars/c/calendars/gregorian.h>
#include <calendars/cxx/calendars/gregorian.h>
#include <calendars/cxx/math/common.h>

#include <array>

#include "utils.h"

CalendarGregorianDate calendar_gregorian_create(int64_t year, CALENDAR_GREGORIAN_MONTH month, int16_t day) {
  return CalendarGregorianDate{year, static_cast<int8_t>(month), day};
}

void calendar_gregorian_init(CalendarGregorianDate *date, int64_t year, CALENDAR_GREGORIAN_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }
  date->year  = year;
  date->month = month;
  date->day   = day;
}

int calendar_gregorian_compare(const CalendarGregorianDate *left, const CalendarGregorianDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

bool calendar_gregorian_is_leap_year(int64_t year) {
  namespace math        = calendars::math;
  const auto yearMod400 = math::mod(year, 400);
  return math::mod(year, 4) == 0 && yearMod400 != 100 && yearMod400 != 200 && yearMod400 != 300;
}

CalendarGregorianDate calendar_gregorian_year_start(int64_t year) { return CalendarGregorianDate{year, MONTH_JANUARY, 1}; }

CalendarGregorianDate calendar_gregorian_year_end(int64_t year) { return CalendarGregorianDate{year, MONTH_DECEMBER, 31}; }

int64_t calendar_gregorian_day_difference(const CalendarGregorianDate *left, const CalendarGregorianDate *right) {
  CalendarRdDate leftDay  = calendar_gregorian_to_rd_date(left);
  CalendarRdDate rightDay = calendar_gregorian_to_rd_date(right);
  return leftDay - rightDay;
}

int64_t calendar_gregorian_day_number(const CalendarGregorianDate *date) {
  if (date == nullptr) {
    return 0;
  }

  auto end = CalendarGregorianDate{date->year - 1, MONTH_DECEMBER, 31};
  return calendar_gregorian_day_difference(date, &end);
}

int64_t calendar_gregorian_days_remaining(const CalendarGregorianDate *date) {
  if (date == nullptr) {
    return 0;
  }

  auto end = CalendarGregorianDate{date->year, MONTH_DECEMBER, 31};
  return calendar_gregorian_day_difference(&end, date);
}

static auto year_from_rd_date(CalendarRdDate date) {
  namespace math = calendars::math;
  const auto d0  = date - gregorian_epoch;
  // 146097 = number of days in 400 years (adjusted for leap years)
  const auto n400 = math::floor<double, int64_t>(static_cast<double>(d0) / 146097.0);
  const auto d1   = math::mod(d0, 146097);
  // 36524 = number of days in 100 years (adjusted for leap years)
  const auto n100 = math::floor<double, int64_t>(static_cast<double>(d1) / 36524.0);
  const auto d2   = math::mod(d1, 36524);
  // 1461 = number of days in 4 years (adjusted for leap years)
  const auto n4 = math::floor<double, int64_t>(static_cast<double>(d2) / 1461.0);
  const auto d3 = math::mod(d2, 1461);
  const auto n1 = math::floor<double, int64_t>(static_cast<double>(d3) / 365.0);
  const auto year =
      400 * n400 + 100 * n100 + 4 * n4 + n1 + 1 - static_cast<int64_t>(static_cast<unsigned>(n100 == 4) | static_cast<unsigned>(n1 == 4));
  return year;
}

CalendarGregorianDate calendar_gregorian_from_rd_date(CalendarRdDate date) {
  namespace math                = calendars::math;
  const auto year               = year_from_rd_date(date);
  const auto yearStart          = calendar_gregorian_year_start(year);
  const auto yearStartRdDate    = calendar_gregorian_to_rd_date(&yearStart);
  const auto priorDays          = date - yearStartRdDate;
  const auto marchFirst         = CalendarGregorianDate{year, MONTH_MARCH, 1};
  const auto marchFirstRdDate   = calendar_gregorian_to_rd_date(&marchFirst);
  const auto correction         = date < marchFirstRdDate ? 0 : calendar_gregorian_is_leap_year(year) ? 1 : 2;
  const auto month              = math::floor<double, int16_t>(static_cast<double>(12 * (priorDays + correction) + 373) / 367.0);
  const auto firstOfMonth       = CalendarGregorianDate{year, month, 1};
  const auto firstOfMonthRdDate = calendar_gregorian_to_rd_date(&firstOfMonth);
  return CalendarGregorianDate{year, month, static_cast<int16_t>(date - firstOfMonthRdDate + 1)};
}

CalendarRdDate calendar_gregorian_to_rd_date(const CalendarGregorianDate *date) {
  namespace math = calendars::math;

  if (date == nullptr) {
    return 0;
  }

  return gregorian_epoch - 1 + 365 * (date->year - 1) + math::floor<double, int64_t>(static_cast<double>(date->year - 1) / 4.0)
         - math::floor<double, int64_t>(static_cast<double>(date->year - 1) / 100.0)
         + math::floor<double, int64_t>(static_cast<double>(date->year - 1) / 400.0)
         + math::floor<double, int64_t>((367 * static_cast<double>(date->month) - 362) / 12.0)
         + (static_cast<int16_t>(date->month) <= 2        ? 0
            : calendar_gregorian_is_leap_year(date->year) ? -1
                                                          : -2)
         + date->day;
}

CalendarGregorianDate calendar_gregorian_nearest_valid(const CalendarGregorianDate *date) {
  auto rdDate = calendar_gregorian_to_rd_date(date);
  return calendar_gregorian_from_rd_date(rdDate);
}

bool calendar_gregorian_is_valid(const CalendarGregorianDate *date) {
  if (date == nullptr) {
    return false;
  }

  auto validDate = calendar_gregorian_nearest_valid(date);
  return calendar_gregorian_compare(date, &validDate) == 0;
}

constexpr auto month_names = std::array{
    u8"January", u8"February", u8"March", u8"April", u8"May", u8"June", u8"July", u8"August", u8"September", u8"October", u8"November", u8"December"};

const int calendar_gregorian_num_month_names = month_names.size();

const char8_t *const *calendar_gregorian_month_names() { return month_names.data(); }
const char8_t        *calendar_gregorian_month_name(CALENDAR_GREGORIAN_MONTH month) {
  auto monthInt = static_cast<int>(month);
  if (monthInt >= 1 && monthInt <= 12) {
    return month_names.at(monthInt - 1);
  }
  return u8"";
}

int16_t calendar_gregorian_days_in_month(int64_t year, CALENDAR_GREGORIAN_MONTH month) {
  switch (month) {
    case MONTH_JANUARY:
    case MONTH_MARCH:
    case MONTH_MAY:
    case MONTH_JULY:
    case MONTH_AUGUST:
    case MONTH_OCTOBER:
    case MONTH_DECEMBER:
      return 31;
    case MONTH_FEBRUARY:
      return calendar_gregorian_is_leap_year(year) ? 29 : 28;
    case MONTH_APRIL:
    case MONTH_JUNE:
    case MONTH_SEPTEMBER:
    case MONTH_NOVEMBER:
      return 30;
  }
  return 0;
}

auto    calendars::gregorian::Date::month_names() noexcept -> std::array<const char8_t *, 12> { return ::month_names; }
int64_t calendars::gregorian::Date::year_from_rd(const calendars::RdDate &rdDate) { return year_from_rd_date(static_cast<CalendarRdDate>(rdDate)); }
