#include <calendars/c/calendars/clock.h>
#include <calendars/cxx/calendars/clock.h>

#include "utils.h"

CalendarClockDate calendar_clock_create(double day, double hour, double minute, double second) {
  return CalendarClockDate{day, hour, minute, second};
}

void calendar_clock_init(CalendarClockDate *date, double day, double hour, double minute, double second) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_clock_create(day, hour, minute, second);
}

CalendarMoment calendar_clock_to_moment(const CalendarClockDate *date) {
  if (date == nullptr) {
    return 0.0;
  }

  return static_cast<CalendarMoment>(calendars::clock::Date{*date}.to_moment());
}

CalendarClockDate calendar_clock_from_moment(CalendarMoment moment) {
  return static_cast<CalendarClockDate>(calendars::Moment{moment}.to<calendars::clock::Date>());
}

int calendar_clock_compare(const CalendarClockDate *left, const CalendarClockDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->day, right->day, left->hour, right->hour, left->minute, right->minute, left->second, right->second);
}

double calendar_clock_time(const CalendarClockDate *date) { return calendars::Moment{calendar_clock_to_moment(date)}.to_time(); }

CalendarClockDate calendar_clock_nearest_second(const CalendarClockDate *date) {
  if (date == nullptr) {
    return calendar_clock_create(0, 0, 0, 0);
  }

  return static_cast<CalendarClockDate>(calendars::clock::Date{*date}.nearest_second());
}
