#include <calendars/c/calendars/common.h>
#include <calendars/cxx/calendars/common.h>
#include <calendars/cxx/math/common.h>

CALENDAR_DAY_OF_WEEK calendar_day_of_week(CalendarRdDate date) {
  return static_cast<CALENDAR_DAY_OF_WEEK>(calendars::math::mod(date - static_cast<int>(CALENDAR_DAY_OF_WEEK ::DAY_OF_WEEK_SUNDAY), 7.0));
}

int calendar_day_of_m_cycle(CalendarRdDate date, int m, int offset) { return static_cast<int>(calendars::math::mod(date - offset, m)); }

CalendarRdDate calendar_day_of_week_on_or_before(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek) {
  auto dayOfWeekPrev = calendar_day_of_week(date - dayOfWeek);
  return date - dayOfWeekPrev;
}

CalendarRdDate calendar_day_of_week_on_or_after(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek) {
  return calendar_day_of_week_on_or_before(date + 6, dayOfWeek);
}

CalendarRdDate calendar_day_of_week_nearest(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek) {
  return calendar_day_of_week_on_or_before(date + 3, dayOfWeek);
}

CalendarRdDate calendar_day_of_week_before(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek) {
  return calendar_day_of_week_on_or_before(date - 1, dayOfWeek);
}

CalendarRdDate calendar_first_week_day(CALENDAR_DAY_OF_WEEK k, CalendarRdDate d) { return calendar_nth_week_day(d, 1, k); }

CalendarRdDate calendar_last_week_day(CALENDAR_DAY_OF_WEEK k, CalendarRdDate d) { return calendar_nth_week_day(d, -1, k); }

CalendarRdDate calendar_day_of_week_after(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek) {
  return calendar_day_of_week_on_or_before(date + 7, dayOfWeek);
}

CalendarRdDate calendar_kth_day_of_m_cycle_on_or_before(CalendarRdDate date, int k, int m, int offset) {
  auto mCycleDay = calendar_day_of_m_cycle(date - k, m, offset);
  return date - mCycleDay;
}

CalendarRdDate calendar_kth_day_of_m_cycle_before(CalendarRdDate date, int k, int m, int offset) {
  return calendar_kth_day_of_m_cycle_on_or_before(date - 1, k, m, offset);
}

CalendarRdDate calendar_kth_day_of_m_cycle_after(CalendarRdDate date, int k, int m, int offset) {
  return calendar_kth_day_of_m_cycle_on_or_before(date + m, k, m, offset);
}

CalendarRdDate calendar_kth_day_of_m_cycle_on_or_after(CalendarRdDate date, int k, int m, int offset) {
  return calendar_kth_day_of_m_cycle_on_or_before(date + m - 1, k, m, offset);
}

CalendarRdDate calendar_kth_day_of_m_cycle_nearest(CalendarRdDate date, int k, int m, int offset) {
  return calendar_kth_day_of_m_cycle_on_or_before(date + m / 2, k, m, offset);
}

CalendarRdDate calendar_nth_week_day(CalendarRdDate date, int n, CALENDAR_DAY_OF_WEEK k) {
  if (n > 0) {
    return calendar_day_of_week_before(date, k) + static_cast<CalendarRdDate>(7 * n);
  }
  else if (n < 0) {
    return calendar_day_of_week_after(date, k) + static_cast<CalendarRdDate>(7 * n);
  }
  else {
    return date;
  }
}

static constexpr auto day_of_week_names = std::array{u8"Sunday", u8"Monday", u8"Tuesday", u8"Wednesday", u8"Thursday", u8"Friday", u8"Saturday"};

const char8_t* const* calendar_day_of_week_names() { return day_of_week_names.data(); }
const char8_t*        calendar_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < day_of_week_names.size()) {
    return day_of_week_names.at(index);
  }
  return u8"";
}

const int calendar_num_day_of_week_names = day_of_week_names.size();
