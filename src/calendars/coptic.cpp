#include <calendars/c/calendars/coptic.h>
#include <calendars/cxx/calendars/common.h>
#include <calendars/cxx/calendars/coptic.h>
#include <calendars/cxx/math/common.h>

#include "epochs.h"
#include "utils.h"

CalendarCopticDate calendar_coptic_create(int64_t year, CALENDAR_COPTIC_MONTH month, int16_t day) {
  return CalendarCopticDate{year, static_cast<int16_t>(month), day};
}

void calendar_coptic_init(CalendarCopticDate *date, int64_t year, CALENDAR_COPTIC_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_coptic_create(year, month, day);
}

CalendarRdDate calendar_coptic_to_rd_date(const CalendarCopticDate *date) {
  if (date == nullptr) {
    return 0;
  }

  namespace math = calendars::math;
  return coptic_epoch - 1 + 365 * (date->year - 1) + math::floor<double, int64_t>(date->year / 4.0) + 30 * (static_cast<int64_t>(date->month) - 1)
         + date->day;
}

CalendarCopticDate calendar_coptic_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;

  const auto copticYear  = math::floor<double, int64_t>(static_cast<double>(4 * (rdDate - coptic_epoch) + 1463) / 1461.0);
  const auto copticMonth = static_cast<CALENDAR_COPTIC_MONTH>(
      math::floor<double, int16_t>(static_cast<double>(calendars::RdDate{rdDate}.day_difference(
                                       calendars::RdDate::from(calendars::coptic::Date{copticYear, calendars::coptic::MONTH::THOOUT, 1})))
                                   / 30.0)
      + 1);
  const auto copticDay = static_cast<int16_t>(
      calendars::RdDate{rdDate}.add_days(1).day_difference(calendars::RdDate::from(calendars::coptic::Date{copticYear, copticMonth, 1})));
  return calendar_coptic_create(copticYear, copticMonth, copticDay);
}

int calendar_coptic_compare(const CalendarCopticDate *left, const CalendarCopticDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

bool calendar_coptic_is_leap_year(int64_t year) {
  namespace math = calendars::math;
  return math::mod(year, 4) == 3;
}

static constexpr auto transliteration_months          = std::array{u8"Thout",
                                                          u8"Paopi",
                                                          u8"Hathor",
                                                          u8"Koiak",
                                                          u8"Tobi",
                                                          u8"Meshir",
                                                          u8"Paremhat",
                                                          u8"Parmouti",
                                                          u8"Pashons",
                                                          u8"Paoni",
                                                          u8"Epip",
                                                          u8"Mesori",
                                                          u8"Pi Kogi Enanvot"};
const int             calendar_coptic_num_month_names = transliteration_months.size();

const char8_t *const *calendar_coptic_month_names() { return transliteration_months.data(); }

static constexpr auto sahidic_months = std::array{u8"Ⲑⲟⲟⲩⲧ",
                                                  u8"Ⲡⲁⲱⲡⲉ",
                                                  u8"Ϩⲁⲑⲱⲣ",
                                                  u8"Ⲕⲟⲓⲁϩⲕ",
                                                  u8"Ⲧⲱⲃⲉ",
                                                  u8"Ⲙϣⲓⲣ",
                                                  u8"Ⲡⲁⲣⲙϩⲟⲧⲡ",
                                                  u8"Ⲡⲁⲣⲙⲟⲩⲧⲉ",
                                                  u8"Ⲡⲁϣⲟⲛⲥ",
                                                  u8"Ⲡⲁⲱⲛⲉ",
                                                  u8"Ⲉⲡⲏⲡ",
                                                  u8"Ⲙⲉⲥⲱⲣⲏ",
                                                  u8"Ⲉⲡⲁⲅⲟⲙⲉⲛⲁⲓ"};

const char8_t *const *calendar_coptic_month_names_sahidic() { return sahidic_months.data(); }

static constexpr auto bohairic_months = std::array{u8"Ⲑⲱⲟⲩⲧ",
                                                   u8"Ⲡⲁⲟⲡⲓ",
                                                   u8"Ⲁⲑⲱⲣ",
                                                   u8"Ⲭⲟⲓⲁⲕ",
                                                   u8"Ⲧⲱⲃⲓ",
                                                   u8"Ⲙⲉϣⲓⲣ",
                                                   u8"Ⲡⲁⲣⲉⲙϩⲁⲧ",
                                                   u8"Ⲫⲁⲣⲙⲟⲩⲑⲓ",
                                                   u8"Ⲡⲁϣⲟⲛⲥ",
                                                   u8"Ⲡⲁⲱⲛⲓ",
                                                   u8"Ⲉⲡⲓⲡ",
                                                   u8"Ⲙⲉⲥⲱⲣⲓ",
                                                   u8"Ⲡⲓⲕⲟⲩϫⲓ ⲛ̀ⲁ̀ⲃⲟⲧ"};

const char8_t *const *calendar_coptic_month_names_bohairic() { return bohairic_months.data(); }

const char8_t *calendar_coptic_month_name(CALENDAR_COPTIC_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < calendar_coptic_num_month_names) {
    return transliteration_months.at(index);
  }
  return u8"";
}

const char8_t *calendar_coptic_month_name_sahidic(CALENDAR_COPTIC_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < calendar_coptic_num_month_names) {
    return sahidic_months.at(index);
  }
  return u8"";
}

const char8_t *calendar_coptic_month_name_bohairic(CALENDAR_COPTIC_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < calendar_coptic_num_month_names) {
    return bohairic_months.at(index);
  }
  return u8"";
}

static constexpr auto days_of_week = std::array{u8"Tkyriakē", u8"Pesnau", u8"Pshoment", u8"Peftoou", u8"Ptiou", u8"Psoou", u8"Psabbaton"};

const char8_t *const *calendar_coptic_day_of_week_names() { return days_of_week.data(); }
const char8_t        *calendar_coptic_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < days_of_week.size()) {
    return days_of_week.at(index);
  }
  return u8"";
}

auto calendars::coptic::Date::month_names() noexcept -> std::array<const char8_t *, 13> { return transliteration_months; }
