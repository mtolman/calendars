#include <calendars/c/calendars/egyptian.h>
#include <calendars/cxx/calendars/egyptian.h>
#include <calendars/cxx/math/common.h>

#include <array>

#include "utils.h"

CalendarEgyptianDate calendar_egyptian_create(int64_t year, int16_t month, int16_t day) { return CalendarEgyptianDate{year, month, day}; }

void calendar_egyptian_init(CalendarEgyptianDate *date, int64_t year, int16_t month, int16_t day) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_egyptian_create(year, month, day);
}

int calendar_egyptian_compare(const CalendarEgyptianDate *left, const CalendarEgyptianDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

CalendarEgyptianDate calendar_egyptian_nearest_valid(const CalendarEgyptianDate *date) {
  return calendar_egyptian_from_rd_date(calendar_egyptian_to_rd_date(date));
}

bool calendar_egyptian_is_valid(const CalendarEgyptianDate *date) {
  auto valid = calendar_egyptian_nearest_valid(date);
  return calendar_egyptian_compare(date, &valid) == 0;
}

CalendarEgyptianDate calendar_egyptian_from_rd_date(CalendarRdDate rdDate) {
  namespace math           = calendars::math;
  const auto days          = rdDate - egyptian_epoch;
  const auto egyptianYear  = math::floor<double, int64_t>(static_cast<double>(days) / 365.0) + 1;
  const auto egyptianMonth = math::floor<double, int64_t>(math::mod(static_cast<double>(days), 365) / 30.0) + 1;
  const auto egyptianDay   = days - 365 * (egyptianYear - 1) - 30 * (egyptianMonth - 1) + 1;
  return calendar_egyptian_create(static_cast<decltype(CalendarEgyptianDate::year)>(egyptianYear),
                                  static_cast<decltype(CalendarEgyptianDate::month)>(egyptianMonth),
                                  static_cast<decltype(CalendarEgyptianDate::day)>(egyptianDay));
}

CalendarRdDate calendar_egyptian_to_rd_date(const CalendarEgyptianDate *date) {
  if (date == nullptr) {
    return 0;
  }

  return egyptian_epoch + ((365 * (date->year - 1) + 30 * static_cast<int64_t>(date->month - 1) + static_cast<int64_t>(date->day - 1)));
}

static constexpr auto egyptian_names = std::array{
    u8"Thoth", u8"Phaophi", u8"Athyr", u8"Choiak", u8"Tybi", u8"Mechir", u8"Phamenoth", u8"Pharmuthi", u8"Pachon", u8"Payni", u8"Epiphi", u8"Mesori"};

const int calendar_egyptian_num_month_names = egyptian_names.size();

const char8_t *calendar_egyptian_month_name(int16_t month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < egyptian_names.size()) {
    return egyptian_names.at(index);
  }
  return u8"";
}

const char8_t *const *calendar_egyptian_month_names() { return egyptian_names.data(); }
auto                  calendars::egyptian::Date::month_names() noexcept { return egyptian_names; }
