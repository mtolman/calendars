#include <calendars/c/calendars/hebrew.h>
#include <calendars/cxx/calendars/hebrew.h>
#include <calendars/cxx/calendars/holidays/hebrew.h>
#include <calendars/cxx/math/common.h>

#include "epochs.h"
#include "utils.h"

CalendarHebrewDate calendar_hebrew_create(int64_t year, CALENDAR_HEBREW_MONTH month, int16_t day) {
  return CalendarHebrewDate{year, static_cast<int16_t>(month), day};
}

void calendar_hebrew_init(CalendarHebrewDate *date, int64_t year, CALENDAR_HEBREW_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_hebrew_create(year, month, day);
}

int calendar_hebrew_compare(const CalendarHebrewDate *left, const CalendarHebrewDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

bool calendar_hebrew_is_leap_year(int64_t year) {
  namespace math = calendars::math;
  return math::mod(7 * year + 1, 19) < 7;
}

CALENDAR_HEBREW_MONTH calendar_hebrew_last_month_of_year(int64_t year) { return calendar_hebrew_is_leap_year(year) ? HEBREW_ADAR_II : HEBREW_ADAR; }

bool calendar_hebrew_is_sabbatical_year(int64_t year) {
  namespace math = calendars::math;
  return math::mod(year, 7) == 0;
}

double calendar_hebrew_molad(int64_t year, CALENDAR_HEBREW_MONTH month) {
  namespace math = calendars::math;
  const auto y   = month < HEBREW_TISHRI ? year + 1 : year;
  const auto monthsElapsed =
      static_cast<double>(month) - static_cast<double>(HEBREW_TISHRI) + math::floor((235.0 * static_cast<double>(y) - 234.0) / 19.0);
  return static_cast<double>(hebrew_epoch) - (876.0 / 25920.0) + monthsElapsed * (29 + 12.0 / 24.0 + 793.0 / 25920.0);
}

int64_t calendar_hebrew_elapsed_days(int64_t year) {
  namespace math           = calendars::math;
  using D                  = double;
  const auto monthsElapsed = math::floor<D, int64_t>(static_cast<D>(235 * year - 234) / 19.0);
  const auto partsElapsed  = 204 + 793 * math::mod(monthsElapsed, 1080);
  const auto hoursElapsed  = 11 + 12 * monthsElapsed + 793 * math::floor<D, int64_t>(static_cast<D>(monthsElapsed) / 1080.0)
                            + math::floor<D, int64_t>(static_cast<D>(partsElapsed) / 1080);
  const auto days = 29 * monthsElapsed + math::floor<D, int64_t>(static_cast<D>(hoursElapsed) / 24.0);
  return math::mod(3 * (days + 1), 7) < 3 ? days + 1 : days;
}

int64_t calendar_hebrew_year_length_correction(int64_t year) {
  const auto ny0 = calendar_hebrew_elapsed_days(year - 1);
  const auto ny1 = calendar_hebrew_elapsed_days(year);
  const auto ny2 = calendar_hebrew_elapsed_days(year + 1);
  if (ny2 - ny1 == 356) {
    return 2;
  }
  else if (ny1 - ny0 == 382) {
    return 1;
  }
  else {
    return 0;
  }
}

CalendarRdDate calendar_hebrew_new_year(int64_t year) {
  return hebrew_epoch + calendar_hebrew_elapsed_days(year) + calendar_hebrew_year_length_correction(year);
}

int16_t calendar_hebrew_last_day_of_month(int64_t year, CALENDAR_HEBREW_MONTH month) {
  constexpr int16_t shortDays = 29;
  constexpr int16_t longDays  = 30;
  const auto        fixedShortMonth =
      month == HEBREW_IYYAR || month == HEBREW_TAMMUZ || month == HEBREW_ELUL || month == HEBREW_TEVET || month == HEBREW_ADAR_II;
  if (fixedShortMonth) {
    return shortDays;
  }

  const auto shortAdar = month == HEBREW_ADAR && !calendar_hebrew_is_leap_year(year);
  if (shortAdar) {
    return shortDays;
  }

  const auto isLongMarheshvan = month == HEBREW_MARHESHVAN && !calendar_hebrew_long_marheshvan(year);
  if (isLongMarheshvan) {
    return shortDays;
  }

  const auto isShortKislev = month == HEBREW_KISLEV && calendar_hebrew_short_kislev(year);
  if (isShortKislev) {
    return shortDays;
  }

  return longDays;
}

bool calendar_hebrew_long_marheshvan(int64_t year) {
  const auto days = calendar_hebrew_days_in_year(year);
  return days == 355 || days == 385;
}

bool calendar_hebrew_short_kislev(int64_t year) {
  const auto days = calendar_hebrew_days_in_year(year);
  return days == 353 || days == 383;
}

int64_t calendar_hebrew_days_in_year(int64_t year) { return calendar_hebrew_new_year(year + 1) - calendar_hebrew_new_year(year); }

CalendarHebrewDate calendar_hebrew_from_rd_date(CalendarRdDate rdDateInput) {
  namespace math        = calendars::math;
  const auto rdDate     = calendars::RdDate{rdDateInput};
  const auto approx     = math::floor<double, int64_t>((98496 * static_cast<double>(rdDateInput - hebrew_epoch)) / 35975351.0) + 1;
  int64_t    hebrewYear = approx - 1;

  for (int adj = 0; adj < 2 && calendar_hebrew_new_year(approx + adj) <= rdDateInput; ++adj) {
    hebrewYear = approx + adj;
  }

  const auto start       = rdDate < calendars::RdDate::from(calendars::hebrew::Date{hebrewYear, HEBREW_NISAN, 1}) ? HEBREW_TISHRI : HEBREW_NISAN;
  auto       hebrewMonth = start;

  for (int16_t adj = 0; adj < 14
                        && rdDate > calendars::RdDate::from(
                               calendars::hebrew::Date{hebrewYear, hebrewMonth, calendar_hebrew_last_day_of_month(hebrewYear, hebrewMonth)});
       adj += 1) {
    hebrewMonth = static_cast<CALENDAR_HEBREW_MONTH>(static_cast<int16_t>(start) + adj);
  }

  const auto hebrewDay = calendars::RdDate{rdDate}.day_difference(calendars::RdDate::from(calendars::hebrew::Date{hebrewYear, hebrewMonth, 1})) + 1;

  return calendar_hebrew_create(hebrewYear, hebrewMonth, static_cast<int16_t>(hebrewDay));
}

CalendarRdDate calendar_hebrew_to_rd_date(const CalendarHebrewDate *date) {
  if (date == nullptr) {
    return 0;
  }

  int64_t calcMonth = 0;
  if (date->month < HEBREW_TISHRI) {
    auto lastMonth = calendar_hebrew_last_month_of_year(date->year);
    for (auto m = static_cast<int16_t>(HEBREW_TISHRI); m <= static_cast<int16_t>(lastMonth) && m > 0; ++m) {
      calcMonth += calendar_hebrew_last_day_of_month(date->year, static_cast<CALENDAR_HEBREW_MONTH>(m));
    }
    for (auto m = static_cast<int16_t>(HEBREW_NISAN); m < static_cast<int16_t>(date->month) && m > 0; ++m) {
      calcMonth += calendar_hebrew_last_day_of_month(date->year, static_cast<CALENDAR_HEBREW_MONTH>(m));
    }
  }
  else {
    for (auto m = static_cast<int16_t>(HEBREW_TISHRI); m < static_cast<int16_t>(date->month) && m > 0; ++m) {
      calcMonth += calendar_hebrew_last_day_of_month(date->year, static_cast<CALENDAR_HEBREW_MONTH>(m));
    }
  }
  return calendar_hebrew_new_year(date->year) + date->day - 1 + calcMonth;
}

static constexpr auto days_of_week_transliterated = std::array{u8"rishon", u8"sheni", u8"shlishi", u8"revi'i", u8"hamishi", u8"shishi", u8"shabbat"};

static constexpr auto days_of_week_hebrew = std::array{u8"ראשון", u8"שני", u8"שלישי", u8"רביעי", u8"חמישי", u8"שישי", u8"שבת"};

const char8_t *const *calendar_hebrew_day_of_week_names() { return days_of_week_transliterated.data(); }

const char8_t *const *calendar_hebrew_day_of_week_names_hebrew() { return days_of_week_hebrew.data(); }

const char8_t *calendar_hebrew_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < days_of_week_transliterated.size()) {
    return days_of_week_transliterated.at(index);
  }
  return u8"";
}

const char8_t *calendar_hebrew_day_of_week_name_hebrew(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < days_of_week_hebrew.size()) {
    return days_of_week_hebrew.at(index);
  }
  return u8"";
}

static constexpr auto transliterated_month_names = std::array{u8"Nisan",
                                                              u8"Iyyar",
                                                              u8"Sivan",
                                                              u8"Tammuz",
                                                              u8"Av",
                                                              u8"Elul",
                                                              u8"Tishri",
                                                              u8"Marheshvan",
                                                              u8"Kislev",
                                                              u8"Tevet",
                                                              u8"Shevat",
                                                              u8"Adar I",
                                                              u8"Adar II"};

static constexpr auto hebrew_month_names =
    std::array{u8"נִיסָן", u8"אִייָר", u8"סיוון", u8"תַּמּוּז", u8"אָב", u8"אֱלוּל", u8"תִּשׁרִי", u8"מרחשוון", u8"כסליו", u8"טֵבֵת", u8"שְׁבָט", u8"אֲדָר א׳", u8"אֲדָר ב׳"};

const char8_t *const *calendar_hebrew_month_names() { return transliterated_month_names.data(); }

const char8_t *const *calendar_hebrew_month_names_hebrew() { return hebrew_month_names.data(); }

const char8_t *calendar_hebrew_month_name(CALENDAR_HEBREW_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < transliterated_month_names.size()) {
    return transliterated_month_names.at(index);
  }
  return u8"";
}

const char8_t *calendar_hebrew_month_name_hebrew(CALENDAR_HEBREW_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < hebrew_month_names.size()) {
    return hebrew_month_names.at(index);
  }
  return u8"";
}

const int calendar_hebrew_num_month_names = hebrew_month_names.size();

auto calendars::hebrew::Date::day_of_week_names() noexcept -> std::array<const char8_t *, 7> { return days_of_week_transliterated; }

auto calendars::hebrew::Date::month_names() noexcept -> std::array<const char8_t *, 13> { return transliterated_month_names; }

auto calendars::hebrew::Date::month_names_hebrew() noexcept -> std::array<const char8_t *, 13> { return hebrew_month_names; }

#include <calendars/cxx/calendars/gregorian.h>

int64_t calendars::hebrew::Date::year_from_gregorian(int64_t gregorianYear) {
  return gregorianYear - calendars::gregorian::Date::year_from_rd(calendars::RdDate{hebrew_epoch}) + 1;
}
