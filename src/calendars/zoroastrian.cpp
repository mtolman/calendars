#include <calendars/c/calendars/zoroastrian.h>
#include <calendars/cxx/calendars/egyptian.h>
#include <calendars/cxx/calendars/zoroastrian.h>

#include <array>

#include "utils.h"

CalendarZoroastrianDate calendar_zoroastrian_create(int64_t year, int16_t month, int16_t day) { return CalendarZoroastrianDate{year, month, day}; }

void calendar_zoroastrian_init(CalendarZoroastrianDate *date, int64_t year, int16_t month, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_zoroastrian_create(year, month, day);
}

int calendar_zoroastrian_compare(const CalendarZoroastrianDate *left, const CalendarZoroastrianDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

CalendarZoroastrianDate calendar_zoroastrian_from_rd_date(CalendarRdDate rdDate) {
  auto egyptian = calendars::RdDate{rdDate}.add_days(egyptian_epoch - zoroastrian_epoch).to<calendars::egyptian::Date>();
  return calendar_zoroastrian_create(egyptian.year(), egyptian.month(), egyptian.day());
}

CalendarRdDate calendar_zoroastrian_to_rd_date(const CalendarZoroastrianDate *date) {
  if (date == nullptr) {
    return 0;
  }
  return zoroastrian_epoch + static_cast<CalendarRdDate>(calendars::RdDate::from(calendars::egyptian::Date{date->year, date->month, date->day}))
         - egyptian_epoch;
}

static constexpr auto month_names = std::array{
    u8"Farvardin", u8"Ardibehesht", u8"Khordad", u8"Tir", u8"Amardad", u8"Shehrevar", u8"Mehr", u8"Aban", u8"Azar", u8"Dae", u8"Bahman", u8"Asfand"};

static constexpr auto day_names = std::array{u8"Hormuz",
                                             u8"Bahman",
                                             u8"Ord\u012bbehesht",
                                             u8"Shahr\u012bvar",
                                             u8"Esfand\u0101rmud",
                                             u8"Xord\u0101d",
                                             u8"Mord\u0101d",
                                             u8"Diy be \u0100zar",
                                             u8"\u0100zar",
                                             u8"\u0100b\u0101n",
                                             u8"Xor",
                                             u8"M\u0101h",
                                             u8"T\u012br",
                                             u8"Goosh",
                                             u8"Diy be Mehr",
                                             u8"Mehr",
                                             u8"Sor\u016bsh",
                                             u8"Rashn",
                                             u8"Favard\u012bn",
                                             u8"Bahr\u0101m",
                                             u8"R\u0101m",
                                             u8"B\u0101d",
                                             u8"Diy be D\u012bn",
                                             u8"D\u012bn",
                                             u8"Ard",
                                             u8"Asht\u0101d",
                                             u8"Asm\u0101n",
                                             u8"Z\u0101my\u0101d",
                                             u8"M\u0101resfand",
                                             u8"An\u012br\u0101n"};

static constexpr auto epagomanae_names = std::array{u8"Ahnad", u8"Ashnad", u8"Esfand\u0101rmud", u8"Axshatar", u8"Behesht"};

const char8_t *const *calendar_zoroastrian_month_names() { return month_names.data(); }
const int             calendar_zoroastrian_num_month_names = month_names.size();

const char8_t *calendar_zoroastrian_month_name(int16_t month) {
  auto index = static_cast<size_t>(month) - 1;
  if (index < month_names.size()) {
    return month_names.at(index);
  }
  return u8"";
}

const int             calendar_zoroastrian_num_day_names = day_names.size();
const char8_t *const *calendar_zoroastrian_day_names() { return static_cast<const char8_t *const *>(day_names.data()); }

const char8_t *calendar_zoroastrian_day_name(int16_t day) {
  auto index = static_cast<size_t>(day) - 1;
  if (index < day_names.size()) {
    return day_names.at(index);
  }
  return u8"";
}

const int             calendar_zoroastrian_num_epagomanae_day_names = epagomanae_names.size();
const char8_t *const *calendar_zoroastrian_epagomanae_day_names() { return epagomanae_names.data(); }

const char8_t *calendar_zoroastrian_epagomanae_day_name(int16_t day) {
  auto index = static_cast<size_t>(day) - 1;
  if (index < epagomanae_names.size()) {
    return epagomanae_names.at(index);
  }
  return u8"";
}

auto calendars::zoroastrian::Date::month_names() const -> std::array<const char8_t *, 12> { return ::month_names; }
auto calendars::zoroastrian::Date::day_names() const -> std::array<const char8_t *, 30> { return ::day_names; }
auto calendars::zoroastrian::Date::epagomanae_names() const -> std::array<const char8_t *, 5> { return ::epagomanae_names; }
