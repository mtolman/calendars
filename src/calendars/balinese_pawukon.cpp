#include <calendars/c/calendars/balinese_pawukon_date.h>
#include <calendars/cxx/calendars/balinese_pawukon_date.h>
#include <calendars/cxx/math/common.h>

#include <array>

#include "utils.h"

CalendarBalinesePawukonDate calendar_balinese_pawukon_create(bool                                luang,
                                                             CALENDAR_BALINESE_PAWUKON_DWIWARA   dwiwara,
                                                             CALENDAR_BALINESE_PAWUKON_TRIWARA   triwara,
                                                             CALENDAR_BALINESE_PAWUKON_CATURWARA caturwara,
                                                             CALENDAR_BALINESE_PAWUKON_PANCAWARA pancawara,
                                                             CALENDAR_BALINESE_PAWUKON_SADWARA   sadwara,
                                                             CALENDAR_BALINESE_PAWUKON_SAPTAWARA saptawara,
                                                             CALENDAR_BALINESE_PAWUKON_ASATAWARA asatawara,
                                                             CALENDAR_BALINESE_PAWUKON_SANGAWARA sangawara,
                                                             CALENDAR_BALINESE_PAWUKON_DASAWARA  dasawara) {
  return CalendarBalinesePawukonDate{static_cast<int>(luang),
                                     dwiwara,
                                     triwara,
                                     caturwara,
                                     pancawara,
                                     sadwara,
                                     saptawara,
                                     asatawara,
                                     sangawara,
                                     dasawara};
}

void calendar_balinese_pawukon_init(CalendarBalinesePawukonDate        *date,
                                    bool                                luang,
                                    CALENDAR_BALINESE_PAWUKON_DWIWARA   dwiwara,
                                    CALENDAR_BALINESE_PAWUKON_TRIWARA   triwara,
                                    CALENDAR_BALINESE_PAWUKON_CATURWARA caturwara,
                                    CALENDAR_BALINESE_PAWUKON_PANCAWARA pancawara,
                                    CALENDAR_BALINESE_PAWUKON_SADWARA   sadwara,
                                    CALENDAR_BALINESE_PAWUKON_SAPTAWARA saptawara,
                                    CALENDAR_BALINESE_PAWUKON_ASATAWARA asatawara,
                                    CALENDAR_BALINESE_PAWUKON_SANGAWARA sangawara,
                                    CALENDAR_BALINESE_PAWUKON_DASAWARA  dasawara) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_balinese_pawukon_create(luang, dwiwara, triwara, caturwara, pancawara, sadwara, saptawara, asatawara, sangawara, dasawara);
}

int calendar_balinese_pawukon_compare(const CalendarBalinesePawukonDate *left, const CalendarBalinesePawukonDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->ekawara,
                                     right->ekawara,
                                     left->dwiwara,
                                     right->dwiwara,
                                     left->triwara,
                                     right->triwara,
                                     left->caturwara,
                                     right->caturwara,
                                     left->pancawara,
                                     right->pancawara,
                                     left->sadwara,
                                     right->sadwara,
                                     left->saptawara,
                                     right->saptawara,
                                     left->asatawara,
                                     right->asatawara,
                                     left->sangawara,
                                     right->sangawara,
                                     left->dasawara,
                                     right->dasawara);
}

CalendarBalinesePawukonDate calendar_balinese_pawukon_from_rd_date(CalendarRdDate rdDate) {
  return calendar_balinese_pawukon_create(calendar_balinese_pawukon_luang_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_dwiwara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_triwara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_caturwara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_pancawara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_sadawara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_saptawara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_asatawara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_sangawara_from_rd_date(rdDate),
                                          calendar_balinese_pawukon_dasawara_from_rd_date(rdDate));
}

int calendar_balinese_pawukon_day_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<int>(math::mod(rdDate - balinese_pawukon_epoch, 210));
}

CALENDAR_BALINESE_PAWUKON_TRIWARA calendar_balinese_pawukon_triwara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_TRIWARA>(math::mod(calendar_balinese_pawukon_day_from_rd_date(rdDate), 3) + 1);
}

CALENDAR_BALINESE_PAWUKON_SADWARA calendar_balinese_pawukon_sadawara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_SADWARA>(math::mod(calendar_balinese_pawukon_day_from_rd_date(rdDate), 6) + 1);
}

CALENDAR_BALINESE_PAWUKON_SAPTAWARA calendar_balinese_pawukon_saptawara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_SAPTAWARA>(math::mod(calendar_balinese_pawukon_day_from_rd_date(rdDate), 7) + 1);
}

CALENDAR_BALINESE_PAWUKON_PANCAWARA calendar_balinese_pawukon_pancawara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_PANCAWARA>(math::amod(calendar_balinese_pawukon_day_from_rd_date(rdDate) + 2, 5));
}

int calendar_balinese_pawukon_week_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return math::floor<double, int>(static_cast<double>(calendar_balinese_pawukon_day_from_rd_date(rdDate)) / 6.0) + 1;
}

static constexpr auto dasawara_i_arr = std::array{5, 9, 7, 4, 8};
static constexpr auto dasawara_j_arr = std::array{5, 4, 3, 7, 8, 6, 9};

CALENDAR_BALINESE_PAWUKON_DASAWARA calendar_balinese_pawukon_dasawara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  const auto i   = static_cast<int>(calendar_balinese_pawukon_pancawara_from_rd_date(rdDate)) - 1;
  const auto j   = static_cast<int>(calendar_balinese_pawukon_saptawara_from_rd_date(rdDate)) - 1;
  return static_cast<CALENDAR_BALINESE_PAWUKON_DASAWARA>(math::mod(1 + dasawara_i_arr.at(i) + dasawara_j_arr.at(j), 10));
}

CALENDAR_BALINESE_PAWUKON_DWIWARA calendar_balinese_pawukon_dwiwara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_DWIWARA>(math::amod(static_cast<int>(calendar_balinese_pawukon_dasawara_from_rd_date(rdDate)), 2));
}

bool calendar_balinese_pawukon_luang_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return math::mod(static_cast<int>(calendar_balinese_pawukon_dasawara_from_rd_date(rdDate)), 2) == 0;
}

CALENDAR_BALINESE_PAWUKON_SANGAWARA calendar_balinese_pawukon_sangawara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_SANGAWARA>(math::mod(std::max(0, calendar_balinese_pawukon_day_from_rd_date(rdDate) - 3), 9) + 1);
}

CALENDAR_BALINESE_PAWUKON_ASATAWARA calendar_balinese_pawukon_asatawara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_ASATAWARA>(
      math::mod(std::max(6, 4 + math::mod(calendar_balinese_pawukon_day_from_rd_date(rdDate) - 70, 210)), 8) + 1);
}

CALENDAR_BALINESE_PAWUKON_CATURWARA calendar_balinese_pawukon_caturwara_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return static_cast<CALENDAR_BALINESE_PAWUKON_CATURWARA>(math::amod(static_cast<int>(calendar_balinese_pawukon_asatawara_from_rd_date(rdDate)), 4));
}

CalendarRdDate calendar_balinese_pawukon_on_or_before(CalendarRdDate rdDate, const CalendarBalinesePawukonDate *balinesePawukonDate) {
  if (balinesePawukonDate == nullptr) {
    return 0;
  }

  namespace math = calendars::math;

  const auto a5    = static_cast<int>(balinesePawukonDate->pancawara) - 1;
  const auto a6    = static_cast<int>(balinesePawukonDate->sadwara) - 1;
  const auto b7    = static_cast<int>(balinesePawukonDate->saptawara) - 1;
  const auto b35   = math::mod(a5 + 14 + 15 * (b7 - a5), 35);
  const auto days  = a6 + 36 * (b35 - a6);
  const auto delta = calendar_balinese_pawukon_day_from_rd_date(0);
  return rdDate - math::mod(rdDate + delta - days, 210);
}

static constexpr auto cycle_names = std::array{
    u8"Ekawara", u8"Dwiwara", u8"Triwara", u8"Caturwara", u8"Pancawara", u8"Sadwara", u8"Saptawara", u8"Asatawara", u8"Sangawara", u8"Dasawara"};
const int calendar_balinese_pawukon_num_cycle_names = cycle_names.size();

const char8_t *const *calendar_balinese_pawukon_cycle_names() { return cycle_names.data(); }

static constexpr auto ekawara_names = std::array{u8"Luang"};

const char8_t *const *calendar_balinese_pawukon_cycle_ekawara_names() { return ekawara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_ekawara_names = ekawara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_ekawara_name(bool ekawara) { return ekawara ? ekawara_names[ 0 ] : u8""; }

static constexpr auto dwiwara_names = std::array{u8"Menga", u8"Pepet"};

const char8_t *const *calendar_balinese_pawukon_cycle_dwiwara_names() { return dwiwara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_dwiwara_names = dwiwara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_dwiwara_name(CALENDAR_BALINESE_PAWUKON_DWIWARA dwiwara) {
  const auto index = static_cast<size_t>(dwiwara) - 1;
  if (index < dwiwara_names.size()) {
    return dwiwara_names.at(index);
  }
  return u8"";
}

static constexpr auto triwara_names = std::array{u8"Pasah", u8"Beteng", u8"Kajeng"};

const char8_t *const *calendar_balinese_pawukon_cycle_triwara_names() { return triwara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_triwara_names = triwara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_triwara_name(CALENDAR_BALINESE_PAWUKON_TRIWARA triwara) {
  const auto index = static_cast<size_t>(triwara) - 1;
  if (index < triwara_names.size()) {
    return triwara_names.at(index);
  }
  return u8"";
}

static constexpr auto caturwara_names = std::array{u8"Sri", u8"Laba", u8"Jaya", u8"Menala"};

const char8_t *const *calendar_balinese_pawukon_cycle_caturwara_names() { return caturwara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_caturwara_names = caturwara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_caturwara_name(CALENDAR_BALINESE_PAWUKON_CATURWARA caturwara) {
  const auto index = static_cast<size_t>(caturwara) - 1;
  if (index < caturwara_names.size()) {
    return caturwara_names.at(index);
  }
  return u8"";
}

static constexpr auto pancawara_names = std::array{u8"Umanis", u8"Paing", u8"Pon", u8"Wage", u8"Keliwon"};

const char8_t *const *calendar_balinese_pawukon_cycle_pancawara_names() { return pancawara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_pancawara_names = pancawara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_pancawara_name(CALENDAR_BALINESE_PAWUKON_PANCAWARA pancawara) {
  const auto index = static_cast<size_t>(pancawara) - 1;
  if (index < pancawara_names.size()) {
    return pancawara_names.at(index);
  }
  return u8"";
}

static constexpr auto sadwara_names = std::array{u8"Tungleh", u8"Aryang", u8"Urukung", u8"Paniron", u8"Was", u8"Maulu"};

const char8_t *const *calendar_balinese_pawukon_cycle_sadwara_names() { return sadwara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_sadwara_names = sadwara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_sadwara_name(CALENDAR_BALINESE_PAWUKON_SADWARA sadwara) {
  const auto index = static_cast<size_t>(sadwara) - 1;
  if (index < sadwara_names.size()) {
    return sadwara_names.at(index);
  }
  return u8"";
}

static constexpr auto saptawara_names = std::array{u8"Redite", u8"Coma", u8"Anggara", u8"Buda", u8"Wraspati", u8"Sukra", u8"Saniscara"};

const char8_t *const *calendar_balinese_pawukon_cycle_saptawara_names() { return saptawara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_saptawara_names = saptawara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_saptawara_name(CALENDAR_BALINESE_PAWUKON_SAPTAWARA saptawara) {
  const auto index = static_cast<size_t>(saptawara) - 1;
  if (index < saptawara_names.size()) {
    return saptawara_names.at(index);
  }
  return u8"";
}

static constexpr auto asatawara_names = std::array{u8"Sri", u8"Indra", u8"Guru", u8"Yama", u8"Ludra", u8"Brahma", u8"Kala", u8"Uma"};

const char8_t *const *calendar_balinese_pawukon_cycle_asatawara_names() { return asatawara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_asatawara_names = asatawara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_asatawara_name(CALENDAR_BALINESE_PAWUKON_ASATAWARA asatawara) {
  const auto index = static_cast<size_t>(asatawara) - 1;
  if (index < asatawara_names.size()) {
    return asatawara_names.at(index);
  }
  return u8"";
}

static constexpr auto sangawara_names =
    std::array{u8"Dangu", u8"Jangur", u8"Gigis", u8"Nohan", u8"Ogan", u8"Erangan", u8"Urungan", u8"Tulus", u8"Dadi"};

const char8_t *const *calendar_balinese_pawukon_cycle_sangawara_names() { return sangawara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_sangawara_names = sangawara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_sangawara_name(CALENDAR_BALINESE_PAWUKON_SANGAWARA sangawara) {
  const auto index = static_cast<size_t>(sangawara) - 1;
  if (index < sangawara_names.size()) {
    return sangawara_names.at(index);
  }
  return u8"";
}

static constexpr auto dasawara_names =
    std::array{u8"Pandita", u8"Pati", u8"Suka", u8"Duka", u8"Sri", u8"Manuh", u8"Manusa", u8"Raja", u8"Dewa", u8"Raksasa"};

const char8_t *const *calendar_balinese_pawukon_cycle_dasawara_names() { return dasawara_names.data(); }
const int             calendar_balinese_pawukon_cycle_num_dasawara_names = dasawara_names.size();
const char8_t        *calendar_balinese_pawukon_cycle_dasawara_name(CALENDAR_BALINESE_PAWUKON_DASAWARA dasawara) {
  const auto index = static_cast<size_t>(dasawara) - 1;
  if (index < dasawara_names.size()) {
    return dasawara_names.at(index);
  }
  return u8"";
}

static constexpr auto day_of_week_names = std::array{u8"Redite", u8"Coma", u8"Anggara", u8"Buda", u8"Wraspati", u8"Sukra", u8"Saniscara"};

const char8_t *const *calendar_balinese_pawukon_day_of_week_names() { return day_of_week_names.data(); }
const char8_t        *calendar_balinese_pawukon_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < day_of_week_names.size()) {
    return day_of_week_names.at(index);
  }
  return u8"";
}

auto calendars::balinese_pawukon::Date::ekawara_cycle_names() noexcept -> std::array<const char8_t *, 1> { return ekawara_names; }
auto calendars::balinese_pawukon::Date::dwiwara_cycle_names() noexcept -> std::array<const char8_t *, 2> { return dwiwara_names; }
auto calendars::balinese_pawukon::Date::triwara_cycle_names() noexcept -> std::array<const char8_t *, 3> { return triwara_names; }
auto calendars::balinese_pawukon::Date::caturwara_cycle_names() noexcept -> std::array<const char8_t *, 4> { return caturwara_names; }
auto calendars::balinese_pawukon::Date::pancawara_cycle_names() noexcept -> std::array<const char8_t *, 5> { return pancawara_names; }
auto calendars::balinese_pawukon::Date::sadwara_cycle_names() noexcept -> std::array<const char8_t *, 6> { return sadwara_names; }
auto calendars::balinese_pawukon::Date::saptawara_cycle_names() noexcept -> std::array<const char8_t *, 7> { return saptawara_names; }
auto calendars::balinese_pawukon::Date::asatawara_cycle_names() noexcept -> std::array<const char8_t *, 8> { return asatawara_names; }
auto calendars::balinese_pawukon::Date::sangawara_cycle_names() noexcept -> std::array<const char8_t *, 9> { return sangawara_names; }
auto calendars::balinese_pawukon::Date::dasawara_cycle_names() noexcept -> std::array<const char8_t *, 10> { return dasawara_names; }
