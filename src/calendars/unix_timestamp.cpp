#include <calendars/c/calendars/unix_timestamp.h>
#include <calendars/cxx/calendars/unix_timestamp.h>
#include <calendars/cxx/math/common.h>

#include "utils.h"

CalendarUnixTimestamp calendar_unix_timestamp_create(int64_t seconds) { return CalendarUnixTimestamp{seconds}; }

void calendar_unix_timestamp_init(CalendarUnixTimestamp *date, int64_t seconds) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_unix_timestamp_create(seconds);
}

int calendar_unix_timestamp_compare(const CalendarUnixTimestamp *left, const CalendarUnixTimestamp *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->timestamp, right->timestamp);
}

CalendarMoment calendar_unix_timestamp_to_moment(const CalendarUnixTimestamp *date) {
  if (date == nullptr) {
    return 0;
  }
  return unix_timestamp_epoch + static_cast<double>(date->timestamp) / (24 * 60 * 60.0);
}

CalendarUnixTimestamp calendar_unix_timestamp_from_moment(CalendarMoment moment) {
  return calendar_unix_timestamp_create(static_cast<int64_t>(std::round(24 * 60 * 60 * (moment - static_cast<double>(unix_timestamp_epoch)))));
}
