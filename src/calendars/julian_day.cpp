#include <calendars/c/calendars/julian_day.h>
#include <calendars/cxx/calendars/julian_day.h>

#include "epochs.h"
#include "utils.h"

CalendarJulianDay calendar_julian_day_create(double datetime) { return CalendarJulianDay{datetime}; }

void calendar_julian_day_init(CalendarJulianDay *date, double datetime) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_julian_day_create(datetime);
}

int calendar_julian_day_compare(const CalendarJulianDay *left, const CalendarJulianDay *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->datetime, right->datetime);
}

CalendarJulianDay calendar_julian_day_from_moment(CalendarMoment moment) { return calendar_julian_day_create(moment - julian_day_epoch); }

CalendarMoment calendar_julian_day_to_moment(const CalendarJulianDay *date) {
  if (date == nullptr) {
    return 0;
  }
  return date->datetime + julian_day_epoch;
}
