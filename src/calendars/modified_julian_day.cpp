#include <calendars/c/calendars/modified_julian_day.h>
#include <calendars/cxx/calendars/modified_julian_day.h>

#include "epochs.h"
#include "utils.h"

CalendarModifiedJulianDayDate calendar_modified_julian_day_create(double seconds) { return CalendarModifiedJulianDayDate{seconds}; }
void                          calendar_modified_julian_day_init(CalendarModifiedJulianDayDate *date, double seconds) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_modified_julian_day_create(seconds);
}

int calendar_modified_julian_day_compare(const CalendarModifiedJulianDayDate *left, const CalendarModifiedJulianDayDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->datetime, right->datetime);
}

CalendarModifiedJulianDayDate calendar_modified_julian_day_from_moment(CalendarMoment moment) {
  return calendar_modified_julian_day_create(moment - modified_julian_day_epoch);
}

CalendarMoment calendar_modified_julian_day_to_moment(const CalendarModifiedJulianDayDate *date) {
  if (date == nullptr) {
    return 0;
  }
  return date->datetime + modified_julian_day_epoch;
}
