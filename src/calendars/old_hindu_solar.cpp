#include <calendars/c/calendars/old_hindu_solar.h>
#include <calendars/cxx/calendars/old_hindu_solar.h>
#include <calendars/cxx/math/common.h>

#include "utils.h"

CalendarOldHinduSolarDate calendar_old_hindu_solar_create(int64_t year, CALENDAR_OLD_HINDU_SOLAR_SAURA saura, int16_t day) {
  return CalendarOldHinduSolarDate{year, static_cast<int16_t>(saura), day};
}

void calendar_old_hindu_solar_init(CalendarOldHinduSolarDate *date, int64_t year, CALENDAR_OLD_HINDU_SOLAR_SAURA saura, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_old_hindu_solar_create(year, saura, day);
}

int calendar_old_hindu_solar_compare(const CalendarOldHinduSolarDate *left, const CalendarOldHinduSolarDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year, right->year, left->saura, right->saura, left->day, right->day);
}

int64_t calendar_old_hindu_solar_jovian_year(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  return math::mod_range(
      27 + math::floor<double, int64_t>(static_cast<double>(calendar_old_hindu_solar_day_count(rdDate)) / (arya_jovian_period / 12.0)),
      1,
      60);
}

int64_t calendar_old_hindu_solar_day_count(CalendarRdDate rdDate) { return rdDate - old_hindu_solar_epoch; }

CalendarOldHinduSolarDate calendar_old_hindu_solar_from_rd_date(CalendarRdDate rdDate) {
  namespace math    = calendars::math;
  const auto sun    = static_cast<double>(calendar_old_hindu_solar_day_count(rdDate)) + 6.0 / 24.0;
  const auto hYear  = math::floor<double, int64_t>(sun / arya_solar_year);
  const auto hMonth = static_cast<CALENDAR_OLD_HINDU_SOLAR_SAURA>(math::mod(math::floor<double, int64_t>(sun / arya_solar_month), 12) + 1);
  const auto jDay   = static_cast<int16_t>(math::floor(math::mod(sun, arya_solar_month)) + 1);
  return calendar_old_hindu_solar_create(hYear, hMonth, jDay);
}

CalendarRdDate calendar_old_hindu_solar_to_rd_date(const CalendarOldHinduSolarDate *date) {
  if (date == nullptr) {
    return 0;
  }
  namespace math = calendars::math;
  return static_cast<int64_t>(std::ceil(static_cast<double>(old_hindu_solar_epoch) + static_cast<double>(date->year) * arya_solar_year
                                        + (static_cast<double>(date->saura) - 1) * arya_solar_month + date->day - 30.0 / 24.0));
}
