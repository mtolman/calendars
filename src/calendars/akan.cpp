#include <calendars/c/calendars/akan.h>
#include <calendars/cxx/calendars/akan.h>
#include <calendars/cxx/math/common.h>

#include <array>

#include "epochs.h"
#include "utils.h"

const int calendar_akan_day_count_cycle_length = 42;

CalendarAkanDate calendar_akan_day_count_create(int prefix, int stem) {
  namespace math = calendars::math;
  return CalendarAkanDate{math::amod(prefix, 6), math::amod(stem, 7)};
}

void calendar_akan_day_count_init(CalendarAkanDate *date, int prefix, int stem) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_akan_day_count_create(prefix, stem);
}

CalendarAkanDate calendar_akan_day_count_from_rd_date(CalendarRdDate rdDate) {
  namespace math = calendars::math;
  auto date      = rdDate - akan_epoch;
  return CalendarAkanDate{static_cast<int>(math::amod(date, 6)), static_cast<int>(math::amod(date, 7))};
}

int calendar_akan_day_count_compare(const CalendarAkanDate *left, const CalendarAkanDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->prefix, right->prefix, left->stem, right->stem);
}

CalendarRdDate calendar_akan_day_count_on_or_before(CalendarRdDate rdDate, const CalendarAkanDate *akanDayCount) {
  if (akanDayCount == nullptr) {
    return akan_epoch;
  }
  namespace math = calendars::math;
  using Akan     = calendars::akan::Date;
  return math::mod_range(calendars::RdDate{0}.template to<Akan>() - Akan{*akanDayCount}, rdDate, rdDate - 42);
}

int calendar_akan_day_count_difference(const CalendarAkanDate *left, const CalendarAkanDate *right) {
  if (left == nullptr || right == nullptr) {
    return 0;
  }
  namespace math = calendars::math;

  const auto prefixDiff = right->prefix - left->prefix;
  const auto stemDiff   = right->stem - left->stem;
  return math::amod(prefixDiff + 36 * (stemDiff - prefixDiff), 42);
}

static constexpr auto prefix_names = std::array{u8"Nwona", u8"Nkyi", u8"Kuru", u8"Kwa", u8"Mono", u8"Fo"};

static constexpr auto stem_names = std::array{u8"wukuo", u8"yaw", u8"fie", u8"memene", u8"kwasi", u8"dwo", u8"bene"};

const int calendar_akan_num_prefix_names = prefix_names.size();
const int calendar_akan_num_stem_names   = stem_names.size();

const char8_t *const *calendar_akan_prefix_names() { return prefix_names.data(); }
const char8_t *const *calendar_akan_stem_names() { return stem_names.data(); }
const char8_t        *calendar_akan_prefix_name(int index) {
  if (index >= 0 && static_cast<size_t>(index) < prefix_names.size()) {
    return prefix_names.at(index);
  }
  return u8"";
}

const char8_t *calendar_akan_stem_name(int index) {
  if (index >= 0 && static_cast<size_t>(index) < stem_names.size()) {
    return stem_names.at(index);
  }
  return u8"";
}
auto calendars::akan::Date::prefix_names() noexcept -> std::array<const char8_t *, 6> { return ::prefix_names; }
auto calendars::akan::Date::stem_names() noexcept -> std::array<const char8_t *, 7> { return ::stem_names; }
