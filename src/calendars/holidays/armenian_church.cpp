#include <calendars/c/calendars/holidays.h>
#include <calendars/c/calendars/holidays/armenian_church.h>
#include <calendars/cxx/calendars.h>

CalendarJulianDate calendar_holidays_armenian_church_christmas_jerusalem(int64_t julianYear) {
  return static_cast<CalendarJulianDate>(calendars::julian::Date{julianYear, calendars::julian::MONTH::JANUARY, 6});
}

CalendarGregorianDate calendar_holidays_armenian_church_christmas(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(calendars::gregorian::Date{gregorianYear, calendars::gregorian::MONTH::JANUARY, 6});
}
