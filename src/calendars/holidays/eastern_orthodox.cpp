#include <calendars/c/calendars/holidays/eastern_orthodox.h>
#include <calendars/cxx/calendars/holidays/eastern_orthodox.h>
#include <calendars/cxx/calendars/julian.h>
#include <calendars/cxx/math/common.h>

CalendarJulianDate calendar_holidays_eastern_orthodox_christmas(int64_t julianYear) { return {julianYear, MONTH_DECEMBER, 25}; }
CalendarJulianDate calendar_holidays_eastern_orthodox_nativity_of_the_virgin_mary(int64_t julianYear) { return {julianYear, MONTH_SEPTEMBER, 8}; }
CalendarJulianDate calendar_holidays_eastern_orthodox_elevation_of_the_life_giving_cross(int64_t julianYear) {
  return {julianYear, MONTH_SEPTEMBER, 14};
}
CalendarJulianDate calendar_holidays_eastern_orthodox_presentation_of_the_virgin_mary_in_the_temple(int64_t julianYear) {
  return {julianYear, MONTH_NOVEMBER, 21};
}
CalendarJulianDate calendar_holidays_eastern_orthodox_theophany(int64_t julianYear) { return {julianYear, MONTH_JANUARY, 6}; }
CalendarJulianDate calendar_holidays_eastern_orthodox_presentation_of_christ_in_the_temple(int64_t julianYear) {
  return {julianYear, MONTH_FEBRUARY, 2};
}
CalendarJulianDate calendar_holidays_eastern_orthodox_the_annunciation(int64_t julianYear) { return {julianYear, MONTH_MARCH, 25}; }
CalendarJulianDate calendar_holidays_eastern_orthodox_the_transfiguration(int64_t julianYear) { return {julianYear, MONTH_AUGUST, 6}; }
CalendarJulianDate calendar_holidays_eastern_orthodox_the_repose_of_the_virgin_mary(int64_t julianYear) { return {julianYear, MONTH_AUGUST, 15}; }
CalendarJulianDate calendar_holidays_eastern_orthodox_easter(int64_t julianYear) {
  // For math to work out in BC years, we shift up by 1 so that 1 BC becomes 0
  //  This is important since 0 is not a valid julian year
  const auto adjustedYear = julianYear >= 0 ? julianYear : julianYear + 1;
  const auto shiftedEpact = calendars::math::mod(14 + 11 * calendars::math::mod(adjustedYear, 19), 30);
  const auto paschalMoon  = calendars::RdDate::from(calendars::julian::Date{julianYear, calendars::julian::MONTH::APRIL, 19}).sub_days(shiftedEpact);
  return static_cast<CalendarJulianDate>(paschalMoon.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY).to<calendars::julian::Date>());
}

void calendar_holidays_eastern_orthodox_the_fast_of_the_repose_of_the_virgin_mary(CalendarHolidayArray **outPointer, int64_t julianYear) {
  calendar_holidays_eastern_orthodox_the_fast_of_the_repose_of_the_virgin_mary_alloc_with(outPointer, julianYear, malloc);
}

void calendar_holidays_eastern_orthodox_the_fast_of_the_repose_of_the_virgin_mary_alloc_with(CalendarHolidayArray **outPointer,
                                                                                             int64_t                julianYear,
                                                                                             CalendarAllocator      allocator) {
  static constexpr auto numDates    = 14;
  auto                  julianStart = CalendarJulianDate{julianYear, MONTH_AUGUST, 1};
  auto                  start       = calendar_julian_to_rd_date(&julianStart);

  CalendarHolidayArray *array;
  calendar_holiday_array_alloc_with(&array, numDates, allocator);
  for (int i = 0; i < numDates; ++i) {
    array->dates[ i ] = start + i;
  }
  *outPointer = array;
}

void calendar_holidays_eastern_orthodox_the_40_day_christmas_fast(CalendarHolidayArray **outPointer, int64_t julianYear) {
  calendar_holidays_eastern_orthodox_the_40_day_christmas_fast_alloc_with(outPointer, julianYear, malloc);
}

void calendar_holidays_eastern_orthodox_the_40_day_christmas_fast_alloc_with(CalendarHolidayArray **outPointer,
                                                                             int64_t                julianYear,
                                                                             CalendarAllocator      allocator) {
  static constexpr auto numDates    = 40;
  auto                  julianStart = CalendarJulianDate{julianYear, MONTH_NOVEMBER, 15};
  auto                  start       = calendar_julian_to_rd_date(&julianStart);

  CalendarHolidayArray *array;
  calendar_holiday_array_alloc_with(&array, numDates, allocator);
  for (int i = 0; i < numDates; ++i) {
    array->dates[ i ] = start + i;
  }
  *outPointer = array;
}

std::array<calendars::julian::Date, 14> calendars::holidays::eastern_orthodox::the_fast_of_the_repose_of_the_virgin_mary(int64_t julianYear) {
  auto res         = std::array<calendars::julian::Date, 14>();
  auto julianStart = CalendarJulianDate{julianYear, MONTH_AUGUST, 1};
  auto start       = calendar_julian_to_rd_date(&julianStart);

  for (size_t i = 0; i < res.size(); ++i) {
    res.at(i) = RdDate{start + static_cast<int64_t>(i)}.to<calendars::julian::Date>();
  }
  return res;
}

std::array<calendars::julian::Date, 40> calendars::holidays::eastern_orthodox::the_40_day_christmas_fast(int64_t julianYear) {
  auto res         = std::array<calendars::julian::Date, 40>();
  auto julianStart = CalendarJulianDate{julianYear, MONTH_NOVEMBER, 15};
  auto start       = calendar_julian_to_rd_date(&julianStart);

  for (size_t i = 0; i < res.size(); ++i) {
    res.at(i) = RdDate{start + static_cast<int64_t>(i)}.to<calendars::julian::Date>();
  }
  return res;
}
