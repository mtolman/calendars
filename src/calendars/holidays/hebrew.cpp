#include <calendars/c/calendars/holidays/hebrew.h>
#include <calendars/cxx/calendars/holidays/hebrew.h>
#include <calendars/cxx/math/common.h>

#include <optional>

static CalendarRdDate calendar_holidays_hebrew_postpone_if_saturday(CalendarRdDate date) {
  if (calendar_day_of_week(date) == DAY_OF_WEEK_SATURDAY) {
    return date + 1;
  }
  return date;
}

CalendarHebrewDate calendar_holidays_hebrew_yom_kippur(int64_t hebrewYear) { return {hebrewYear, HEBREW_TISHRI, 10}; }

CalendarHebrewDate calendar_holidays_hebrew_rosh_ha_shanah(int64_t hebrewYear) { return {hebrewYear, HEBREW_TISHRI, 1}; }

CalendarHebrewDate calendar_holidays_hebrew_sukkot(int64_t hebrewYear) { return {hebrewYear, HEBREW_TISHRI, 15}; }

CalendarHebrewDate calendar_holidays_hebrew_hoshana_rabba(int64_t hebrewYear) { return {hebrewYear, HEBREW_TISHRI, 21}; }

CalendarHebrewDate calendar_holidays_hebrew_shemini_azeret(int64_t hebrewYear) { return {hebrewYear, HEBREW_TISHRI, 22}; }

CalendarHebrewDate calendar_holidays_hebrew_simhat_torah(int64_t hebrewYear) { return {hebrewYear, HEBREW_TISHRI, 23}; }

CalendarHebrewDate calendar_holidays_hebrew_passover(int64_t hebrewYear) { return {hebrewYear, HEBREW_NISAN, 15}; }

CalendarHebrewDate calendar_holidays_hebrew_passover_end(int64_t hebrewYear) { return {hebrewYear, HEBREW_NISAN, 21}; }

CalendarHebrewDate calendar_holidays_hebrew_shavout(int64_t hebrewYear) { return {hebrewYear, HEBREW_SIVAN, 6}; }

void calendar_holidays_hebrew_sukkot_intermediate_days(CalendarHolidayArray **outPointer, int64_t hebrewYear) {
  calendar_holidays_hebrew_sukkot_intermediate_days_alloc_with(outPointer, hebrewYear, malloc);
}

void calendar_holidays_hebrew_sukkot_intermediate_days_alloc_with(CalendarHolidayArray **outPointer,
                                                                  int64_t                hebrewYear,
                                                                  CalendarAllocator      allocator) {
  CalendarHolidayArray *array;
  calendar_holiday_array_alloc_with(&array, 6, allocator);
  array->dates[ 0 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 16}.to_rd_date());
  array->dates[ 1 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 17}.to_rd_date());
  array->dates[ 2 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 18}.to_rd_date());
  array->dates[ 3 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 19}.to_rd_date());
  array->dates[ 4 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 20}.to_rd_date());
  array->dates[ 5 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 21}.to_rd_date());
  *outPointer       = array;
}

void calendar_holidays_hebrew_passover_days(CalendarHolidayArray **outPointer, int64_t hebrewYear) {
  return calendar_holidays_hebrew_passover_days_alloc_with(outPointer, hebrewYear, malloc);
}

void calendar_holidays_hebrew_passover_days_alloc_with(CalendarHolidayArray **outPointer, int64_t hebrewYear, CalendarAllocator allocator) {
  CalendarHolidayArray *array;
  calendar_holiday_array_alloc_with(&array, 5, allocator);
  array->dates[ 0 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 16}.to_rd_date());
  array->dates[ 1 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 17}.to_rd_date());
  array->dates[ 2 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 18}.to_rd_date());
  array->dates[ 3 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 19}.to_rd_date());
  array->dates[ 4 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 20}.to_rd_date());
  *outPointer       = array;
}

void calendar_holidays_hebrew_hanukkah_days(CalendarHolidayArray **outPointer, int64_t hebrewYear) {
  calendar_holidays_hebrew_hanukkah_days_alloc_with(outPointer, hebrewYear, malloc);
}

void calendar_holidays_hebrew_hanukkah_days_alloc_with(CalendarHolidayArray **outPointer, int64_t hebrewYear, CalendarAllocator allocator) {
  CalendarHolidayArray *array;
  calendar_holiday_array_alloc_with(&array, 8, allocator);
  array->dates[ 0 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date());
  array->dates[ 1 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()) + 1;
  array->dates[ 2 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()) + 2;
  array->dates[ 3 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()) + 3;
  array->dates[ 4 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()) + 4;
  array->dates[ 5 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()) + 5;
  array->dates[ 6 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()) + 6;
  array->dates[ 7 ] = static_cast<CalendarRdDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()) + 7;
  *outPointer       = array;
}

CalendarHebrewDate calendar_holidays_hebrew_tu_b_shevat(int64_t hebrewYear) { return {hebrewYear, HEBREW_SHEVAT, 15}; }

CalendarHebrewDate calendar_holidays_hebrew_purim(int64_t hebrewYear) {
  return {hebrewYear, static_cast<int16_t>(calendar_hebrew_last_month_of_year(hebrewYear)), 14};
}

CalendarHebrewDate calendar_holidays_hebrew_ta_anit_esther(int64_t hebrewYear) {
  const auto purimDate   = calendar_holidays_hebrew_purim(hebrewYear);
  const auto purimRdDate = calendar_hebrew_to_rd_date(&purimDate);
  if (calendar_day_of_week(purimRdDate) == DAY_OF_WEEK_SUNDAY) {
    return calendar_hebrew_from_rd_date(purimRdDate - 3);
  }
  else {
    return calendar_hebrew_from_rd_date(purimRdDate - 1);
  }
}

CalendarHebrewDate calendar_holidays_hebrew_tishah_be_av(int64_t hebrewYear) {
  const auto hebrewDate = CalendarHebrewDate{hebrewYear, HEBREW_AV, 9};
  return calendar_hebrew_from_rd_date(calendar_holidays_hebrew_postpone_if_saturday(calendar_hebrew_to_rd_date(&hebrewDate)));
}

CalendarHebrewDate calendar_holidays_hebrew_tzom_gedaliah(int64_t hebrewYear) {
  const auto hebrewDate = CalendarHebrewDate{hebrewYear, HEBREW_TISHRI, 3};
  return calendar_hebrew_from_rd_date(calendar_holidays_hebrew_postpone_if_saturday(calendar_hebrew_to_rd_date(&hebrewDate)));
}

CalendarHebrewDate calendar_holidays_hebrew_tzom_tammuz(int64_t hebrewYear) {
  const auto hebrewDate = CalendarHebrewDate{hebrewYear, HEBREW_TAMMUZ, 17};
  return calendar_hebrew_from_rd_date(calendar_holidays_hebrew_postpone_if_saturday(calendar_hebrew_to_rd_date(&hebrewDate)));
}

CalendarHebrewDate calendar_holidays_hebrew_yom_ha_shoah(int64_t hebrewYear) {
  const auto hebrewDate = CalendarHebrewDate{hebrewYear, HEBREW_NISAN, 27};
  return calendar_hebrew_from_rd_date(calendar_holidays_hebrew_postpone_if_saturday(calendar_hebrew_to_rd_date(&hebrewDate)));
}

CalendarHebrewDate calendar_holidays_hebrew_yom_ha_zikkaron(int64_t hebrewYear) {
  const auto iyyar4Hebrew = CalendarHebrewDate{hebrewYear, HEBREW_IYYAR, 4};
  const auto iyyar4       = calendar_hebrew_to_rd_date(&iyyar4Hebrew);
  const auto dayOfWeek    = calendar_day_of_week(iyyar4);
  if (dayOfWeek == DAY_OF_WEEK_THURSDAY || dayOfWeek == DAY_OF_WEEK_FRIDAY) {
    return calendar_hebrew_from_rd_date(calendar_day_of_week_before(iyyar4, DAY_OF_WEEK_WEDNESDAY));
  }
  else if (dayOfWeek == DAY_OF_WEEK_SUNDAY) {
    return calendar_hebrew_from_rd_date(iyyar4 + 1);
  }
  else {
    return calendar_hebrew_from_rd_date(iyyar4);
  }
}

CalendarHebrewDate calendar_holidays_hebrew_birthday(const CalendarHebrewDate *birthDate, int64_t hebrewYear) {
  if (birthDate == nullptr) {
    return static_cast<CalendarHebrewDate>(calendars::hebrew::Date{});
  }

  const auto hBirthDate = calendars::hebrew::Date{*birthDate};
  const auto lastMonth  = calendars::hebrew::Date::last_month_of_year(hebrewYear);
  if (hBirthDate.month() == lastMonth) {
    return {hebrewYear, static_cast<int16_t>(lastMonth), hBirthDate.day()};
  }
  else {
    return static_cast<CalendarHebrewDate>(
        calendars::hebrew::Date{hebrewYear, hBirthDate.month(), 1}.to_rd_date().add_days(hBirthDate.day() - 1).to<calendars::hebrew::Date>());
  }
}

CalendarHebrewDate calendar_holidays_hebrew_yahrzeit(const CalendarHebrewDate *deathDate, int64_t hebrewYear) {
  if (deathDate == nullptr) {
    return static_cast<CalendarHebrewDate>(calendars::hebrew::Date{});
  }

  const auto hDeathDate = calendars::hebrew::Date{*deathDate};
  if (hDeathDate.month() == calendars::hebrew::MONTH::MARHESHVAN && hDeathDate.day() == 30
      && !calendars::hebrew::Date::is_long_marheshvan(hebrewYear)) {
    return static_cast<CalendarHebrewDate>(
        calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 1}.to_rd_date().sub_days(1).to<calendars::hebrew::Date>());
  }
  else if (hDeathDate.month() == calendars::hebrew::MONTH::KISLEV && hDeathDate.day() == 30 && calendars::hebrew::Date::is_short_kislev(hebrewYear)) {
    return static_cast<CalendarHebrewDate>(
        calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TEVET, 1}.to_rd_date().sub_days(1).to<calendars::hebrew::Date>());
  }
  else if (hDeathDate.month() == calendars::hebrew::MONTH::ADAR_II) {
    return static_cast<CalendarHebrewDate>(
        calendars::hebrew::Date{hebrewYear, calendars::hebrew::Date::last_month_of_year(hebrewYear), hDeathDate.day()}
            .to_rd_date()
            .sub_days(1)
            .to<calendars::hebrew::Date>());
  }
  else if (hDeathDate.month() == calendars::hebrew::MONTH::ADAR && hDeathDate.day() == 30 && !calendars::hebrew::Date::is_leap_year(hebrewYear)) {
    return static_cast<CalendarHebrewDate>(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::SHEVAT, 30});
  }
  else {
    return static_cast<CalendarHebrewDate>(
        calendars::hebrew::Date{hebrewYear, hDeathDate.month(), 1}.to_rd_date().add_days(hDeathDate.day() - 1).to<calendars::hebrew::Date>());
  }
}

bool calendar_holidays_hebrew_omer(CalendarRdDate rdDate, int64_t *out) {
  auto res = calendars::holidays::hebrew::omer(calendars::RdDate{rdDate});
  if (res.has_value()) {
    *out = res.value();
    return true;
  }
  return false;
}

std::array<calendars::hebrew::Date, 6> calendars::holidays::hebrew::sukkot_intermediate_days(int64_t hebrewYear) {
  return std::array<calendars::hebrew::Date, 6>{calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 16},
                                                calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 17},
                                                calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 18},
                                                calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 19},
                                                calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 20},
                                                calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::TISHRI, 21}};
}

std::array<calendars::hebrew::Date, 5> calendars::holidays::hebrew::passover_days(int64_t hebrewYear) {
  return {calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 16},
          calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 17},
          calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 18},
          calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 19},
          calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::NISAN, 20}};
}

std::array<calendars::hebrew::Date, 8> calendars::holidays::hebrew::hanukkah(int64_t hebrewYear) {
  return {
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date()),
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date().add_days(1)),
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date().add_days(2)),
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date().add_days(3)),
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date().add_days(4)),
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date().add_days(5)),
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date().add_days(6)),
      calendars::hebrew::Date::from_rd_date(calendars::hebrew::Date{hebrewYear, calendars::hebrew::MONTH::KISLEV, 25}.to_rd_date().add_days(7)),
  };
}
