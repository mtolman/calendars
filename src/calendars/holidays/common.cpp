#include <calendars/c/calendars/holidays/common.h>
#include <calendars/cxx/calendars/holidays/common.h>

#include <cstring>

void calendar_holiday_array_alloc(CalendarHolidayArray** outPointer, int size) { calendar_holiday_array_alloc_with(outPointer, size, malloc); }

void calendar_holiday_array_alloc_with(CalendarHolidayArray** outPointer, int size, CalendarAllocator allocator) {
  if (outPointer == nullptr) {
    return;
  }

  auto* array  = static_cast<CalendarHolidayArray*>(allocator(sizeof(CalendarHolidayArray)));
  array->dates = static_cast<CalendarRdDate*>(allocator(sizeof(CalendarRdDate) * size));
  memset(array->dates, 0, sizeof(CalendarRdDate) * size);
  array->size = size;
  *outPointer = array;
}

void calendar_holiday_array_free(CalendarHolidayArray** array) { calendar_holiday_array_free_with(array, free); }

void calendar_holiday_array_free_with(CalendarHolidayArray** array, CalendarDeallocator deallocator) {
  if (array == nullptr || *array == nullptr) {
    return;
  }

  deallocator((*array)->dates);
  (*array)->dates = nullptr;
  (*array)->size  = 0;
  deallocator(*array);
  *array = nullptr;
}
