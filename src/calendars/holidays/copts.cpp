#include <calendars/c/calendars/holidays/copts.h>

CalendarCopticDate calendar_holidays_copts_christmas(int64_t copticYear) { return {copticYear, COPTIC_KOIAK, 29}; }

CalendarCopticDate calendar_holidays_copts_building_of_the_cross(int64_t copticYear) { return {copticYear, COPTIC_THOOUT, 17}; }

CalendarCopticDate calendar_holidays_copts_jesus_circumcision(int64_t copticYear) { return {copticYear, COPTIC_TOBE, 6}; }

CalendarCopticDate calendar_holidays_copts_epiphany(int64_t copticYear) { return {copticYear, COPTIC_TOBE, 11}; }

CalendarCopticDate calendar_holidays_copts_marys_announcement(int64_t copticYear) { return {copticYear, COPTIC_PAREMOTEP, 29}; }

CalendarCopticDate calendar_holidays_copts_jesus_transfiguration(int64_t copticYear) { return {copticYear, COPTIC_MESORE, 13}; }
