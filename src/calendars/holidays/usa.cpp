#include <calendars/c/calendars/holidays/usa.h>
#include <calendars/cxx/calendars.h>

using Gregorian = calendars::gregorian::Date;
using Month     = calendars::gregorian::MONTH;
using Dow       = calendars::DAY_OF_WEEK;

CalendarGregorianDate calendar_holidays_usa_independence_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::JULY, 4});
}
CalendarGregorianDate calendar_holidays_usa_labor_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::SEPTEMBER, 1}.to_rd_date().first_week_day(Dow::MONDAY).to<Gregorian>());
}
CalendarGregorianDate calendar_holidays_usa_memorial_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::MAY, 31}.to_rd_date().last_week_day(Dow::MONDAY).to<Gregorian>());
}
CalendarGregorianDate calendar_holidays_usa_election_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::NOVEMBER, 2}.to_rd_date().first_week_day(Dow::TUESDAY).to<Gregorian>());
}
CalendarGregorianDate calendar_holidays_usa_thanksgiving(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::NOVEMBER, 1}.to_rd_date().nth_week_day(4, Dow::THURSDAY).to<Gregorian>());
}
CalendarGregorianDate calendar_holidays_usa_black_friday(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(
      Gregorian{gregorianYear, Month::NOVEMBER, 1}.to_rd_date().nth_week_day(4, Dow::THURSDAY).add_days(1).to<Gregorian>());
}
CalendarGregorianDate calendar_holidays_usa_new_years(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::JANUARY, 1});
}
CalendarGregorianDate calendar_holidays_usa_new_years_eve(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::DECEMBER, 31});
}
CalendarGregorianDate calendar_holidays_usa_presidents_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::FEBRUARY, 1}.to_rd_date().nth_week_day(3, Dow::MONDAY).to<Gregorian>());
}
CalendarGregorianDate calendar_holidays_usa_juneteenth(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::JUNE, 19});
}
CalendarGregorianDate calendar_holidays_usa_columbus_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::OCTOBER, 1}.to_rd_date().nth_week_day(2, Dow::MONDAY).to<Gregorian>());
}
CalendarGregorianDate calendar_holidays_usa_indigenous_peoples_day(int gregorianYear) { return calendar_holidays_usa_columbus_day(gregorianYear); }
CalendarGregorianDate calendar_holidays_usa_veterans_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::NOVEMBER, 11});
}
CalendarGregorianDate calendar_holidays_usa_martin_luther_king_jr_day(int gregorianYear) {
  return static_cast<CalendarGregorianDate>(Gregorian{gregorianYear, Month::JANUARY, 1}.to_rd_date().nth_week_day(3, Dow::MONDAY).to<Gregorian>());
}
