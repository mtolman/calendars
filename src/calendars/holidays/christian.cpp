#include <calendars/c/calendars/holidays/christian.h>
#include <calendars/cxx/calendars.h>
#include <calendars/cxx/math/common.h>

CalendarGregorianDate calendar_holidays_christian_christmas(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(calendars::gregorian::Date{gregorianYear, calendars::gregorian::MONTH::DECEMBER, 25});
}

CalendarGregorianDate calendar_holidays_christian_christmas_eve(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(calendars::gregorian::Date{gregorianYear, calendars::gregorian::MONTH::DECEMBER, 24});
}

CalendarGregorianDate calendar_holidays_christian_advent_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(calendars::gregorian::Date{gregorianYear, calendars::gregorian::MONTH::NOVEMBER, 30}
                                                .to_rd_date()
                                                .day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY)
                                                .to<calendars::gregorian::Date>());
}
CalendarGregorianDate calendar_holidays_christian_epiphany(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(calendars::gregorian::Date{gregorianYear, calendars::gregorian::MONTH::JANUARY, 2}
                                                .to_rd_date()
                                                .first_week_day(calendars::DAY_OF_WEEK::SUNDAY)
                                                .to<calendars::gregorian::Date>());
}

static calendars::RdDate rd_easter(int64_t gregorianYear) {
  namespace math       = calendars::math;
  const auto century   = math::floor(static_cast<double>(gregorianYear) / 100.0) + 1;
  const auto yearMod19 = math::mod(gregorianYear, 19);
  const auto shiftedEpact =
      math::mod(14 + 11 * yearMod19 - math::floor<double, int64_t>(century * 0.75) + math::floor<double, int64_t>((5 + 8 * century) / 25.0), 30);
  const auto adjustedEpact = (shiftedEpact == 0 || (shiftedEpact == 1 && 10 < yearMod19)) ? shiftedEpact + 1 : shiftedEpact;
  const auto paschalMoon =
      calendars::RdDate::from(calendars::gregorian::Date{gregorianYear, calendars::gregorian::MONTH::APRIL, 19}).sub_days(adjustedEpact);
  return paschalMoon.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY);
}

CalendarGregorianDate calendar_holidays_christian_easter(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).to<calendars::gregorian::Date>());
}

CalendarGregorianDate calendar_holidays_christian_septuagesima_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(63).to<calendars::gregorian::Date>());
}

CalendarGregorianDate calendar_holidays_christian_sexagesima_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(56).to<calendars::gregorian::Date>());
  ;
}

CalendarGregorianDate calendar_holidays_christian_shrove_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(49).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_shrove_monday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(48).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_shrove_tuesday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(47).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_mardi_gras(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(47).to<calendars::gregorian::Date>());
  ;
}

CalendarGregorianDate calendar_holidays_christian_ash_wednesday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(46).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_passion_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(14).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_palm_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(7).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_holy_thursday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(3).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_maundy_thursday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(3).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_good_friday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).sub_days(2).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_rogation_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(35).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_ascension_day(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(39).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_pentecost(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(49).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_whitsunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(49).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_whit_monday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(50).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_trinity_sunday(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(56).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_corpus_christi(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(60).to<calendars::gregorian::Date>());
  ;
}
CalendarGregorianDate calendar_holidays_christian_corpus_christi_us_catholic(int64_t gregorianYear) {
  return static_cast<CalendarGregorianDate>(rd_easter(gregorianYear).add_days(63).to<calendars::gregorian::Date>());
  ;
}

void calendar_holidays_christian_the_40_days_of_lent(CalendarHolidayArray** outPointer, int64_t gregorianYear) {
  calendar_holidays_christian_the_40_days_of_lent_alloc_with(outPointer, gregorianYear, malloc);
}
void calendar_holidays_christian_the_40_days_of_lent_alloc_with(CalendarHolidayArray** outPointer,
                                                                int64_t                gregorianYear,
                                                                CalendarAllocator      allocator) {
  CalendarHolidayArray* lent_array;
  calendar_holiday_array_alloc_with(&lent_array, 40, allocator);
  auto date = rd_easter(gregorianYear).sub_days(46);
  for (size_t i = 0; i < 40; ++i, date = date.add_days(1)) {
    lent_array->dates[ i ] = static_cast<CalendarRdDate>(date);
  }
  *outPointer = lent_array;
}

#include <calendars/cxx/calendars/holidays/christian.h>

auto calendars::holidays::christian::christmas(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_christmas(gregorianYear)};
}
auto calendars::holidays::christian::christmas_eve(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_christmas_eve(gregorianYear)};
}
auto calendars::holidays::christian::advent_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_advent_sunday(gregorianYear)};
}
auto calendars::holidays::christian::epiphany(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_epiphany(gregorianYear)};
}
auto calendars::holidays::christian::easter(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_easter(gregorianYear)};
}
auto calendars::holidays::christian::septuagesima_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_septuagesima_sunday(gregorianYear)};
}
auto calendars::holidays::christian::sexagesima_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_sexagesima_sunday(gregorianYear)};
}
auto calendars::holidays::christian::shrove_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_shrove_sunday(gregorianYear)};
}
auto calendars::holidays::christian::shrove_monday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_shrove_monday(gregorianYear)};
}
auto calendars::holidays::christian::shrove_tuesday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_shrove_tuesday(gregorianYear)};
}
auto calendars::holidays::christian::mardi_gras(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_mardi_gras(gregorianYear)};
}
auto calendars::holidays::christian::ash_wednesday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_ash_wednesday(gregorianYear)};
}
auto calendars::holidays::christian::passion_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_passion_sunday(gregorianYear)};
}
auto calendars::holidays::christian::palm_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_palm_sunday(gregorianYear)};
}
auto calendars::holidays::christian::holy_thursday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_holy_thursday(gregorianYear)};
}
auto calendars::holidays::christian::maundy_thursday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_maundy_thursday(gregorianYear)};
}
auto calendars::holidays::christian::good_friday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_good_friday(gregorianYear)};
}
auto calendars::holidays::christian::rogation_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_rogation_sunday(gregorianYear)};
}
auto calendars::holidays::christian::ascension_day(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_ascension_day(gregorianYear)};
}
auto calendars::holidays::christian::pentecost(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_pentecost(gregorianYear)};
}
auto calendars::holidays::christian::whitsunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_whitsunday(gregorianYear)};
}
auto calendars::holidays::christian::whit_monday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_whit_monday(gregorianYear)};
}
auto calendars::holidays::christian::trinity_sunday(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_trinity_sunday(gregorianYear)};
}
auto calendars::holidays::christian::corpus_christi(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_corpus_christi(gregorianYear)};
}
auto calendars::holidays::christian::corpus_christi_us_catholic(int64_t gregorianYear) -> gregorian::Date {
  return gregorian::Date{calendar_holidays_christian_corpus_christi_us_catholic(gregorianYear)};
}
auto calendars::holidays::christian::the_40_days_of_lent(int64_t gregorianYear) -> std::array<gregorian::Date, 40> {
  auto res  = std::array<gregorian::Date, 40>();
  auto date = rd_easter(gregorianYear).sub_days(46);
  for (size_t i = 0; i < 40; ++i, date = date.add_days(1)) {
    res.at(i) = date.to<gregorian::Date>();
  }
  return res;
}
