#include <calendars/c/calendars/ethiopic.h>
#include <calendars/cxx/calendars/coptic.h>
#include <calendars/cxx/calendars/ethiopic.h>

#include "epochs.h"
#include "utils.h"

CalendarEthiopicDate calendar_ethiopic_create(int64_t year, CALENDAR_ETHIOPIC_MONTH month, int16_t day) {
  return CalendarEthiopicDate{year, static_cast<int16_t>(month), day};
}

void calendar_ethiopic_init(CalendarEthiopicDate *date, int64_t year, CALENDAR_ETHIOPIC_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_ethiopic_create(year, month, day);
}

CalendarRdDate calendar_ethiopic_to_rd_date(const CalendarEthiopicDate *date) {
  if (date == nullptr) {
    return 0;
  }

  const auto coptic = calendars::coptic::Date{date->year, static_cast<calendars::coptic::MONTH>(date->month), date->day};
  return static_cast<CalendarRdDate>(calendars::RdDate::from(coptic).add_days(ethiopic_epoch - coptic_epoch));
}

CalendarEthiopicDate calendar_ethiopic_from_rd_date(CalendarRdDate rdDate) {
  const auto coptic = calendars::RdDate{rdDate}.add_days(coptic_epoch - ethiopic_epoch).to<calendars::coptic::Date>();
  return calendar_ethiopic_create(coptic.year(), static_cast<CALENDAR_ETHIOPIC_MONTH>(coptic.month()), coptic.day());
}

int calendar_ethiopic_compare(const CalendarEthiopicDate *left, const CalendarEthiopicDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

bool calendar_ethiopic_is_leap_year(int64_t year) {
  namespace math = calendars::math;
  return math::mod(year, 4) == 3;
}

static constexpr auto transliteration_names = std::array{u8"Mäskäräm",
                                                         u8"Ṭəqəmt",
                                                         u8"Ḫədar",
                                                         u8"Taḫśaś",
                                                         u8"Ṭərr",
                                                         u8"Yäkatit",
                                                         u8"Mägabit",
                                                         u8"Miyazya",
                                                         u8"Gənbo",
                                                         u8"Säne",
                                                         u8"Ḥamle",
                                                         u8"Nähase",
                                                         u8"Ṗagume"};

const char8_t *const *calendar_ethiopic_month_names() { return transliteration_names.data(); }
const char8_t        *calendar_ethiopic_month_name(CALENDAR_ETHIOPIC_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < transliteration_names.size()) {
    return transliteration_names.at(index);
  }
  return u8"";
}

static constexpr auto day_of_week_names = std::array{u8"Ehud", u8"Segno", u8"Maksegno", u8"Rabue", u8"Hamus", u8"Arb", u8"Kidame"};

const char8_t *const *calendar_ethiopic_day_of_week_names() { return day_of_week_names.data(); }
const char8_t        *calendar_ethiopic_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < day_of_week_names.size()) {
    return day_of_week_names.at(index);
  }
  return u8"";
}
auto calendars::ethiopic::Date::month_names() noexcept -> std::array<const char8_t *, 13> { return transliteration_names; }
