#include <calendars/c/calendars/radix.h>
#include <calendars/cxx/calendars/radix.h>

void calendar_radix_template_init(
    CalendarRadixTemplate* date, int num_day_cycles, const int* day_cycles, int num_time_cycles, const int* time_cycles) {
  if (date == nullptr) {
    return;
  }

  auto dayCycles  = std::vector<int>{day_cycles, day_cycles + num_day_cycles};     // NOLINT
  auto timeCycles = std::vector<int>{time_cycles, time_cycles + num_time_cycles};  // NOLINT
  date->data      = new calendars::radix::DateTemplate<>(dayCycles, timeCycles);
}

void calendar_radix_template_deinit(CalendarRadixTemplate* date) {
  if (date == nullptr) {
    return;
  }

  auto* memory = static_cast<calendars::radix::DateTemplate<>*>(date->data);
  delete memory;  // NOLINT
  date->data = nullptr;
}

void calendar_radix_date_init(CalendarRadixDate*           date,
                              const CalendarRadixTemplate* dateTemplate,
                              int                          num_day_cycle_values,
                              const double*                day_cycle_values,
                              int                          num_time_cycle_values,
                              const double*                time_cycle_values) {
  if (date == nullptr) {
    return;
  }

  if (dateTemplate == nullptr) {
    date->data = nullptr;
    return;
  }

  auto  dayValues      = std::vector<double>{day_cycle_values, day_cycle_values + num_day_cycle_values};     // NOLINT
  auto  timeValues     = std::vector<double>{time_cycle_values, time_cycle_values + num_time_cycle_values};  // NOLINT
  auto* templateMemory = static_cast<calendars::radix::DateTemplate<>*>(dateTemplate->data);
  date->data           = new calendars::radix::Date<>(templateMemory->make_date(dayValues, timeValues));
}

void calendar_radix_date_init_from_moment(CalendarRadixDate* date, const CalendarRadixTemplate* dateTemplate, CalendarMoment moment) {
  if (date == nullptr) {
    return;
  }

  if (dateTemplate == nullptr) {
    date->data = nullptr;
    return;
  }

  auto* templateMemory = static_cast<calendars::radix::DateTemplate<>*>(dateTemplate->data);
  date->data           = new calendars::radix::Date(templateMemory->make_date(calendars::Moment{moment}));
}

CalendarMoment calendar_radix_date_to_moment(const CalendarRadixDate* date) {
  auto* dateMemory = static_cast<calendars::radix::Date<>*>(date->data);
  return static_cast<CalendarMoment>(dateMemory->to_moment());
}

int calendar_radix_date_size(const CalendarRadixDate* date) {
  auto* dateMemory = static_cast<calendars::radix::Date<>*>(date->data);
  return dateMemory->size();
}

double calendar_radix_date_get(const CalendarRadixDate* date, int index) {
  auto* dateMemory = static_cast<calendars::radix::Date<>*>(date->data);
  return dateMemory->get(index);
}

bool calendar_radix_date_set(const CalendarRadixDate* date, int index, double value) {
  auto* dateMemory = static_cast<calendars::radix::Date<>*>(date->data);
  return dateMemory->set(index, value);
}

void calendar_radix_date_deinit(CalendarRadixDate* date) {
  if (date == nullptr) {
    return;
  }

  auto* memory = static_cast<calendars::radix::Date<>*>(date->data);
  delete memory;  // NOLINT
  date->data = nullptr;
}

template struct calendars::radix::DateTemplate<std::allocator<int>>;
template class calendars::radix::Date<std::allocator<int>, std::allocator<double>>;
