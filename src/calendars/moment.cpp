#include <calendars/c/calendars/moment.h>
#include <calendars/cxx/calendars/common.h>
#include <calendars/cxx/math/common.h>

double calendar_moment_to_time(CalendarMoment moment) { return calendars::math::mod(moment, 1.0); }
