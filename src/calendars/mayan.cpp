#include <calendars/c/calendars/mayan/haab.h>
#include <calendars/c/calendars/mayan/long_date.h>
#include <calendars/c/calendars/mayan/tzolkin.h>
#include <calendars/cxx/calendars/mayan.h>
#include <calendars/cxx/math/common.h>

#include "epochs.h"
#include "utils.h"

auto calendars::mayan::long_date::Date::from_rd_date(const RdDate& rdDate) noexcept -> Date {
  return Date{rdDate.sub_days(mayan_long_date_epoch).to<Radix>()};
}

auto calendars::mayan::long_date::Date::to_rd_date() const noexcept -> calendars::RdDate {
  return RdDate::from(date).add_days(mayan_long_date_epoch);
}

CalendarHaabDate calendar_haab_create(CALENDAR_HAAB_MONTH month, int16_t day) { return CalendarHaabDate{static_cast<int16_t>(month), day}; }

void calendar_haab_init(CalendarHaabDate* date, CALENDAR_HAAB_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_haab_create(month, day);
}

int calendar_haab_compare(const CalendarHaabDate* left, const CalendarHaabDate* right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->month, right->month, left->day, right->day);
}

int64_t calendar_haab_ordinal(const CalendarHaabDate* date) {
  if (date == nullptr) {
    return 0;
  }
  return (static_cast<int64_t>(date->month) - 1) * 20 + static_cast<int64_t>(date->day);
}

CalendarHaabDate calendar_haab_from_rd_date(CalendarRdDate rdDate) {
  namespace math       = calendars::math;
  const auto count     = math::mod(rdDate - mayan_haab_epoch, 365);
  const auto haabDay   = math::mod(count, 20);
  const auto haabMonth = static_cast<CALENDAR_HAAB_MONTH>(math::floor(count / 20) + 1);
  return calendar_haab_create(haabMonth, static_cast<int16_t>(haabDay));
}
CalendarTzolkinDate calendar_tzolkin_create(int16_t number, CALENDAR_TZOLKIN_NAME name) {
  return CalendarTzolkinDate{number, static_cast<int16_t>(name)};
}

void calendar_tzolkin_init(CalendarTzolkinDate* date, int16_t number, CALENDAR_TZOLKIN_NAME name) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_tzolkin_create(number, name);
}

int calendar_tzolkin_compare(const CalendarTzolkinDate* left, const CalendarTzolkinDate* right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->number, right->number, left->name, right->name);
}

int64_t calendar_tzolkin_ordinal(const CalendarTzolkinDate* date) {
  if (date == nullptr) {
    return 0;
  }

  namespace math = calendars::math;
  return math::mod(date->number - 1 + 39 * (date->number - static_cast<int>(date->name)), 260);
}

CalendarTzolkinDate calendar_tzolkin_from_rd_date(CalendarRdDate rdDate) {
  namespace math    = calendars::math;
  const auto count  = rdDate - mayan_tzolkin_epoch + 1;
  const auto number = math::amod(count, 13);
  const auto name   = static_cast<CALENDAR_TZOLKIN_NAME>(math::amod(count, 20));
  return calendar_tzolkin_create(static_cast<int16_t>(number), name);
}

const int64_t calendar_long_date_units_kin        = static_cast<int64_t>(calendars::mayan::long_date::UNITS::KIN);
const int64_t calendar_long_date_units_uinal      = static_cast<int64_t>(calendars::mayan::long_date::UNITS::UINAL);
const int64_t calendar_long_date_units_tun        = static_cast<int64_t>(calendars::mayan::long_date::UNITS::TUN);
const int64_t calendar_long_date_units_katun      = static_cast<int64_t>(calendars::mayan::long_date::UNITS::KATUN);
const int64_t calendar_long_date_units_baktun     = static_cast<int64_t>(calendars::mayan::long_date::UNITS::BAKTUN);
const int64_t calendar_long_date_units_pictun     = static_cast<int64_t>(calendars::mayan::long_date::UNITS::PICTUN);
const int64_t calendar_long_date_units_calabtun   = static_cast<int64_t>(calendars::mayan::long_date::UNITS::CALABTUN);
const int64_t calendar_long_date_units_kinchiltun = static_cast<int64_t>(calendars::mayan::long_date::UNITS::KINCHILTUN);
const int64_t calendar_long_date_units_alautun    = static_cast<int64_t>(calendars::mayan::long_date::UNITS::ALAUTUN);

CalendarLongDate calendar_mayan_long_date_create(double cycle0, double cycle1, double cycle2, double cycle3) {
  return CalendarLongDate{cycle0, cycle1, cycle2, cycle3};
}

void calendar_mayan_long_date_init(CalendarLongDate* date, double cycle0, double cycle1, double cycle2, double cycle3) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_mayan_long_date_create(cycle0, cycle1, cycle2, cycle3);
}

CalendarLongDate calendar_mayan_long_date_from_rd_date(CalendarRdDate rdDate) {
  auto longDate = calendars::mayan::long_date::Date::from_rd_date(calendars::RdDate{rdDate});
  return static_cast<CalendarLongDate>(longDate);
}

CalendarRdDate calendar_mayan_long_date_to_rd_date(const CalendarLongDate* date) {
  if (date == nullptr) {
    return 0;
  }
  return static_cast<CalendarRdDate>(calendars::mayan::long_date::Date{*date}.to_rd_date());
}

static constexpr auto month_names = std::array{u8"Pop",
                                               u8"Uo",
                                               u8"Zip",
                                               u8"Zotz",
                                               u8"Tzec",
                                               u8"Xul",
                                               u8"Yaxkin",
                                               u8"Mol",
                                               u8"Chen",
                                               u8"Yax",
                                               u8"Zac",
                                               u8"Ceh",
                                               u8"Mac",
                                               u8"Kankin",
                                               u8"Muan",
                                               u8"Pax",
                                               u8"Kayab",
                                               u8"Cumku",
                                               u8"Uayeb"};

auto calendars::mayan::haab::Date::month_names() noexcept -> std::array<const char8_t*, 19> { return ::month_names; }

const char8_t* const* calendar_haab_month_names() { return month_names.data(); }
const char8_t*        calendar_haab_month_name(CALENDAR_HAAB_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < month_names.size()) {
    return month_names.at(index);
  }
  return u8"";
}
const int calendar_haab_num_month_names = month_names.size();

static constexpr auto tzolkin_names =
    std::array{u8"Imix",  u8"Ik", u8"Akbal", u8"Kan", u8"Chichan", u8"Cimi", u8"Manik", u8"Lamat",  u8"Muluc", u8"Oc",
               u8"Chuen", u8"Eb", u8"Ben",   u8"Ix",  u8"Men",     u8"Cib",  u8"Caban", u8"Etznab", u8"Cauac", u8"Ahau"};

const int calendar_tzolkin_num_names = tzolkin_names.size();
auto      calendars::mayan::tzolkin::Date::name_strs() noexcept -> std::array<const char8_t*, 20> { return std::array<const char8_t*, 20>(); }

const char8_t* const* calendar_tzolkin_names() { return tzolkin_names.data(); }
const char8_t*        calendar_tzolkin_name(CALENDAR_TZOLKIN_NAME month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < tzolkin_names.size()) {
    return tzolkin_names.at(index);
  }
  return u8"";
}
