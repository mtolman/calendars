#include <calendars/c/calendars/armenian.h>
#include <calendars/cxx/calendars/armenian.h>
#include <calendars/cxx/calendars/egyptian.h>
#include <calendars/cxx/math/common.h>

#include <array>

#include "utils.h"

CalendarArmenianDate calendar_armenian_create(int64_t year, int16_t month, int16_t day) { return CalendarArmenianDate{year, month, day}; }

void calendar_armenian_init(CalendarArmenianDate *date, int64_t year, int16_t month, int16_t day) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_armenian_create(year, month, day);
}

int calendar_armenian_compare(const CalendarArmenianDate *left, const CalendarArmenianDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

CalendarArmenianDate calendar_armenian_nearest_valid(const CalendarArmenianDate *date) {
  return calendar_armenian_from_rd_date(calendar_armenian_to_rd_date(date));
}

bool calendar_armenian_is_valid(const CalendarArmenianDate *date) {
  auto valid = calendar_armenian_nearest_valid(date);
  return calendar_armenian_compare(date, &valid) == 0;
}

CalendarArmenianDate calendar_armenian_from_rd_date(CalendarRdDate rdDate) {
  auto egyptian = calendars::RdDate{rdDate}.add_days(egyptian_epoch - armenian_epoch).to<calendars::egyptian::Date>();
  return calendar_armenian_create(egyptian.year(), egyptian.month(), egyptian.day());
}

CalendarRdDate calendar_armenian_to_rd_date(const CalendarArmenianDate *date) {
  if (date == nullptr) {
    return 0;
  }

  return static_cast<CalendarRdDate>(
      calendars::RdDate::from(calendars::egyptian::Date{date->year, date->month, date->day}).add_days(armenian_epoch - egyptian_epoch));
}

static constexpr auto names = std::array{u8"Nawasardi",
                                         u8"Ho\u1E59i",
                                         u8"Sahmi",
                                         u8"Tr\u0113",
                                         u8"K'aloch",
                                         u8"Arach",
                                         u8"Mehekani",
                                         u8"Areg",
                                         u8"Ahekani",
                                         u8"Mareri",
                                         u8"Margach",
                                         u8"Hrotich"};

const int calendar_armenian_num_month_names = names.size();

const char8_t *const *calendar_armenian_month_names() { return names.data(); }

const char8_t *calendar_armenian_month_name(int16_t month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < names.size()) {
    return names.at(index);
  }
  return u8"";
}

auto calendars::armenian::Date::month_names() noexcept -> std::array<const char8_t *, 12> { return names; }
