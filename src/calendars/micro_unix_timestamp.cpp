#include <calendars/cxx/calendars/micro_unix_timestamp.h>

#include "epochs.h"
#include "utils.h"

CalendarMicroUnixTimestamp calendar_micro_unix_timestamp_create(double seconds) { return CalendarMicroUnixTimestamp{seconds}; }

void calendar_micro_unix_timestamp_init(CalendarMicroUnixTimestamp *date, double seconds) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_micro_unix_timestamp_create(seconds);
}

int calendar_micro_unix_timestamp_compare(const CalendarMicroUnixTimestamp *left, const CalendarMicroUnixTimestamp *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->timestamp, right->timestamp);
}

CalendarMicroUnixTimestamp calendar_micro_unix_timestamp_from_moment(CalendarMoment moment) {
  return calendar_micro_unix_timestamp_create(24 * 60 * 60 * (moment - static_cast<double>(unix_timestamp_epoch)));
}

CalendarMoment calendar_micro_unix_timestamp_to_moment(const CalendarMicroUnixTimestamp *date) {
  if (date == nullptr) {
    return 0;
  }
  return unix_timestamp_epoch + static_cast<double>(date->timestamp) / (24 * 60 * 60.0);
}
