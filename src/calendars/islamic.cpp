#include <calendars/c/calendars/islamic.h>
#include <calendars/cxx/calendars/islamic.h>
#include <calendars/cxx/math/common.h>

#include "epochs.h"
#include "utils.h"

CalendarIslamicDate calendar_islamic_create(int64_t year, CALENDAR_ISLAMIC_MONTH month, int16_t day) {
  return CalendarIslamicDate{year, static_cast<int16_t>(month), day};
}

void calendar_islamic_init(CalendarIslamicDate *date, int64_t year, CALENDAR_ISLAMIC_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_islamic_create(year, month, day);
}

int calendar_islamic_compare(const CalendarIslamicDate *left, const CalendarIslamicDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

bool calendar_islamic_is_leap_year(int64_t year) {
  namespace math = calendars::math;
  return math::mod(14 + 11 * year, 30) < 11;
}

CalendarIslamicDate calendar_islamic_from_rd_date(CalendarRdDate rdDate) {
  namespace math           = calendars::math;
  const auto year          = math::floor<double, int64_t>(static_cast<double>(30 * (rdDate - islamic_epoch) + 10646) / 10631.0);
  const auto muharramFirst = calendar_islamic_create(year, ISLAMIC_MUHARRAM, 1);
  const auto priorDays     = rdDate - calendar_islamic_to_rd_date(&muharramFirst);
  const auto month         = math::floor<double, CALENDAR_ISLAMIC_MONTH>(static_cast<double>(priorDays * 11 + 330) / 325.0);
  const auto monthFirst    = calendar_islamic_create(year, month, 1);
  return calendar_islamic_create(year, month, static_cast<int16_t>(rdDate - calendar_islamic_to_rd_date(&monthFirst) + 1));
}

CalendarRdDate calendar_islamic_to_rd_date(const CalendarIslamicDate *date) {
  if (date == nullptr) {
    return 0;
  }

  namespace math = calendars::math;

  return islamic_epoch - 1 + (date->year - 1) * 354 + math::floor<double, int64_t>(static_cast<double>(3 + 11 * date->year) / 30.0)
         + 29 * (static_cast<int64_t>(date->month) - 1) + math::floor<double, int64_t>(static_cast<double>(date->month) / 2.0) + date->day;
}

static constexpr auto days_of_week =
    std::array{u8"al-ʾAḥad", u8"al-Ithnayn", u8"ath-Thulāthāʾ", u8"al-ʾArbiʿāʾ", u8"al-Khamīs", u8"al-Jumʿah", u8"as-Sabt"};

const char8_t *const *calendar_islamic_days_of_week() { return days_of_week.data(); }

static constexpr auto days_of_week_arabic = std::array{u8"ٱلْأَحَد", u8"الاِثْنَيْن", u8"ٱلثُّلَاثَاء", u8"ٱلْأَرْبِعَاء", u8"ٱلْخَمِيس", u8"ٱلْجُمْعَة", u8"ٱلسَّبْت"};

const char8_t *const *calendar_islamic_days_of_week_arabic() { return days_of_week_arabic.data(); }

const char8_t *calendar_islamic_day_of_week(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < days_of_week.size()) {
    return days_of_week.at(index);
  }
  return u8"";
}

const char8_t *calendar_islamic_day_of_week_arabic(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < days_of_week_arabic.size()) {
    return days_of_week_arabic.at(index);
  }
  return u8"";
}

static constexpr auto months = std::array{u8"al-Muḥarram",
                                          u8"Ṣafar",
                                          u8"Rabīʿ al-ʾAwwal",
                                          u8"Rabīʿ ath-Thānī",
                                          u8"Jumādā al-ʾŪlā",
                                          u8"Jumādā ath-Thāniyah",
                                          u8"Rajab",
                                          u8"Shaʿbān",
                                          u8"Ramaḍān",
                                          u8"Shawwāl",
                                          u8"Ḏū al-Qaʿdah",
                                          u8"Ḏū al-Ḥijjah"};

const char8_t *const *calendar_islamic_month_names() { return months.data(); }

static constexpr auto months_arabic = std::array{u8"ٱلْمُحَرَّم",
                                                 u8"صَفَر",
                                                 u8"رَبِيع ٱلْأَوَّل",
                                                 u8"رَبِيع ٱلثَّانِي",
                                                 u8"جُمَادَىٰ ٱلْأُولَىٰ",
                                                 u8"جُمَادَىٰ ٱلثَّانِيَة",
                                                 u8"رَجَب",
                                                 u8"شَعْبَان",
                                                 u8"رَمَضَان",
                                                 u8"شَوَّال",
                                                 u8"ذُو ٱلْقَعْدَة",
                                                 u8"ذُو ٱلْحِجَّة"};

const char8_t *const *calendar_islamic_month_names_arabic() { return months_arabic.data(); }

const int calendar_islamic_num_month_names = months.size();

const char8_t *calendar_islamic_month_name(CALENDAR_ISLAMIC_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < months.size()) {
    return months.at(index);
  }
  return u8"";
}

const char8_t *calendar_islamic_month_name_arabic(CALENDAR_ISLAMIC_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < months_arabic.size()) {
    return months_arabic.at(index);
  }
  return u8"";
}
