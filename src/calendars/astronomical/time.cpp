#include <calendars/c/calendars/astronomical/time.h>
#include <calendars/cxx/calendars/astronomical/time.h>
#include <calendars/cxx/math/common.h>


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//////                                    Time  Arithmetic                                    //////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

CalendarTimeHM calendar_time_from_day_fraction(double dayFraction) {
  using namespace calendars;
  // Get just the day fraction
  dayFraction = math::mod(dayFraction, 1.);
  // Convert to hours
  dayFraction *= 24.0;
  return {
    // Get the hours
    .hours = (int)dayFraction,
    // Get the minutes
    .minutes = (int)(math::mod(dayFraction, 1.) * 60)
  };
}

CalendarTimeHM calendar_time_hm_create(int hours, int minutes) {
  return {
    .hours=hours,
    .minutes=minutes
  };
}

double calendar_time_from_hours_minutes(CalendarTimeHM hoursMinutes) {
  return (((double)hoursMinutes.minutes / 60.0) + (double)(hoursMinutes.hours)) / 24.;
}

CalendarTimeHM add_hours(CalendarTimeHM hm, int hours) {
  return {
    .hours = hm.hours + hours,
    .minutes = hm.minutes
  };
}

CalendarTimeHM add_minutes(CalendarTimeHM hm, int minutes) {
  return {
    .hours = hm.hours,
    .minutes = hm.minutes + minutes
  };
}

CalendarTimeHM sub_hours(CalendarTimeHM hm, int hours) {
  return {
    .hours = hm.hours - hours,
    .minutes = hm.minutes
  };
}

CalendarTimeHM sub_minutes(CalendarTimeHM hm, int minutes) {
  return {
    .hours = hm.hours,
    .minutes = hm.minutes - minutes
  };
}



CalendarAstroTimezone zone_from_longitude(double longitude){
  return {longitude / 360};
}

calendars::astronomical::Timezone calendars::astronomical::zone_from_longitude(SI::degree_t<double> longitude){
  using namespace SI::literals;
  return {SI::days_t<double>{longitude / 360._deg}};
}

double calendar_astro_equation_of_time(CalendarAstroUniversalDateTime t) {
  return calendars::astronomical::equation_of_time(calendars::astronomical::UniversalDateTime{t}).value();
}

SI::days_t<double> calendars::astronomical::equation_of_time(const UniversalDateTime& dateTime) {
  using namespace SI::literals;
  const auto c = calendars::astronomical::julian_centuries(dateTime);
  const auto c2 = c * c;
  const auto c3 = c2 * c;
  const auto lambda = 280.46645_deg + 36000.76983_deg * c + 0.0003032_deg * c2;
  const auto anomaly = 357.52910_deg + 35999.05030_deg * c
                       - 0.0001559_deg * c2 - 0.00000048_deg * c3;
  const auto eccentricity = 0.016708617 - 0.000042037 * c - 0.0000001236 * c2;
  const auto e = obli
}

SI::degree_t<double> calendars::astronomical::obliquity(const UniversalDateTime& t) {
  using namespace SI::Geo::literals;
  const auto c = calendars::astronomical::julian_centuries(t);
  DegMinSec{23, 26, 21.448}.as_degrees() + (-46.8150_minutes * c - 0.00059_minutes * c2);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//////                                    Time Conversions                                    //////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////


CalendarAstroUniversalDateTime calendar_astro_timezone_to_universal(CalendarAstroTimezoneDateTime timezoneTime){
  return (CalendarAstroUniversalDateTime)calendars::astronomical::to_universal(
    calendars::astronomical::TimezoneDateTime{timezoneTime}
  );
}

CalendarAstroUniversalDateTime calendar_astro_local_to_universal(CalendarAstroLocalDateTime localTime){
  return (CalendarAstroUniversalDateTime)calendars::astronomical::to_universal(
    calendars::astronomical::LocalDateTime{localTime}
  );
}

CalendarAstroUniversalDateTime calendar_astro_dynamical_to_universal(CalendarAstroDynamicalDateTime dateTime) {
  return (CalendarAstroUniversalDateTime)calendars::astronomical::to_universal(
    calendars::astronomical::DynamicalDateTime{dateTime}
  );
}

CalendarAstroLocalDateTime calendar_astro_timezone_to_local(CalendarAstroTimezoneDateTime timezoneTime, CalendarEarthPos location) {
  return (CalendarAstroLocalDateTime)calendars::astronomical::to_location(
    calendars::astronomical::TimezoneDateTime{timezoneTime},
    calendars::astronomical::EarthPos{location}
  );
}

CalendarAstroLocalDateTime calendar_astro_universal_to_local(CalendarAstroUniversalDateTime universalTime, CalendarEarthPos location) {
  return (CalendarAstroLocalDateTime)calendars::astronomical::to_location(
    calendars::astronomical::UniversalDateTime{universalTime},
    calendars::astronomical::EarthPos{location}
  );
}

CalendarAstroLocalDateTime calendar_astro_dynamical_to_local(CalendarAstroDynamicalDateTime dateTime, CalendarEarthPos location) {
  return (CalendarAstroLocalDateTime)calendars::astronomical::to_location(
    calendars::astronomical::DynamicalDateTime{dateTime},
    calendars::astronomical::EarthPos{location}
  );
}

CalendarAstroTimezoneDateTime calendar_astro_local_to_timezone(CalendarAstroLocalDateTime localTime, CalendarAstroTimezone timezone) {
  return (CalendarAstroTimezoneDateTime)calendars::astronomical::to_timezone(
    calendars::astronomical::LocalDateTime{localTime},
    calendars::astronomical::Timezone{timezone}
  );
}

CalendarAstroTimezoneDateTime calendar_astro_universal_to_timezone(CalendarAstroUniversalDateTime universalTime, CalendarAstroTimezone timezone){
  return (CalendarAstroTimezoneDateTime)calendars::astronomical::to_timezone(
    calendars::astronomical::UniversalDateTime{universalTime},
    calendars::astronomical::Timezone{timezone}
  );
}

CalendarAstroTimezoneDateTime calendar_astro_dynamical_to_timezone(CalendarAstroDynamicalDateTime dateTime, CalendarAstroTimezone timezone) {
  return (CalendarAstroTimezoneDateTime)calendars::astronomical::to_timezone(
    calendars::astronomical::DynamicalDateTime{dateTime},
    calendars::astronomical::Timezone{timezone}
  );
}

CalendarAstroDynamicalDateTime calendar_astro_local_to_dynamical(CalendarAstroLocalDateTime localTime) {
  return (CalendarAstroDynamicalDateTime)calendars::astronomical::to_dynamical(
    calendars::astronomical::LocalDateTime{localTime}
  );
}

CalendarAstroDynamicalDateTime calendar_astro_universal_to_dynamical(CalendarAstroUniversalDateTime universalTime)  {
  return (CalendarAstroDynamicalDateTime)calendars::astronomical::to_dynamical(
    calendars::astronomical::UniversalDateTime{universalTime}
  );
}

CalendarAstroDynamicalDateTime calendar_astro_timezone_to_dynamical(CalendarAstroTimezoneDateTime dateTime) {
  return (CalendarAstroDynamicalDateTime)calendars::astronomical::to_dynamical(
    calendars::astronomical::TimezoneDateTime{dateTime}
  );
}


CalendarMoment calendar_astro_universal_to_moment(CalendarAstroUniversalDateTime dateTime) {
  return (CalendarMoment)calendars::astronomical::UniversalDateTime(dateTime).to_moment();
}

CalendarMoment calendar_astro_timezone_to_moment(CalendarAstroTimezoneDateTime dateTime)  {
  return (CalendarMoment)calendars::astronomical::TimezoneDateTime(dateTime).to_moment();
}

CalendarMoment calendar_astro_local_to_moment(CalendarAstroLocalDateTime dateTime)  {
  return (CalendarMoment)calendars::astronomical::LocalDateTime(dateTime).to_moment();
}

CalendarMoment calendar_astro_dynamical_to_moment(CalendarAstroDynamicalDateTime dateTime) {
  return (CalendarMoment)calendars::astronomical::DynamicalDateTime(dateTime).to_moment();
}

CalendarAstroUniversalDateTime calendar_astro_universal_from_moment(CalendarMoment moment)  {
  return (CalendarAstroUniversalDateTime)calendars::astronomical::UniversalDateTime::from_moment(calendars::Moment{moment});
}

CalendarAstroDynamicalDateTime calendar_astro_dynamical_from_moment(CalendarMoment moment) {
  return (CalendarAstroDynamicalDateTime)calendars::astronomical::DynamicalDateTime::from_moment(calendars::Moment{moment});
}



////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//////                               Ephemeris Correction Start                               //////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

double calendar_astro_ephemeris_correction(CalendarRdDate date) {
  return calendars::astronomical::ephemeris_correction(calendars::RdDate{date}).value();
}

double calendar_astro_ephemeris_correction_gregorian(CalendarGregorianDate date) {
  return calendars::astronomical::ephemeris_correction(calendars::gregorian::Date{date}).value();
}

SI::days_t<double> calendars::astronomical::ephemeris_correction(const calendars::RdDate& date) {
  return ephemeris_correction(date.to<gregorian::Date>());
}



////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//////                               Ephemeris Correction Impl                                //////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////




static SI::days_t<double> c2051(const calendars::gregorian::Date& date) {
  const auto centuryOffset = static_cast<double>(date.year() - 1820) / 100.;
  return SI::seconds_t<double>{
      -20. + 32. * (centuryOffset * centuryOffset) + 0.5628 * (2150 - date.year())
  };
}

static double y2000(const calendars::gregorian::Date& date) {
  return static_cast<double>(date.year() - 2000);
}

static SI::days_t<double> c2006(const calendars::gregorian::Date& date) {
  const auto _y2000 = y2000(date);
  const auto _2y2000 = _y2000 * _y2000;
  return SI::seconds_t<double>{
    62.92 + 0.32217 * _y2000 + 0.005589 * _2y2000
  };
}

static SI::days_t<double> c1987(const calendars::gregorian::Date& date) {
  const auto _y2000 = y2000(date);
  const auto _2y2000 = _y2000 * _y2000;
  const auto _3y2000 = _2y2000 * _y2000;
  const auto _4y2000 = _2y2000 * _2y2000;
  const auto _5y2000 = _2y2000 * _2y2000 * _y2000;
  return SI::seconds_t<double> {
    63.86 + 0.3345 * _y2000 - 0.060374 * _2y2000
    + 0.0017275 * _3y2000 + 0.000651814 * _4y2000
    + 0.00002373599 * _5y2000
  };
}

static double y1820(const calendars::gregorian::Date& date) {
  return static_cast<double>(date.year() - 1820.) / 100.;
}

static SI::days_t<double> cOther(const calendars::gregorian::Date& date) {
  const auto _y1820 = y1820(date);
  const auto _2y1820 = _y1820 * _y1820;
  return SI::seconds_t<double> {
    -20.0 + 32. * _2y1820
  };
}

static double c(const calendars::gregorian::Date& date) {
  return static_cast<double>(
    calendars::gregorian::Date{1900, calendars::gregorian::MONTH::JANUARY, 1}
      .to_rd_date()
      .day_difference(
        calendars::gregorian::Date{date.year(), calendars::gregorian::MONTH::JULY, 1}.to_rd_date()
      )
  ) / 36525.;
}

static SI::days_t<double> c1900(const calendars::gregorian::Date& date) {
  const auto _c = c(date);
  const auto _2c = _c * _c;
  const auto _3c = _2c * _c;
  const auto _4c = _2c * _2c;
  const auto _5c = _2c * _2c * _c;
  const auto _6c = _3c * _3c;
  const auto _7c = _3c * _3c * _c;
  return SI::days_t<double>{
    -0.00002 + 0.000297 * _c + 0.025184 * _2c
    - 0.181133 * _3c + 0.553040 * _4c - 0.861938 * _5c
    + 0.677066 * _6c - 0.212591 * _7c
  };
}

static SI::days_t<double> c1800(const calendars::gregorian::Date& date) {
  const auto _c = c(date);
  const auto _2c = _c * _c;
  const auto _3c = _2c * _c;
  const auto _4c = _2c * _2c;
  const auto _5c = _2c * _2c * _c;
  const auto _6c = _3c * _3c;
  const auto _7c = _3c * _3c * _c;
  const auto _8c = _4c * _4c;
  const auto _9c = _4c * _4c * _c;
  const auto _10c = _5c * _5c;
  return SI::days_t<double>{
    -0.000009 + 0.003844 * _c + 0.083563 * _2c
    + 0.865736 * _3c + 4.867575 * _4c + 15.845535 * _5c
    + 31.332267 * _6c + 38.291999 * _7c + 28.316289 * _8c
    + 11.636204 * _9c + 2.043794 * _10c
  };
}

static double y1700(const calendars::gregorian::Date& date) {
  return static_cast<double>(date.year() - 1700);
}

static SI::days_t<double> c1700(const calendars::gregorian::Date& date) {
  const auto _y1700 = y1700(date);
  const auto _2y1700 = _y1700 * _y1700;
  const auto _3y1700 = _2y1700 * _2y1700;

  return SI::seconds_t<double>{
    8.118780842 - 0.005092142 * _y1700
    + 0.003336121 * _2y1700 - 0.0000266484 * _3y1700
  };
}

static double y1600(const calendars::gregorian::Date& date) {
  return static_cast<double>(date.year() - 1600);
}

static SI::days_t<double> c1600(const calendars::gregorian::Date& date) {
  const auto _y1600 = y1600(date);
  const auto _2y1600 = _y1600 * _y1600;
  const auto _3y1600 = _2y1600 * _2y1600;

  return SI::seconds_t<double>{
    120. - 0.9808 * _y1600 - 0.01532 * _2y1600
    + 0.000140272128 * _3y1600
  };
}

static double y1000(const calendars::gregorian::Date& date) {
  return static_cast<double>(date.year() - 1000) / 100.;
}

static SI::days_t<double> c500(const calendars::gregorian::Date& date) {
  const auto _y1000 = y1000(date);
  const auto _2y1000 = _y1000 * _y1000;
  const auto _3y1000 = _2y1000 * _y1000;
  const auto _4y1000 = _2y1000 * _2y1000;
  const auto _5y1000 = _2y1000 * _2y1000 * _y1000;
  const auto _6y1000 = _3y1000 * _3y1000;

  return SI::seconds_t<double>{
    1574.2 - 556.01 * _y1000 + 71.23472 * _2y1000
    + 0.319781 * _3y1000 - 0.8503463 * _4y1000
    - 0.005050998 * _5y1000 + 0.0083572073 * _6y1000
  };
}

static double y0(const calendars::gregorian::Date& date) {
  return static_cast<double>(date.year()) / 100.;
}

static SI::days_t<double> c0(const calendars::gregorian::Date& date) {
  const auto _y0 = y0(date);
  const auto _2y0 = _y0 * _y0;
  const auto _3y0 = _2y0 * _y0;
  const auto _4y0 = _2y0 * _2y0;
  const auto _5y0 = _2y0 * _2y0 * _y0;
  const auto _6y0 = _3y0 * _3y0;
  return SI::seconds_t<double>{
    10583.6 - 1014.41 * _y0 + 33.78311 * _2y0
    - 5.952053 * _3y0 - 0.1798452 * _4y0 + 0.022174192 * _5y0
    + 0.0090316521 * _6y0
  };
}

SI::days_t<double> calendars::astronomical::ephemeris_correction(const calendars::gregorian::Date& date){
  if (date.year() >= 2151) {
    // Too far into future (from time written) for accurate predictions
    return cOther(date);
  }
  else if (2051 <= date.year()) {
    return c2051(date);
  }
  else if (2006 <= date.year()) {
    return c2006(date);
  }
  else if (1987 <= date.year()) {
    return c1987(date);
  }
  else if (1900 <= date.year()) {
    return c1900(date);
  }
  else if (1800 <= date.year()) {
    return c1800(date);
  }
  else if (1700 <= date.year()) {
    return c1700(date);
  }
  else if (1600 <= date.year()) {
    return c1600(date);
  }
  else if (500 <= date.year()) {
    return c500(date);
  }
  else if (-500 < date.year()) {
    return c0(date);
  }
  else {
    // too little historical data (as of time written) for accurate predictions
    return cOther(date);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//////                                Ephemeris Correction End                                //////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//////                                     Julian Helpers                                     //////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

double calendars::astronomical::julian_centuries(const calendars::astronomical::UniversalDateTime & dateTime){
  return julian_centuries(to_dynamical(dateTime));
}

double calendars::astronomical::julian_centuries(const calendars::astronomical::DynamicalDateTime & dateTime){
  return (dateTime.dateTime.value() - j2000()) / 36525.;
}

double calendars::astronomical::j2000(){
  return calendars::astronomical::TimeHM{SI::hours_t<double>{12}}.dayFraction().value()
    + RdDate::from(gregorian::Date::year_start(2000)).to<Moment>().datetime();
}
