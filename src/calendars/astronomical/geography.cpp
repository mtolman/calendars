#include <calendars/c/calendars/astronomical/geography.h>
#include <calendars/cxx/calendars/astronomical/geography.h>
#include <calendars/c/calendars/astronomical/time.h>
#include <cmath>
#include <calendars/cxx/math/common.h>

CalendarEarthPos calendar_earth_pos_create(double latitude, double longitude, double elevation, double zoneOffset) {
  return {
    latitude,
    longitude,
    elevation,
    zoneOffset
  };
}

CalendarEarthPos calendar_earth_pos_urbana() {
  return {.latitude=40.1,.longitude=-88.2,.elevation=225, .zoneOffset=calendar_time_from_hours_minutes({.hours=-6,.minutes=0})};
}

CalendarEarthPos calendar_earth_pos_greenwhich() {
  return {.latitude=51.4777815,.longitude=0,.elevation=46.9, .zoneOffset=0};
}

CalendarEarthPos calendar_earth_pos_mecca() {
  return {
    .latitude=calendar_lat_lon_degrees_from_dms({.degrees=21, .minutes=25, .seconds=24}),
    .longitude=calendar_lat_lon_degrees_from_dms({.degrees=39, .minutes=49, .seconds=24}),
    .elevation=298,
    .zoneOffset=calendar_time_from_hours_minutes({.hours=3,.minutes=0})
  };
}

CalendarEarthPos calendar_earth_pos_jerusalem() {
  return {
    .latitude=31.78,
    .longitude=35.24,
    .elevation=740,
    .zoneOffset=calendar_time_from_hours_minutes({.hours=2,.minutes=0})
  };
}

CalendarEarthPos calendar_earth_pos_acre() {
  return {
    .latitude=32.94,
    .longitude=35.09,
    .elevation=22,
    .zoneOffset=calendar_time_from_hours_minutes({.hours=2,.minutes=0})
  };
}

double calendar_lat_lon_degrees_from_dms(CalendarGeoDegMinSec u){
  return (double)u.degrees + ((double)u.minutes / 60.) + ((double)u.seconds / 3600.);
}

double calendar_pos_direction(CalendarEarthPos p1, CalendarEarthPos p2) {
  auto left = calendars::astronomical::EarthPos(p1);
  auto right = calendars::astronomical::EarthPos(p2);
  return left.direction(right).value();
}

SI::degree_t<double> calendars::astronomical::EarthPos::direction(const calendars::astronomical::EarthPos & other) const noexcept{
  using namespace SI::literals;
  static constexpr auto epsilon = 0.000001;
  if (calendars::math::approx_eq(other.latitude(), 90._deg)) {
    return SI::degree_t<double>{0};
  }
  if (calendars::math::approx_eq(other.latitude(), -90._deg)) {
    return SI::degree_t<double>{180};
  }

  auto y = math::sin(longitude() - other.longitude()).value();
  auto x = (math::cos(latitude()) * math::tan(latitude()) - math::sin(latitude()) * math::cos(longitude() - other.longitude())).value();

  if (calendars::math::approx_eq(x, 0) && calendars::math::approx_eq(y, 0)) {
    return SI::degree_t<double>{0};
  }

  return math::arctan(y, x);
}
