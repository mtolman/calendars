#include <calendars/c/calendars/julian.h>
#include <calendars/cxx/math/common.h>

#include "epochs.h"
#include "utils.h"

CalendarJulianDate calendar_julian_create(int64_t year, CALENDAR_JULIAN_MONTH month, int16_t day) {
  return CalendarJulianDate{year, static_cast<int16_t>(month), day};
}

void calendar_julian_init(CalendarJulianDate *date, int64_t year, CALENDAR_JULIAN_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_julian_create(year, month, day);
}

int calendar_julian_compare(const CalendarJulianDate *left, const CalendarJulianDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->year, right->year, left->month, right->month, left->day, right->day);
}

bool calendar_julian_is_leap_year(int64_t year) {
  namespace math = calendars::math;
  return math::mod(year, 4) == (year > 0 ? 0 : 3);
}

CalendarJulianDate calendar_julian_from_rd_date(CalendarRdDate rdDate) {
  namespace math         = calendars::math;
  const auto approx      = math::floor<double, int64_t>(static_cast<double>(4 * rdDate - julian_epoch + 1464) / 1461.0);
  const auto julianYear  = approx <= 0 ? approx - 1 : approx;
  const auto julianStart = calendar_julian_create(julianYear, MONTH_JANUARY, 1);
  const auto priorDays   = rdDate - calendar_julian_to_rd_date(&julianStart);
  const auto marchFirst  = calendar_julian_create(julianYear, MONTH_MARCH, 1);
  const auto correction  = rdDate < calendar_julian_to_rd_date(&marchFirst) ? 0 : calendar_julian_is_leap_year(julianYear) ? 1 : 2;
  const auto julianMonth = math::floor<double, int16_t>(static_cast<double>(12 * (priorDays + correction) + 373) / 367.0);
  const auto monthStart  = calendar_julian_create(julianYear, static_cast<CALENDAR_JULIAN_MONTH>(julianMonth), 1);
  const auto julianDay   = static_cast<int16_t>(rdDate - calendar_julian_to_rd_date(&monthStart) + 1);
  return calendar_julian_create(julianYear, static_cast<CALENDAR_JULIAN_MONTH>(julianMonth), julianDay);
}

CalendarRdDate calendar_julian_to_rd_date(const CalendarJulianDate *julianDate) {
  namespace math = calendars::math;

  if (julianDate == nullptr) {
    return 0;
  }

  const auto y = julianDate->year < 0 ? julianDate->year + 1 : julianDate->year;
  return julian_epoch - 1 + 365 * (y - 1) + math::floor<double, int64_t>(static_cast<double>(y - 1) / 4.0)
         + +math::floor<double, int64_t>((367.0 * static_cast<double>(julianDate->month) - 362.0) / 12.0)
         + (static_cast<int16_t>(julianDate->month) <= 2     ? 0
            : calendar_julian_is_leap_year(julianDate->year) ? -1
                                                             : -2)
         + julianDate->day;
}
