#include <calendars/c/calendars/icelandic.h>
#include <calendars/cxx/calendars/icelandic.h>
#include <calendars/cxx/math/radix.h>

#include "epochs.h"
#include "utils.h"

CalendarIcelandicDate calendar_icelandic_create(int64_t year, CALENDAR_ICELANDIC_SEASON season, int8_t week, CALENDAR_DAY_OF_WEEK weekday) {
  return CalendarIcelandicDate{year, static_cast<int16_t>(season), week, static_cast<int8_t>(weekday)};
}
void calendar_icelandic_init(CalendarIcelandicDate *date, int64_t year, CALENDAR_ICELANDIC_SEASON season, int8_t week, CALENDAR_DAY_OF_WEEK weekday) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_icelandic_create(year, season, week, weekday);
}

CalendarRdDate calendar_icelandic_summer(int64_t year) {
  namespace math   = calendars::math;
  using YxA        = math::YxA<math::Radix<4, 25, 4>, math::Radix<97, 24, 1, 0>>;
  const auto apr19 = calendars::RdDate{icelandic_epoch + 365 * (year - 1) + YxA::sum(year)};
  return static_cast<CalendarRdDate>(apr19.day_of_week_on_or_after(calendars::DAY_OF_WEEK::THURSDAY));
}

CalendarRdDate calendar_icelandic_winter(int64_t year) { return calendar_icelandic_summer(year + 1) - 180; }

bool calendar_icelandic_is_leap_year(int64_t year) { return calendar_icelandic_summer(year + 1) - calendar_icelandic_winter(year) != 364; }

CALENDAR_ICELANDIC_MONTH calendar_icelandic_month(const CalendarIcelandicDate *date) {
  if (date == nullptr) {
    return ICELANDIC_MONTH_HARPA;
  }
  const auto midsummer = calendar_icelandic_winter(date->year) - 90;
  const auto rdDate    = calendar_icelandic_to_rd_date(date);
  const auto start     = date->season == CALENDAR_ICELANDIC_SEASON::ICELANDIC_SEASON_WINTER ? calendar_icelandic_winter(date->year)
                         : rdDate >= midsummer
                             ? midsummer - 90
                             : (rdDate < calendar_icelandic_summer(date->year) + 90 ? calendar_icelandic_summer(date->year) : midsummer);
  return static_cast<CALENDAR_ICELANDIC_MONTH>(calendars::math::floor<double, int>(static_cast<double>(rdDate - start) / 30.0 + 1));
}

int calendar_icelandic_compare(const CalendarIcelandicDate *left, const CalendarIcelandicDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->year, right->year, left->season, right->season, left->week, right->week, left->weekday, right->weekday);
}

CalendarIcelandicDate calendar_icelandic_from_rd_date(CalendarRdDate rdDate) {
  namespace math          = calendars::math;
  const auto approx       = math::floor<double, int64_t>((static_cast<double>(rdDate - icelandic_epoch + 369) * 400.0) / 146097.0);
  const auto approxSummer = calendar_icelandic_summer(approx);
  const auto year         = rdDate >= approxSummer ? approx : approx - 1;
  const auto winter       = calendar_icelandic_winter(year);
  const auto season       = rdDate < winter ? ICELANDIC_SEASON_SUMMER : ICELANDIC_SEASON_WINTER;
  const auto start        = season == ICELANDIC_SEASON_SUMMER ? calendar_icelandic_summer(year) : calendar_icelandic_winter(year);
  const auto week         = math::floor<double, int>(static_cast<double>(rdDate - start) / 7.0) + 1;
  const auto weekday      = calendar_day_of_week(rdDate);
  return calendar_icelandic_create(year, season, static_cast<int8_t>(week), weekday);
}

CalendarRdDate calendar_icelandic_to_rd_date(const CalendarIcelandicDate *date) {
  namespace math = calendars::math;
  if (date == nullptr) {
    return 0;
  }

  const auto start = date->season == ICELANDIC_SEASON_SUMMER ? calendar_icelandic_summer(date->year) : calendar_icelandic_winter(date->year);
  const auto shift = date->season == ICELANDIC_SEASON_SUMMER ? calendars::DAY_OF_WEEK::THURSDAY : calendars::DAY_OF_WEEK::SATURDAY;

  return start + (7LL * (date->week - 1) + math::mod(static_cast<int64_t>(date->weekday) - static_cast<int64_t>(shift), 7));
}

static constexpr auto day_of_week_names =
    std::array{u8"Sunnudagur", u8"Mánudagur", u8"Þriðjudagur", u8"Miðvikudagur", u8"Fimmtudagur", u8"Föstudagur", u8"Laugardagur"};

const char8_t *const *calendar_icelandic_day_of_week_names() { return day_of_week_names.data(); }

const char8_t *calendar_icelandic_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek) {
  const auto index = static_cast<size_t>(dayOfWeek);
  if (index < day_of_week_names.size()) {
    return day_of_week_names.at(index);
  }
  return u8"";
}
