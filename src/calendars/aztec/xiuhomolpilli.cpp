#include <calendars/c/calendars/aztec/xiuhomolpilli.h>
#include <calendars/cxx/calendars/aztec/tonalpohualli.h>
#include <calendars/cxx/calendars/aztec/xihuitl.h>
#include <calendars/cxx/calendars/aztec/xiuhomolpilli.h>

#include "utils.h"

bool calendar_xiuhomolpilli_is_valid(const CalendarXiuhomolpilliDate *date) {
  if (date == nullptr) {
    return false;
  }

  switch (date->name) {
    case CALENDAR_XIUHOMOLPILLI_NAME::XIUHOMOLPILLI_ACATL:
    case CALENDAR_XIUHOMOLPILLI_NAME::XIUHOMOLPILLI_CALLI:
    case CALENDAR_XIUHOMOLPILLI_NAME::XIUHOMOLPILLI_TECPATL:
    case CALENDAR_XIUHOMOLPILLI_NAME::XIUHOMOLPILLI_TOCHTLI:
      return true;
  }

  return false;
}

int calendar_xiuhomolpilli_compare(const CalendarXiuhomolpilliDate *left, const CalendarXiuhomolpilliDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right);
  return calendars::utils::chain_cmp(left->number, right->number, left->name, right->name);
}

CalendarXiuhomolpilliDate calendar_xiuhomolpilli_create(int16_t number, CALENDAR_XIUHOMOLPILLI_NAME name) {
  return CalendarXiuhomolpilliDate{number, static_cast<int16_t>(name)};
}

void calendar_xiuhomolpilli_init(CalendarXiuhomolpilliDate *date, int16_t number, CALENDAR_XIUHOMOLPILLI_NAME name) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_xiuhomolpilli_create(number, name);
}
CalendarXiuhomolpilliDateOptional calendar_xiuhomolpilli_from_rd_date(CalendarRdDate date) {
  auto resultOptional = CalendarXiuhomolpilliDateOptional{false, calendar_xiuhomolpilli_create(0, CALENDAR_XIUHOMOLPILLI_NAME::XIUHOMOLPILLI_CALLI)};

  const auto month = calendars::RdDate{date}.to<calendars::aztec::xihuitl::Date>().month();
  if (month == static_cast<calendars::aztec::xihuitl::MONTH>(19)) {
    return resultOptional;
  }
  const auto tonalpohualli = calendars::aztec::xihuitl::Date{static_cast<calendars::aztec::xihuitl::MONTH>(18), 20}
                                 .on_or_before(calendars::RdDate{date + 364})
                                 .to<calendars::aztec::tonalpohualli::Date>();
  const auto res =
      calendars::aztec::xiuhomolpilli::Date{tonalpohualli.number(), static_cast<calendars::aztec::xiuhomolpilli::NAME>(tonalpohualli.name())};
  if (!res.is_valid()) {
    return resultOptional;
  }
  return {true, static_cast<CalendarXiuhomolpilliDate>(res)};
}
