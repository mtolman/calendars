#include <calendars/c/calendars/aztec/xihuitl.h>
#include <calendars/cxx/calendars/aztec/xihuitl.h>
#include <calendars/cxx/math/common.h>

#include <array>

#include "utils.h"

CalendarXihuitlDate calendar_xihuitl_create(CALENDAR_XIHUITL_MONTH month, int16_t day) {
  return CalendarXihuitlDate{static_cast<int16_t>(month), day};
}

void calendar_xihuitl_init(CalendarXihuitlDate *date, CALENDAR_XIHUITL_MONTH month, int16_t day) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_xihuitl_create(month, day);
}

int calendar_xihuitl_ordinal(const CalendarXihuitlDate *date) {
  if (date == nullptr) {
    return 0;
  }

  return (static_cast<int>(date->month) - 1) * 20 + static_cast<int>(date->day) - 1;
}

int calendar_xihuitl_compare(const CalendarXihuitlDate *left, const CalendarXihuitlDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::impl::chain_cmp(left->month, right->month, left->day, right->day);
}

CalendarXihuitlDate calendar_xihuitl_from_rd_date(CalendarRdDate rdDate) {
  namespace math        = calendars::math;
  const auto count      = math::mod(rdDate - xihuitl_correlation, 365);
  const auto aztecDay   = static_cast<int16_t>(math::mod(count, 20) + 1);
  const auto aztecMonth = static_cast<CALENDAR_XIHUITL_MONTH>(math::floor<double, int>(static_cast<double>(count) / 20.0) + 1);
  return calendar_xihuitl_create(aztecMonth, aztecDay);
}

CalendarRdDate calendar_xihuitl_on_or_before(CalendarRdDate rdDate, const CalendarXihuitlDate *date) {
  namespace math = calendars::math;
  if (date == nullptr) {
    return 0;
  }

  return math::mod_range(xihuitl_correlation + calendar_xihuitl_ordinal(date), rdDate, rdDate - 365);
}
CalendarRdDateOptional xihuitl_tonalpohualli_on_or_before(CalendarRdDate                   date,
                                                          const CalendarXihuitlDate       *xihuitl,
                                                          const CalendarTonalpohualliDate *tonalpohualli) {
  namespace math   = calendars::math;
  auto resOptional = CalendarRdDateOptional{false, 0};

  if (xihuitl == nullptr || tonalpohualli == nullptr) {
    return resOptional;
  }

  const auto xihuitlCount       = calendar_xihuitl_ordinal(xihuitl) + xihuitl_correlation;
  const auto tonalpohualliCount = calendar_tonalpohualli_ordinal(tonalpohualli) + tonalpohualli_correlation;
  const auto diff               = xihuitlCount - tonalpohualliCount;
  if (math::mod(diff, 5) == 0) {
    return CalendarRdDateOptional{true, math::mod_range(xihuitlCount + 365 * diff, date, date - 18980)};
  }
  return resOptional;
}

static constexpr auto month_names = std::array{u8"Izcalli",
                                               u8"Atlcahualo",
                                               u8"Tlacaxipehualiztli",
                                               u8"Tozoztontli",
                                               u8"Huei Tozoztli",
                                               u8"Toxcatl",
                                               u8"Etzalcualiztli",
                                               u8"Tecuilhuitontli",
                                               u8"Huei Tecuilhuitl",
                                               u8"Tlaxochimaco",
                                               u8"Xocotlhuetzi",
                                               u8"Ochpaniztli",
                                               u8"Teotleco",
                                               u8"Tepeilhuitl",
                                               u8"Quecholli",
                                               u8"Panquetzaliztli",
                                               u8"Atemoztli",
                                               u8"Tititl",
                                               u8"Nemontemi"};

const int             calendar_xihuitl_num_month_names = month_names.size();
const char8_t *const *calendar_xihuitl_month_names() { return month_names.data(); }
const char8_t        *calendar_xihuitl_month_name(CALENDAR_XIHUITL_MONTH month) {
  const auto index = static_cast<size_t>(month) - 1;
  if (index < month_names.size()) {
    return month_names.at(index);
  }
  return u8"";
}
auto calendars::aztec::xihuitl::Date::month_names() noexcept -> std::array<const char8_t *, 19> { return ::month_names; }
