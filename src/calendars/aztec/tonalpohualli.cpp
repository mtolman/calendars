#include <calendars/c/calendars/aztec/tonalpohualli.h>
#include <calendars/cxx/calendars/aztec/tonalpohualli.h>
#include <calendars/cxx/math/common.h>

#include "epochs.h"
#include "utils.h"

CalendarTonalpohualliDate calendar_tonalpohualli_create(int16_t number, CALENDAR_TONALPOHUALLI_NAME name) {
  return CalendarTonalpohualliDate{number, static_cast<int16_t>(name)};
}

void calendar_tonalpohualli_init(CalendarTonalpohualliDate *date, int16_t number, CALENDAR_TONALPOHUALLI_NAME name) {
  if (date == nullptr) {
    return;
  }

  *date = calendar_tonalpohualli_create(number, name);
}

int calendar_tonalpohualli_ordinal(const CalendarTonalpohualliDate *date) {
  if (date == nullptr) {
    return 0;
  }

  namespace math = calendars::math;
  return math::mod(static_cast<int>(date->number) - 1 + 39 * (static_cast<int>(date->number) - static_cast<int>(date->name)), 260);
}

int calendar_tonalpohualli_compare(const CalendarTonalpohualliDate *left, const CalendarTonalpohualliDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->number, right->number, left->name, right->name);
}

CalendarTonalpohualliDate calendar_tonalpohualli_from_rd_date(CalendarRdDate rdDate) {
  namespace math    = calendars::math;
  const auto count  = rdDate - tonalpohualli_correlation + 1;
  const auto number = math::amod(count, 13);
  const auto name   = math::amod(count, 20);
  return calendar_tonalpohualli_create(static_cast<int16_t>(number), static_cast<CALENDAR_TONALPOHUALLI_NAME>(name));
}

CalendarRdDate calendar_tonalpohualli_on_or_before(CalendarRdDate rdDate, const CalendarTonalpohualliDate *date) {
  namespace math = calendars::math;
  return math::mod_range(tonalpohualli_correlation + calendar_tonalpohualli_ordinal(date), rdDate, rdDate - 260);
}

static constexpr auto names = std::array{u8"Cipactli", u8"Ehecatl",       u8"Calli",      u8"Cuetzpallin", u8"Coatl",     u8"Miquiztli", u8"Mazatl",
                                         u8"Tochtli",  u8"Atl",           u8"Itzcuintli", u8"Ozomatli",    u8"Malinalli", u8"Acatl",     u8"Ocelotl",
                                         u8"Quauhtli", u8"Cozcaquauhtli", u8"Ollin",      u8"Tecpatl",     u8"Quiahuitl", u8"Xochitl"};

const char8_t *const *calendar_tonalpohualli_names() { return names.data(); }
const char8_t        *calendar_tonalpohualli_name(CALENDAR_TONALPOHUALLI_NAME name) {
  const auto index = static_cast<size_t>(name) - 1;
  if (index < names.size()) {
    return names.at(index);
  }
  return u8"";
}
const int calendar_tonalpohualli_num_names = names.size();
auto      calendars::aztec::tonalpohualli::Date::name_strs() noexcept -> std::array<const char8_t *, 20> { return names; }
