#include <calendars/c/calendars/old_hindu_luni_solar.h>
#include <calendars/cxx/calendars/old_hindu_luni_solar.h>
#include <calendars/cxx/math/common.h>

#include "epochs.h"
#include "utils.h"

CalendarOldHinduLuniSolarDate calendar_old_hindu_luni_solar_create(int64_t                                year,
                                                                   CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT month,
                                                                   bool                                   isLeapYear,
                                                                   int16_t                                day) {
  return CalendarOldHinduLuniSolarDate{year, static_cast<int16_t>(month), isLeapYear, day};
}
void calendar_old_hindu_luni_solar_init(
    CalendarOldHinduLuniSolarDate *date, int64_t year, CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT month, bool isLeapYear, int16_t day) {
  if (date == nullptr) {
    return;
  }
  *date = calendar_old_hindu_luni_solar_create(year, month, isLeapYear, day);
}

int calendar_old_hindu_luni_solar_compare(const CalendarOldHinduLuniSolarDate *left, const CalendarOldHinduLuniSolarDate *right) {
  CALENDAR_IMPL_POINTER_COMPARE(left, right)
  return calendars::utils::chain_cmp(left->year,
                                     right->year,
                                     left->sanskrit,
                                     right->sanskrit,
                                     left->isLeapMonth,
                                     right->isLeapMonth,
                                     left->day,
                                     right->day);
}

bool calendar_old_hindu_luni_solar_is_leap_year(int64_t year) {
  namespace math = calendars::math;
  return math::mod(static_cast<double>(year) * arya_solar_year - arya_solar_month, arya_lunar_month) >= 23902504679.0 / 1282400064.0;
}

CalendarOldHinduLuniSolarDate calendar_old_hindu_luni_solar_from_rd_date(CalendarRdDate rdDate) {
  namespace math          = calendars::math;
  const auto sun          = static_cast<double>(calendar_old_hindu_solar_day_count(rdDate)) + 6.0 / 24.0;
  const auto newMoon      = sun - math::mod(sun, arya_lunar_month);
  const auto newMoonMonth = math::mod(newMoon, arya_solar_month);
  const bool leap         = arya_solar_month - arya_lunar_month >= newMoonMonth && newMoonMonth > 0;
  const auto hMonth       = math::mod(static_cast<int64_t>(std::ceil(newMoon / arya_solar_month)), 12) + 1;
  const auto hDay         = static_cast<int16_t>(math::mod(math::floor(sun / arya_lunar_day), 30) + 1);
  const auto hYear        = static_cast<int64_t>(std::ceil((newMoon + arya_solar_month) / arya_solar_year)) - 1;
  return calendar_old_hindu_luni_solar_create(hYear, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(hMonth), leap, hDay);
}

CalendarRdDate calendar_old_hindu_luni_solar_to_rd_date(const CalendarOldHinduLuniSolarDate *date) {
  if (date == nullptr) {
    return 0;
  }

  namespace math          = calendars::math;
  const auto mina         = static_cast<double>(12 * date->year - 1) * arya_solar_month;
  const auto lunarNewYear = arya_lunar_month * (math::floor(mina / arya_lunar_month) + 1);
  const auto adjustment =
      !date->isLeapMonth && std::ceil((lunarNewYear - mina) / (arya_solar_month - arya_lunar_month)) <= static_cast<double>(date->sanskrit)
          ? static_cast<int64_t>(date->sanskrit)
          : static_cast<int64_t>(date->sanskrit) - 1;
  return static_cast<int64_t>(
      std::ceil(old_hindu_solar_epoch + lunarNewYear + arya_lunar_month * adjustment + (date->day - 1) * arya_lunar_day - 6.0 / 24.0));
}
