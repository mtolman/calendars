#include <calendars/cxx/calendars/cyclic.h>

#include "../utils.h"

auto calendars::cyclic::single_cycle::DateTemplate::operator<=>(const DateTemplate& o) const noexcept -> std::strong_ordering {
  return impl::compare_int(utils::chain_cmp(type, o.type, epoch, o.epoch, Y, o.Y, M, o.M, deltaY, o.deltaY, deltaM, o.deltaM));
}
auto calendars::cyclic::single_cycle::DateTemplate::make(int64_t year, int32_t month, int32_t day) -> calendars::cyclic::single_cycle::Date {
  return calendars::cyclic::single_cycle::Date(year, month, day, *this);
}

auto calendars::cyclic::single_cycle::Date::operator<=>(const calendars::cyclic::single_cycle::Date& o) const noexcept -> std::strong_ordering {
  return impl::compare_int(utils::chain_cmp(dateTemplate(), o.dateTemplate(), year(), o.year(), month(), o.month(), day(), o.day()));
}

auto calendars::cyclic::single_cycle::Date::to_rd_date() const noexcept -> calendars::RdDate {
  if (type == Type::STRICTLY_BEFORE) {
    return calendars::RdDate{epoch + math::pnum_to_int(math::floor((mYear - 1) * Y + deltaY))
                             + math::pnum_to_int(math::floor((mMonth - 1) * M + deltaM)) + mDay - 1};
  }
  else if (type == Type::UP_TO_AND_INCLUDING) {
    return calendars::RdDate{epoch + math::pnum_to_int(math::ceil((mYear - 1) * Y - deltaY))
                             + math::pnum_to_int(math::floor((mMonth - 1) * M + deltaM)) + mDay - 1};
  }
  else if (type == Type::MEAN_MONTH) {
    return calendars::RdDate{epoch + math::pnum_to_int(math::floor((mYear - 1) * Y + deltaY + (mMonth - 1) * M)) + mDay - 1};
  }
  else {
    return calendars::RdDate{epoch + math::pnum_to_int(math::ceil((mYear - 1) * Y + deltaY + (mMonth - 1) * M)) + mDay - 1};
  }
}

auto calendars::cyclic::single_cycle::Date::from_rd_date(const calendars::RdDate&              date,
                                                         calendars::cyclic::single_cycle::Type type,
                                                         int64_t                               epoch,
                                                         const calendars::math::pnum_t&        Y,
                                                         const calendars::math::pnum_t&        M,
                                                         const calendars::math::pnum_t&        deltaY,
                                                         const calendars::math::pnum_t&        deltaM) -> calendars::cyclic::single_cycle::Date {
  if (type == Type::STRICTLY_BEFORE) {
    const auto d     = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch + 1) - deltaY;
    const auto year  = math::pnum_to_int(math::ceil(d / Y));
    const auto n     = math::ceil(d - (year - 1) * Y) - deltaM;
    const auto month = math::pnum_to_int<int32_t>(math::ceil(n / M));
    const auto day   = math::pnum_to_int<int32_t>(math::ceil(math::amod(n, M)));
    return Date{year, month, day, type, epoch, Y, M, deltaY, deltaM};
  }
  else if (type == Type::UP_TO_AND_INCLUDING) {
    const auto d     = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch) + deltaY;
    const auto year  = math::pnum_to_int(math::floor(d / Y)) + 1;
    const auto n     = math::floor(math::mod(d, Y)) + 1 - deltaM;
    const auto month = math::pnum_to_int<int32_t>(math::ceil(n / M));
    const auto day   = math::pnum_to_int<int32_t>(math::ceil(math::amod(n, M)));
    return Date{year, month, day, type, epoch, Y, M, deltaY, deltaM};
  }
  else if (type == Type::MEAN_MONTH) {
    const auto    d     = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch + 1) - deltaY;
    const auto    year  = math::pnum_to_int(math::ceil(d / Y));
    const int32_t L     = math::pnum_to_int<int32_t>(Y / M);
    const auto    m     = math::ceil(d / M) - 1;
    const int32_t month = math::pnum_to_int<int32_t>(math::mod(m, L + 1));
    const auto    day   = math::pnum_to_int<int32_t>(math::ceil(math::amod(d, M)));
    return Date{year, month, day, type, epoch, Y, M, deltaY, deltaM};
  }
  else {
    const auto    d     = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch) - deltaY;
    const auto    year  = math::pnum_to_int(math::floor(d / Y) + 1);
    const int32_t L     = math::pnum_to_int<int32_t>(Y / M);
    const int32_t month = math::mod(math::pnum_to_int<int32_t>(math::floor(d / M)), L + 1);
    const auto    day   = math::pnum_to_int<int32_t>(math::floor(math::mod(d, M))) + 1;
    return Date{year, month, day, type, epoch, Y, M, deltaY, deltaM};
  }
}

auto calendars::cyclic::single_cycle::Date::from_rd_date(const calendars::RdDate&                             date,
                                                         const calendars::cyclic::single_cycle::DateTemplate& dateTemplate) noexcept
    -> calendars::cyclic::single_cycle::Date {
  return from_rd_date(date, dateTemplate.type, dateTemplate.epoch, dateTemplate.Y, dateTemplate.M, dateTemplate.deltaY, dateTemplate.deltaM);
}
