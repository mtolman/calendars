#pragma once

#include "epochs.h"

namespace calendars::utils {
  namespace impl {
    inline int chain_cmp() { return 0; }

    inline bool chain_lt() { return false; }

    inline bool chain_eq() { return true; }

    template<typename L, typename R, typename... T>
    bool chain_lt(L left, R right, T... args) {
      return left < right || (left == right && chain_lt(args...));
    }

    template<typename L, typename R, typename... T>
    bool chain_eq(L left, R right, T... args) {
      return left == right && chain_eq(args...);
    }

    template<typename L, typename R, typename... T>
    int chain_cmp(L left, R right, T... args) {
      if (left < right) {
        return -1;
      }
      else if (left > right) {
        return 1;
      }
      return chain_cmp(args...);
    }
  }  // namespace impl

  template<typename... T>
  int chain_lt(T... args) {
    return impl::chain_lt(args...);
  }

  template<typename... T>
  int chain_cmp(T... args) {
    return impl::chain_cmp(args...);
  }

  template<typename... T>
  bool chain_eq(T... args) {
    return impl::chain_eq(args...);
  }

#define CALENDAR_IMPL_POINTER_COMPARE(LEFT, RIGHT) \
  if (LEFT == RIGHT) {  return 0; } \
  if (LEFT == nullptr) { return -1; } \
  if (RIGHT == nullptr) { return 1; }
}  // namespace calendars::utils
