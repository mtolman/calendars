#pragma once

#include <calendars/c/calendars/ethiopic.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::ethiopic {
  enum class MONTH {
    MASKARAM = 1,
    TEQEMT   = 2,
    HEDAR    = 3,
    TAKHSAS  = 4,
    TER      = 5,
    YAKATIT  = 6,
    MAGABIT  = 7,
    MIYAZYA  = 8,
    GENBOT   = 9,
    SANE     = 10,
    HAMLE    = 11,
    NAHASE   = 12,
    PAGUEMEN = 13
  };

  class Date {
    CalendarEthiopicDate date;
   public:
    explicit Date(CalendarEthiopicDate date) : date(date) {}
    explicit operator CalendarEthiopicDate() const noexcept { return date; }
    explicit Date(int64_t year = 0, MONTH month = MONTH::MASKARAM, int16_t day = 1) : date(calendar_ethiopic_create(year, static_cast<CALENDAR_ETHIOPIC_MONTH>(month), day)) {}
    explicit Date(int64_t year, CALENDAR_ETHIOPIC_MONTH month, int16_t day) : date(calendar_ethiopic_create(year, month, day)) {}

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_ethiopic_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return is_leap_year(date.year); }
    [[nodiscard]] static auto is_leap_year(int64_t year) noexcept -> bool { return calendar_ethiopic_is_leap_year(year); }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) -> Date { return Date{ calendar_ethiopic_from_rd_date(static_cast<CRdDate>(rdDate))}; }
    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{ calendar_ethiopic_to_rd_date(&date)}; }

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(date.month); }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto month_name() const noexcept -> const char8_t* { return calendar_ethiopic_month_name(static_cast<CALENDAR_ETHIOPIC_MONTH>(date.month)); }
    [[nodiscard]] static auto month_names() noexcept -> std::array<const char8_t*, 13>;
  };
}
