#pragma once

#include <calendars/c/calendars/islamic.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::islamic {
  enum class MONTH : int16_t {
    MUHARRAM = 1,
    SAFAR,
    RABI_I,
    RABI_II,
    JUMADA_I,
    JUMADA_II,
    RAJAB,
    SHABAN,
    RAMADAN,
    SHAWWAL,
    DHU_AL_QADA,
    DHU_AL_HIJJA
  };

  class Date {
    CalendarIslamicDate date;
   public:
    explicit Date(CalendarIslamicDate date) : date(date) {}
    explicit Date(int64_t year = 1, MONTH month = MONTH::MUHARRAM, int16_t day = 1) : Date(year, static_cast<CALENDAR_ISLAMIC_MONTH>(month), day) {}
    Date(int64_t year, CALENDAR_ISLAMIC_MONTH month, int16_t day) : date(calendar_islamic_create(year, month, day)) {}
    explicit operator CalendarIslamicDate() const noexcept { return date; }

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(date.month); }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_islamic_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)
    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return is_leap_year(date.year); }
    [[nodiscard]] static auto is_leap_year(int64_t year) noexcept -> bool  { return calendar_islamic_is_leap_year(year); }
    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{ calendar_islamic_to_rd_date(&date)}; }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{ calendar_islamic_from_rd_date(static_cast<CRdDate>(rdDate))}; }
  };
}
