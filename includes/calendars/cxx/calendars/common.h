#pragma once

/** @file */

#include <calendars/c/calendars/common.h>
#include <calendars/c/calendars/moment.h>

#include <cmath>
#include <ostream>
#include <type_traits>
#include <vector>
#include <array>

#include <calendars/cxx/math/common.h>
#include "traits.h"

#define CALENDAR_CXX_COMPARISON_OPS(T)                                                                                                                            \
  bool operator==(const T& o) const noexcept { return (*this <=> o) == std::strong_ordering::equal; }                                                \
  bool operator!=(const T& o) const noexcept { return (*this <=> o) != std::strong_ordering::equal; }                                                \
  bool operator<(const T& o) const noexcept { return (*this <=> o) == std::strong_ordering::less; }                                                  \
  bool operator<=(const T& o) const noexcept { return (*this <=> o) != std::strong_ordering::greater; }                                              \
  bool operator>(const T& o) const noexcept { return (*this <=> o) == std::strong_ordering::greater; }                                               \
  bool operator>=(const T& o) const noexcept { return (*this <=> o) != std::strong_ordering::less; }

namespace calendars {
  using CRdDate     = CalendarRdDate;
  enum class DAY_OF_WEEK {
    SUNDAY = DAY_OF_WEEK_SUNDAY,
    MONDAY = DAY_OF_WEEK_MONDAY,
    TUESDAY = DAY_OF_WEEK_TUESDAY,
    WEDNESDAY = DAY_OF_WEEK_WEDNESDAY,
    THURSDAY = DAY_OF_WEEK_THURSDAY,
    FRIDAY = DAY_OF_WEEK_FRIDAY,
    SATURDAY = DAY_OF_WEEK_SATURDAY,
  };

  class Moment {
    CalendarMoment moment;

   public:
    explicit Moment(CalendarMoment moment = 0) : moment(moment) {}

    explicit operator CalendarMoment() const noexcept { return moment; }

    /**
     * Returns the time portion of the moment (aka. the time of day)
     * @return
     */
    [[nodiscard]] auto to_time() const noexcept -> double { return calendar_moment_to_time(moment); }

    /**
     * Handles converting a Moment to a target date
     * @tparam T
     * @return
     */
    template<typename T>
    [[nodiscard]] T to() const noexcept requires traits::FromMoment<T> {
      if constexpr (std::is_same_v<T, Moment>) {
        return *this;
      }
      else {
        return T::from_moment(*this);
      }
    }

    /**
     * Handles converting a Moment to a target date
     * @tparam T
     * @return
     */
    template<typename T>
    [[nodiscard]] static Moment from(const T& date) noexcept requires traits::ToMoment<T> {
      if constexpr (std::is_same_v<std::decay_t<T>, Moment>) {
        return date;
      }
      else {
        return date.to_moment();
      }
    }

    /**
     * @return A double representing the combined date and time
     */
    [[nodiscard]] auto datetime() const noexcept -> double { return moment; }

    /**
     * Compares two Moments
     * @param other Moment to compare
     * @return
     */
    auto operator<=>(const Moment& other) const noexcept -> std::partial_ordering { return moment <=> other.moment; }
    bool operator==(const Moment& o) const noexcept { return (*this <=> o) == std::partial_ordering::equivalent; }
    bool operator!=(const Moment& o) const noexcept { return (*this <=> o) != std::partial_ordering::equivalent; }
    bool operator<(const Moment& o) const noexcept { return (*this <=> o) == std::partial_ordering::less; }
    bool operator<=(const Moment& o) const noexcept { return (*this <=> o) != std::partial_ordering::greater; }
    bool operator>(const Moment& o) const noexcept { return (*this <=> o) == std::partial_ordering::greater; }
    bool operator>=(const Moment& o) const noexcept { return (*this <=> o) != std::partial_ordering::less; }
  };

  /**
   * A C++ wrapper around RdDate which provides several convenience methods
   */
  class RdDate {
    /**
     * C RdDate underlying the C++ class
     */
    CRdDate date;

   public:
    /**
     * Creates a new Date from a C RdDate
     * @param date C RdDate
     */
    explicit RdDate(CRdDate date = 0) : date(date) {}

    /**
     * Converts a Date to a C RdDate
     * @return C RdDate
     */
    explicit operator CRdDate() const { return date; }

    /**
     * Compares two RdDates
     * @param other Date to compare
     * @return
     */
    auto operator<=>(const RdDate& other) const -> std::strong_ordering { return date <=> other.date; }

    CALENDAR_CXX_COMPARISON_OPS(RdDate)

    /**
     * Creates a date from a C++ calendar date (e.g. calendars::gregorian::Date)
     * @tparam T Type being converted from
     * @param date Date instance being converted from
     * @return Date
     */
    template<class T>
    [[nodiscard]] static auto from(const T& date) noexcept -> RdDate requires traits::ToRdDate<T> {
      if constexpr (std::is_same_v<std::decay_t<T>, RdDate>) {
        return date;
      }
      else if constexpr (std::is_same_v<std::decay_t<T>, Moment>) {
        return RdDate{calendars::math::floor<decltype(date.datetime()), int64_t>(date.datetime())};
      }
      else if constexpr (traits::ToRdDateFunction<T>) {
        return date.to_rd_date();
      }
      else {
        return from(Moment::from(date));
      }
    }

    /**
     * Converts a date to a specific C++ calendar date (e.g. calendars::gregorian::Date)
     * @tparam T Type being converted to
     * @return Instance of target date type
     */
    template<class T>
    [[nodiscard]] T to() const noexcept requires traits::FromRdDate<T> {
      if constexpr (std::is_same_v<std::decay_t<T>, RdDate>) {
        return *this;
      }
      else if constexpr (std::is_same_v<T, Moment>) {
        return Moment{static_cast<CalendarMoment>(date)};
      }
      else if constexpr(traits::FromRdDateFunction<T>) {
        return T::from_rd_date(*this);
      }
      else {
        return to<Moment>().to<T>();
      }
    }

    /**
     * Returns the day of week for the current date (Sunday through Saturday)
     * @return Day of week for current date
     */
    [[nodiscard]] auto day_of_week() const noexcept -> DAY_OF_WEEK { return static_cast<DAY_OF_WEEK>(calendar_day_of_week(date)); }

    /**
     * Returns a new Date with n number of days added
     * @param days Number of days to add
     * @return new Date object with the number of days added
     */
    [[nodiscard]] auto add_days(int64_t days) const noexcept -> RdDate { return RdDate{date + days}; }

    /**
     * Returns a new Date with n number of days subtracted
     * @param days Number of days to subtract
     * @return new Date object with the number of days subtracted
     */
    [[nodiscard]] auto sub_days(int64_t days) const noexcept -> RdDate { return RdDate{date - days}; }

    /**
     * Returns the difference in the number of days between the current date and another date
     * @param other Other date to use for calculation
     * @return Number of days between the two dates
     */
    [[nodiscard]] auto day_difference(const RdDate& other) const noexcept -> int64_t { return date - other.date; }

    /**
     * Gets the day of an m-cycle represented by the current date
     * The number returned will be from [0,m)
     *
     * @param m The number of days in the cycle (e.g. for day of the week, set m to 7)
     * @param offset The offset of the cycle (day of the week has an offset of 0)
     * @return An integer from [0,m) representing the day in the m-cycle
     */
    [[nodiscard]] auto day_of_m_cycle(int32_t m, int32_t offset = 0) const noexcept -> int { return calendar_day_of_m_cycle(date, m, offset); }

    /**
     * Finds the first date on or before the current date that occurs on the target day of the week
     *
     * @param dayOfWeek Day of the week to look for
     * @return An Date representing the day of the week
     *
     */
    [[nodiscard]] auto day_of_week_on_or_before(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate {
      return RdDate{calendar_day_of_week_on_or_before(date, static_cast<CALENDAR_DAY_OF_WEEK>(dayOfWeek))};
    }

    /**
     * @copydoc day_of_week_on_or_before
     * @see day_of_week_on_or_before
     */
    [[nodiscard]] inline auto k_day_on_or_before(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return day_of_week_on_or_before(dayOfWeek); }

    /**
     * Finds the first date on or after the current date that occurs on the target day of the week
     *
     * @param dayOfWeek Day of the week to look for
     * @return An Date representing the day of the week
     *
     */
    [[nodiscard]] auto day_of_week_on_or_after(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate {
      return RdDate{calendar_day_of_week_on_or_after(date, static_cast<CALENDAR_DAY_OF_WEEK>(dayOfWeek))};
    }

    /**
     * @copydoc day_of_week_on_or_after
     * @see day_of_week_on_or_after
     */
    [[nodiscard]] inline auto k_day_on_or_after(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return day_of_week_on_or_after(dayOfWeek); }

    /**
     * Finds the first date nearest the current date that occurs on the target day of the week
     *
     * @param dayOfWeek Day of the week to look for
     * @return An Date representing the day of the week
     *
     */
    [[nodiscard]] auto day_of_week_nearest(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return RdDate{calendar_day_of_week_nearest(date, static_cast<CALENDAR_DAY_OF_WEEK>(dayOfWeek))}; }

    /**
     * @copydoc day_of_week_nearest
     * @see day_of_week_nearest
     */
    [[nodiscard]] inline auto k_day_nearest(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return day_of_week_nearest(dayOfWeek); }

    /**
     * Finds the first date before the current date that occurs on the target day of the week
     *
     * @param dayOfWeek Day of the week to look for
     * @return An Date representing the day of the week
     *
     */
    [[nodiscard]] auto day_of_week_before(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return RdDate{calendar_day_of_week_before(date, static_cast<CALENDAR_DAY_OF_WEEK>(dayOfWeek))}; }

    /**
     * @copydoc day_of_week_before
     * @see day_of_week_before
     */
    [[nodiscard]] inline auto k_day_before(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return day_of_week_before(dayOfWeek); }

    /**
     * Finds the first date after the current date that occurs on the target day of the week
     *
     * @param dayOfWeek Day of the week to look for
     * @return An Date representing the day of the week
     *
     */
    [[nodiscard]] auto day_of_week_after(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return RdDate{calendar_day_of_week_after(date, static_cast<CALENDAR_DAY_OF_WEEK>(dayOfWeek))}; }

    /**
     * @copydoc day_of_week_after
     * @see day_of_week_after
     */
    [[nodiscard]] inline auto k_day_after(DAY_OF_WEEK dayOfWeek) const noexcept -> RdDate { return day_of_week_after(dayOfWeek); }

    /**
     * Finds the kth day of the m-cycle that occurs on or before the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] auto kth_day_of_m_cycle_on_or_before(int k, int m, int offset) const noexcept -> RdDate {
      return RdDate{calendar_kth_day_of_m_cycle_on_or_before(date, k, m, offset)};
    }

    /**
     * Finds the kth day of the m-cycle that occurs before the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] auto kth_day_of_m_cycle_before(int k, int m, int offset) const noexcept -> RdDate {
      return RdDate{calendar_kth_day_of_m_cycle_before(date, k, m, offset)};
    }

    /**
     * Finds the kth day of the m-cycle that occurs on or after the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] auto kth_day_of_m_cycle_on_or_after(int k, int m, int offset) const noexcept -> RdDate {
      return RdDate{calendar_kth_day_of_m_cycle_on_or_after(date, k, m, offset)};
    }

    /**
     * Finds the kth day of the m-cycle that occurs after the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] auto kth_day_of_m_cycle_after(int k, int m, int offset) const noexcept -> RdDate {
      return RdDate{calendar_kth_day_of_m_cycle_after(date, k, m, offset)};
    }

    /**
     * Finds the kth day of the m-cycle that occurs nearest the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] auto kth_day_of_m_cycle_nearest(int k, int m, int offset) const noexcept -> RdDate {
      return RdDate{calendar_kth_day_of_m_cycle_nearest(date, k, m, offset)};
    }

    /**
     * Returns the nth occurrence of a day on a given week before the current date (or if n is
     * negative after d) If n is zero, it will return the current date instead
     * @param n The number of the occurrence before the current date (or if negative, the number of
     * the occurrence after)
     * @param k The week day to search for
     * @return
     */
    [[nodiscard]] auto nth_week_day(int n, DAY_OF_WEEK k) const noexcept -> RdDate {
      return RdDate{ calendar_nth_week_day(date, n, static_cast<CALENDAR_DAY_OF_WEEK>(k))};
    }

    /**
     * Return the cyclic positions in the range of the current date to the end date (exclusive)
     * Used to collect all occurrences of events (e.g. holidays) in an interval of time (e.g. a Gregorian year)
     *
     * Assumes $0 \le pthMoment \lt cDayCycle$
     *
     * @param end End of the cyclic range
     * @param pthMoment Starting pth moment search. First result on or after a given moment of the pth moment (basically the cycle start)
     * @param cDayCycle The number of days in the occurrence day cycle (basically the cycle length)
     * @param delta Congruent modulo of cDayCycle to the position of RdDate{0} in the repeating cycle (basically the cycle modulo offset)
     * @return
     */
     template<typename Allocator = std::allocator<RdDate>>
    [[nodiscard]] auto positions_in_range(const RdDate& end, int64_t pthMoment, int64_t cDayCycle, int64_t delta) const -> std::vector<RdDate, Allocator> {
      auto res   = std::vector<RdDate, Allocator>{};
      auto start = date;
      while (true) {
        auto curDate = math::mod_range<int64_t>(pthMoment - delta, start, start + cDayCycle);
        if (curDate >= end.date) {
          return res;
        }
        res.emplace_back(curDate);
        start += cDayCycle;
      }
    }

    /**
     * Returns a vector of n days starting at the current date. Includes the current date as the starting element
     * A positive numDays will return days following the current date.
     * A negative numDays will return days before the current date.
     * @param numDays Number of days to return
     * @return
     */
    template<typename Allocator = std::allocator<RdDate>>
    [[nodiscard]] auto n_days(int64_t numDays) const -> std::vector<RdDate>  {
      auto res = std::vector<RdDate, Allocator>{};
      auto dir = numDays > 0 ? 1 : -1;

      res.reserve(numDays);

      auto cur       = *this;
      auto totalDays = std::abs(numDays);
      for (auto count = 0; count < totalDays; ++count) {
        res.emplace_back(cur);
        cur = cur.add_days(dir);
      }
      return res;
    }

    /**
     * Returns a vector of the n days following the current date. The current date is excluded from the list.
     *
     * A positive numDays will return days following the current date.
     * A negative numDays will return days before the current date.
     *
     * @param numDays Number of days to return
     * @return
     */
    template<typename Allocator = std::allocator<RdDate>>
    [[nodiscard]] auto next_n_days(int64_t numDays) const -> std::vector<RdDate> {
      if (numDays >= 0) {
        return add_days(1).n_days<Allocator>(numDays);
      }
      return add_days(-1).n_days<Allocator>(numDays);
    }

    /**
     * Returns a vector of the days in a range (end is excluded).
     * If the end date is lower than the current date, then the range will be backwards
     *
     * @param end End date (excluded from vector)
     * @return
     */
    template<typename Allocator = std::allocator<RdDate>>
    [[nodiscard]] auto days_in_range(const RdDate& end) const -> std::vector<RdDate, Allocator> {
      auto dir = *this <= end ? 1 : -1;
      auto res = std::vector<RdDate, Allocator>{};
      res.reserve(std::abs(day_difference(end)) + 1);

      for (auto curDate = *this; curDate != end; curDate = curDate.add_days(dir)) {
        res.emplace_back(curDate);
      }
      return res;
    }

    /**
     * OStream operator for RdDate objects
     * @param os Out stream
     * @param rd RdDate
     * @return Out stream (for chaining)
     */
    friend std::ostream& operator<<(std::ostream& os, const RdDate& rd) {
      os << "RdDate<" << rd.date << ">";
      return os;
    }

    RdDate first_week_day(DAY_OF_WEEK k) const noexcept { return RdDate{calendar_first_week_day(static_cast<CALENDAR_DAY_OF_WEEK>(k), date)}; }

    RdDate last_week_day(DAY_OF_WEEK k) const noexcept { return RdDate{calendar_last_week_day(static_cast<CALENDAR_DAY_OF_WEEK>(k), date)}; }
  };

  /**
   * Represents a range of dates
   * The "end" date is always excluded from iteration
   * @tparam DateType Type of date to use. Must be convertible to and from RdDate
   */
  template<typename DateType = RdDate>
  class DateRange {
   public:
    /**
     * Creates a new date range from the begining (inclusive) to the end (exclusive)
     * @param begin Date to begin from. Defaults to RdDate{0}
     * @param end Date to end before (it is exclusive). Defaults to the maximum RdDate value.
     */
    explicit DateRange(
        const DateType& begin = RdDate{0}.template to<DateType>(),
        const DateType& end = RdDate{std::numeric_limits<CRdDate>::max()}.template to<DateType>()
    ) :
        start(begin), finish(end)
    {}

    /**
     * Iterator over dates in increasing order (B.C. -> A.D)
     */
    struct Iterator {
      using iterator_cateogry = std::random_access_iterator_tag;
      using difference_type = std::int64_t;
      using value_type = DateType;
      using pointer = std::add_pointer_t<std::add_const_t<value_type>>;
      using reference = std::add_lvalue_reference_t<std::add_const_t<value_type>>;

      explicit Iterator(const DateType& date) noexcept : value(date) {}

      auto operator*() const -> reference { return value; }
      auto operator->() const -> pointer { return &value; }

      Iterator& operator++() {
        if constexpr (std::is_same_v<DateType, RdDate>) {
          value = value.add_days(1);
        }
        else {
          value = RdDate::from(value).add_days(1).template to<DateType>();
        }
        return *this;
      }

      Iterator operator++(int) {
        const auto cpy = *this;
        ++(*this);
        return cpy;
      }

      difference_type operator-(const Iterator& other) const noexcept {
        if constexpr (std::is_same_v<DateType, RdDate>) {
          return value.day_difference(other.value);
        }
        else {
          return RdDate::from(value).day_difference(RdDate::from(other.value));
        }
      }

      Iterator operator+(const difference_type& diff) const {
        if constexpr (std::is_same_v<DateType, RdDate>) {
          return Iterator{value.add_days(diff)};
        }
        else {
          return Iterator{RdDate::from(value).add_days(diff).template to<DateType>()};
        }
      }
      Iterator operator-(const difference_type& diff) const { return (*this) + (-diff); }

      value_type operator[] (const difference_type& diff) const { return *(*this + diff); }

      auto operator<=>(const Iterator& o) const noexcept -> std::strong_ordering { return value <=> o.value; }

      CALENDAR_CXX_COMPARISON_OPS(Iterator)
     private:
      DateType value;
    };

    /**
     * Iterator over dates in decreasing order (A.D. -> B.C.)
     */
    struct ReverseIterator {

      using iterator_cateogry = std::random_access_iterator_tag;
      using difference_type = std::int64_t;
      using value_type = DateType;
      using pointer = std::add_pointer_t<std::add_const_t<value_type>>;
      using reference = std::add_lvalue_reference_t<std::add_const_t<value_type>>;

      explicit ReverseIterator(const DateType& date) noexcept : iterator(date) {}

      auto operator*() const -> reference { return *iterator; }
      auto operator->() const -> pointer { return iterator.operator->(); }

      ReverseIterator& operator++() {
        iterator = iterator - 1;
        return *this;
      }

      ReverseIterator operator++(int) {
        const auto cpy = *this;
        ++(*this);
        return cpy;
      }

      difference_type operator-(const ReverseIterator& other) const noexcept {
        return -(iterator - other.iterator);
      }

      ReverseIterator operator+(const difference_type& diff) const {
        return ReverseIterator{iterator - diff};
      }
      ReverseIterator operator-(const difference_type& diff) const { return (*this) + (-diff); }

      value_type operator[] (const difference_type& diff) const { return *(*this + diff); }

      auto operator<=>(const ReverseIterator& o) const noexcept -> std::strong_ordering { return o.iterator <=> iterator; }

      CALENDAR_CXX_COMPARISON_OPS(ReverseIterator)
     private:
      explicit ReverseIterator(const Iterator& iterator) : iterator(iterator) {};

      Iterator iterator;
    };

    using iterator = Iterator;
    using const_iterator = Iterator;
    using reverse_iterator = ReverseIterator;

    /**
     * @return Start of the date range
     */
    [[nodiscard]] auto begin() const noexcept -> iterator { return iterator{start}; }

    /**
     * @return End of the date range
     */
    [[nodiscard]] auto end() const noexcept -> iterator { return iterator{finish}; }

    /**
     * @return Start of the reversed date range
     */
    [[nodiscard]] auto rbegin() const noexcept -> reverse_iterator { return reverse_iterator{finish} + 1; }

    /**
     * @return End of the reversed date range
     */
    [[nodiscard]] auto rend() const noexcept -> reverse_iterator { return reverse_iterator{start} + 1; }

    /**
     * @return Constant start of the date range
     */
    [[nodiscard]] auto cbegin() const noexcept -> const_iterator { return const_iterator {start}; }

    /**
     * @return Constant end of the date range
     */
    [[nodiscard]] auto cend() const noexcept -> const_iterator { return const_iterator{finish}; }

    /**
     * @return Size of the date range (number of days that will be returned by the date range)
     */
    [[nodiscard]] auto size() const noexcept -> typename iterator::difference_type { return end() - begin(); }
   private:
    DateType start;
    DateType finish;
  };

  namespace impl {
    inline auto compare_int(int cmp) -> std::strong_ordering {
      if (cmp < 0) {
        return std::strong_ordering::less;
      }
      else if (cmp > 0) {
        return std::strong_ordering::greater;
      }
      else {
        return std::strong_ordering::equal;
      }
    }
  }

#define CALENDAR_CXX_COMMON_METHODS(CxxClassName, CFieldName, CMethodName) \
  /** @brief Returns whether the date is valid */                                                                         \
  [[nodiscard]] auto is_valid() const noexcept -> bool { return calendar_ ## CMethodName ## _is_valid(&CFieldName); } \
  /** @brief Returns the closest valid date. If the date was valid, returns a copy of the date */                                                                         \
  [[nodiscard]] auto nearest_valid() const noexcept -> CxxClassName { return CxxClassName{calendar_ ## CMethodName ## _nearest_valid(&CFieldName)}; } \
  /** @brief Does a three way date comparison */                                                                         \
  auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_ ## CMethodName ## _compare(&date, &other.date)); }

#define CALENDAR_CXX_BIDIRECTIONAL_CONVERSIONS(CxxClassName, CFieldName, CMethodName) \
  /** @breif Creates a gregorian date from an RdDate @param rdDate RdDate to convert from */ \
  [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> CxxClassName { return Date{calendar_ ## CMethodName ## _from_rd_date(static_cast<CRdDate>(rdDate))}; } \
  /** Converts the current gregorian date to an RdDate */ \
  [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_ ## CMethodName ## _to_rd_date(&CFieldName)}; }

}  // namespace calendars
