#pragma once

#include <calendars/c/calendars/egyptian.h>
#include "common.h"

namespace calendars::egyptian {
  /**
   * Represents an ancient Egyptian date
   */
  class Date {
    CalendarEgyptianDate date;
   public:
    /**
     * Creates a new egyptian date from a C egyptian date
     * @param date
     */
    explicit Date(CalendarEgyptianDate date) : date(date) {}

    /**
     * Creates a new egyptian date from a year, month, and day
     * @param year Egyptian calendar year
     * @param month Egyptian calendar month (1-13)
     * @param day Egyptian calendar day (1-30)
     */
    explicit Date(int64_t year = 0, int16_t month = 1, int16_t day = 1) : date(calendar_egyptian_create(year, month, day)) {}

    /**
     * Converts to a C Egyptian date
     * @return
     */
    explicit operator CalendarEgyptianDate() const noexcept { return date; }

    /**=
     * @return Whether the current date is a valid Egyptian date
     */
    [[nodiscard]] auto is_valid() const noexcept -> bool { return calendar_egyptian_is_valid(&date); }

    /**
     * @return The closest valid egyptian date. If the date was already valid, a copy of the date is returned
     */
    [[nodiscard]] auto nearest_valid() const noexcept -> Date { return Date{calendar_egyptian_nearest_valid(&date)}; }

    /**
     * Does a three way comparison on an egyptian date
     * @param other Date to compare against
     * @return
     */
    auto               operator<=>(const Date& other) const noexcept -> std::strong_ordering {
      return impl::compare_int(calendar_egyptian_compare(&date, &other.date));
    }

    /**
     * Converts an RdDate to an Egyptian date
     * @param rdDate RdDate to convert
     */
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date {
      return Date{calendar_egyptian_from_rd_date(static_cast<CRdDate>(rdDate))};
    }

    /**
     * Converts the current Egyptian to an RdDate date
     */
    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_egyptian_to_rd_date(&date)}; }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    /**
     * @return Egyptian year of date
     */
    [[nodiscard]] auto year() const noexcept -> decltype(CalendarEgyptianDate::year) { return date.year; }

    /**
     * @return Egyptian month of date
     */
    [[nodiscard]] auto month() const noexcept -> decltype(CalendarEgyptianDate::month) { return date.month; }

    /**
     * @return Egyptian day of date
     */
    [[nodiscard]] auto day() const noexcept -> decltype(CalendarEgyptianDate::day) { return date.day; }

    [[nodiscard]] auto month_name() const noexcept -> const char8_t* { return calendar_egyptian_month_name(date.month); }
    [[nodiscard]] static auto month_names() noexcept;
  };
}
