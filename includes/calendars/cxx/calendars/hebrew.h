#pragma once

#include <calendars/c/calendars/hebrew.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::hebrew {
  enum class MONTH {
    NISAN = 1,
    IYYAR,
    SIVAN,
    TAMMUZ,
    AV,
    ELUL,
    TISHRI,
    MARHESHVAN,
    KISLEV,
    TEVET,
    SHEVAT,
    ADAR,
    ADAR_II
  };

  class Date {
    CalendarHebrewDate date;

   public:
    explicit Date(CalendarHebrewDate date) : date(date) {}
    explicit Date(int64_t year = 0, MONTH month = MONTH::NISAN, int16_t day = 1) : Date(year, static_cast<CALENDAR_HEBREW_MONTH>(month), day) {}
    Date(int64_t year, CALENDAR_HEBREW_MONTH month, int16_t day) : date(calendar_hebrew_create(year, month, day)) {}
    explicit operator CalendarHebrewDate() const noexcept { return date; }

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_hebrew_to_rd_date(&date)}; }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{ calendar_hebrew_from_rd_date(static_cast<CRdDate>(rdDate))}; }
    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_hebrew_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(date.month); }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return is_leap_year(date.year); }
    [[nodiscard]] static auto is_leap_year(int64_t year) noexcept -> bool { return calendar_hebrew_is_leap_year(year); }

    [[nodiscard]] auto is_long_marheshvan() const noexcept -> bool { return is_long_marheshvan(date.year); }
    [[nodiscard]] static auto is_long_marheshvan(int64_t year) noexcept -> bool { return calendar_hebrew_long_marheshvan(year); }

    [[nodiscard]] auto is_short_kislev() const noexcept -> bool { return is_short_kislev(date.year); }
    [[nodiscard]] static auto is_short_kislev(int64_t year) noexcept -> bool { return calendar_hebrew_short_kislev(year); }

    [[nodiscard]] auto last_month_of_year() const noexcept -> MONTH { return last_month_of_year(date.year); }
    [[nodiscard]] static auto last_month_of_year(int64_t year) noexcept -> MONTH { return static_cast<MONTH>(calendar_hebrew_last_month_of_year(year)); }

    [[nodiscard]] auto is_sabbatical_year() const noexcept -> bool { return is_sabbatical_year(date.year); }
    [[nodiscard]] static auto is_sabbatical_year(int64_t year) noexcept -> bool { return calendar_hebrew_is_sabbatical_year(year); }

    [[nodiscard]] auto molad() const noexcept -> double { return molad(date.year, static_cast<MONTH>(date.month)); }
    [[nodiscard]] static auto molad(int64_t year, MONTH month) noexcept -> double { return calendar_hebrew_molad(year, static_cast<CALENDAR_HEBREW_MONTH>(month)); }

    [[nodiscard]] static auto elapsed_days(int64_t year) noexcept -> int64_t { return calendar_hebrew_elapsed_days(year); }
    [[nodiscard]] static auto year_length_correction(int64_t year) noexcept -> int64_t { return calendar_hebrew_year_length_correction(year); }
    [[nodiscard]] static auto new_year(int64_t year) noexcept -> RdDate { return RdDate{calendar_hebrew_new_year(year)}; }
    [[nodiscard]] static auto last_day_of_month(int64_t year, MONTH month) noexcept -> int16_t { return calendar_hebrew_last_day_of_month(year, static_cast<CALENDAR_HEBREW_MONTH>(month)); }
    [[nodiscard]] static auto days_in_year(int64_t year) noexcept -> int64_t { return calendar_hebrew_days_in_year(year); }

    [[nodiscard]] static auto day_of_week_names() noexcept -> std::array<const char8_t*, 7>;
    [[nodiscard]] static auto day_of_week_name(DAY_OF_WEEK dayOfWeek) noexcept -> const char8_t* { return calendar_hebrew_day_of_week_name(static_cast<CALENDAR_DAY_OF_WEEK>(dayOfWeek)); }

    [[nodiscard]] static auto month_names() noexcept -> std::array<const char8_t*, 13>;
    [[nodiscard]] static auto month_name(MONTH month) noexcept -> const char8_t* { return calendar_hebrew_month_name(static_cast<CALENDAR_HEBREW_MONTH>(month)); }
    [[nodiscard]] static auto month_names_hebrew() noexcept -> std::array<const char8_t*, 13>;
    [[nodiscard]] static auto month_name_hebrew(MONTH month) noexcept -> const char8_t* { return calendar_hebrew_month_name_hebrew(static_cast<CALENDAR_HEBREW_MONTH>(month)); }
    [[nodiscard]] auto month_name() const noexcept -> const char8_t* { return month_name(static_cast<MONTH>(date.month)); }
    [[nodiscard]] auto month_name_hebrew() const noexcept -> const char8_t* { return month_name_hebrew(static_cast<MONTH>(date.month)); }


    /**
     * OStream operator
     * @param os Out stream
     * @param d Hebrew Date
     * @return
     */
    friend std::ostream& operator<<(std::ostream& os, const Date& d) {
      os << "Hebrew<" << d.year() << "-" << static_cast<int>(d.month()) << "-" << d.day() << ">";
      return os;
    }
    static int64_t year_from_gregorian(int64_t gregorianYear);
  };
}
