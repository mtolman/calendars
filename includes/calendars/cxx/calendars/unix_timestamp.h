#pragma once

#include <calendars/c/calendars/unix_timestamp.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::unix_timestamp {
  class Date {
    CalendarUnixTimestamp date;
   public:
    explicit Date(CalendarUnixTimestamp date) : date(date) {}
    explicit Date(int64_t seconds = 0) : date(calendar_unix_timestamp_create(seconds)) {}
    explicit operator CalendarUnixTimestamp() const noexcept { return date; }

    [[nodiscard]] auto timestamp() const noexcept -> int64_t { return date.timestamp; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_unix_timestamp_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto to_moment() const noexcept -> Moment { return Moment{ calendar_unix_timestamp_to_moment(&date)}; }
    [[nodiscard]] static auto from_moment(const Moment& moment) noexcept -> Date { return Date{ calendar_unix_timestamp_from_moment(static_cast<CalendarMoment>(moment))}; }
  };
}
