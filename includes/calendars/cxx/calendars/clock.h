#pragma once

#include <array>
#include <calendars/c/calendars/clock.h>
#include <calendars/cxx/calendars/comp_radix.h>
#include <calendars/cxx/math/common.h>

namespace calendars::clock {
  class Date {
    enum CLOCK_COMPONENT
    {
      DAY,
      HOUR,
      MINUTE,
      SECOND
    };

   public:
    using Radix = comp_radix::Date<0, 24, 60, 60>;

    explicit Date(const CalendarClockDate& value) : Date(value.day, value.hour, value.minute, value.second) {}
    explicit Date(std::array<double, 4> values) : value(values) {}
    Date(std::initializer_list<double> vals) : value(vals) {}
    explicit Date(const Moment& moment) : value(Radix::from_moment(moment)) {}
    explicit Date(Radix value) : value(value) {}

    explicit operator CalendarClockDate() const noexcept { return calendar_clock_create(day(), hour(), minute(), second()); }

    Date(double days, double hours, double minutes, double seconds) : value(std::array{days, hours, minutes, seconds}) {}

    [[nodiscard]] auto operator<=>(const Date& other) const { return value <=> other.value; }
    [[nodiscard]] bool operator>=(const Date& o) const { auto cmp = (*this <=> o); return cmp == std::partial_ordering::equivalent || cmp == std::partial_ordering::greater; }
    [[nodiscard]] bool operator>(const Date& o) const { return (*this <=> o) == std::partial_ordering::greater; }
    [[nodiscard]] bool operator<=(const Date& o) const { auto cmp = (*this <=> o); return cmp == std::partial_ordering::equivalent || cmp == std::partial_ordering::less; }
    [[nodiscard]] bool operator==(const Date& o) const { return (*this <=> o) == std::partial_ordering::equivalent; }
    [[nodiscard]] bool operator!=(const Date& o) const { return (*this <=> o) != std::partial_ordering::equivalent; }
    [[nodiscard]] bool operator<(const Date& o) const { return (*this <=> o) == std::partial_ordering::less; }

    /**
     * Returns a clock rounded to the nearest second
     * @return
     */
    [[nodiscard]] Date nearest_second() const {
      return Moment{std::round((time() * 24 * 60 * 60)) / (24.0 * 60 * 60)}.to<Date>();
    }

    [[nodiscard]] double time() const noexcept { return Moment::from(*this).to_time(); }

    [[nodiscard]] static Date from_moment(const Moment& moment) { return Date{moment}; }

    [[nodiscard]] Moment to_moment() const { return value.to_moment(); }

    [[nodiscard]] double day() const noexcept { return this->value.value[DAY]; }
    [[nodiscard]] double hour() const noexcept { return this->value.value[HOUR]; }
    [[nodiscard]] double minute() const noexcept { return this->value.value[MINUTE]; }
    [[nodiscard]] double second() const noexcept { return this->value.value[SECOND]; }

    [[nodiscard]] Date with_day(double day) const {
      auto newValue = value;
      newValue[DAY] = day;
      return Date{newValue};
    }

    [[nodiscard]] Date with_hour(double hour) const {
      auto newValue = value;
      newValue[HOUR] = hour;
      return Date{newValue};
    }

    [[nodiscard]] Date with_minute(double minute) const {
      auto newValue = value;
      newValue[MINUTE] = minute;
      return Date{newValue};
    }

    [[nodiscard]] Date with_second(double second) const {
      auto newValue = value;
      newValue[SECOND] = second;
      return Date{newValue};
    }
   private:
    Radix value;
  };
}
