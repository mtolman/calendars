#pragma once

#include <calendars/c/calendars/julian.h>
#include <calendars/cxx/calendars/gregorian.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::julian {
  using MONTH = calendars::gregorian::MONTH;

  class Date {
    CalendarJulianDate date;

   public:
    explicit Date(const CalendarJulianDate& date) : date(date) {}

    explicit Date(int64_t year = 1, MONTH month = MONTH::JANUARY, int16_t day = 1) : date(calendar_julian_create(year, static_cast<CALENDAR_JULIAN_MONTH>(month), day)) {}
    Date(int64_t year, CALENDAR_JULIAN_MONTH month, int16_t day) : date(calendar_julian_create(year, month, day)) {}

    explicit operator CalendarJulianDate() const noexcept { return date; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_julian_compare(&date, &other.date)); }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return is_leap_year(date.year); }

    [[nodiscard]] static auto is_leap_year(int64_t year) noexcept -> bool { return calendar_julian_is_leap_year(year); }

    [[nodiscard]] static auto from_rd_date(const RdDate& date) noexcept -> Date { return Date{calendar_julian_from_rd_date(static_cast<CRdDate>(date))}; }

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_julian_to_rd_date(&date)}; }

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }

    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(date.month); }

    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto month_name() const noexcept -> std::u8string { return { calendar_gregorian_month_name(static_cast<CALENDAR_GREGORIAN_MONTH>(month()))}; }
    [[nodiscard]] static auto month_names() noexcept { return calendars::gregorian::Date::month_names(); }
  };
}
