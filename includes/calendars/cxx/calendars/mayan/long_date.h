#pragma once

#include <calendars/c/calendars/mayan/long_date.h>
#include <calendars/cxx/calendars/comp_radix.h>

namespace calendars::mayan::long_date {
  class Date {
   public:
    using Radix = calendars::comp_radix::Date<4, 20, 20, 18, 20>;

    explicit Date(Radix date) : date(date) {}
    explicit Date(CalendarLongDate date): date({date.cycle0, date.cycle1, date.cycle2, date.cycle3}) {}
    Date(std::initializer_list<double> radix) : date(Radix{radix}) {}

    explicit operator CalendarLongDate() const noexcept { return CalendarLongDate{date.get<0>(), date.get<1>(), date.get<2>(), date.get<3>()}; }

    [[nodiscard]] auto operator<=>(const Date& other) const -> std::partial_ordering { return date <=> other.date; }
    [[nodiscard]] bool operator>=(const Date& o) const { auto cmp = (*this <=> o); return cmp == std::partial_ordering::equivalent || cmp == std::partial_ordering::greater; }
    [[nodiscard]] bool operator>(const Date& o) const { return (*this <=> o) == std::partial_ordering::greater; }
    [[nodiscard]] bool operator<=(const Date& o) const { auto cmp = (*this <=> o); return cmp == std::partial_ordering::equivalent || cmp == std::partial_ordering::less; }
    [[nodiscard]] bool operator==(const Date& o) const { return (*this <=> o) == std::partial_ordering::equivalent; }
    [[nodiscard]] bool operator!=(const Date& o) const { return (*this <=> o) != std::partial_ordering::equivalent; }
    [[nodiscard]] bool operator<(const Date& o) const { return (*this <=> o) == std::partial_ordering::less; }

    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date;
    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate;

   private:
    Radix date;
  };

  enum class UNITS : int64_t {
    KIN = 1,
    UINAL = KIN * 20,
    TUN = UINAL * 18,
    KATUN = TUN * 20,
    BAKTUN = KATUN * 20,
    PICTUN = BAKTUN * 20,
    CALABTUN = PICTUN * 20,
    KINCHILTUN = CALABTUN * 20,
    ALAUTUN = KINCHILTUN * 20,
  };
}
