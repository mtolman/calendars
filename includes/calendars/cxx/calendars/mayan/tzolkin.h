#pragma once

#include <calendars/c/calendars/mayan/tzolkin.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::mayan::tzolkin {
  enum class NAME
  {
    IMIX = 1,
    IK,
    AKBAL,
    KAN,
    CHICCHAN,
    CIMI,
    MANIK,
    LAMAT,
    MULUC,
    OC,
    CHUEN,
    EB,
    BEN,
    IX,
    MEN,
    CIB,
    CABAN,
    ETZNAB,
    CAUAC,
    AHAU,
  };

  class Date {
    CalendarTzolkinDate date;
   public:
    explicit Date(CalendarTzolkinDate date) : date(date) {}
    explicit Date(int16_t number = 1, NAME name = NAME::IMIX) : Date(number, static_cast<CALENDAR_TZOLKIN_NAME>(name)) {}
    Date(int16_t number, CALENDAR_TZOLKIN_NAME name) : date(calendar_tzolkin_create(number, name)) {}
    explicit operator CalendarTzolkinDate() const noexcept { return date; }

    [[nodiscard]] auto number() const noexcept -> int16_t { return date.number; }
    [[nodiscard]] auto name() const noexcept -> NAME { return static_cast<NAME>(date.name); }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_tzolkin_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto ordinal() const noexcept -> int64_t { return calendar_tzolkin_ordinal(&date); }

    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{calendar_tzolkin_from_rd_date(static_cast<CRdDate>(rdDate))}; }

    [[nodiscard]] auto name_str() const noexcept -> const char8_t* { return calendar_tzolkin_name(static_cast<CALENDAR_TZOLKIN_NAME>(date.name)); }
    [[nodiscard]] static auto name_strs() noexcept -> std::array<const char8_t*, 20>;
  };
}
