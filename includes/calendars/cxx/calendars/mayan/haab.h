#pragma once

#include <calendars/c/calendars/mayan/haab.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::mayan::haab {
  enum class MONTH {
    POP = 1,
    UO,
    ZIP,
    ZOTZ,
    TZEC,
    XUL,
    YAXKIN,
    MOL,
    CHEN,
    YAX,
    ZAC,
    CEH,
    MAC,
    KANKIN,
    MUAN,
    PAX,
    KAYAB,
    CUMKU,
    UAYEB,
  };

  class Date {
    CalendarHaabDate date;
   public:
    explicit Date(CalandarHaabDate date) : date(date) {}
    explicit Date(MONTH month = MONTH::POP, int16_t day = 1) : date(calendar_haab_create(static_cast<CALENDAR_HAAB_MONTH>(month), day)) {}
    Date(CALENDAR_HAAB_MONTH month, int16_t day) : date(calendar_haab_create(month, day)) {}
    explicit operator CalandarHaabDate() const noexcept { return date; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_haab_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{calendar_haab_from_rd_date(static_cast<CRdDate>(rdDate))}; }
    [[nodiscard]] auto ordinal() const noexcept -> int64_t { return calendar_haab_ordinal(&date); }

    [[nodiscard]] auto month_name() const noexcept -> const char8_t* { return calendar_haab_month_name(static_cast<CALENDAR_HAAB_MONTH>(date.month)); }
    [[nodiscard]] static auto month_names() noexcept -> std::array<const char8_t*, 19>;
  };
}
