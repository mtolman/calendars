#pragma once

#include <calendars/c/calendars/zoroastrian.h>
#include <calendars/cxx/calendars/common.h>
#include <string>

namespace calendars::zoroastrian {
  class Date {
    CalendarZoroastrianDate date;
   public:
    explicit Date(CalendarZoroastrianDate date) : date(date) {}
    explicit Date(int64_t year = 0, int16_t month = 1, int16_t day = 1): date(calendar_zoroastrian_create(year, month, day)) {}
    explicit operator CalendarZoroastrianDate() const noexcept { return date; }

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto month() const noexcept -> int16_t { return date.month; }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_zoroastrian_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_zoroastrian_to_rd_date(&date)}; }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{ calendar_zoroastrian_from_rd_date(static_cast<CRdDate>(rdDate))}; }

    [[nodiscard]] auto month_name() const -> std::u8string {
      return {calendar_zoroastrian_month_name(date.month)};
    }

    [[nodiscard]] auto day_name() const -> std::u8string {
      if (date.month == 13) {
        return { calendar_zoroastrian_epagomanae_day_name(date.day)};
      }
      else {
        return {calendar_zoroastrian_day_name(date.day)};
      }
    }

    [[nodiscard]] auto month_names() const -> std::array<const char8_t*,12>;
    [[nodiscard]] auto day_names() const -> std::array<const char8_t*,30>;
    [[nodiscard]] auto epagomanae_names() const -> std::array<const char8_t*,5>;
  };
}
