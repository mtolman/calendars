#pragma once

#include <calendars/c/calendars/aztec/xihuitl.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::aztec::xihuitl {
  using MONTH = CALENDAR_XIHUITL_MONTH;

  /**
   * Represents an Aztec Xihuitl date.
   */
  class Date {
    CalendarXihuitlDate date;
   public:
    /**
     * Creates an Aztec Xihuitl date from a C Xihuitl date
     * @param date C Xihuitl date
     */
    explicit Date(CalendarXihuitlDate date) : date(date) {}

    /**
     * Creates a new Xihuitl date
     * @param month Xihuitl month
     * @param day Xihuitl day
     */
    explicit Date(MONTH month, int16_t day) : date(calendar_xihuitl_create(month, day)) {}

    /**
     * Casts to a C Xihuitl date
     */
    explicit operator CalendarXihuitlDate() const noexcept { return date; }

    /**
     * @return Xihuitl ordinal
     */
    [[nodiscard]] auto ordinal() const noexcept -> int { return calendar_xihuitl_ordinal(&date); }

    [[nodiscard]] auto on_or_before(const RdDate& rdDate) const noexcept -> RdDate {
      return RdDate{calendar_xihuitl_on_or_before(static_cast<CRdDate>(rdDate), &date)};
    }

    /**
     * Compares Xihuitl dates
     * @param other date to compare
     * @return
     */
    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_xihuitl_compare(&date, &other.date)); }

    /**
     * @return Xihuitl month
     */
    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(date.month); }

    /**
     * @return Xihuitl day
     */
    [[nodiscard]] auto day() const noexcept -> decltype(CalendarXihuitlDate::day) { return date.day; }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    /**
     * Converts from an RdDate
     * @param rdDate RdDate to convert from
     * @return
     */
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) -> Date { return Date{calendar_xihuitl_from_rd_date(static_cast<CRdDate>(rdDate))}; }

    [[nodiscard]] auto month_name() const noexcept -> const char8_t* { return calendar_xihuitl_month_name(static_cast<CALENDAR_XIHUITL_MONTH>(date.month)); }
    [[nodiscard]] static auto month_names() noexcept -> std::array<const char8_t*, 19>;
  };
}
