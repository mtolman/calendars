#pragma once

#include <calendars/c/calendars/aztec/xiuhomolpilli.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::aztec::xiuhomolpilli {
  using NAME = CALENDAR_XIUHOMOLPILLI_NAME;

  class Date {
    CalendarXiuhomolpilliDate date;
   public:

    explicit Date(CalendarXiuhomolpilliDate date) : date(date) {}

    explicit Date(int16_t number, NAME name) : date(calendar_xiuhomolpilli_create(number, name)) {}

    explicit operator CalendarXiuhomolpilliDate() const noexcept { return date; }

    [[nodiscard]] auto name() const noexcept -> NAME { return static_cast<NAME>(date.name); }

    [[nodiscard]] auto number() const noexcept -> int16_t { return date.number; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_xiuhomolpilli_compare(&date, &other.date)); }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto is_valid() const noexcept -> bool { return calendar_xiuhomolpilli_is_valid(&date); }
  };
}
