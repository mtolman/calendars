#pragma once

#include <calendars/c/calendars/aztec/tonalpohualli.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::aztec::tonalpohualli {
  using NAME = CALENDAR_TONALPOHUALLI_NAME;

  /**
   * Represents an Aztec Tonalpohualli date.
   */
  class Date {
    CalendarTonalpohualliDate date;

   public:
    /**
     * Create Tonalpahualli from C Tonalpahualli
     * @param date C Tonalpahualli
     */
    explicit Date(CalendarTonalpohualliDate date) : date(date) {}

    /**
     * Create Tonalpahuali from number and name
     * @param number Tonalpahuali number
     * @param name Tonalpahuali name
     */
    Date(int16_t number, NAME name) : date(calendar_tonalpohualli_create(number, name)) {}

    /**
     * Casts to a C Tonalpohualli date
     * @return
     */
    explicit operator CalendarTonalpohualliDate() const noexcept { return date; }

    /**
     * @return Tonalpohualli number
     */
    [[nodiscard]] auto number() const noexcept -> decltype(CalendarTonalpohualliDate::number) { return date.number; }

    /**
     * @return Tonalpohualli name
     */
    [[nodiscard]] auto name() const noexcept -> NAME { return static_cast<NAME>(date.name); }

    /**
     * @return Tonalpahuali ordinal
     */
    [[nodiscard]] auto ordinal() const noexcept -> int { return calendar_tonalpohualli_ordinal(&date); }

    /**
     * Compares Tonalpahuali dates
     * @param other Date to compare
     * @return
     */
    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return calendars::impl::compare_int(calendar_tonalpohualli_compare(&date, &other.date)); }

    CALENDAR_CXX_COMPARISON_OPS(Date);

    /**
     * Create Tonalpahuali from an RdDate
     * @param date RdDate to convert from
     * @return
     */
    [[nodiscard]] static auto from_rd_date(const RdDate& date) noexcept -> Date { return Date{ calendar_tonalpohualli_from_rd_date(static_cast<CRdDate>(date))}; }

    [[nodiscard]] auto name_str() const noexcept -> const char8_t* { return calendar_tonalpohualli_name(static_cast<CALENDAR_TONALPOHUALLI_NAME>(date.name)); };

    [[nodiscard]] static auto name_strs() noexcept -> std::array<const char8_t*, 20>;
  };
}
