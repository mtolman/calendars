#pragma once

#include <calendars/c/calendars/julian_day.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::julian_day {
  class Date {
    CalendarJulianDay date;
   public:
    explicit Date(CalendarJulianDay date) : date(date) {}
    explicit Date(double datetime = 0.0) : date(calendar_julian_day_create(datetime)) {}
    explicit operator CalendarJulianDay() const noexcept { return date; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_julian_day_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)
    [[nodiscard]] auto to_moment() const noexcept -> Moment { return Moment{ calendar_julian_day_to_moment(&date)}; }
    [[nodiscard]] static auto from_moment(const Moment& moment) noexcept -> Date { return Date{ calendar_julian_day_from_moment(static_cast<CalendarMoment>(moment))}; }
    [[nodiscard]] auto datetime() const noexcept -> double { return date.datetime; }
  };
}
