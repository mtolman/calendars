#pragma once

#include <calendars/c/calendars/holidays/armenian_church.h>
#include <calendars/cxx/calendars.h>

namespace calendars::holidays::armenian_church {
  inline auto christmas_jerusalem(int64_t julianYear) -> julian::Date {
    return julian::Date{calendar_holidays_armenian_church_christmas_jerusalem(julianYear)};
  }
  inline auto christmas(int64_t gregorianYear) -> gregorian::Date {
    return gregorian::Date{calendar_holidays_armenian_church_christmas(gregorianYear)};
  }
}  // namespace calendars::holidays::armenian_church
