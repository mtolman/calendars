#pragma once

#include <calendars/c/calendars/holidays/eastern_orthodox.h>
#include <calendars/cxx/calendars/julian.h>

namespace calendars::holidays::eastern_orthodox {
  inline calendars::julian::Date christmas(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_christmas(julianYear)}; }
  inline calendars::julian::Date nativity_of_the_virgin_mary(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_nativity_of_the_virgin_mary(julianYear)}; }
  inline calendars::julian::Date elevation_of_the_life_giving_cross(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_elevation_of_the_life_giving_cross(julianYear)}; }
  inline calendars::julian::Date presentation_of_the_virgin_mary_in_the_temple(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_presentation_of_the_virgin_mary_in_the_temple(julianYear)}; }
  inline calendars::julian::Date theophany(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_theophany(julianYear)}; }
  inline calendars::julian::Date presentation_of_christ_in_the_temple(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_presentation_of_christ_in_the_temple(julianYear)}; }
  inline calendars::julian::Date the_annunciation(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_the_annunciation(julianYear)}; }
  inline calendars::julian::Date the_transfiguration(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_the_transfiguration(julianYear)}; }
  inline calendars::julian::Date the_repose_of_the_virgin_mary(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_the_repose_of_the_virgin_mary(julianYear)}; }
  inline calendars::julian::Date easter(int64_t julianYear) { return calendars::julian::Date{calendar_holidays_eastern_orthodox_easter(julianYear)}; }

  std::array<calendars::julian::Date, 14> the_fast_of_the_repose_of_the_virgin_mary(int64_t julianYear);

  std::array<calendars::julian::Date, 40> the_40_day_christmas_fast(int64_t julianYear);
}
