#pragma once

#include <calendars/c/calendars/holidays/hebrew.h>
#include <calendars/cxx/calendars/hebrew.h>
#include <array>
#include <optional>

namespace calendars::holidays::hebrew {
  inline calendars::hebrew::Date yom_kippur(int64_t hebrewYear) {
    return calendars::hebrew::Date { calendar_holidays_hebrew_yom_kippur(hebrewYear) };
  }

  inline calendars::hebrew::Date rosh_ha_shanah(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_rosh_ha_shanah(hebrewYear)};
  }

  inline calendars::hebrew::Date sukkot(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_sukkot(hebrewYear)};
  }

  inline calendars::hebrew::Date hoshana_rabba(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_hoshana_rabba(hebrewYear)};
  }

  inline calendars::hebrew::Date shemini_azeret(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_shemini_azeret(hebrewYear)};
  }

  inline calendars::hebrew::Date simhat_torah(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_simhat_torah(hebrewYear)};
  }

  inline calendars::hebrew::Date passover(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_passover(hebrewYear)};
  }

  inline calendars::hebrew::Date passover_end(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_passover_end(hebrewYear)};
  }

  inline calendars::hebrew::Date shavout(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_shavout(hebrewYear)};
  }

  std::array<calendars::hebrew::Date, 6> sukkot_intermediate_days(int64_t hebrewYear);

  std::array<calendars::hebrew::Date, 5> passover_days(int64_t hebrewYear);

  std::array<calendars::hebrew::Date, 8> hanukkah(int64_t hebrewYear);

  inline calendars::hebrew::Date tu_b_shevat(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_tu_b_shevat(hebrewYear)};
  }

  inline calendars::hebrew::Date purim(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_purim(hebrewYear)};
  }

  inline calendars::hebrew::Date ta_anit_esther(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_ta_anit_esther(hebrewYear)};
  }

  inline calendars::hebrew::Date tishah_be_av(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_tishah_be_av(hebrewYear)};
  }

  inline calendars::hebrew::Date tzom_gedaliah(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_tzom_gedaliah(hebrewYear)};
  }

  inline calendars::hebrew::Date tzom_tammuz(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_tzom_tammuz(hebrewYear)};
  }

  inline calendars::hebrew::Date yom_ha_shoah(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_yom_ha_shoah(hebrewYear)};
  }

  inline calendars::hebrew::Date yom_ha_zikkaron(int64_t hebrewYear) {
    return calendars::hebrew::Date{calendar_holidays_hebrew_yom_ha_zikkaron(hebrewYear)};
  }

  inline calendars::hebrew::Date birthday(const calendars::hebrew::Date &birthDate, int64_t hebrewYear) {
    const auto c = static_cast<CalendarHebrewDate>(birthDate);
    return calendars::hebrew::Date{calendar_holidays_hebrew_birthday(&c, hebrewYear)};
  }

  inline calendars::hebrew::Date yahrzeit(const calendars::hebrew::Date &deathDate, int64_t hebrewYear) {
    const auto c = static_cast<CalendarHebrewDate>(deathDate);
    return calendars::hebrew::Date{calendar_holidays_hebrew_yahrzeit(&c, hebrewYear)};
  }

  inline std::optional<int64_t> omer(const calendars::RdDate& date) {
    const auto cHebrew = calendar_holidays_hebrew_passover(date.to<calendars::hebrew::Date>().year());
    auto       c       = static_cast<CalendarRdDate>(date.sub_days(calendar_hebrew_to_rd_date(&cHebrew)));
    if (1 <= c && c <= 49) {
      return static_cast<int64_t>(calendars::math::mod(std::floor(static_cast<double>(c) / 7.0), 7));
    }
    return std::nullopt;
  }
}

