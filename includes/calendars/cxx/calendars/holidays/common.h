#pragma once

#include <calendars/cxx/calendars/traits.h>
#include <calendars/cxx/calendars/gregorian.h>
#include <vector>
#include <functional>
#include <cinttypes>

namespace calendars::holidays {
  template<typename T>
  using HolidayCreatorFromYear = T (*)(int64_t year);
  template<typename T, size_t N>
  using FixedHolidaysCreatorFromYear = std::array<T, N> (*)(int64_t year);

  /**
   *
   * @tparam T
   * @param callable
   * @param gregorianYear
   * @return
   */
  template<typename T, typename Allocator = std::allocator<gregorian::Date>>
  [[nodiscard]] auto holiday_lambda_in_gregorian_year(std::function<T (int64_t)> callable, int64_t gregorianYear) -> std::vector<gregorian::Date, Allocator> requires traits::YearReadable<T> && traits::ToRdDate<T> {
    auto res = std::vector<gregorian::Date, Allocator>{};
    auto [startGreg, endGreg] = gregorian::Date::year_boundaries(gregorianYear);
    auto startRd = startGreg.to_rd_date();
    auto endRd = endGreg.to_rd_date();
    auto startTargetYear = startRd.template to<T>().year();
    auto endTargetYear = endRd.template to<T>().year();
    for (auto curYear = startTargetYear; curYear <= endTargetYear; ++curYear) {
      auto targetDate = callable(curYear).to_rd_date();
      if (targetDate >= startRd && targetDate <= endRd) {
        res.emplace_back(targetDate.template to<gregorian::Date>());
      }
    }
    return res;
  }

  /**
   * Takes any function pointer for creating a holiday in a calendaring system's year and returns the equivalent holidays in a gregorian year
   * @tparam T Auto-inferrable. The return date type from the holiday. Determines the calendaring system's "year" (e.g. a JulianDate => Julian year, an ArmenianDate => Armenian year)
   * @param callable Method to determine holiday from year
   * @param gregorianYear Gregorian year to get all occurrences from
   * @return
   */
  template<typename T, typename Allocator = std::allocator<gregorian::Date>>
  [[nodiscard]] auto holiday_in_gregorian_year(HolidayCreatorFromYear<T> callable, int64_t gregorianYear) -> std::vector<gregorian::Date, Allocator> requires traits::YearReadable<T> && traits::ToRdDate<T> {
    return holiday_lambda_in_gregorian_year<T>(callable, gregorianYear);
  }

  /**
   *
   * @tparam T
   * @param callable
   * @param gregorianYear
   * @return
   */
  template<typename T, size_t N, typename Allocator = std::allocator<gregorian::Date>>
  [[nodiscard]] auto holidays_lambda_in_gregorian_year(std::function<std::array<T, N> (int64_t)> callable, int64_t gregorianYear) -> std::vector<gregorian::Date, Allocator> requires traits::YearReadable<T> && traits::ToRdDate<T> {
    auto res = std::vector<gregorian::Date, Allocator>{};
    auto [startGreg, endGreg] = gregorian::Date::year_boundaries(gregorianYear);
    auto startRd = startGreg.to_rd_date();
    auto endRd = endGreg.to_rd_date();
    auto startTargetYear = startRd.template to<T>().year();
    auto endTargetYear = endRd.template to<T>().year();
    for (auto curYear = startTargetYear; curYear <= endTargetYear; ++curYear) {
      auto targetDates = callable(curYear);
      for(const auto& targetResDate : targetDates) {
        const auto targetDate = targetResDate.to_rd_date();
        if (targetDate >= startRd && targetDate <= endRd) {
          res.emplace_back(targetDate.template to<gregorian::Date>());
        }
      }
    }
    res.shrink_to_fit();
    return res;
  }

  /**
   * Takes any function pointer for creating a holiday in a calendaring system's year and returns the equivalent holidays in a gregorian year
   * @tparam T Auto-inferrable. The return date type from the holiday. Determines the calendaring system's "year" (e.g. a JulianDate => Julian year, an ArmenianDate => Armenian year)
   * @param callable Method to determine holiday from year
   * @param gregorianYear Gregorian year to get all occurrences from
   * @return
   */
  template<typename T, size_t N, typename Allocator = std::allocator<gregorian::Date>>
  [[nodiscard]] auto holidays_in_gregorian_year(FixedHolidaysCreatorFromYear<T, N> callable, int64_t gregorianYear) -> std::vector<gregorian::Date, Allocator> requires traits::YearReadable<T> && traits::ToRdDate<T> {
    return holidays_lambda_in_gregorian_year<T, N>(callable, gregorianYear);
  }
}
