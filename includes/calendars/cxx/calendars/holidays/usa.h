#pragma once

#include <calendars/c/calendars/holidays/usa.h>
#include <calendars/cxx/calendars/gregorian.h>

namespace calendars::holidays::usa {
  inline auto independence_day(int64_t gregorianYear) -> gregorian::Date {
    return gregorian::Date{calendar_holidays_usa_independence_day(gregorianYear)};
  }
  inline auto labor_day(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_labor_day(gregorianYear)}; }
  inline auto memorial_day(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_memorial_day(gregorianYear)}; }
  inline auto election_day(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_election_day(gregorianYear)}; }
  inline auto thanksgiving(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_thanksgiving(gregorianYear)}; }
  inline auto black_friday(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_black_friday(gregorianYear)}; }
  inline auto new_years(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_new_years(gregorianYear)}; }
  inline auto new_years_eve(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_new_years_eve(gregorianYear)}; }
  inline auto presidents_day(int64_t gregorianYear) -> gregorian::Date {
    return gregorian::Date{calendar_holidays_usa_presidents_day(gregorianYear)};
  }
  inline auto martin_luther_king_jr_day(int64_t gregorianYear) -> gregorian::Date {
    return gregorian::Date{calendar_holidays_usa_martin_luther_king_jr_day(gregorianYear)};
  }
  inline auto juneteenth(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_juneteenth(gregorianYear)}; }
  inline auto columbus_day(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_columbus_day(gregorianYear)}; }
  inline auto indigenous_peoples_day(int64_t gregorianYear) -> gregorian::Date {
    return gregorian::Date{calendar_holidays_usa_indigenous_peoples_day(gregorianYear)};
  }
  inline auto veterans_day(int64_t gregorianYear) -> gregorian::Date { return gregorian::Date{calendar_holidays_usa_veterans_day(gregorianYear)}; }
}  // namespace calendars::holidays::usa
