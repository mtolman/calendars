#pragma once

#include <calendars/cxx/calendars/coptic.h>
#include <calendars/c/calendars/holidays/copts.h>

namespace calendars::holidays::copts {
  inline auto christmas(int64_t copticYear) -> calendars::coptic::Date { return calendars::coptic::Date{calendar_holidays_copts_christmas(copticYear)}; }
  inline auto building_of_the_cross(int64_t copticYear) -> calendars::coptic::Date { return calendars::coptic::Date{calendar_holidays_copts_building_of_the_cross(copticYear)}; }
  inline auto jesus_circumcision(int64_t copticYear) -> calendars::coptic::Date { return calendars::coptic::Date{calendar_holidays_copts_jesus_circumcision(copticYear)}; }
  inline auto epiphany(int64_t copticYear) -> calendars::coptic::Date { return calendars::coptic::Date{calendar_holidays_copts_epiphany(copticYear)}; }
  inline auto marys_announcement(int64_t copticYear) -> calendars::coptic::Date { return calendars::coptic::Date{calendar_holidays_copts_marys_announcement(copticYear)}; }
  inline auto jesus_transfiguration(int64_t copticYear) -> calendars::coptic::Date { return calendars::coptic::Date{calendar_holidays_copts_jesus_transfiguration(copticYear)}; }
}
