#pragma once

#include <calendars/cxx/calendars/common.h>
#include <array>

namespace calendars::holidays::christian {
  auto christmas(int64_t gregorianYear) -> gregorian::Date;

  auto christmas_eve(int64_t gregorianYear) -> gregorian::Date;

  auto advent_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto epiphany(int64_t gregorianYear) -> gregorian::Date;

  auto easter(int64_t gregorianYear) -> gregorian::Date;

  auto septuagesima_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto sexagesima_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto shrove_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto shrove_monday(int64_t gregorianYear) -> gregorian::Date;

  auto shrove_tuesday(int64_t gregorianYear) -> gregorian::Date;

  auto mardi_gras(int64_t gregorianYear) -> gregorian::Date;

  auto ash_wednesday(int64_t gregorianYear) -> gregorian::Date;

  auto passion_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto palm_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto holy_thursday(int64_t gregorianYear) -> gregorian::Date;

  auto maundy_thursday(int64_t gregorianYear) -> gregorian::Date;

  auto good_friday(int64_t gregorianYear) -> gregorian::Date;

  auto rogation_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto ascension_day(int64_t gregorianYear) -> gregorian::Date;

  auto pentecost(int64_t gregorianYear) -> gregorian::Date;

  auto whitsunday(int64_t gregorianYear) -> gregorian::Date;

  auto whit_monday(int64_t gregorianYear) -> gregorian::Date;

  auto trinity_sunday(int64_t gregorianYear) -> gregorian::Date;

  auto corpus_christi(int64_t gregorianYear) -> gregorian::Date;

  auto corpus_christi_us_catholic(int64_t gregorianYear) -> gregorian::Date;

  auto the_40_days_of_lent(int64_t gregorianYear) -> std::array<gregorian::Date, 40>;
}
