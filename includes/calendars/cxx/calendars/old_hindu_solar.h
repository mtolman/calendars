#pragma once

#include <calendars/c/calendars/old_hindu_solar.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::old_hindu_solar {
  enum class SAURA {
    ARIES,
    TAURUS,
    GEMINI,
    CANCER,
    LEO,
    VIRGO,
    LIBRA,
    SCORPIO,
    SAGITTARIUS,
    CAPRICORN,
    AQUARIUS,
    PISCES
  };

  class Date {
    CalendarOldHinduSolarDate date;
   public:
    explicit Date(CalendarOldHinduSolarDate date) : date(date) {}
    explicit Date(int64_t year = 1, SAURA saura = SAURA::ARIES, int16_t day = 1) : Date(year, static_cast<CALENDAR_OLD_HINDU_SOLAR_SAURA>(saura), day) {}
    Date(int64_t year, CALENDAR_OLD_HINDU_SOLAR_SAURA saura, int16_t day) : date(calendar_old_hindu_solar_create(year, saura, day)) {}

    explicit operator CalendarOldHinduSolarDate() const noexcept { return date; }

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto saura() const noexcept -> SAURA { return static_cast<SAURA>(date.saura); }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_old_hindu_solar_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{ calendar_old_hindu_solar_to_rd_date(&date)}; }

    [[nodiscard]] static auto jovian_year(const RdDate& date) noexcept -> int64_t { return calendar_old_hindu_solar_jovian_year(static_cast<CRdDate>(date)); }
    [[nodiscard]] static auto day_count(const RdDate& date) noexcept -> int64_t { return calendar_old_hindu_solar_day_count(static_cast<CRdDate>(date)); }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{ calendar_old_hindu_solar_from_rd_date(static_cast<CRdDate>(rdDate))}; }
  };
}
