#pragma once

#include <calendars/cxx/math/common.h>
#include <calendars/cxx/math/numbers.h>

#include "common.h"
#include "comp_cyclic.h"

namespace calendars::cyclic {
  namespace single_cycle {
    using Type = comp_cyclic::single_cycle::Type;

    class Date;

    struct DateTemplate {
      const Type         type;
      const int64_t      epoch;
      const math::pnum_t Y;
      const math::pnum_t M;
      const math::pnum_t deltaY;
      const math::pnum_t deltaM;

      DateTemplate(Type type, int64_t epoch, math::pnum_t Y, math::pnum_t M, math::pnum_t deltaY = math::int_to_pnum(0), math::pnum_t deltaM = math::int_to_pnum(0))
        : type(type), epoch(epoch), Y(std::move(Y)), M(std::move(M)), deltaY(std::move(deltaY)), deltaM(std::move(deltaM)) {}

      [[nodiscard]] auto operator<=>(const DateTemplate& o) const noexcept -> std::strong_ordering;

      CALENDAR_CXX_COMPARISON_OPS(DateTemplate)

      auto make(int64_t year, int32_t month, int32_t day) -> Date;

      friend auto operator<<(std::ostream& os, const DateTemplate& d) -> std::ostream& {
        os << "SingleCycleTemplate<" << static_cast<int>(d.type) << ", " << d.epoch << ", " << d.Y << ", " << d.M << ", " << d.deltaY << ", " << d.deltaM << ">";
        return os;
      }
    };

    class Date {
      Type         type;
      int64_t      epoch;
      math::pnum_t Y;
      math::pnum_t M;
      math::pnum_t deltaY;
      math::pnum_t deltaM;

      int64_t mYear;
      int32_t mMonth;
      int32_t mDay;

     public:
      Date() : type(Type::AT_OR_BEFORE_MEAN_MONTH), epoch(0), Y(12), M(30), deltaY(0), deltaM(0), mYear(0), mMonth(0), mDay(0) {}
      Date(int64_t      year,
           int32_t      month,
           int32_t      day,
           const DateTemplate& dateTemplate)
          : type(dateTemplate.type)
          , epoch(dateTemplate.epoch)
          , Y(dateTemplate.Y)
          , M(dateTemplate.M)
          , deltaY(dateTemplate.deltaY)
          , deltaM(dateTemplate.deltaM)
          , mYear(year)
          , mMonth(month)
          , mDay(day) {}

      Date(int64_t year, int32_t month, int32_t day, Type type, int64_t epoch, const math::pnum_t& Y, const math::pnum_t& M, const math::pnum_t& deltaY = math::int_to_pnum(0), const math::pnum_t& deltaM = math::int_to_pnum(0))
      : type(type), epoch(epoch), Y(Y), M(M), deltaY(deltaY), deltaM(deltaM), mYear(year), mMonth(month), mDay(day)
      {}

      auto with(int64_t year, int32_t month, int32_t day) const -> Date {
        return Date{year, month, day, dateTemplate()};
      }

      auto dateTemplate() const -> DateTemplate {
        return DateTemplate{type, epoch, Y, M, deltaY, deltaM};
      }

      auto year() const noexcept { return mYear; }

      auto month() const noexcept { return mMonth; }

      auto day() const noexcept { return mDay; }

      [[nodiscard]] auto to_rd_date() const noexcept -> RdDate;

      static auto from_rd_date(const calendars::RdDate& date, const DateTemplate& dateTemplate) noexcept -> Date;

      static auto from_rd_date(const calendars::RdDate& date, Type type, int64_t epoch, const math::pnum_t& Y, const math::pnum_t& M, const math::pnum_t& deltaY = math::int_to_pnum(0), const math::pnum_t& deltaM = math::int_to_pnum(0)) -> Date;

      [[nodiscard]] auto operator<=>(const Date& o) const noexcept -> std::strong_ordering;

      CALENDAR_CXX_COMPARISON_OPS(Date)

      friend auto operator<<(std::ostream& os, const Date& d) -> std::ostream& {
        os << "SingleCycle<" << d.year() << "-" << d.month() << "-" << d.day() << ", " << d.dateTemplate() << ">";
        return os;
      }
    };
  }  // namespace single_cycle

  namespace double_cycle {
    using Type = comp_cyclic::double_cycle::Type;
  }
}  // namespace calendars::cyclic
