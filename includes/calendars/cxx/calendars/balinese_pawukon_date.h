#pragma once

#include <calendars/c/calendars/balinese_pawukon_date.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::balinese_pawukon {

  enum class DWIWARA : int8_t {
    MENGA = 1, PEPET
  };
  enum class TRIWARA : int8_t {
    PASAH = 1, BETENG, KAJENG
  };
  enum class CATURWARA : int8_t {
    SRI = 1, LABA, JAYA, MENALA
  };
  enum class PANCAWARA : int8_t {
    UMANIS = 1, PAING, PON, WAGE, KELIWON
  };
  enum class SADWARA : int8_t {
    TUNGLEH = 1, ARYANG, URUKUNG, PANIRON, WAS, MAULU
  };
  enum class SAPTAWARA : int8_t {
    REDITE = 1, COMA, ANGGARA, BUDA, WRASPATI, SUKRA, SANISCARA
  };
  enum class ASATAWARA : int8_t {
    SRI = 1, INDRA, GURU, YAMA, LUDRA, BRAHMA, KALA, UMA
  };
  enum class SANGAWARA : int8_t {
    DANGU = 1, JANGUR, GIGIS, NOHAN, OGAN, ERANGAN, URUNGAN, TULUS, DADI
  };
  enum class DASAWARA : int8_t {
    PANDITA = 1, PATI, SUKA, DUKA, SRI, MANUH, MANUSA, RAJA, DEWA, RAKSASA = 0
  };

  enum class EKAWARA: int8_t {
    LUANG = 1
  };

  class Date {
    CalendarBalinesePawukonDate date;
   public:
    explicit Date(const CalendarBalinesePawukonDate& date) : date(date) {}

    explicit Date(bool luang, DWIWARA dwiwara, TRIWARA triwara, CATURWARA caturwara, PANCAWARA pancawara, SADWARA sadwara, SAPTAWARA saptawara, ASATAWARA asatawara, SANGAWARA sangawara, DASAWARA dasawara)
      : Date(
            luang,
            static_cast<CALENDAR_BALINESE_PAWUKON_DWIWARA>(dwiwara),
            static_cast<CALENDAR_BALINESE_PAWUKON_TRIWARA>(triwara),
            static_cast<CALENDAR_BALINESE_PAWUKON_CATURWARA>(caturwara),
            static_cast<CALENDAR_BALINESE_PAWUKON_PANCAWARA>(pancawara),
            static_cast<CALENDAR_BALINESE_PAWUKON_SADWARA>(sadwara),
            static_cast<CALENDAR_BALINESE_PAWUKON_SAPTAWARA>(saptawara),
            static_cast<CALENDAR_BALINESE_PAWUKON_ASATAWARA>(asatawara),
            static_cast<CALENDAR_BALINESE_PAWUKON_SANGAWARA>(sangawara),
            static_cast<CALENDAR_BALINESE_PAWUKON_DASAWARA>(dasawara)
        ) {}

    Date(
        bool luang,
         CALENDAR_BALINESE_PAWUKON_DWIWARA dwiwara,
         CALENDAR_BALINESE_PAWUKON_TRIWARA triwara,
         CALENDAR_BALINESE_PAWUKON_CATURWARA caturwara,
         CALENDAR_BALINESE_PAWUKON_PANCAWARA pancawara,
         CALENDAR_BALINESE_PAWUKON_SADWARA   sadwara,
         CALENDAR_BALINESE_PAWUKON_SAPTAWARA saptawara,
         CALENDAR_BALINESE_PAWUKON_ASATAWARA asatawara,
         CALENDAR_BALINESE_PAWUKON_SANGAWARA sangawara,
         CALENDAR_BALINESE_PAWUKON_DASAWARA  dasawara
        ) : date(calendar_balinese_pawukon_create(luang, dwiwara, triwara, caturwara, pancawara, sadwara, saptawara, asatawara, sangawara, dasawara)) {}

    explicit operator CalendarBalinesePawukonDate() const noexcept { return date; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_balinese_pawukon_compare(&date, &other.date)); }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto on_or_before(const RdDate& rdDate) const noexcept -> RdDate { return RdDate{ calendar_balinese_pawukon_on_or_before(static_cast<CRdDate>(rdDate), &date)}; }

    [[nodiscard]] auto is_luang() const noexcept -> bool { return date.ekawara; }
    [[nodiscard]] auto ekawara() const noexcept -> bool { return date.ekawara; }
    [[nodiscard]] auto dwiwara() const noexcept -> DWIWARA { return static_cast<DWIWARA>(date.dwiwara); }
    [[nodiscard]] auto triwara() const noexcept -> TRIWARA { return static_cast<TRIWARA>(date.triwara); }
    [[nodiscard]] auto caturwara() const noexcept -> CATURWARA { return static_cast<CATURWARA>(date.caturwara); }
    [[nodiscard]] auto pancawara() const noexcept -> PANCAWARA { return static_cast<PANCAWARA>(date.pancawara); }
    [[nodiscard]] auto sadwara() const noexcept -> SADWARA { return static_cast<SADWARA>(date.sadwara); }
    [[nodiscard]] auto saptawara() const noexcept -> SAPTAWARA { return static_cast<SAPTAWARA>(date.saptawara); }
    [[nodiscard]] auto asatawara() const noexcept -> ASATAWARA { return static_cast<ASATAWARA>(date.asatawara); }
    [[nodiscard]] auto sangawara() const noexcept -> SANGAWARA { return static_cast<SANGAWARA>(date.sangawara); }
    [[nodiscard]] auto dasawara() const noexcept -> DASAWARA { return static_cast<DASAWARA>(date.dasawara); }

    [[nodiscard]] auto ekawara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_ekawara_name(date.ekawara); }
    [[nodiscard]] auto dwiwara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_dwiwara_name(static_cast<CALENDAR_BALINESE_PAWUKON_DWIWARA>(date.dwiwara)); }
    [[nodiscard]] auto triwara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_triwara_name(static_cast<CALENDAR_BALINESE_PAWUKON_TRIWARA>(date.triwara)); }
    [[nodiscard]] auto caturwara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_caturwara_name(static_cast<CALENDAR_BALINESE_PAWUKON_CATURWARA>(date.caturwara)); }
    [[nodiscard]] auto pancawara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_pancawara_name(static_cast<CALENDAR_BALINESE_PAWUKON_PANCAWARA>(date.pancawara)); }
    [[nodiscard]] auto sadwara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_sadwara_name(static_cast<CALENDAR_BALINESE_PAWUKON_SADWARA>(date.sadwara)); }
    [[nodiscard]] auto saptawara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_saptawara_name(static_cast<CALENDAR_BALINESE_PAWUKON_SAPTAWARA>(date.saptawara)); }
    [[nodiscard]] auto asatawara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_asatawara_name(static_cast<CALENDAR_BALINESE_PAWUKON_ASATAWARA>(date.asatawara)); }
    [[nodiscard]] auto sangawara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_sangawara_name(static_cast<CALENDAR_BALINESE_PAWUKON_SANGAWARA>(date.sangawara)); }
    [[nodiscard]] auto dasawara_cycle() const noexcept -> const char8_t* { return calendar_balinese_pawukon_cycle_dasawara_name(static_cast<CALENDAR_BALINESE_PAWUKON_DASAWARA>(date.dasawara)); }

    [[nodiscard]] static auto ekawara_cycle_names() noexcept -> std::array<const char8_t*, 1>;
    [[nodiscard]] static auto dwiwara_cycle_names() noexcept -> std::array<const char8_t*, 2>;
    [[nodiscard]] static auto triwara_cycle_names() noexcept -> std::array<const char8_t*, 3>;
    [[nodiscard]] static auto caturwara_cycle_names() noexcept -> std::array<const char8_t*, 4>;
    [[nodiscard]] static auto pancawara_cycle_names() noexcept -> std::array<const char8_t*, 5>;
    [[nodiscard]] static auto sadwara_cycle_names() noexcept -> std::array<const char8_t*, 6>;
    [[nodiscard]] static auto saptawara_cycle_names() noexcept -> std::array<const char8_t*, 7>;
    [[nodiscard]] static auto asatawara_cycle_names() noexcept -> std::array<const char8_t*, 8>;
    [[nodiscard]] static auto sangawara_cycle_names() noexcept -> std::array<const char8_t*, 9>;
    [[nodiscard]] static auto dasawara_cycle_names() noexcept -> std::array<const char8_t*, 10>;

    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) -> Date { return Date{calendar_balinese_pawukon_from_rd_date(static_cast<CRdDate>(rdDate))}; }
  };
}
