#pragma once

#include <calendars/c/calendars/modified_julian_day.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::modified_julian_day {
  class Date {
    CalendarModifiedJulianDay date;

   public:
    explicit Date(CalendarModifiedJulianDay date) : date(date) {}
    explicit Date(double datetime = 0) : date(calendar_modified_julian_day_create(datetime)) {}
    explicit operator CalendarModifiedJulianDay() const noexcept { return date; }

    [[nodiscard]] auto datetime() const noexcept { return date.datetime; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_modified_julian_day_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto to_moment() const noexcept -> Moment { return Moment{calendar_modified_julian_day_to_moment(&date)}; }
    [[nodiscard]] static auto from_moment(const Moment& moment) -> Date { return Date{ calendar_modified_julian_day_from_moment(static_cast<CalendarMoment>(moment))}; }
  };
}
