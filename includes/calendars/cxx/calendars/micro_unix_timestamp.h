#pragma once

#include <calendars/c/calendars/micro_unix_timestamp.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::micro_unix_timestamp {
  class Date {
    CalendarMicroUnixTimestamp date;
   public:
    explicit Date(CalendarMicroUnixTimestamp date) : date(date) {}
    explicit Date(double timestamp = 0) : Date(calendar_micro_unix_timestamp_create(timestamp)) {}
    explicit operator CalendarMicroUnixTimestamp() const noexcept { return date; }

    [[nodiscard]] auto timestamp() const noexcept -> double { return date.timestamp; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_micro_unix_timestamp_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto to_moment() const noexcept -> Moment { return Moment{ calendar_micro_unix_timestamp_to_moment(&date)}; }
    [[nodiscard]] static auto from_moment(const Moment& moment) noexcept -> Date { return Date{ calendar_micro_unix_timestamp_from_moment(static_cast<CalendarMoment>(moment))}; }
  };
}
