#pragma once

#include <calendars/c/calendars/armenian.h>
#include "common.h"

namespace calendars::armenian {
  /**
   * Represents an ancient Armenian date
   */
  class Date {
    CalendarArmenianDate date;
   public:
    /**
     * Creates a new armenian date from a C armenian date
     * @param date
     */
    explicit Date(CalendarArmenianDate date) : date(date) {}

    /**
     * Creates a new armenian date from a year, month, and day
     * @param year Armenian calendar year
     * @param month Armenian calendar month (1-13)
     * @param day Armenian calendar day (1-30)
     */
    explicit Date(int64_t year = 0, int16_t month = 1, int16_t day = 1) : date(calendar_armenian_create(year, month, day)) {}

    /**
     * Converts to a C Armenian date
     * @return
     */
    explicit operator CalendarArmenianDate() const noexcept { return date; }

    /**=
     * @return Whether the current date is a valid Armenian date
     */
    [[nodiscard]] auto is_valid() const noexcept -> bool { return calendar_armenian_is_valid(&date); }

    /**
     * @return The closest valid armenian date. If the date was already valid, a copy of the date is returned
     */
    [[nodiscard]] auto nearest_valid() const noexcept -> Date { return Date{calendar_armenian_nearest_valid(&date)}; }

    /**
     * Does a three way comparison on an armenian date
     * @param other Date to compare against
     * @return
     */
    auto               operator<=>(const Date& other) const noexcept -> std::strong_ordering {
      return impl::compare_int(calendar_armenian_compare(&date, &other.date));
    }

    /**
     * Converts an RdDate to an Armenian date
     * @param rdDate RdDate to convert
     */
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date {
      return Date{calendar_armenian_from_rd_date(static_cast<CRdDate>(rdDate))};
    }

    /**
     * Converts the current Armenian to an RdDate date
     */
    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_armenian_to_rd_date(&date)}; }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    /**
     * @return Armenian year of date
     */
    [[nodiscard]] auto year() const noexcept -> decltype(CalendarArmenianDate::year) { return date.year; }

    /**
     * @return Armenian month of date
     */
    [[nodiscard]] auto month() const noexcept -> decltype(CalendarArmenianDate::month) { return date.month; }

    /**
     * @return Armenian day of date
     */
    [[nodiscard]] auto day() const noexcept -> decltype(CalendarArmenianDate::day) { return date.day; }

    [[nodiscard]] auto month_name() const noexcept -> const char8_t* { return calendar_armenian_month_name(date.month); }
    [[nodiscard]] static auto month_names() noexcept -> std::array<const char8_t*, 12>;
  };
}
