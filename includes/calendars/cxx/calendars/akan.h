#pragma once

#include <calendars/c/calendars/akan.h>
#include <calendars/cxx/calendars/common.h>
#include <string>

namespace calendars::akan {
  /**
   * Represents an Akan Day
   * Unlike Gregorian days which have a single 7-day cycle,
   * Akan days have a dual cycle with the first cycle being 6 long and the second being 7 long.
   * When multiplied together, it gives us a 42-length total cycle for akan days.
   */
  class Date {
    CalendarAkanDate date;

   public:
    /**
     * Creates an AkanDay from a C AkanDay
     * @param dayCount C Akan day
     */
    explicit Date(CalendarAkanDate dayCount) : date(dayCount) {}

    explicit operator CalendarAkanDate() const noexcept { return date; }

    /**
     * Creates a new AkanDay from a prefix and stem
     * @param prefix Akan prefix
     * @param stem Akan stem
     */
    explicit Date(int prefix = 0, int stem = 0) : date(calendar_akan_day_count_create(prefix, stem)) {}

    /**
     * Compares two akan days
     * @param other Akan day count to check against
     * @return
     */
    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_akan_day_count_compare(&date, &other.date)); }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    /**
     * Gets the difference between two Akan days
     * @param other Day to get difference of
     * @return
     */
    [[nodiscard]] auto operator-(const Date& other) const noexcept -> int { return calendar_akan_day_count_difference(&date, &other.date); }

    /**
     * Converts an RdDate to an Akan day count
     * @param rdDate RdDate to convert
     * @return
     */
    [[nodiscard]] static Date from_rd_date(const RdDate& rdDate) noexcept { return Date{calendar_akan_day_count_from_rd_date(static_cast<CRdDate>(rdDate))}; }

    /**
     * Gets the Akan prefix
     */
    [[nodiscard]] auto prefix() const noexcept -> int { return date.prefix; }

    /**
     * Gets the Akan stem
     */
    [[nodiscard]] auto stem() const noexcept -> int { return date.stem; }

    /**
     * Returns an RdDate on or before the provided RdDate with the current Akan day count
     * @param rdDate RdDate acting as a reference point
     * @return
     */
    [[nodiscard]] auto rd_date_on_or_before(RdDate rdDate) -> RdDate { return RdDate{calendar_akan_day_count_on_or_before(static_cast<CalendarRdDate>(rdDate), &date)}; }

    [[nodiscard]] auto name() const -> std::u8string {
      return std::u8string{calendar_akan_prefix_name(date.prefix)} + calendar_akan_stem_name(date.stem);
    }

    [[nodiscard]] static auto prefix_names() noexcept -> std::array<const char8_t*, 6>;
    [[nodiscard]] static auto stem_names() noexcept -> std::array<const char8_t*, 7>;
  };
}
