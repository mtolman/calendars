#pragma once

#include "common.h"
#include <numeric>
#include <algorithm>
#include <array>
#include <utility>
#include <memory>
#include "../template_utils.h"
#include <calendars/cxx/math/common.h>
#include <calendars/c/calendars/radix.h>

namespace calendars::radix {
  template<typename IntAllocator = std::allocator<int>, typename DoubleAllocator = std::allocator<double>>
  class Date;

  /**
   * Represents a template for Radix dates (the lenght of the day and time cycles)
   */
  template<typename IntAllocator = std::allocator<int>>
  struct DateTemplate {
    std::shared_ptr<const std::vector<int, IntAllocator>> templateDayCycles;
    std::shared_ptr<const std::vector<int, IntAllocator>> templateTimeCycles;

   public:
    /**
     * Creates a RadixDate template lists of cycles
     * @param dayCycles Initializer list of day cycles
     * @param timeCycles Initializer list of time cycles
     */
    explicit DateTemplate(std::initializer_list<int> dayCycles, std::initializer_list<int> timeCycles);

    /**
     * Creates a RadixDate template lists of cycles
     * @param dayCycles List of day cycles
     * @param timeCycles List of time cycles
     */
    template<typename IncomingAllocator = std::allocator<int>>
    explicit DateTemplate(const std::vector<int, IncomingAllocator>& dayCycles, const std::vector<int, IncomingAllocator>& timeCycles);

    /**
     * Gets the associated element from the radix date
     * @param index
     * @return
     */
    const int& operator[](size_t index) const;

    /**
     * Compares two date templates
     * @param o Date template to compare aginst
     * @return
     */
    [[nodiscard]] auto operator<=>(const DateTemplate& o) const noexcept -> std::strong_ordering;
    CALENDAR_CXX_COMPARISON_OPS(DateTemplate)

    template<typename DoubleAllocator = std::allocator<double>>
    [[nodiscard]] auto make_date(std::initializer_list<double> dayCycleValues, std::initializer_list<double> timeCycleValues) const -> Date<IntAllocator, DoubleAllocator>;

    template<typename InDoubleAllocator = std::allocator<double>, typename OutDoubleAllocator = std::allocator<double>>
    [[nodiscard]] auto make_date(const std::vector<double, InDoubleAllocator>& dayCycleValues, const std::vector<double, InDoubleAllocator>& timeCycleValues) const -> Date<IntAllocator, OutDoubleAllocator>;

    template<typename DoubleAllocator = std::allocator<double>>
    [[nodiscard]] auto make_date(const Moment& moment) const -> Date<IntAllocator, DoubleAllocator>;

    template<typename DoubleAllocator = std::allocator<double>>
    [[nodiscard]] auto make_date(std::initializer_list<double> values) const -> Date<IntAllocator, DoubleAllocator>;

    struct CTemplateCleanup {
      auto operator()(CalendarRadixTemplate* outPointer) const noexcept -> void {
        calendar_radix_template_deinit(outPointer);
        delete outPointer;
      }
    };

    [[nodiscard]] auto to_c_template() const -> std::unique_ptr<CalendarRadixTemplate, CTemplateCleanup> {
      return std::unique_ptr<CalendarRadixTemplate, CTemplateCleanup>(new CalendarRadixTemplate{static_cast<void*>(new calendars::radix::DateTemplate<>(*this->templateDayCycles, *this->templateTimeCycles))}, CTemplateCleanup{});
    }
  };

  /**
   * Represents date (and optionally datetimes) as an array of numbers with each number
   * representing a part of the date. When representing dates this way, conversions are done
   * assuming a perfectly cyclical number of units for each segment (non-cyclical segments, such
   * as Gregorian months, will not get converted to and from correctly). For example, a date could
   * be represented as [weeks, days, hours, minutes, timestamp] where each week is 7 days, each day
   * is 24 hours, each hour 60 minutes, and each minute 60 timestamp. This calendars would be
   * represented by RadixDate{{7}, {24, 60, 60}}.
   *
   * The parameters are split into two sections: Day units, and Time units.
   *
   * The day units represent the cycles for a full day (weeks, months, years, etc).
   * The time units represent the time cycles inside of a day (hours, minuts, timestamp, etc).
   */
  template<typename IntAllocator, typename DoubleAllocator>
  class Date {
    std::shared_ptr<const std::vector<int, IntAllocator>> templateDayCycles;
    std::shared_ptr<const std::vector<int, IntAllocator>> templateTimeCycles;
    std::vector<double> value;
   public:
    explicit Date(const CalendarRadixDate& cDate);

    template<typename InAllocator = std::allocator<int>>
    Date(
        const std::shared_ptr<const std::vector<int, InAllocator>>& templateDayCycles,
        const std::shared_ptr<const std::vector<int, InAllocator>>& templateTimeCycles,
        std::initializer_list<double> values
    );

    template<typename InIntAllocator = std::allocator<int>, typename InDoubleAllocator = std::allocator<double>>
    Date(
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateDayCycles,
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateTimeCycles,
        const std::vector<double, InDoubleAllocator>& values
    ) : templateDayCycles(templateDayCycles), templateTimeCycles(templateTimeCycles), value(values) {}

    template<typename InIntAllocator = std::allocator<int>>
    Date(
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateDayCycles,
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateTimeCycles,
        std::initializer_list<double> dayCycleValues,
        std::initializer_list<double> timeCycleValues
    ): Date(templateDayCycles, templateTimeCycles, std::vector<double>(dayCycleValues), std::vector<double>(timeCycleValues)) {}

    template<typename InIntAllocator = std::allocator<int>, typename InDoubleAllocator = std::allocator<double>>
    Date(
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateDayCycles,
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateTimeCycles,
        const std::vector<double, InDoubleAllocator>& dayCycleValues,
        const std::vector<double, InDoubleAllocator>& timeCycleValues
    );

    template<typename InIntAllocator = std::allocator<int>>
    Date(
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateDayCycles,
        const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateTimeCycles,
        const Moment& moment
    );

    template<typename InAllocator = std::allocator<int>>
    Date with_values(std::vector<double> values) {
      return Date{templateDayCycles, templateTimeCycles, values};
    }

    std::vector<double> raw_values() const {
      return value;
    }

    [[nodiscard]] auto to_moment() const noexcept -> Moment;

    [[nodiscard]] auto operator[](size_t index) const -> double;

    [[nodiscard]] auto size() const noexcept -> size_t;

    [[nodiscard]] auto get(size_t index) const noexcept -> double;

    auto set(size_t index, double val) noexcept -> bool {
      if (index < value.size()) {
        value[index] = val;
        return true;
      }
      return false;
    }

    [[nodiscard]] auto with(size_t index, double val) const noexcept -> Date {
      auto newDate = *this;
      newDate.set(index, val);
      return newDate;
    }

    struct CDateCleanup {
      auto operator()(CalendarRadixDate * outPointer) const noexcept -> void {
        calendar_radix_date_deinit(outPointer);
        delete outPointer;
      }
    };

    [[nodiscard]] auto to_c_date() const -> std::unique_ptr<CalendarRadixDate , CDateCleanup> {
      return std::unique_ptr<CalendarRadixDate, CDateCleanup>(new CalendarRadixDate{static_cast<void*>(new calendars::radix::Date<>(this->templateDayCycles, this->templateTimeCycles, this->value))}, CDateCleanup{});
    }
  };

  // Implementation details
  template<typename IntAllocator, typename DoubleAllocator>
  template<typename InAllocator>
  Date<IntAllocator, DoubleAllocator>::Date(const std::shared_ptr<const std::vector<int, InAllocator>>& templateDayCycles,
                                            const std::shared_ptr<const std::vector<int, InAllocator>>& templateTimeCycles,
                                            std::initializer_list<double>                               values)
      : templateDayCycles(templateDayCycles), templateTimeCycles(templateTimeCycles), value({}) {
    value.reserve(templateDayCycles->size() + templateTimeCycles->size() + 1);

    for (auto index = 0; values.size() + index <= templateDayCycles->size() + templateTimeCycles->size(); ++index) {
      value.push_back(0);
    }

    std::copy(values.begin(),
              values.begin() + std::min(values.size(), templateDayCycles->size() + templateTimeCycles->size() + 1),  // NOLINT
              std::back_inserter(value));
  }

  template<typename IntAllocator, typename DoubleAllocator>
  auto Date<IntAllocator, DoubleAllocator>::get(size_t index) const noexcept -> double {
    if (index >= value.size()) {
      return std::numeric_limits<double>::quiet_NaN();
    }
    return value[index];
  }

  template<typename IntAllocator, typename DoubleAllocator>
  auto Date<IntAllocator, DoubleAllocator>::size() const noexcept -> size_t { return value.size(); }

  template<typename IntAllocator, typename DoubleAllocator>
  auto Date<IntAllocator, DoubleAllocator>::operator[](size_t index) const -> double { return value.at(index); }

  template<typename IntAllocator, typename DoubleAllocator>
  auto Date<IntAllocator, DoubleAllocator>::to_moment() const noexcept -> Moment {
    double result = 0.0;

    double wholeCoefficient = std::accumulate(templateDayCycles->cbegin(), templateDayCycles->cend(), 1.0, std::multiplies<>());
    double fracCoefficient  = 1.0;

    for (size_t i = 0; i <= templateDayCycles->size(); ++i) {
      result += value.at(i) * wholeCoefficient;
      if (i < templateDayCycles->size()) {
        wholeCoefficient /= templateDayCycles->at(i);
      }
    }

    for (size_t i = 0; i < templateTimeCycles->size(); ++i) {
      fracCoefficient *= templateTimeCycles->at(i);
      result += value.at(templateDayCycles->size() + 1 + i) / fracCoefficient;
    }

    return Moment{result};
  }

  template<typename IntAllocator, typename DoubleAllocator>
  template<typename InIntAllocator>
  Date<IntAllocator, DoubleAllocator>::Date(const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateDayCycles,
                                            const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateTimeCycles,
                                            const Moment&                                  moment)
      : templateDayCycles(templateDayCycles), templateTimeCycles(templateTimeCycles), value({}) {
    namespace math          = calendars::math;
    double wholeCoefficient = 1.0;
    auto   seg              = *templateDayCycles;
    wholeCoefficient        = std::accumulate(seg.begin(), seg.end(), 1.0, std::multiplies<>());

    double fracCoefficient = 1.0;

    value.resize(templateDayCycles->size() + templateTimeCycles->size() + 1);
    value[ 0 ] = math::floor(moment.datetime() / wholeCoefficient);
    if (!templateDayCycles->empty()) {
      wholeCoefficient /= templateDayCycles->at(0);

      for (size_t i = 1; i <= templateDayCycles->size(); ++i) {
        value.at(i) = math::mod(math::floor(moment.datetime() / wholeCoefficient), templateDayCycles->at(i - 1));
        if (i < templateDayCycles->size()) {
          wholeCoefficient /= templateDayCycles->at(i);
        }
      }
    }

    if (!templateTimeCycles->empty()) {
      for (size_t i = 0; i < templateTimeCycles->size() - 1; ++i) {
        fracCoefficient *= templateTimeCycles->at(i);
        value.at(i + templateDayCycles->size() + 1) = math::mod(math::floor(moment.datetime() * fracCoefficient), templateTimeCycles->at(i));
      }

      fracCoefficient *= templateTimeCycles->at(templateTimeCycles->size() - 1);
      value.at(templateDayCycles->size() + templateTimeCycles->size()) =
          math::mod(moment.datetime() * fracCoefficient, templateTimeCycles->at(templateTimeCycles->size() - 1));
    }
  }


  template<typename IntAllocator, typename DoubleAllocator>
  template<typename InIntAllocator, typename InDoubleAllocator>
  Date<IntAllocator, DoubleAllocator>::Date(const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateDayCycles,
                                            const std::shared_ptr<const std::vector<int, InIntAllocator>>& templateTimeCycles,
                                            const std::vector<double, InDoubleAllocator>&                            dayCycleValues,
                                            const std::vector<double, InDoubleAllocator>&                            timeCycleValues)
      : templateDayCycles(templateDayCycles), templateTimeCycles(templateTimeCycles), value({}) {
    value.reserve(templateDayCycles->size() + templateTimeCycles->size() + 1);

    for (auto index = 0; dayCycleValues.size() + index <= templateDayCycles->size(); ++index) {
      value.push_back(0);
    }
    std::copy(dayCycleValues.begin(),
              dayCycleValues.begin()
              + static_cast<typename decltype(dayCycleValues.begin())::difference_type>(std::min(dayCycleValues.size(), templateDayCycles->size() + 1)),
              std::back_inserter(value));

    for (auto index = 0; timeCycleValues.size() + index < templateTimeCycles->size(); ++index) {
      value.push_back(0);
    }
    std::copy(timeCycleValues.begin(),
              timeCycleValues.begin()
              + static_cast<typename decltype(timeCycleValues.begin())::difference_type>(std::min(timeCycleValues.size(), templateTimeCycles->size())),
              std::back_inserter(value));
  }

  template<typename IntAllocator, typename DoubleAllocator>
  Date<IntAllocator, DoubleAllocator>::Date(const CalendarRadixDate& cDate)
      : templateDayCycles(std::make_shared<const std::vector<int>>()), templateTimeCycles(std::make_shared<const std::vector<int>>()), value({}) {
    if (cDate.data == nullptr) {
      return;
    }

    if constexpr (std::is_same_v<IntAllocator, std::allocator<int>> && std::is_same_v<DoubleAllocator, std::allocator<double>>) {
      *this = *(static_cast<Date<IntAllocator, DoubleAllocator>*>(cDate.data));
    }
    else {
      auto date = *(static_cast<Date<std::allocator<int>, std::allocator<double>>*>(cDate.data));
      auto dateTemplate = DateTemplate<IntAllocator>{*date.templateDayCycles, *date.templateTimeCycles};
      *this = dateTemplate.make_date(date.value);
    }
  }

  template<typename IntAllocator>
  template<typename DoubleAllocator>
  auto DateTemplate<IntAllocator>::make_date(std::initializer_list<double> values) const -> Date<IntAllocator, DoubleAllocator> {
    return Date{templateDayCycles, templateTimeCycles, values};
  }

  template<typename IntAllocator>
  template<typename InDoubleAllocator, typename OutDoubleAllocator>
  auto DateTemplate<IntAllocator>::make_date(const std::vector<double, InDoubleAllocator>& dayCycleValues,
                                             const std::vector<double, InDoubleAllocator>& timeCycleValues) const -> Date<IntAllocator, OutDoubleAllocator> {
    return calendars::radix::Date<IntAllocator, OutDoubleAllocator>(templateDayCycles, templateTimeCycles, dayCycleValues, timeCycleValues);
  }
  template<typename IntAllocator>
  template<typename DoubleAllocator>
  auto DateTemplate<IntAllocator>::make_date(const Moment& moment) const -> Date<IntAllocator, DoubleAllocator> { return Date<IntAllocator, DoubleAllocator>(templateDayCycles, templateTimeCycles, moment); }

  template<typename IntAllocator>
  template<typename DoubleAllocator>
  auto DateTemplate<IntAllocator>::make_date(std::initializer_list<double> dayCycleValues, std::initializer_list<double> timeCycleValues) const
      -> Date<IntAllocator, DoubleAllocator> {
    return Date<IntAllocator, DoubleAllocator>(templateDayCycles, templateTimeCycles, dayCycleValues, timeCycleValues);
  }
  template<typename IntAllocator>
  auto DateTemplate<IntAllocator>::operator<=>(const DateTemplate& o) const noexcept -> std::strong_ordering {
    if (templateDayCycles == o.templateDayCycles && templateTimeCycles == o.templateTimeCycles) {
      return std::strong_ordering::equal;
    }

    auto daySizeComp = templateDayCycles->size() <=> o.templateDayCycles->size();
    if (daySizeComp != std::strong_ordering::equivalent) {
      return daySizeComp;
    }

    auto timeSizeComp = templateTimeCycles->size() <=> o.templateTimeCycles->size();
    if (timeSizeComp != std::strong_ordering::equivalent) {
      return timeSizeComp;
    }

    for (size_t i = 0; i < templateDayCycles->size(); ++i) {
      auto cmp = templateDayCycles->at(i) <=> o.templateDayCycles->at(i);
      if (cmp != std::strong_ordering::equal) {
        return cmp;
      }
    }

    for (size_t i = 0; i < templateTimeCycles->size(); ++i) {
      auto cmp = templateTimeCycles->at(i) <=> o.templateTimeCycles->at(i);
      if (cmp != std::strong_ordering::equal) {
        return cmp;
      }
    }
    return std::strong_ordering::equal;
  }

  template<typename IntAllocator>
  const int& DateTemplate<IntAllocator>::operator[](size_t index) const {
    if (index < templateDayCycles->size()) {
      return templateDayCycles->at(index);
    }
    else {
      return templateTimeCycles->at(index - templateDayCycles->size());
    }
  }

  template<typename IntAllocator>
  template<typename IncomingAllocator>
  DateTemplate<IntAllocator>::DateTemplate(const std::vector<int, IncomingAllocator>& dayCycles,
                                           const std::vector<int, IncomingAllocator>& timeCycles)
      : templateDayCycles(std::make_shared<const std::vector<int, IntAllocator>>(dayCycles))
      , templateTimeCycles(std::make_shared<const std::vector<int, IntAllocator>>(timeCycles))
  {}

  template<typename IntAllocator>
  DateTemplate<IntAllocator>::DateTemplate(std::initializer_list<int> dayCycles, std::initializer_list<int> timeCycles)
      : templateDayCycles(std::make_shared<const std::vector<int, IntAllocator>>(dayCycles))
      , templateTimeCycles(std::make_shared<const std::vector<int, IntAllocator>>(timeCycles))
  {}

  extern template struct DateTemplate<std::allocator<int>>;
  extern template class Date<std::allocator<int>, std::allocator<double>>;
}
