#pragma once

#include <cstdint>
#include <concepts>

namespace calendars {
  class RdDate;
  class Moment;
}

namespace calendars::traits {

  namespace impl {
    template<typename U>
    class HasFromMoment {
     private:
      template<typename T, T>
      struct helper;
      template<typename T>
      static std::uint8_t check(helper<T (*)(const Moment&), &T::from_moment>*);
      template<typename T>
      static std::uint16_t check(...);
     public:
      static constexpr bool value = sizeof(check<U>(0)) == sizeof(std::uint8_t);
    };

    template<typename U>
    class HasFromDate {
     private:
      template<typename T, T>
      struct helper;
      template<typename T>
      static std::uint8_t check(helper<T (*)(const RdDate&), &T::from_rd_date>*);
      template<typename T>
      static std::uint16_t check(...);
     public:
      static constexpr bool value = sizeof(check<U>(0)) == sizeof(std::uint8_t);
    };

    template<typename T>
    constexpr bool has_from_moment = impl::HasFromMoment<T>::value; // NOLINT(readability-identifier-naming)

    template<typename T>
    constexpr bool has_from_rd_date = impl::HasFromDate<T>::value; // NOLINT(readability-identifier-naming)
  }

  /**
   * Checks if a year can be read from a date
   * @tparam T Type to check
   */
  template<class T>
  concept YearReadable = requires (const T& d) {
    { d.year() } -> std::convertible_to<int64_t>;
  };

  /**
   * Checks if there is a function that converts a type to an RdDate
   * @tparam T Type to check
   */
  template<class T>
  concept ToRdDateFunction = requires (const T& d) {
    { d.to_rd_date() } -> std::same_as<calendars::RdDate>;
  };

  /**
   * Checks if there is a function that converts a type to a Moment
   * @tparam T Type to check
   */
  template<class T>
  concept ToMomentFunction = requires (const T& d) {
    { d.to_moment() } -> std::same_as<calendars::Moment>;
  };

  /**
   * Concept for checking if a function exists to make a type from an RdDate
   * Note: This is using old style SFINAE-style checks since RdDate is an incomplete type here,
   *        so newer concept patterns don't work
   * @tparam T Type to check
   */
  template<class T>
  concept FromRdDateFunction = impl::has_from_rd_date<T>;

  /**
   * Concept for checking if a function exists to make a type from a Moment
   * Note: This is using old style SFINAE-style checks since Moment is an incomplete type here,
   *        so newer concept patterns don't work
   * @tparam T Type to check
   */
  template<class T>
  concept FromMomentFunction = impl::has_from_moment<T>;

  /**
   * Checks if a type can be converted to a Moment
   * @tparam T Type to check
   */
  template <class T>
  concept ToMoment = std::is_same_v<T, calendars::Moment> || ToMomentFunction<T>;

  /**
   * Checks if a type can be made from a Moment
   * @tparam T Type to check
   */
  template <class T>
  concept FromMoment = std::is_same_v<T, calendars::Moment> || FromMomentFunction<T>;

  /**
   * Checks if a type can be converted to a Moment and made from a Moment
   * @tparam T Type to check
   */
  template<class T>
  concept ToFromMoment = ToMoment<T> && FromMoment<T>;

  /**
   * Checks if a type can be converted to an RdDate
   * @tparam T Type to check
   */
  template<class T>
  concept ToRdDate = std::is_same_v<T, calendars::RdDate> || ToRdDateFunction<T> || ToMoment<T>;

  /**
   * Checks if a type can be made from an RdDate
   * @tparam T Type to check
   */
  template<class T>
  concept FromRdDate = std::is_same_v<T, calendars::RdDate> || FromRdDateFunction<T> || FromMoment<T>;

  /**
   * Checks if a type can be converted to a RdDate and made from a RdDate
   * @tparam T Type to check
   */
  template<class T>
  concept ToFromDate = ToFromMoment<T> || (ToRdDate<T> && FromRdDate<T>);

  namespace assertions {
    #define CALENDARS_TRAITS_ASSERTIONS_IMPL__DO_NOT_DEFINE(TRAIT, ASSERT_NAME) \
    namespace impl { template<typename T> struct class_name ## ASSERT_NAME : std::false_type {}; \
    template<TRAIT T> struct class_name ## ASSERT_NAME <T> : std::true_type {}; }                \
    template<typename T> constexpr bool ASSERT_NAME = impl::class_name ## ASSERT_NAME <T>::value;

    CALENDARS_TRAITS_ASSERTIONS_IMPL__DO_NOT_DEFINE(ToFromDate, convertible_to_from_rd_date)
    CALENDARS_TRAITS_ASSERTIONS_IMPL__DO_NOT_DEFINE(ToFromMoment, convertible_to_from_moment)
    CALENDARS_TRAITS_ASSERTIONS_IMPL__DO_NOT_DEFINE(ToRdDate, convertible_to_rd_date)
    CALENDARS_TRAITS_ASSERTIONS_IMPL__DO_NOT_DEFINE(FromRdDate, convertible_from_rd_date)
    CALENDARS_TRAITS_ASSERTIONS_IMPL__DO_NOT_DEFINE(ToMoment, convertible_to_moment)
    CALENDARS_TRAITS_ASSERTIONS_IMPL__DO_NOT_DEFINE(FromMoment, convertible_from_moment)
  }
}
