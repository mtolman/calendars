#pragma once

#include <calendars/c/calendars/coptic.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::coptic {
  enum class MONTH : int16_t {
    THOOUT     = 1,
    PAOPE      = 2,
    ATHOR      = 3,
    KOIAK      = 4,
    TOBE       = 5,
    MESHIRE    = 6,
    PAREMOTEP  = 7,
    PARMOUTE   = 8,
    PASHONS    = 9,
    PAONE      = 10,
    EPEP       = 11,
    MESORE     = 12,
    EPAGOMENAE = 13
  };

  class Date {
    CalendarCopticDate date;
   public:
    explicit Date(CalendarCopticDate date) : date(date) {}
    explicit Date(int64_t year = 0, MONTH month = MONTH::THOOUT, int16_t day = 1) : Date(year, static_cast<CALENDAR_COPTIC_MONTH>(month), day) {}
    Date(int64_t year, CALENDAR_COPTIC_MONTH month, int16_t day) : Date(calendar_coptic_create(year, month, day)) {}
    explicit operator CalendarCopticDate() const noexcept { return date; }

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_coptic_to_rd_date(&date)}; }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) -> Date { return Date{ calendar_coptic_from_rd_date(static_cast<CRdDate>(rdDate))}; }
    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_coptic_compare(&date, &other.date)); }
    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return is_leap_year(date.year); }
    [[nodiscard]] static auto is_leap_year(int64_t year) noexcept -> bool { return calendar_coptic_is_leap_year(year); }

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(date.month); }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto month_name() const noexcept -> const char8_t* { return calendar_coptic_month_name(static_cast<CALENDAR_COPTIC_MONTH>(date.month)); }
    [[nodiscard]] static auto month_names() noexcept -> std::array<const char8_t*, 13>;

    CALENDAR_CXX_COMPARISON_OPS(Date)
  };
}