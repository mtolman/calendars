#pragma once

#include <calendars/c/calendars/astronomical/geography.h>
#include <calendars/cxx/math/common.h>

namespace calendars::astronomical {
  class DegMinSec {
    SI::Geo::degree_t<double> m_degrees;
    SI::Geo::minute_t<double> m_minutes;
    SI::Geo::second_t<double> m_seconds;
  public:
    DegMinSec(CalendarGeoDegMinSec c)
      : m_degrees(c.degrees)
      , m_minutes(c.minutes)
      , m_seconds(c.seconds)
      {}

    DegMinSec(SI::Geo::degree_t<double> degrees, SI::Geo::minute_t<double> minutes, SI::Geo::second_t<double> seconds)
      : m_degrees(degrees)
      , m_minutes(minutes)
      , m_seconds(seconds)
      {}

    operator CalendarGeoDegMinSec() const noexcept {
      return {.degrees = m_degrees.value(), .minutes = m_minutes.value(), .seconds = m_seconds.value()};
    }

    SI::Geo::degree_t<double> as_degrees() const noexcept {
      return m_degrees + m_minutes + m_seconds;
    }

    SI::Geo::degree_t<int> degrees() const noexcept { return m_degrees; };
    SI::Geo::minute_t<int> minutes() const noexcept { return m_minutes; };
    SI::Geo::second_t<int> seconds() const noexcept { return m_seconds; };
  };

  class EarthPos {
      SI::Geo::degree_t<double> m_latitude;
      SI::Geo::degree_t<double> m_longitude;
      SI::meter_t<double> m_elevation;
      SI::days_t<double> m_zoneOffset;
  public:
    EarthPos(
      SI::Geo::degree_t<double> latitude,
      SI::Geo::degree_t<double> longitude,
      SI::meter_t<double> elevation,
      SI::days_t<double> zoneOffset
    )
    : m_latitude(latitude)
    , m_longitude(longitude)
    , m_elevation(elevation)
    , m_zoneOffset(zoneOffset)
    {}

    EarthPos(
      const DegMinSec& latitude,
      const DegMinSec& longitude,
      SI::meter_t<double> elevation,
      SI::days_t<double> zoneOffset
    ) : EarthPos(
        latitude.as_degrees(),
        longitude.as_degrees(),
        elevation,
        zoneOffset
      ) {}

    EarthPos(CalendarEarthPos pos) : EarthPos(
        SI::Geo::degree_t<double>(pos.latitude),
        SI::Geo::degree_t<double>(pos.longitude),
        SI::meter_t<double>(pos.elevation),
        SI::days_t<double>(pos.zoneOffset)
      ) {}

    operator CalendarEarthPos() const noexcept {
      return {
        .latitude=m_latitude.value(),
        .longitude=m_longitude.value(),
        .elevation=m_elevation.value(),
        .zoneOffset=m_zoneOffset.value()
      };
    }

    SI::Geo::degree_t<double> latitude() const noexcept { return m_latitude; }
    SI::Geo::degree_t<double> longitude() const noexcept { return m_longitude; }
    SI::meter_t<double> elevation() const noexcept { return m_elevation; }
    SI::days_t<double> zoneOffset() const noexcept { return m_zoneOffset; }
    SI::degree_t<double> direction(const EarthPos& other) const noexcept;

    static EarthPos urbana() { return {calendar_earth_pos_urbana()}; }
    static EarthPos greenwhich() { return {calendar_earth_pos_greenwhich()}; }
    static EarthPos mecca() { return {calendar_earth_pos_mecca()}; }
    static EarthPos jerusalem() { return {calendar_earth_pos_jerusalem()}; }
    static EarthPos acre() { return {calendar_earth_pos_acre()}; }
  };
}
