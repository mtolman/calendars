#pragma once

#include <calendars/cxx/math/common.h>
#include <calendars/c/calendars/astronomical/time.h>
#include "geography.h"
#include <calendars/cxx/calendars/common.h>
#include <calendars/cxx/calendars/gregorian.h>
#include <concepts>
#include <calendars/cxx/calendars/traits.h>

/**
 * NOTE: The timezones derived here are geographic means rather than political boundaries
 */

namespace calendars::astronomical {
  class TimeHM {
    SI::hours_t<int> m_hours;
    SI::minutes_t<int> m_minutes;
  public:
    TimeHM(SI::days_t<double> dayFraction)
      : m_hours(math::mod(dayFraction.as<SI::hours_t<double>>().value(), 24))
      , m_minutes(math::mod(dayFraction.as<SI::minutes_t<double>>().value(), 60))
    {}

    TimeHM(SI::hours_t<int> hours, SI::minutes_t<int> minutes)
      : m_hours(hours)
      , m_minutes(minutes)
      {}

    TimeHM()
      : m_hours(SI::hours_t<int>{0})
      , m_minutes(SI::minutes_t<int>{0})
    {}

    TimeHM(CalendarTimeHM ctime)
      : m_hours(SI::hours_t<int>(ctime.hours))
      , m_minutes(SI::minutes_t<int>(ctime.minutes))
      {}

    operator CalendarTimeHM() const noexcept {
      return {
        .hours=m_hours.value(),
        .minutes=m_minutes.value()
      };
    }

    SI::hours_t<int> hours() const noexcept { return m_hours; }
    SI::minutes_t<int> minutes() const noexcept { return m_minutes; }
    SI::days_t<double> dayFraction() const noexcept {
      return m_hours.as<SI::hours_t<double>>() + m_minutes.as<SI::minutes_t<double>>();
    }

    TimeHM operator+(SI::hours_t<int> hours) const noexcept { return {m_hours + hours, m_minutes}; }
    TimeHM operator+(SI::minutes_t<int> minutes) const noexcept { return {m_hours, m_minutes + minutes}; }
    TimeHM operator-(SI::hours_t<int> hours) const noexcept { return {m_hours - hours, m_minutes}; }
    TimeHM operator-(SI::minutes_t<int> minutes) const noexcept { return {m_hours, m_minutes - minutes}; }

    TimeHM operator+(const TimeHM& other) const noexcept {
      return {m_hours + other.m_hours, m_minutes + other.m_minutes};
    }
    TimeHM operator-(const TimeHM& other) const noexcept {
      return {m_hours - other.m_hours, m_minutes - other.m_minutes};
    }
  };

  struct Timezone {
    SI::days_t<double> offset;
    TimeHM hours_minutes() const { return TimeHM(offset); }

    Timezone(SI::days_t<double> offset)
      : offset(offset)
    {}

    Timezone(CalendarAstroTimezone c)
      : offset(SI::days_t<double>{c.offset})
    {}

    operator CalendarAstroTimezone() const noexcept { return {offset.value()}; }
  };

  Timezone zone_from_longitude(SI::degree_t<double> longitude);

  template<typename T>
  concept DateTime = requires(const T& a) {
    { a.timezone() } -> std::same_as<Timezone>;
    { a.date_time() } -> std::same_as<SI::days_t<double>>;
  };

  namespace impl {
    template<typename T>
    struct IsDateTime : std::false_type {};

    template<DateTime T>
    struct IsDateTime<T> : std::true_type{};
  }

  template<typename T>
  constexpr bool is_date_time = impl::IsDateTime<T>::value;

  struct UniversalDateTime;
  struct TimezoneDateTime;
  struct LocalDateTime;
  struct DynamicalDateTime;

  SI::days_t<double> ephemeris_correction(const RdDate& date);
  SI::days_t<double> ephemeris_correction(const gregorian::Date& date);

  template<DateTime T>
  UniversalDateTime to_universal(const T& time) {
    return {
      time.date_time() - time.timezone().offset
    };
  }

  template<DateTime T>
  TimezoneDateTime to_timezone(const T& time, const Timezone& timezone) {
    return {
      to_universal(time).date_time() + timezone.offset, timezone
    };
  }

  template<DateTime T>
  LocalDateTime to_location(const T& time, const EarthPos& location) {
    return {
      to_universal(time).date_time() + zone_from_longitude(location.longitude()).offset, location
    };
  }

  template<DateTime T>
  DynamicalDateTime to_dynamical(const T& time) {
    if constexpr(std::is_same_v<T, DynamicalDateTime>) {
      return time;
    }
    else {
      auto universal = to_universal(time);
      return {
        universal.date_time() + ephemeris_correction(calendars::RdDate::from(universal.to_moment()))
      };
    }
  }

  struct UniversalDateTime {
    SI::days_t<double> dateTime;

    UniversalDateTime(SI::days_t<double> dateTime)
      : dateTime(dateTime)
    {}

    UniversalDateTime(CalendarAstroUniversalDateTime time)
      : dateTime(SI::days_t<double>{time.dateTime})
    {}

    operator CalendarAstroUniversalDateTime() const noexcept { return {dateTime.value()}; }

    Timezone timezone() const noexcept { return Timezone{SI::days_t<double>{0}}; }
    SI::days_t<double> date_time() const noexcept { return dateTime; }

    [[nodiscard]] static auto from_moment(const Moment& moment) -> UniversalDateTime {
      return UniversalDateTime{SI::days_t<double>{moment.datetime()}};
    }

    [[nodiscard]] auto to_moment() const noexcept -> Moment { return Moment{dateTime.value()}; }
  };
  static_assert(is_date_time<UniversalDateTime>, "Universal time must be a date time");
  static_assert(traits::assertions::convertible_to_from_rd_date<UniversalDateTime>, "Universal time must be convertible to/from RdDate");
  static_assert(traits::assertions::convertible_to_from_moment<UniversalDateTime>, "Universal time must be convertible to/from Moment");

  struct TimezoneDateTime {
    SI::days_t<double> dateTime;
    Timezone m_timezone;

    TimezoneDateTime(SI::days_t<double> dateTime, Timezone timezone)
      : dateTime(dateTime)
      , m_timezone(timezone)
    {}

    TimezoneDateTime(CalendarAstroTimezoneDateTime time)
      : dateTime(SI::days_t<double>{time.dateTime})
      , m_timezone(time.timezone)
    {}

    operator CalendarAstroTimezoneDateTime() const noexcept { return {dateTime.value(), {m_timezone.offset.value()}}; }

    Timezone timezone() const noexcept { return m_timezone; }
    SI::days_t<double> date_time() const noexcept { return dateTime; };
    [[nodiscard]] auto to_moment() const noexcept -> Moment { return to_universal(*this).to_moment(); }
  };
  static_assert(is_date_time<TimezoneDateTime>, "Timezone time must be a date time");
  static_assert(traits::assertions::convertible_to_moment<TimezoneDateTime>, "Timezone time must be convertible to Moment");
  using StandardDateTime = TimezoneDateTime;

  struct LocalDateTime {
    SI::days_t<double> dateTime;
    EarthPos location;

    LocalDateTime(SI::days_t<double> dateTime, EarthPos location)
      : dateTime(dateTime)
      , location(location)
    {}

    LocalDateTime(CalendarAstroLocalDateTime time)
      : dateTime(SI::days_t<double>{time.dateTime})
      , location(time.location)
    {}

    operator CalendarAstroLocalDateTime() const noexcept { return {dateTime.value(), (CalendarEarthPos)location}; }

    Timezone timezone() const noexcept { return zone_from_longitude(location.longitude()); }
    SI::days_t<double> date_time() const noexcept { return dateTime; };
    [[nodiscard]] auto to_moment() const noexcept -> Moment { return to_universal(*this).to_moment(); }
  };
  static_assert(is_date_time<LocalDateTime>, "Local time must be a date time");
  static_assert(traits::assertions::convertible_to_moment<LocalDateTime>, "Local time must be convertible to Moment");

  struct DynamicalDateTime {
    SI::days_t<double> dateTime;

    DynamicalDateTime(SI::days_t<double> dateTime) : dateTime(dateTime) {}
    DynamicalDateTime(const CalendarAstroDynamicalDateTime& time) : dateTime(time.dateTime) {}
    operator CalendarAstroDynamicalDateTime() const { return {dateTime.value()}; }

    Timezone timezone() const noexcept { return Timezone{ephemeris_correction(RdDate::from(Moment(dateTime.value())))}; }
    SI::days_t<double> date_time() const noexcept { return dateTime; };
    [[nodiscard]] static auto from_moment(const Moment& moment) -> DynamicalDateTime {
      return {SI::days_t<double>{moment.datetime()} - ephemeris_correction(RdDate::from(moment))};
    }
    [[nodiscard]] auto to_moment() const noexcept -> Moment { return to_universal(*this).to_moment(); }
  };
  static_assert(is_date_time<DynamicalDateTime>, "Dynamical time must be a date time");
  static_assert(traits::assertions::convertible_to_from_rd_date<DynamicalDateTime>, "Dynamical time must be convertible to/from RdDate");
  static_assert(traits::assertions::convertible_to_from_moment<DynamicalDateTime>, "Dynamical time must be convertible to/from Moment");

  double julian_centuries(const UniversalDateTime& dateTime);
  double julian_centuries(const DynamicalDateTime& dateTime);

  double j2000();

  SI::days_t<double> equation_of_time(const UniversalDateTime& dateTime);

  SI::degree_t<double> obliquity(const UniversalDateTime& t);
}
