#pragma once

#include <calendars/cxx/calendars/common.h>
#include <calendars/c/calendars/iso.h>

namespace calendars::iso {
  class Date {
    CalendarIsoDate date;
   public:
    explicit Date(CalendarIsoDate date) : date(date) {}
    explicit Date(int64_t year = 0, int16_t week = 1, int16_t day = 1) : date(calendar_iso_create(year, week, day)) {}
    explicit operator CalendarIsoDate() const noexcept { return date; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_iso_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto week() const noexcept -> int16_t { return date.week; }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto is_long_year() const noexcept -> bool { return is_long_year(date.year); }
    [[nodiscard]] static auto is_long_year(int64_t year) noexcept -> bool { return calendar_iso_is_long_year(year); }

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{ calendar_iso_to_rd_date(&date)}; }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{ calendar_iso_from_rd_date(static_cast<CRdDate>(rdDate))}; }
  };
}
