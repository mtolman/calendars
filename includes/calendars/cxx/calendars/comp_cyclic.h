#pragma once

#include "common.h"
#include <calendars/cxx/math/common.h>
#include <calendars/cxx/math/numbers.h>

namespace calendars::comp_cyclic {

  namespace single_cycle {
    enum class Type {
      STRICTLY_BEFORE,
      UP_TO_AND_INCLUDING,
      MEAN_MONTH,
      AT_OR_BEFORE_MEAN_MONTH
    };
    
    template<
        Type TYPE,
        int64_t EPOCH,
        int64_t YEAR_NUMERATOR,
        int64_t YEAR_DENOMINATOR,
        int64_t MONTH_NUMERATOR,
        int64_t MONTH_DENOMINATOR,
        int64_t DELTA_YEAR_NUMERATOR = 0,
        int64_t DELTA_YEAR_DENOMINATOR = 1,
        int64_t DELTA_MONTH_NUMERATOR = 0,
        int64_t DELTA_MONTH_DENOMINATOR = 1>
    struct Date {
     private:
      static const math::pnum_t Y;
      static const math::pnum_t M;
      static const math::pnum_t deltaY;
      static const math::pnum_t deltaM;
      static const Type type = TYPE;
      
      int64_t mYear;
      int32_t mMonth;
      int32_t mDay;
     public:
      static constexpr int64_t epoch = EPOCH;
      explicit Date(int64_t year = 0, int32_t month = 0, int32_t day = 0) : mYear(year), mMonth(month), mDay(day) {}

      [[nodiscard]] auto year() const noexcept -> int64_t { return year; }
      [[nodiscard]] auto month() const noexcept -> int64_t { return month; }
      [[nodiscard]] auto day() const noexcept -> int64_t { return day; }

      [[nodiscard]] auto to_rd_date() const noexcept -> RdDate {
        if constexpr (type == Type::STRICTLY_BEFORE) {
          return calendars::RdDate{epoch + math::pnum_to_int(math::floor((mYear - 1) * Y + deltaY)) + math::pnum_to_int(math::floor((mMonth - 1) * M + deltaM)) + mDay - 1};
        }
        else if constexpr (type == Type::UP_TO_AND_INCLUDING) {
          return calendars::RdDate{epoch + math::pnum_to_int(math::ceil((mYear - 1) * Y - deltaY))
                                   + math::pnum_to_int(math::floor((mMonth - 1) * M  + deltaM)) + mDay - 1};
        }
        else if constexpr (type == Type::MEAN_MONTH) {
          return calendars::RdDate{
              epoch + math::pnum_to_int(math::floor((mYear - 1) * Y + deltaY + (mMonth - 1) * M))
              + mDay - 1
          };
        }
        else if constexpr (type == Type::AT_OR_BEFORE_MEAN_MONTH) {
          return calendars::RdDate{
              epoch + math::pnum_to_int(math::ceil((mYear - 1) * Y + deltaY + (mMonth - 1) * M))
              + mDay - 1
          };
        }
      }

      static auto from_rd_date(const calendars::RdDate& date) noexcept -> Date {
        if constexpr (type == Type::STRICTLY_BEFORE) {
          const auto d     = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch + 1) - deltaY;
          const auto year  = math::pnum_to_int(math::ceil(d / Y));
          const auto n     = math::ceil(d - (year - 1) * Y) - deltaM;
          const auto month = math::pnum_to_int<int32_t>(math::ceil(n / M));
          const auto day   = math::pnum_to_int<int32_t>(math::ceil(math::amod(n, M)));
          return Date{year, month, day};
        }
        else if constexpr (type == Type::UP_TO_AND_INCLUDING) {
          const auto d     = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch) + deltaY;
          const auto year  = math::pnum_to_int(math::floor(d/Y)) + 1;
          const auto n     = math::floor(math::mod(d, Y)) + 1 - deltaM;
          const auto month = math::pnum_to_int<int32_t>(math::ceil(n / M));
          const auto day   = math::pnum_to_int<int32_t>(math::ceil(math::amod(n, M)));
          return Date{year, month, day};
        }
        else if constexpr (type == Type::MEAN_MONTH) {
          const auto d = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch + 1) - deltaY;
          const auto year = math::pnum_to_int(math::ceil(d / Y));
          const int32_t L = math::pnum_to_int<int32_t>(Y/M);
          const auto m = math::ceil(d/M) - 1;
          const int32_t month = math::pnum_to_int<int32_t>(math::mod(m, L + 1));
          const auto day = math::pnum_to_int<int32_t>(math::ceil(math::amod(d, M)));
          return Date{year, month, day};
        }
        else if constexpr (type == Type::AT_OR_BEFORE_MEAN_MONTH) {
          const auto d = math::int_to_pnum(static_cast<CalendarRdDate>(date) - epoch) - deltaY;
          const auto year = math::pnum_to_int(math::floor(d / Y) + 1);
          const int32_t L = math::pnum_to_int<int32_t>(Y/M);
          const int32_t month = math::mod(math::pnum_to_int<int32_t>(math::floor(d/M)), L + 1);
          const auto day = math::pnum_to_int<int32_t>(math::floor(math::mod(d, M))) + 1;
          return Date{year, month, day};
        }
      }

      [[nodiscard]] auto operator<=>(const Date& o) const noexcept -> std::strong_ordering {
        auto yearCmp = mYear <=> o.mYear;
        if (yearCmp != std::strong_ordering::equal) {
          return yearCmp;
        }

        auto monthCmp = mMonth <=> o.mMonth;
        if (monthCmp != std::strong_ordering::equal) {
          return monthCmp;
        }

        auto dayCmp = mDay <=> o.mDay;
        return dayCmp;
      }

      CALENDAR_CXX_COMPARISON_OPS(Date)
    };

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::Y = math::int_to_pnum(YEAR_NUMERATOR) / math::int_to_pnum(YEAR_DENOMINATOR);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::M = math::int_to_pnum(MONTH_NUMERATOR) / math::int_to_pnum(MONTH_DENOMINATOR);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::deltaY = math::int_to_pnum(DELTA_YEAR_NUMERATOR) / math::int_to_pnum(DELTA_YEAR_DENOMINATOR);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::deltaM = math::int_to_pnum(DELTA_MONTH_NUMERATOR) / math::int_to_pnum(DELTA_MONTH_DENOMINATOR);

  }
  
  namespace double_cycle {
    enum class Type {
      //    STRICTLY_BEFORE = 0, // The book calculations are broken, so not using it for now
      AT_OR_BEFORE = 1,
    };

    /**
   * Represents a generic double cyclic calendar
   * Note: this uses only "at or before" calculations and not "strictly before".
   * The strictly before calculations are not yet figured out
   * (the book has an issue with year = m/L since that expands to year = (days*L/L) + deltaY/L = days + deltaY/L.
   * A year should obviously not be equal to the number of days. When deltaY = 0, then year = days, which should never happen.
   * I haven't figured out what the correct formula is, and I'm not really invested in doing so right now.
   *
   * From what I can tell, the conversion from a strictly before double cycle date to an RdDate appears correct, but it is the
   * conversion from an RdDate to the cyclic date which appears to be incorrect.
   *
   * @tparam TYPE
   * @tparam EPOCH
   * @tparam YEAR_NUMERATOR
   * @tparam MONTH_NUMERATOR
   * @tparam YEAR_DENOMINATOR
   * @tparam MONTH_DENOMINATOR
   * @tparam DELTA_YEAR_NUMERATOR
   * @tparam DELTA_YEAR_DENOMINATOR
   * @tparam DELTA_MONTH_NUMERATOR
   * @tparam DELTA_MONTH_DENOMINATOR
     */
    template<
        Type TYPE,
        int64_t EPOCH,
        int64_t YEAR_NUMERATOR,
        int64_t YEAR_DENOMINATOR,
        int64_t MONTH_NUMERATOR,
        int64_t MONTH_DENOMINATOR,
        int64_t DELTA_YEAR_NUMERATOR = 0,
        int64_t DELTA_YEAR_DENOMINATOR = 1,
        int64_t DELTA_MONTH_NUMERATOR = 0,
        int64_t DELTA_MONTH_DENOMINATOR = 1
        >
    struct Date {
      static constexpr int64_t epoch = EPOCH;
      static const math::pnum_t Y;
      static const math::pnum_t M;
      static const math::pnum_t deltaY;
      static const math::pnum_t deltaM;
      static const math::pnum_t leapYearFrac;

      static const math::pnum_t L;

      int64_t year;
      int32_t month;
      int32_t day;

      RdDate to_rd_date() const noexcept {
        const auto m = math::floor((year - 1) * L + deltaY) + month - 1;
        return RdDate{epoch + math::pnum_to_int(math::ceil(m*M - deltaM)) + day - 1};
      }

      static Date from_rd_date(const RdDate& date) noexcept {
        const auto d = static_cast<CalendarRdDate>(date) - epoch + deltaM;
        const auto m = math::floor(d/M) + 1 - deltaY;
        const auto year = math::pnum_to_int(math::ceil(m/L));
        const auto month = math::pnum_to_int<int32_t>(math::ceil(math::amod(m, L)));
        const auto day = math::pnum_to_int<int32_t>(math::floor(math::mod(d, M)) + 1);
        return Date{year, month, day};
      }

      [[nodiscard]] auto operator<=>(const Date& o) const noexcept -> std::strong_ordering {
        auto yearCmp = year <=> o.year;
        if (yearCmp != std::strong_ordering::equal) {
          return yearCmp;
        }

        auto monthCmp = month <=> o.month;
        if (monthCmp != std::strong_ordering::equal) {
          return monthCmp;
        }

        auto dayCmp = day <=> o.day;
        return dayCmp;
      }

      CALENDAR_CXX_COMPARISON_OPS(Date)
    };

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::Y = math::int_to_pnum(YEAR_NUMERATOR) / math::int_to_pnum(YEAR_DENOMINATOR);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::M = math::int_to_pnum(MONTH_NUMERATOR) / math::int_to_pnum(MONTH_DENOMINATOR);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::deltaY = math::int_to_pnum(DELTA_YEAR_NUMERATOR) / math::int_to_pnum(DELTA_YEAR_DENOMINATOR);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::deltaM = math::int_to_pnum(DELTA_MONTH_NUMERATOR) / math::int_to_pnum(DELTA_MONTH_DENOMINATOR);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::leapYearFrac = math::mod((math::int_to_pnum(YEAR_NUMERATOR) / math::int_to_pnum(YEAR_DENOMINATOR)) / (math::int_to_pnum(MONTH_NUMERATOR) / math::int_to_pnum(MONTH_DENOMINATOR)), 1);

    template<Type TYPE, int64_t EPOCH, int64_t YEAR_NUMERATOR, int64_t YEAR_DENOMINATOR, int64_t MONTH_NUMERATOR, int64_t MONTH_DENOMINATOR, int64_t DELTA_YEAR_NUMERATOR, int64_t DELTA_YEAR_DENOMINATOR, int64_t DELTA_MONTH_NUMERATOR, int64_t DELTA_MONTH_DENOMINATOR>
    const math::pnum_t Date<TYPE, EPOCH, YEAR_NUMERATOR, YEAR_DENOMINATOR, MONTH_NUMERATOR, MONTH_DENOMINATOR, DELTA_YEAR_NUMERATOR, DELTA_YEAR_DENOMINATOR, DELTA_MONTH_NUMERATOR, DELTA_MONTH_DENOMINATOR>::L = (math::int_to_pnum(YEAR_NUMERATOR) / math::int_to_pnum(YEAR_DENOMINATOR)) / (math::int_to_pnum(MONTH_NUMERATOR) / math::int_to_pnum(MONTH_DENOMINATOR));
  }
}