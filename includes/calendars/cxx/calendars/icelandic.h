#pragma once

#include <calendars/c/calendars/icelandic.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::icelandic {
  enum class SEASON {
    SPRING = ICELANDIC_SEASON_SPRING,
    SUMMER = ICELANDIC_SEASON_SUMMER,
    AUTUMN = ICELANDIC_SEASON_AUTUMN,
    WINTER = ICELANDIC_SEASON_WINTER,
  };

  enum class MONTH {
    HARPA,
    SKERPLA,
    SOLMANUDUR,
    HEYANNIR,
    TVIMANUDUR,
    HAUSTMANUDUR,
    GORMANUDUR,
    YLIR,
    MORSUGUR,
    THORRI,
    GOA,
    EINMANUDUR,
  };

  class Date {
    CalendarIcelandicDate date;

   public:
    explicit Date(CalendarIcelandicDate date) : date(date) {}
    explicit Date(int64_t year = 0, SEASON season = SEASON::SPRING, int8_t week = 1, DAY_OF_WEEK weekday = DAY_OF_WEEK::SUNDAY) : Date(year, static_cast<CALENDAR_ICELANDIC_SEASON>(season), week, static_cast<CALENDAR_DAY_OF_WEEK>(weekday)) {}
    Date(int64_t year, CALENDAR_ICELANDIC_SEASON season, int8_t week, CALENDAR_DAY_OF_WEEK weekday) : date(calendar_icelandic_create(year, season, week, weekday)) {}
    explicit operator CalendarIcelandicDate() const noexcept { return date; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_icelandic_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(calendar_icelandic_month(&date)); }
    [[nodiscard]] auto season() const noexcept -> SEASON { return static_cast<SEASON>(date.season); }
    [[nodiscard]] auto week() const noexcept -> int8_t { return date.week; }
    [[nodiscard]] auto weekday() const noexcept -> DAY_OF_WEEK { return static_cast<DAY_OF_WEEK>(date.weekday); }
    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return is_leap_year(date.year); }

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_icelandic_to_rd_date(&date)}; }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{calendar_icelandic_from_rd_date(static_cast<CRdDate>(rdDate))}; }

    [[nodiscard]] static auto is_leap_year(int64_t year) -> bool { return calendar_icelandic_is_leap_year(year); }
    [[nodiscard]] static auto summer(int64_t year) -> RdDate { return RdDate{ calendar_icelandic_summer(year)}; }
    [[nodiscard]] static auto winter(int64_t year) -> RdDate { return RdDate{ calendar_icelandic_winter(year)}; }
  };
}
