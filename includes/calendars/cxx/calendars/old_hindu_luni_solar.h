#pragma once

#include <calendars/c/calendars/old_hindu_luni_solar.h>
#include <calendars/c/calendars/old_hindu_solar.h>
#include <calendars/cxx/calendars/common.h>

namespace calendars::old_hindu_luni_solar {
  enum class SANSKRIT {
    CAITRA,
    VAISAKHA,
    JYESTHA,
    ASADHA,
    SRAVANA,
    BHADRAPADA,
    ASVINA,
    KARTIKA,
    MARGASIRSA,
    PAUSA,
    MAGHA,
    PHALGUNA
  };

  class Date {
    CalendarOldHinduLuniSolarDate date;

   public:
    explicit Date(CalendarOldHinduLuniSolarDate date) : date(date) {}
    explicit Date(int64_t year = 1, SANSKRIT sanskrit = SANSKRIT::CAITRA, bool isLeapMonth = true, int16_t day = 1) : Date(year, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(sanskrit), isLeapMonth, day) {}
    Date(int64_t year, CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT sanskrit, bool isLeapMonth, int16_t day) : date(calendar_old_hindu_luni_solar_create(year, sanskrit, isLeapMonth, day)) {}

    [[nodiscard]] auto year() const noexcept -> int64_t { return date.year; }
    [[nodiscard]] auto sanskrit() const noexcept -> SANSKRIT { return static_cast<SANSKRIT>(date.sanskrit); }
    [[nodiscard]] auto is_leap_month() const noexcept -> bool { return date.isLeapMonth; }
    [[nodiscard]] auto day() const noexcept -> int16_t { return date.day; }

    [[nodiscard]] auto operator<=>(const Date& other) const noexcept -> std::strong_ordering { return impl::compare_int(calendar_old_hindu_luni_solar_compare(&date, &other.date)); }
    CALENDAR_CXX_COMPARISON_OPS(Date)

    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return is_leap_year(date.year); }
    [[nodiscard]] static auto is_leap_year(int64_t year) noexcept -> bool { return calendar_old_hindu_luni_solar_is_leap_year(year); }

    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_old_hindu_luni_solar_to_rd_date(&date)}; }
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date { return Date{ calendar_old_hindu_luni_solar_from_rd_date(static_cast<CRdDate>(rdDate))}; }
  };
}
