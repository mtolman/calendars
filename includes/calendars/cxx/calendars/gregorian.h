#pragma once

/** @file */

#include <calendars/c/calendars/gregorian.h>

#include <array>
#include <compare>
#include <string>
#include <tuple>

#include "common.h"

namespace calendars::gregorian {
  enum class MONTH {
    JANUARY = MONTH_JANUARY,
    FEBRUARY = MONTH_FEBRUARY,
    MARCH = MONTH_MARCH,
    APRIL = MONTH_APRIL,
    MAY = MONTH_MAY,
    JUNE = MONTH_JUNE,
    JULY = MONTH_JULY,
    AUGUST = MONTH_AUGUST,
    SEPTEMBER = MONTH_SEPTEMBER,
    OCTOBER = MONTH_OCTOBER,
    NOVEMBER = MONTH_NOVEMBER,
    DECEMBER = MONTH_DECEMBER,
  };

  class Date {
    CalendarGregorianDate date;

   public:
    /**
     * Creates a Gregorian date from a C Gregorian Date
     * @param date C Gregorian Date
     */
    explicit Date(CalendarGregorianDate date) noexcept : date(date) {}

    /**
     * Creates a new Gregorian Date using Year, Month, Day.
     * Note: This constructor does NOT do any data validation!
     * @see Date::is_valid for data validation
     * @see Date::nearest_valid for data sanitization
     * @param year Gregorian Year (positive for AD, negative for BC). Defaults to 0 A.D.
     * @param month Gregorian Month. Defaults to January.
     * @param day Day of the gregorian month. Defaults to 1
     */
    explicit Date(int64_t year = 0, MONTH month = MONTH::JANUARY, int16_t day = 1) noexcept : date(calendar_gregorian_create(year, static_cast<CALENDAR_GREGORIAN_MONTH>(month), day)) {}

    /**
     * Creates a new Gregorian Date using Year, Month, Day.
     * Note: This constructor does NOT do any data validation!
     * @see Date::is_valid for data validation
     * @see Date::nearest_valid for data sanitization
     * @param year Gregorian Year (positive for AD, negative for BC). Defaults to 0 A.D.
     * @param month C Gregorian Month. Defaults to January.
     * @param day Day of the gregorian month. Defaults to 1
     */
    Date(int64_t year, CALENDAR_GREGORIAN_MONTH month, int16_t day) noexcept : date(calendar_gregorian_create(year, month, day)) {}

    /**
     * Casts to a C gregorian date
     * @return
     */
    explicit operator CalendarGregorianDate() const noexcept { return date; }

    /**
     * @return Whether the current date is a valid Gregorian date
     */
    [[nodiscard]] auto is_valid() const noexcept -> bool { return calendar_gregorian_is_valid(&date); }

    /**
     * @return The closest valid Gregorian date. If the date is already valid, returns a copy of the current date.
     */
    [[nodiscard]] auto nearest_valid() const noexcept -> Date { return Date{calendar_gregorian_nearest_valid(&date)}; }

    /**
     * Does a strong three way comparison of dates
     * @param other Date to compare against
     * @return
     */
    auto operator<=>(const Date& other) const noexcept -> std::strong_ordering {
      return impl::compare_int(calendar_gregorian_compare(&date, &other.date));
    }

    /**
     * Creates a Gregorian date from an RdDate
     * @param rdDate RdDate to convert
     * @return
     */
    [[nodiscard]] static auto from_rd_date(const RdDate& rdDate) noexcept -> Date {
      return Date{calendar_gregorian_from_rd_date(static_cast<CRdDate>(rdDate))};
    }

    /**
     * Creates an RdDate from the current GregorianDate
     * @return
     */
    [[nodiscard]] auto to_rd_date() const noexcept -> RdDate { return RdDate{calendar_gregorian_to_rd_date(&date)}; }

    CALENDAR_CXX_COMPARISON_OPS(Date)

    /**
     * @return Gregorian year of date
     */
    [[nodiscard]] auto year() const noexcept -> decltype(CalendarGregorianDate::year) { return date.year; }

    /**
     * @return Gregorian month of date
     */
    [[nodiscard]] auto month() const noexcept -> MONTH { return static_cast<MONTH>(date.month); }

    /**
     * @return Gregorian day of date
     */
    [[nodiscard]] auto day() const noexcept -> decltype(CalendarGregorianDate::day) { return date.day; }

    /**
     * Returns whether a given year is a Gregorian leap year
     * @param year Gregorian year. Positive for A.D., negative for B.C.
     * @return
     */
    [[nodiscard]] static auto is_leap_year(int64_t year) noexcept -> bool { return calendar_gregorian_is_leap_year(year); }

    /**
     * Returns if the current date is a leap year
     * @return
     */
    [[nodiscard]] auto is_leap_year() const noexcept -> bool { return Date::is_leap_year(date.year); }

    /**
     * Returns the starting date (Jan 1) for the current year
     * @return
     */
    [[nodiscard]] auto year_start() const noexcept -> Date { return year_start(date.year); }

    /**
     * Returns the starting date (Jan 1) for the given Gregorian year
     * @param year Gregorian year. Positive for A.D., negative for B.C.
     * @return
     */
    [[nodiscard]] static auto year_start(int64_t year) noexcept -> Date { return Date{calendar_gregorian_year_start(year)}; }

    /**
     * Returns the ending date (Dec 31) for the current year
     * @return
     */
    [[nodiscard]] auto year_end() const noexcept -> Date { return year_end(date.year); }

    /**
     * Returns the ending date (Dec 31) for the given Gregorian year
     * @param year Gregorian year. Positive for A.D., negative for B.C.
     * @return
     */
    [[nodiscard]] static auto year_end(int64_t year) noexcept -> Date { return Date{calendar_gregorian_year_end(year)}; }

    /**
     * Returns the start and end dates for the current year
     * @return Year start and then year end
     */
    [[nodiscard]] auto year_boundaries() const noexcept -> std::tuple<Date, Date> { return year_boundaries(date.year); }

    /**
     * Returns the start and end dates for the given Gregorian year
     * @param year Gregorian year. Positive for A.D., negative for B.C.
     * @return Year start and then year end
     */
    [[nodiscard]] static auto year_boundaries(int64_t year) noexcept -> std::tuple<Date, Date> {
      return std::make_tuple(year_start(year), year_end(year));
    }

    /**
     * Returns a vector with all of the days in the current year.
     * The returned dates are all RdDates
     * @return
     */
    template<typename Allocator = std::allocator<RdDate>>
    [[nodiscard]] auto year_days() const -> std::vector<RdDate, Allocator> { return year_days<Allocator>(date.year); }

    /**
     * Returns a vector with all of the days in the given Gregorian year.
     * The returned dates are all RdDates
     * @param year Gregorian year. Positive for A.D., negative for B.C.
     * @return
     */
    template<typename Allocator = std::allocator<RdDate>>
    [[nodiscard]] static auto year_days(int64_t year) -> std::vector<RdDate, Allocator> {
      return year_start(year).to_rd_date().days_in_range<Allocator>(year_start(year + 1).to_rd_date());
    }

    /**
     * For a given gregorian date, returns the numbered day of the year.
     * For example, January 1, 2014 will return 1, while December 31, 2011 will return 365
     * @return
     */
    [[nodiscard]] auto day_number() const noexcept { return calendar_gregorian_day_number(&date); }

    /**
     * For a given gregorian date, returns the number of days remaining in the year.
     * For example, January 1, 2014 will return 364, while December 31, 2011 will return 0
     * @return
     */
    [[nodiscard]] auto days_remaining() const noexcept { return calendar_gregorian_days_remaining(&date); }

    [[nodiscard]] auto month_name() const noexcept -> std::u8string {
      return {calendar_gregorian_month_name(static_cast<CALENDAR_GREGORIAN_MONTH>(month()))};
    }

    /**
     * Returns an array of month names
     * @return
     */
    [[nodiscard]] static auto month_names() noexcept -> std::array<const char8_t*, 12>;
    /**
     * Returns the number of days in the current month
     * @return
     */
    [[nodiscard]] auto days_in_month() noexcept -> int16_t { return days_in_month(date.year, static_cast<MONTH>(date.month)); }

    /**
     * Returns the number of days in a month
     * @param year  Gregorian year
     * @param month Gregorian month
     * @return
     */
    [[nodiscard]] static auto days_in_month(int64_t year, MONTH month) noexcept -> int16_t { return calendar_gregorian_days_in_month(year, static_cast<CALENDAR_GREGORIAN_MONTH>(month)); }

    /**
     * OStream operator
     * @param os Out stream
     * @param gd Gregorian Date
     * @return
     */
    friend std::ostream& operator<<(std::ostream& os, const Date& gd) {
      os << "Gregorian<" << gd.date.year << "-" << gd.date.month << "-" << gd.date.day << ">";
      return os;
    }
    static int64_t year_from_rd(const RdDate& rdDate);
  };
}  // namespace calendars::gregorian
