#pragma once

#include <cmath>

#include "numbers.h"
#include <SI/length.h>
#include <SI/angle.h>
#include <SI/time.h>

namespace SI {
  template<typename T>
  using meter_t = metre_t<T>;

  template <typename _type>
  using days_t = time_t<_type, std::ratio<86400>>;

  template <>
  struct unit_symbol<'T', std::ratio<86400, 1>>
    : SI::detail::unit_symbol_impl<'d', 'a', 'y', 's'> {};

  template <typename _type>
  using weeks_t = time_t<_type, std::ratio<604800>>;

  template <>
  struct unit_symbol<'T', std::ratio<604800, 1>>
    : SI::detail::unit_symbol_impl<'w', 'e', 'e', 'k', 's'> {};

  inline namespace literals {
    template <char... _digits> constexpr SI::days_t<int64_t > operator""_days() {
      return SI::days_t<int64_t>{SI::detail::parsing::Number<_digits...>::value};
    }

    template <char... _digits> constexpr SI::days_t<int64_t > operator""_weeks() {
      return SI::days_t<int64_t>{SI::detail::parsing::Number<_digits...>::value};
    }

    constexpr SI::days_t<long double> operator"" _days(long double h) {
      return SI::days_t<long double>{h};
    }

    constexpr SI::weeks_t<long double> operator"" _weeks(long double h) {
      return SI::weeks_t<long double>{h};
    }
  }

  namespace Geo {
    template<typename T>
    using degree_t = SI::degree_t<T>;

    template<typename _type>
    using minute_t = angle_t<_type, std::ratio<625000000000, 2148591731740587>>;

    template<typename _type>
    using second_t = angle_t<_type, std::ratio<31250000000, 6445775195221761>>;

    inline namespace literals {
      template <char... _digits> constexpr SI::Geo::degree_t<int64_t > operator""_degree() {
        return SI::Geo::degree_t<int64_t>{SI::detail::parsing::Number<_digits...>::value};
      }

      template <char... _digits> constexpr SI::Geo::minute_t<int64_t > operator""_minutes() {
        return SI::Geo::minute_t<int64_t>{SI::detail::parsing::Number<_digits...>::value};
      }

      template <char... _digits> constexpr SI::Geo::second_t<int64_t > operator""_seconds() {
        return SI::Geo::second_t<int64_t>{SI::detail::parsing::Number<_digits...>::value};
      }

      constexpr SI::Geo::degree_t<long double> operator"" _degree(long double h) {
        return SI::Geo::degree_t<long double>{h};
      }

      constexpr SI::Geo::minute_t<long double> operator"" _minutes(long double h) {
        return SI::Geo::minute_t<long double>{h};
      }

      constexpr SI::Geo::second_t<long double> operator"" _seconds(long double h) {
        return SI::Geo::second_t<long double>{h};
      }
    }
  }

  template <>
  struct unit_symbol<'r', std::ratio<625000000000, 2148591731740587>>
      : SI::detail::unit_symbol_impl<'\''> {};

  template <>
  struct unit_symbol<'r', std::ratio<31250000000, 6445775195221761>>
      : SI::detail::unit_symbol_impl<'\'', '\''> {};
}

namespace calendars::math {
  template<typename T, typename R = T>
  R floor(T num) noexcept {
    if constexpr (std::is_integral<T>::value) {
      return static_cast<R>(num);
    }
#ifdef CALENDAR_BOOST_MODE
    else if constexpr (std::is_same_v<T, pnum_t>) {
      if (num >= 0) {
        return static_cast<R>(numerator(num) / denominator(num));
      }
      else {
        return static_cast<R>((numerator(num) + 1) / denominator(num) - 1);
      }
    }
#endif
    else {
      return static_cast<R>(std::floor(num));
    }
  }

  template<typename T, typename R = T>
  R ceil(T num) noexcept {
    if constexpr (std::is_integral<T>::value) {
      return static_cast<R>(num);
    }
#ifdef CALENDAR_BOOST_MODE
    else if constexpr (std::is_same_v<T, pnum_t>) {
      if (num >= 0) {
        return static_cast<R>((numerator(num) - 1) / denominator(num) + 1);
      }
      else {
        return static_cast<R>(numerator(num) / denominator(num));
      }
    }
#endif
    else {
      return static_cast<R>(std::ceil(num));
    }
  }

  template<typename T, typename R>
  T mod(T left, R right) noexcept {
    if constexpr (std::is_integral_v<T> && std::is_integral_v<R>) {
      return static_cast<T>(left - right * floor(static_cast<long double>(left) / static_cast<long double>(right)));
    }
    else if constexpr (std::is_integral_v<T>) {
      return static_cast<T>(left - right * floor(static_cast<R>(left) / right));
    }
    else if constexpr (std::is_integral_v<R>) {
      return static_cast<T>(left - right * floor(left / static_cast<T>(right)));
    }
    return static_cast<T>(left - right * floor(left / right));
  }

  /**
   * x mod [a..b)
   * @tparam T param type
   * @param x input
   * @param a range start
   * @param b range end
   * @return
   */
  template<typename T>
  auto mod_range(T x, int64_t a, int64_t b) noexcept -> T {
    return a == b ? x : a + mod(x - a, b - a);
  }

  /**
   * x mod [1..y]
   * @tparam T input type
   * @tparam R range type
   * @param x input
   * @param y range end
   * @return
   */
  template<typename T, typename R>
  auto amod(T x, R y) noexcept -> T {
    return static_cast<T>(y + mod(x, -y));
  }

  template<typename T, typename R = T>
  auto approx_eq(T x, R y, T epsilon = T{0.000001}) noexcept -> bool {
#ifdef CALENDAR_BOOST_MODE
    if constexpr (std::is_same_v<T, pnum_t>) {
      return (x <= y + epsilon) && (x >= y - epsilon);
    }
    else
#endif
    return (x <= y + epsilon) & (x >= y - epsilon);
  }

  inline SI::radian_t<double> sin(SI::radian_t<double> radians) {
    return SI::radian_t<double>{std::sin(radians.value())};
  }

  inline SI::radian_t<double> cos(SI::radian_t<double> radians) {
    return SI::radian_t<double>{std::cos(radians.value())};
  }

  inline SI::radian_t<double> tan(SI::radian_t<double> radians) {
    return SI::radian_t<double>{std::tan(radians.value())};
  }

  template<typename T>
  auto sign(T x) -> T {
    if (approx_eq(x, T{0})) {
      return T{0};
    }
    if (x < T{0}) {
      return T{-1};
    }
    return T{1};
  }

  inline SI::radian_t<double> arctan(double ratio) { return SI::radian_t<double>(std::atan(ratio)); }

  namespace impl {
     inline SI::radian_t<double> arctan_helper(double y, double x) {
        using namespace SI::literals;
        if (approx_eq(x, 0)) {
          return SI::degree_t<double>(sign(y) * (90._deg).value());
        }
        if (x >= 0) {
          return arctan(y / x);
        }
        return arctan(y / x) + 180._deg;
     }
  }

  inline SI::radian_t<double> arctan(double y, double x) {
    using namespace SI::literals;
    if (approx_eq(y, x)) {
      return SI::radian_t<double>(std::numeric_limits<double>::quiet_NaN());
    }
    auto v = impl::arctan_helper(y, x);
    return SI::degree_t<double>(mod(v.as<SI::degree_t<double>>().value(), (360._deg).value()));
  }
}  // namespace calendars::math
