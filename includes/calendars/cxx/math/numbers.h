#pragma once

#include <cstdint>

#define CALENDAR_NUMBER_MODE_DOUBLE 0
#define CALENDAR_NUMBER_MODE_BOOST 1

#if CALENDAR_NUMBER_MODE == CALENDAR_NUMBER_MODE_BOOST
#define CALENDAR_BOOST_MODE
#endif

#ifdef CALENDAR_BOOST_MODE

#include <boost/rational.hpp>
#include <boost/multiprecision/cpp_int.hpp>

namespace calendars::math {
  /**
   * Precise number type, used for calculations that need lots of precision for divisions
   */
  using pnum_t = boost::rational<boost::multiprecision::cpp_int>;

  /**
   * Converts precise numbers to ints. Recommended to do a ceil or a floor first for rounding purposes
   * @tparam T Resulting integer type. Defaults to int64_t
   * @param p Precise number
   * @return Integer representation of p
   */
  template<typename T = int64_t>
  auto pnum_to_int(const pnum_t& p) -> T { return (p.numerator() / p.denominator()).convert_to<T>(); }

  /**
   * Converts an integer into a precise number type
   * @tparam T Input integer type. Auto detected
   * @param i Integer to convert
   * @return Precise number representation of i
   */
  template<typename T>
  auto int_to_pnum(T i) -> pnum_t { return pnum_t{i}; }
}

#else
namespace calendars::math {
  /**
   * Precise number type, used for calculations that need lots of precision for divisions
   */
  using pnum_t = double;

  /**
   * Converts precise numbers to ints. Recommended to do a ceil or a floor first for rounding purposes
   * @tparam T Resulting integer type. Defaults to int64_t
   * @param p Precise number
   * @return Integer representation of p
   */
  template<typename T = int64_t>
  auto pnum_to_int(pnum_t p) -> T { return static_cast<T>(p); }

  /**
   * Converts an integer into a precise number type
   * @tparam T Input integer type. Auto detected
   * @param i Integer to convert
   * @return Precise number representation of i
   */
  template<typename T>
  auto int_to_pnum(T i) -> pnum_t { return static_cast<pnum_t>(i); }
}
#endif