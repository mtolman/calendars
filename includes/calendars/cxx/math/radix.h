#pragma once

#include <cinttypes>
#include <array>
#include "common.h"

namespace calendars::math {

  template<int... Nums>
  struct Radix {};

  template<typename Y, typename A>
  struct YxA;

  /**
   * Takes a Radix for year cycles and approximate number of days in the cycle, and approximates the number of days elapsed
   * @tparam YC Year cycles
   * @tparam AC Approximate days per Cycle
   * @tparam Ys List of Year cycles
   * @tparam As List of Approximate days
   */
  template<template<int...> typename YC, template<int...> typename AC, int... Ys, int... As>
  struct YxA<YC<Ys...>, AC<As...>> {
    static_assert(sizeof...(Ys) + 1 == sizeof...(As), "Invalid dimensions given; As must be 1 larger than Ys");

    /**
     * Sums all of the approximate days that have occurred in a number of years (assumes day 0 is the start of year 0)
     * @param year The year number to count
     * @return
     */
    static int64_t sum(int64_t year) {
      const std::array<int, sizeof...(Ys)> yearDivs             = {Ys...};
      const std::array<int, sizeof...(As)> segmentMultiplicands = {As...};
      std::array<int, sizeof...(Ys) + 1>   yearSegments         = {Ys...};
      for (size_t i = 0; i < yearDivs.size(); ++i) {
        yearSegments.at(yearSegments.size() - 1 - i) = static_cast<int>(math::mod(year, yearDivs.at(yearDivs.size() - 1 - i)));
        year                                         = year / yearDivs.at(yearDivs.size() - 1 - i);
      }
      yearSegments[ 0 ] = static_cast<int>(year);

      int64_t total = 0;
      for (size_t i = 0; i < yearSegments.size(); ++i) {
        total += yearSegments.at(i) * segmentMultiplicands.at(i);
      }
      return total;
    }
  };
}
