#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_JULIAN_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_JULIAN_H

/**
 * @file
 * @defgroup c_julian C - Julian
 * @brief The C - Julian group is in the headers c/calendars/julian.h. For Julian month names, use the gregorian month name methods.
 */

#include <calendars/c/calendars/gregorian.h>
#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Represents a Julian month
 * @ingroup c_julia
 */
typedef CALENDAR_GREGORIAN_MONTH CALENDAR_JULIAN_MONTH;

/**
 * Represents a Julian date
 */
typedef struct {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarJulianDate;

/**
 * Creates a julian date
 * @param year Julian year (>= 1 for A.D, < 0 for B.C)
 * @param month Julian month
 * @param day Julian day
 * @return
 */
CalendarJulianDate calendar_julian_create(int64_t year, CALENDAR_JULIAN_MONTH month, int16_t day);

/**
 * Initializes a julian date
 * @param date Date to initialize
* @param year Julian year (>= 1 for A.D, < 0 for B.C)
* @param month Julian month
* @param day Julian day
 */
void calendar_julian_init(CalendarJulianDate* date, int64_t year, CALENDAR_JULIAN_MONTH month, int16_t day);

/**
 * Compares two julian dates
 * @param left left hand operand
 * @param right right hand operand
 * @return
 */
int calendar_julian_compare(const CalendarJulianDate* left, const CalendarJulianDate* right);

/**
 * Checks if a given year is a leap year
 * @param year Year to check
 * @return
 */
bool calendar_julian_is_leap_year(int64_t year);

/**
 * Creates a Julian date from an RdDate
 * @param rdDate RdDate to convert
 * @return
 */
CalendarJulianDate calendar_julian_from_rd_date(CalendarRdDate rdDate);

/**
 * Converts a Julian date into an RdDate
 * @param julianDate Julian date to convert
 * @return
 */
CalendarRdDate calendar_julian_to_rd_date(const CalendarJulianDate* julianDate);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_JULIAN_H
