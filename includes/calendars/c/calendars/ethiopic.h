#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_ETHIOPIC_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_ETHIOPIC_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  ETHIOPIC_MASKARAM = 1,
  ETHIOPIC_TEQEMT   = 2,
  ETHIOPIC_HEDAR    = 3,
  ETHIOPIC_TAKHSAS  = 4,
  ETHIOPIC_TER      = 5,
  ETHIOPIC_YAKATIT  = 6,
  ETHIOPIC_MAGABIT  = 7,
  ETHIOPIC_MIYAZYA  = 8,
  ETHIOPIC_GENBOT   = 9,
  ETHIOPIC_SANE     = 10,
  ETHIOPIC_HAMLE    = 11,
  ETHIOPIC_NAHASE   = 12,
  ETHIOPIC_PAGUEMEN = 13
} CALENDAR_ETHIOPIC_MONTH;

typedef struct {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarEthiopicDate;

CalendarEthiopicDate calendar_ethiopic_create(int64_t year, CALENDAR_ETHIOPIC_MONTH month, int16_t day);

void calendar_ethiopic_init(CalendarEthiopicDate* date, int64_t year, CALENDAR_ETHIOPIC_MONTH month, int16_t day);

CalendarRdDate calendar_ethiopic_to_rd_date(const CalendarEthiopicDate* date);

CalendarEthiopicDate calendar_ethiopic_from_rd_date(CalendarRdDate rdDate);

int calendar_ethiopic_compare(const CalendarEthiopicDate* left, const CalendarEthiopicDate* right);

bool calendar_ethiopic_is_leap_year(int64_t year);

const char8_t* const* calendar_ethiopic_month_names();
const char8_t* calendar_ethiopic_month_name(CALENDAR_ETHIOPIC_MONTH month);

const char8_t* const* calendar_ethiopic_day_of_week_names();
const char8_t* calendar_ethiopic_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_ETHIOPIC_H
