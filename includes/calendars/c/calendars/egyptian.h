#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_EGYPTIAN_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_EGYPTIAN_H

/**
 * @file
 * @defgroup c_egyptian C - Egyptian
 * @brief The C - Egyptian group is in the headers c/calendars/egyptian.h
 */

#include "common.h"

#if __cplusplus
extern "C" {
#endif

/**
 * Represents an egyptian dte
 * @ingroup c_egyptian
 */
typedef struct {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarEgyptianDate;

/**
 * Creates a new egyptian date
 * @ingroup c_egyptian
 * @param year Egyptian year
 * @param month Egyptian month (1-13)
 * @param day Egyptian day (1-30)
 * @return
 */
CalendarEgyptianDate calendar_egyptian_create(int64_t year, int16_t month, int16_t day);

/**
 * Initializes an egyptian date
 * @ingroup c_egyptian
 * @param date Date to initialize
 * @param year Egyptian year
 * @param month Egyptian month
 * @param day Egyptian day
 */
void calendar_egyptian_init(CalendarEgyptianDate* date, int64_t year, int16_t month, int16_t day);

/**
 * Compares egyptian dates
 * @ingroup c_egyptian
 * @param left Left hand operand
 * @param right Right hand operand
 * @return
 */
int calendar_egyptian_compare(const CalendarEgyptianDate* left, const CalendarEgyptianDate* right);

/**
 * Returns the nearest valid egyptian date. If the provided date is valid, returns a copy
 * @ingroup c_egyptian
 * @param date Egyptian date
 * @return
 */
CalendarEgyptianDate calendar_egyptian_nearest_valid(const CalendarEgyptianDate* date);

/**
 * Returns whether the egyptian date is valid
 * @ingroup c_egyptian
 * @param date Date to check
 * @return
 */
bool calendar_egyptian_is_valid(const CalendarEgyptianDate* date);

/**
 * Creates an egyptian date from an RdDate
 * @ingroup c_egyptian
 * @param rdDate RdDate to convert
 * @return
 */
CalendarEgyptianDate calendar_egyptian_from_rd_date(CalendarRdDate rdDate);

/**
 * Creates an RdDate from an Egyptian date
 * @ingroup c_egyptian
 * @param date Date to convert
 * @return
 */
CalendarRdDate calendar_egyptian_to_rd_date(const CalendarEgyptianDate* date);

const char8_t* const* calendar_egyptian_month_names();

extern const int calendar_egyptian_num_month_names;

const char8_t* calendar_egyptian_month_name(int16_t month);

#if __cplusplus
};
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_EGYPTIAN_H
