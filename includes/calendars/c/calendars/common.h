#ifndef C_CALENDAR_LIBRARY_COMMONS_COMMON_H
#define C_CALENDAR_LIBRARY_COMMONS_COMMON_H

/**
 * @file
 * @defgroup c_common C - Common
 * @brief Common date operations as well as holds the common
 */

#if __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#include <stdbool.h>
typedef unsigned char char8_t;
#endif

#if __cplusplus
extern "C" {
#else
#endif

/**
 * Typedef for an RdDate (counts number of days since Jan 1, 1 A.D. Gregorian)
 * @ingroup c_common
 */
typedef int64_t CalendarRdDate;

/**
 * Represents a value which may or may not be an RdDate
 * An RdDate is only present if is_present is true
 */
typedef struct {
  bool is_present;
  CalendarRdDate date;
} CalendarRdDateOptional;

// TODO: Add day of week names

/**
 * Enum class for each day of the week.
 * Days of week start with Sunday and run through Saturday.
 * Additionally, the days of the week are a 0-based index.
 * This means that DAY_OF_WEEK_SUNDAY = 0, MONDAY = 1, ...
* @ingroup c_common
 */
typedef enum {
  DAY_OF_WEEK_SUNDAY = 0,
  DAY_OF_WEEK_MONDAY    = 1,
  DAY_OF_WEEK_TUESDAY   = 2,
  DAY_OF_WEEK_WEDNESDAY = 3,
  DAY_OF_WEEK_THURSDAY  = 4,
  DAY_OF_WEEK_FRIDAY    = 5,
  DAY_OF_WEEK_SATURDAY  = 6
} CALENDAR_DAY_OF_WEEK;

const char8_t* const* calendar_day_of_week_names();
const char8_t* calendar_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek);
extern const int calendar_num_day_of_week_names;

/**
 * Returns the day of week for an RdDate
* @ingroup c_common
 * @param date
 * @return
 */
CALENDAR_DAY_OF_WEEK calendar_day_of_week(CalendarRdDate date);

/**
 * Gets the day of an m-cycle represented by the provided date
 * The number returned will be from [0,m)
 *
* @ingroup c_common
 * @param date The RdDate to use
 * @param m The number of days in the cycle (e.g. for day of the week, set m to 7)
 * @param offset The offset of the cycle (day of the week has an offset of 0)
 * @return An integer from [0,m) representing the day in the m-cycle
 */
int calendar_day_of_m_cycle(CalendarRdDate date, int m, int offset);

/**
 * Finds the first date on or before the provided date that occurs on the target day of the week
 * For example, if the date was 1, then
 * day_of_week_on_or_before(CALENDAR_DAY_OF_WEEK::SUNDAY) would return Date{0}. If the current date was
 * 8, then day_of_week_on_or_before(CALENDAR_DAY_OF_WEEK::MONDAY) would return 8
 *
 * @ingroup c_common
 * @param date The RdDate to use
 * @param dayOfWeek Day of the week to look for
 * @return An Date representing the day of the week
 */
CalendarRdDate calendar_day_of_week_on_or_before(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek);

/**
 * Finds the first date on or after the provided date that occurs on the target day of the week
 *
 * @ingroup c_common
 * @param date The RdDate to use
 * @param dayOfWeek Day of the week to look for
 * @return An Date representing the day of the week
 */
CalendarRdDate calendar_day_of_week_on_or_after(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek);
#define calendar_kday_on_or_after calendar_day_of_week_on_or_before

/**
 * Finds the first date nearest the provided date that occurs on the target day of the week
 *
 * @ingroup c_common
 * @param date The RdDate to use
 * @param dayOfWeek Day of the week to look for
 * @return An Date representing the day of the week
 */
CalendarRdDate calendar_day_of_week_nearest(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek);
#define calendar_kday_nearest calendar_day_of_week_on_or_before

/**
 * Finds the first date before the provided date that occurs on the target day of the week
 *
 * @ingroup c_common
 * @param date The RdDate to use
 * @param dayOfWeek Day of the week to look for
 * @return An Date representing the day of the week
 */
CalendarRdDate calendar_day_of_week_before(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek);
#define calendar_kday_before calendar_day_of_week_on_or_before

/**
 * Finds the first date after the provided date that occurs on the target day of the week
 *
 * @ingroup c_common
 * @param date The RdDate to use
 * @param dayOfWeek Day of the week to look for
 * @return An Date representing the day of the week
 */
CalendarRdDate calendar_day_of_week_after(CalendarRdDate date, CALENDAR_DAY_OF_WEEK dayOfWeek);
#define calendar_kday_after calendar_day_of_week_on_or_after

/**
 * Finds the kth day of the m-cycle that occurs on or before the current date
 *
* @ingroup c_common
 * @param date The RdDate to use
 * @param k The kth day of the m-cycle
 * @param m The number of days in the m-cycle
 * @param offset The offset of the m-cycle
 * @return An Date with the result
 */
CalendarRdDate calendar_kth_day_of_m_cycle_on_or_before(CalendarRdDate date, int k, int m, int offset);

/**
 * Finds the kth day of the m-cycle that occurs before the current date
 *
* @ingroup c_common
 * @param date The RdDate to use
 * @param k The kth day of the m-cycle
 * @param m The number of days in the m-cycle
 * @param offset The offset of the m-cycle
 * @return An Date with the result
 */
CalendarRdDate calendar_kth_day_of_m_cycle_before(CalendarRdDate date, int k, int m, int offset);

/**
 * Finds the kth day of the m-cycle that occurs after the current date
 *
* @ingroup c_common
 * @param date The RdDate to use
 * @param k The kth day of the m-cycle
 * @param m The number of days in the m-cycle
 * @param offset The offset of the m-cycle
 * @return An Date with the result
 */
CalendarRdDate calendar_kth_day_of_m_cycle_after(CalendarRdDate date, int k, int m, int offset);

/**
 * Finds the kth day of the m-cycle that occurs on or after the current date
 *
* @ingroup c_common
 * @param date The RdDate to use
 * @param k The kth day of the m-cycle
 * @param m The number of days in the m-cycle
 * @param offset The offset of the m-cycle
 * @return An Date with the result
 */
CalendarRdDate calendar_kth_day_of_m_cycle_on_or_after(CalendarRdDate date, int k, int m, int offset);

/**
 * Finds the kth day of the m-cycle that occurs nearest the current date
 *
* @ingroup c_common
 * @param date The RdDate to use
 * @param k The kth day of the m-cycle
 * @param m The number of days in the m-cycle
 * @param offset The offset of the m-cycle
 * @return An Date with the result
 */
CalendarRdDate calendar_kth_day_of_m_cycle_nearest(CalendarRdDate date, int k, int m, int offset);

/**
 * Returns the nth occurrence of a day on a given week before the current date (or if n is
 * negative after d) If n is zero, it will return the current date instead
 *
 * @ingroup c_common
 * @param date The RdDate to use
 * @param n The number of the occurrence before the current date (or if negative, the number of
 * the occurrence after)
 * @param k The week day to search for
 * @return
 */
CalendarRdDate calendar_nth_week_day(CalendarRdDate date, int n, CALENDAR_DAY_OF_WEEK k);

CalendarRdDate calendar_first_week_day(CALENDAR_DAY_OF_WEEK k, CalendarRdDate d);

CalendarRdDate calendar_last_week_day(CALENDAR_DAY_OF_WEEK k, CalendarRdDate d);

#if __cplusplus
};
#endif

#endif  // C_CALENDAR_LIBRARY_COMMONS_COMMON_H
