#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_AKAN_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_AKAN_H

#include <calendars/c/calendars/common.h>

/**
 * @file
 * @defgroup c_aakn C - Akan
 * @brief Akan day counts for inside a cycle. Does not track which cycle it is on
 */

#if __cplusplus
extern "C" {
#endif

typedef struct CalendarAkanDayCount {
  int prefix;
  int stem;
} CalendarAkanDate;

/**
 * The number of cycles in the Akan day count
 */
extern const int calendar_akan_day_count_cycle_length;

/**
 * Creates a new Akan day count
 * @param prefix Akan prefix
 * @param stem Akan stem
 * @return
 */
CalendarAkanDate calendar_akan_day_count_create(int prefix, int stem);

/**
 * Initializes a new Akan day count
 * @param date Akan date to initialize
 * @param prefix Akan prefix
 * @param stem Akan stem
 */
void calendar_akan_day_count_init(CalendarAkanDate* date, int prefix, int stem);

/**
 * Creates an Akan day count from an RdDate
 * @param rdDate RdDate to convert
 * @return
 */
CalendarAkanDate calendar_akan_day_count_from_rd_date(CalendarRdDate rdDate);

/**
 * Compares two akan day counts
 * @param left Left hand operand
 * @param right Right hand operand
 * @return
 */
int calendar_akan_day_count_compare(const CalendarAkanDate* left, const CalendarAkanDate* right);

/**
 * Returns the RdDate either before or on the provided RdDate which has an Akan day count equal to the provided day count
 * @param rdDate RdDate Absolute point in time to use as a reference
 * @param akanDayCount Desired Akan day count
 * @return
 */
CalendarRdDate calendar_akan_day_count_on_or_before(CalendarRdDate rdDate, const CalendarAkanDate* akanDayCount);

/**
 * Returns the difference between two akan day counts
 * @param left Left hand operand
 * @param right Right hand operand
 * @return
 */
int calendar_akan_day_count_difference(const CalendarAkanDate* left, const CalendarAkanDate* right);

const char8_t* const * calendar_akan_prefix_names();
extern const int calendar_akan_num_prefix_names;
const char8_t* calendar_akan_prefix_name(int index);
const char8_t* const * calendar_akan_stem_names();
extern const int calendar_akan_num_stem_names;
const char8_t* calendar_akan_stem_name(int index);

#if __cplusplus
};
#endif

#endif  // C_CALENDAR_LIBRARY_DATE_TYPE_AKAN_H
