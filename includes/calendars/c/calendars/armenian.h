#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_ARMENIAN_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_ARMENIAN_H

/**
 * @file
 * @defgroup c_armenian C - Armenian
 * @brief The C - Armenian group is in the headers c/calendars/armenian.h
 */

#include "common.h"

#if __cplusplus
extern "C" {
#endif

/**
 * Represents an armenian dte
 * @ingroup c_armenian
 */
typedef struct {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarArmenianDate;

/**
 * Creates a new armenian date
 * @ingroup c_armenian
 * @param year Armenian year
 * @param month Armenian month (1-13)
 * @param day Armenian day (1-30)
 * @return
 */
CalendarArmenianDate calendar_armenian_create(int64_t year, int16_t month, int16_t day);

/**
 * Initializes an armenian date
 * @ingroup c_armenian
 * @param date Date to initialize
 * @param year Armenian year
 * @param month Armenian month
 * @param day Armenian day
 */
void calendar_armenian_init(CalendarArmenianDate* date, int64_t year, int16_t month, int16_t day);

/**
 * Compares armenian dates
 * @ingroup c_armenian
 * @param left Left hand operand
 * @param right Right hand operand
 * @return
 */
int calendar_armenian_compare(const CalendarArmenianDate* left, const CalendarArmenianDate* right);

/**
 * Returns the nearest valid armenian date. If the provided date is valid, returns a copy
 * @ingroup c_armenian
 * @param date Armenian date
 * @return
 */
CalendarArmenianDate calendar_armenian_nearest_valid(const CalendarArmenianDate* date);

/**
 * Returns whether the armenian date is valid
 * @ingroup c_armenian
 * @param date Date to check
 * @return
 */
bool calendar_armenian_is_valid(const CalendarArmenianDate* date);

/**
 * Creates an armenian date from an RdDate
 * @ingroup c_armenian
 * @param rdDate RdDate to convert
 * @return
 */
CalendarArmenianDate calendar_armenian_from_rd_date(CalendarRdDate rdDate);

/**
 * Creates an RdDate from an Armenian date
 * @ingroup c_armenian
 * @param date Date to convert
 * @return
 */
CalendarRdDate calendar_armenian_to_rd_date(const CalendarArmenianDate* date);

const char8_t* const* calendar_armenian_month_names();

extern const int calendar_armenian_num_month_names;

const char8_t* calendar_armenian_month_name(int16_t month);

#if __cplusplus
};
#endif

#endif  // C_CALENDAR_LIBRARY_DATE_TYPE_ARMENIAN_H
