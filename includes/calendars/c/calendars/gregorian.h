#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_GREGORIAN
#define C_CALENDAR_LIBRARY_DATE_TYPE_GREGORIAN

/**
 * @file
 * @defgroup c_gregorian C - Gregorian
 * @brief The C - Gregorian group is in the headers c/calendars/gregorian.h
 */


#include "common.h"

#if __cplusplus
extern "C" {
#endif

/**
 * Represents a Gregorian month
 * @ingroup c_gregorian
 */
typedef enum {
  MONTH_JANUARY = 1,
  MONTH_FEBRUARY = 2,
  MONTH_MARCH = 3,
  MONTH_APRIL = 4,
  MONTH_MAY = 5,
  MONTH_JUNE = 6,
  MONTH_JULY = 7,
  MONTH_AUGUST = 8,
  MONTH_SEPTEMBER = 9,
  MONTH_OCTOBER = 10,
  MONTH_NOVEMBER = 11,
  MONTH_DECEMBER = 12,
} CALENDAR_GREGORIAN_MONTH;


/**
 * Represents a Gregorian date (year, month, day)
 * @ingroup c_gregorian
 */
typedef struct {
  int64_t year;
  int16_t month; // GREGORIAN_MONTH
  int16_t day;
} CalendarGregorianDate;


/**
 * Creates a new gregorian date
 *
 * @ingroup c_gregorian
 * @param year Year of the date. Positive values are A.D. negative values are B.C.
 * @param month Month of the date (1-12, or CALENDAR_GREGORIAN_MONTH)
 * @param day Day of the month (1-31)
 * @return A new CalendarGregorianDate
 */
CalendarGregorianDate calendar_gregorian_create(int64_t year, CALENDAR_GREGORIAN_MONTH month, int16_t day);

/**
 * Initializes an existing gregorian date
 *
 * @ingroup c_gregorian
 * @param date Pointer to date being initialized
 * @param year Year of the date. Positive values are A.D. negative values are B.C.
 * @param month Month of the date (1-12, or CALENDAR_GREGORIAN_MONTH)
 * @param day Day of the month (1-31)
 */
void calendar_gregorian_init(CalendarGregorianDate* date, int64_t year, CALENDAR_GREGORIAN_MONTH month, int16_t day);

/**
 * Does a three-way comparison on two gregorian dates. Returns a negative value, 0, or a positive value.
 * A value less than 0 indicates "left < right".
 * A value greater than 0 indicates "left > right".
 * A 0 indicates "left == right".
 *
 * @ingroup c_gregorian
 * @param left Left-hand value for comparison.
 * @param right Right-hand value for comparison.
 */
int calendar_gregorian_compare(const CalendarGregorianDate* left, const CalendarGregorianDate* right);

/**
 * Converts a gregorian date to the nearest valid gregorian date. For example, 2021-02-29 would become 2021-03-01.
 * Valid dates are not affected.
 * Useful for fixing invalid data.
 * @param date Potentially invalid date
 * @return Valid gregorian date
 */
CalendarGregorianDate calendar_gregorian_nearest_valid(const CalendarGregorianDate* date);

/**
 * Checks whether a gregorian date is valid
 * @param date Date  check
 * @return
 */
bool calendar_gregorian_is_valid(const CalendarGregorianDate* date);

/**
 * Returns whether a year is a leap year on the gregorian calendar.
 *
 * @ingroup c_gregorian
 * @param year Year to check for leep-year quality
 * @return True if it is a leap year, false otherwise
 */
bool calendar_gregorian_is_leap_year(int64_t year);

/**
 * Returns the date representing the start of a gregorian year
 *
 * @ingroup c_gregorian
 * @param year Gregorian year
 * @return Gregorian date for the start of the year
 */
CalendarGregorianDate calendar_gregorian_year_start(int64_t year);

/**
 * Returns the date representing the end of a gregorian year
 *
 * @ingroup c_gregorian
 * @param year Gregorian year
 * @return Gregorian date for the end of the year
 */
CalendarGregorianDate calendar_gregorian_year_end(int64_t year);

/**
 * Gets the number of days different between two gregorian dates.
 * Equivalent to `calendar_to_rd_date_gregorian(*left) - calendar_gregorian_to_rd_date(*right)`.
 *
 * @ingroup c_gregorian
 * @param left The left-hand date for the difference
 * @param right The right-hand date for the difference
 * @return The difference in days. A negative value indicates that the left date is smaller than the right date.
 */
int64_t calendar_gregorian_day_difference(const CalendarGregorianDate* left, const CalendarGregorianDate* right);

/**
 * For a given gregorian date, returns the numbered day of the year.
 * For example, January 1, 2014 will return 1, while December 31, 2011 will return 365
 *
 * @ingroup c_gregorian
 * @param date Date to use
 * @return
 */
int64_t calendar_gregorian_day_number(const CalendarGregorianDate* date);

/**
 * For a given gregorian month and year, returns the number of days in that month.
 *
 * @ingroup c_gregorian
 * @param date Date to use
 * @return
 */
int16_t calendar_gregorian_days_in_month(int64_t year, CALENDAR_GREGORIAN_MONTH month);

/**
 * For a given gregorian date, returns the number of days remaining in the year.
 * For example, January 1, 2014 will return 364, while December 31, 2011 will return 0
 *
 * @ingroup c_gregorian
 * @param date Date to use
 * @return
 */
int64_t calendar_gregorian_days_remaining(const CalendarGregorianDate* date);

/**
 * Converts a CalendarRdDate into a CalendarGregorianDate
 *
 * @ingroup c_gregorian
 * @param date Date to use
 * @return
 */
CalendarGregorianDate calendar_gregorian_from_rd_date(CalendarRdDate date);

/**
 * Converts a CalendarGregorianDate into a CalendarRdDate
 *
 * @ingroup c_gregorian
 * @param date Date to use
 * @return
 */
CalendarRdDate calendar_gregorian_to_rd_date(const CalendarGregorianDate* date);

const char8_t* const* calendar_gregorian_month_names();
extern const int calendar_gregorian_num_month_names;
const char8_t* calendar_gregorian_month_name(CALENDAR_GREGORIAN_MONTH month);

#if __cplusplus
};
#endif

#endif  // C_CALENDAR_LIBRARY_DATE_TYPE_GREGORIAN
