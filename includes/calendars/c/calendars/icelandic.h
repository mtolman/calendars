#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_ICELANDIC_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_ICELANDIC_H

#include <calendars/c/calendars/common.h>

#if __cplusplus
extern "C" {
#endif

typedef enum {
  ICELANDIC_SEASON_SPRING = 0,
  ICELANDIC_SEASON_SUMMER = 90,
  ICELANDIC_SEASON_AUTUMN = 180,
  ICELANDIC_SEASON_WINTER = 270,
} CALENDAR_ICELANDIC_SEASON;

typedef enum {
  ICELANDIC_MONTH_HARPA,
  ICELANDIC_MONTH_SKERPLA,
  ICELANDIC_MONTH_SOLMANUDUR,
  ICELANDIC_MONTH_HEYANNIR,
  ICELANDIC_MONTH_TVIMANUDUR,
  ICELANDIC_MONTH_HAUSTMANUDUR,
  ICELANDIC_MONTH_GORMANUDUR,
  ICELANDIC_MONTH_YLIR,
  ICELANDIC_MONTH_MORSUGUR,
  ICELANDIC_MONTH_THORRI,
  ICELANDIC_MONTH_GOA,
  ICELANDIC_MONTH_EINMANUDUR,
} CALENDAR_ICELANDIC_MONTH;

typedef struct CalendarIcelandicDate {
  int64_t year;
  int16_t season;
  int8_t week;
  int8_t weekday;
} CalendarIcelandicDate;

CalendarIcelandicDate calendar_icelandic_create(int64_t year, CALENDAR_ICELANDIC_SEASON season, int8_t week, CALENDAR_DAY_OF_WEEK weekday);

void calendar_icelandic_init(CalendarIcelandicDate* date, int64_t year, CALENDAR_ICELANDIC_SEASON season, int8_t week, CALENDAR_DAY_OF_WEEK weekday);

CalendarRdDate calendar_icelandic_summer(int64_t year);

CalendarRdDate calendar_icelandic_winter(int64_t year);

bool calendar_icelandic_is_leap_year(int64_t year);

CALENDAR_ICELANDIC_MONTH calendar_icelandic_month(const CalendarIcelandicDate* date);

int calendar_icelandic_compare(const CalendarIcelandicDate* left, const CalendarIcelandicDate* right);

CalendarIcelandicDate calendar_icelandic_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_icelandic_to_rd_date(const CalendarIcelandicDate* date);

const char8_t* const* calendar_icelandic_day_of_week_names();
const char8_t* calendar_icelandic_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek);

#if __cplusplus
};
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_ICELANDIC_H
