#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_OLD_HINDU_LUNI_SOLAR_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_OLD_HINDU_LUNI_SOLAR_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

// TODO: Add sanskrit names

typedef enum
{
  OLD_HINDU_LUNI_SOLAR_CAITRA,
  OLD_HINDU_LUNI_SOLAR_VAISAKHA,
  OLD_HINDU_LUNI_SOLAR_JYESTHA,
  OLD_HINDU_LUNI_SOLAR_ASADHA,
  OLD_HINDU_LUNI_SOLAR_SRAVANA,
  OLD_HINDU_LUNI_SOLAR_BHADRAPADA,
  OLD_HINDU_LUNI_SOLAR_ASVINA,
  OLD_HINDU_LUNI_SOLAR_KARTIKA,
  OLD_HINDU_LUNI_SOLAR_MARGASIRSA,
  OLD_HINDU_LUNI_SOLAR_PAUSA,
  OLD_HINDU_LUNI_SOLAR_MAGHA,
  OLD_HINDU_LUNI_SOLAR_PHALGUNA
} CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT;

typedef struct CalendarOldHinduLuniSolarDate {
  int64_t year;
  int16_t sanskrit;
  bool isLeapMonth;
  int16_t day;
} CalendarOldHinduLuniSolarDate;

CalendarOldHinduLuniSolarDate calendar_old_hindu_luni_solar_create(int64_t year,
                                                                   CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT month, bool isLeapMonth, int16_t day);

void calendar_old_hindu_luni_solar_init(CalendarOldHinduLuniSolarDate* date, int64_t year, CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT month, bool isLeapMonth, int16_t day);

int calendar_old_hindu_luni_solar_compare(const CalendarOldHinduLuniSolarDate* left, const CalendarOldHinduLuniSolarDate* right);

bool calendar_old_hindu_luni_solar_is_leap_year(int64_t year);

CalendarOldHinduLuniSolarDate calendar_old_hindu_luni_solar_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_old_hindu_luni_solar_to_rd_date(const CalendarOldHinduLuniSolarDate* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_OLD_HINDU_LUNI_SOLAR_H
