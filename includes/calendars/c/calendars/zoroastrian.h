#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_ZOROASTRIAN_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_ZOROASTRIAN_H

#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarZoroastrianDate {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarZoroastrianDate;

CalendarZoroastrianDate calendar_zoroastrian_create(int64_t year, int16_t month, int16_t day);

void calendar_zoroastrian_init(CalendarZoroastrianDate* date, int64_t year, int16_t month, int16_t day);

int calendar_zoroastrian_compare(const CalendarZoroastrianDate* left, const CalendarZoroastrianDate* right);

CalendarZoroastrianDate calendar_zoroastrian_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_zoroastrian_to_rd_date(const CalendarZoroastrianDate* date);

const char8_t *const *calendar_zoroastrian_month_names();
const char8_t        *calendar_zoroastrian_month_name(int16_t month);
extern const int calendar_zoroastrian_num_month_names;

const char8_t *const *calendar_zoroastrian_day_names();
const char8_t        *calendar_zoroastrian_day_name(int16_t day);
extern const int calendar_zoroastrian_num_day_names;

const char8_t *const *calendar_zoroastrian_epagomanae_day_names();
const char8_t        *calendar_zoroastrian_epagomanae_day_name(int16_t day);
extern const int calendar_zoroastrian_num_epagomanae_day_names;

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_ZOROASTRIAN_H
