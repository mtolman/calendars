#ifndef C_CALENDAR_LIBRARY_HOLIDAYS_USA_H
#define C_CALENDAR_LIBRARY_HOLIDAYS_USA_H

#include <calendars/c/calendars/gregorian.h>

#ifdef __cplusplus
extern "C" {
#endif

CalendarGregorianDate calendar_holidays_usa_independence_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_labor_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_memorial_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_martin_luther_king_jr_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_election_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_thanksgiving(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_black_friday(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_new_years(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_new_years_eve(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_presidents_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_juneteenth(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_columbus_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_indigenous_peoples_day(int gregorianYear);
CalendarGregorianDate calendar_holidays_usa_veterans_day(int gregorianYear);

#ifdef __cplusplus
}
#endif

#endif  // C_CALENDAR_LIBRARY_HOLIDAYS_USA_H
