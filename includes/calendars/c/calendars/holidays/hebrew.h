#ifndef C_CALENDAR_LIBRARY_HOLIDAYS_HEBREW_H
#define C_CALENDAR_LIBRARY_HOLIDAYS_HEBREW_H

#include <calendars/c/calendars/hebrew.h>
#include "common.h"

#if __cplusplus
extern "C" {
#endif

CalendarHebrewDate calendar_holidays_hebrew_yom_kippur(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_rosh_ha_shanah(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_sukkot(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_hoshana_rabba(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_shemini_azeret(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_simhat_torah(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_passover(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_passover_end(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_shavout(int64_t hebrewYear);

void calendar_holidays_hebrew_sukkot_intermediate_days(CalendarHolidayArray** outPointer, int64_t hebrewYear);
void calendar_holidays_hebrew_sukkot_intermediate_days_alloc_with(CalendarHolidayArray** outPointer, int64_t hebrewYear, CalendarAllocator allocator);

void calendar_holidays_hebrew_passover_days(CalendarHolidayArray** outPointer, int64_t hebrewYear);
void calendar_holidays_hebrew_passover_days_alloc_with(CalendarHolidayArray** outPointer, int64_t hebrewYear, CalendarAllocator allocator);

void calendar_holidays_hebrew_hanukkah_days(CalendarHolidayArray** outPointer, int64_t hebrewYear);
void calendar_holidays_hebrew_hanukkah_days_alloc_with(CalendarHolidayArray** outPointer, int64_t hebrewYear, CalendarAllocator allocator);

CalendarHebrewDate calendar_holidays_hebrew_tu_b_shevat(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_purim(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_ta_anit_esther(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_tishah_be_av(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_tzom_gedaliah(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_tzom_tammuz(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_yom_ha_shoah(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_yom_ha_zikkaron(int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_birthday(const CalendarHebrewDate* birthDate, int64_t hebrewYear);
CalendarHebrewDate calendar_holidays_hebrew_yahrzeit(const CalendarHebrewDate* deathDate, int64_t hebrewYear);

bool calendar_holidays_hebrew_omer(CalendarRdDate rdDate, int64_t* out);

#if __cplusplus
};
#endif

#endif //C_CALENDAR_LIBRARY_HOLIDAYS_HEBREW_H
