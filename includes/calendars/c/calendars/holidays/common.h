#ifndef C_CALENDAR_LIBRARY_HOLIDAYS_COMMON_H
#define C_CALENDAR_LIBRARY_HOLIDAYS_COMMON_H

#include <calendars/c/calendars/common.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarHolidayArray {
  CalendarRdDate* dates;
  int size;
} CalendarHolidayArray;

typedef void* (* CalendarAllocator) (size_t);
typedef void (* CalendarDeallocator) (void*);

void calendar_holiday_array_alloc(CalendarHolidayArray** outPointer, int size);
void calendar_holiday_array_alloc_with(CalendarHolidayArray** outPointer, int size, CalendarAllocator allocator);
void calendar_holiday_array_free(CalendarHolidayArray** array);
void calendar_holiday_array_free_with(CalendarHolidayArray** array, CalendarDeallocator deallocator);

#ifdef __cplusplus
}
#endif

#endif  // C_CALENDAR_LIBRARY_DATE_TYPE_COMMON_H
