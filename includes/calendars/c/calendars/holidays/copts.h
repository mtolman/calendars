#ifndef C_CALENDAR_LIBRARY_HOLIDAYS_COPTS_H
#define C_CALENDAR_LIBRARY_HOLIDAYS_COPTS_H

#include <calendars/c/calendars/coptic.h>

#ifdef __cplusplus
extern "C" {
#endif

CalendarCopticDate calendar_holidays_copts_christmas(int64_t copticYear);
CalendarCopticDate calendar_holidays_copts_building_of_the_cross(int64_t copticYear);
CalendarCopticDate calendar_holidays_copts_jesus_circumcision(int64_t copticYear);
CalendarCopticDate calendar_holidays_copts_epiphany(int64_t copticYear);
CalendarCopticDate calendar_holidays_copts_marys_announcement(int64_t copticYear);
CalendarCopticDate calendar_holidays_copts_jesus_transfiguration(int64_t copticYear);

#ifdef __cplusplus
}
#endif

#endif  // C_CALENDAR_LIBRARY_HOLIDAYS_COPTS_H
