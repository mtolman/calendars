#ifndef C_CALENDAR_LIBRARY_HOLIDAYS_ARMENIAN_CHURCH_H
#define C_CALENDAR_LIBRARY_HOLIDAYS_ARMENIAN_CHURCH_H

#include <calendars/c/calendars/julian.h>
#include <calendars/c/calendars/gregorian.h>

#ifdef __cplusplus
extern "C" {
#endif

CalendarJulianDate calendar_holidays_armenian_church_christmas_jerusalem(int64_t julianYear);
CalendarGregorianDate calendar_holidays_armenian_church_christmas(int64_t gregorianYear);

#ifdef __cplusplus
}
#endif

#endif  // C_CALENDAR_LIBRARY_HOLIDAYS_ARMENIAN_CHURCH_H
