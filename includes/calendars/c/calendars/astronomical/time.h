#ifndef CALENDARS_TIME_H
#define CALENDARS_TIME_H

#include "geography.h"
#include <calendars/c/calendars/common.h>
#include <calendars/c/calendars/moment.h>
#include <calendars/c/calendars/gregorian.h>

/**
 * NOTE: The timezones derived here are geographic means rather than political boundaries
 */

#if __cplusplus
extern "C" {
#endif
  typedef struct CalendarTimeHM {
    int hours;
    int minutes;
  } CalendarTimeHM;

  CalendarTimeHM calendar_time_from_day_fraction(double dayFraction);
  double calendar_time_from_hours_minutes(CalendarTimeHM hoursMinutes);
  CalendarTimeHM calendar_time_hm_create(int hours, int minutes);

  CalendarTimeHM add_hours(CalendarTimeHM, int hours);
  CalendarTimeHM add_minutes(CalendarTimeHM, int minutes);
  CalendarTimeHM sub_hours(CalendarTimeHM, int hours);
  CalendarTimeHM sub_minutes(CalendarTimeHM, int minutes);

  typedef struct CalendarAstroTimezone {
    double offset;
  } CalendarAstroTimezone;

  CalendarAstroTimezone zone_from_longitude(double longitude);

  typedef struct CalendarAstroUniversalDateTime {
    double dateTime;
  } CalendarAstroUniversalDateTime;

  typedef struct CalendarAstroTimezoneDateTime {
    double dateTime;
    CalendarAstroTimezone timezone;
  } CalendarAstroTimezoneDateTime;
  typedef CalendarAstroTimezoneDateTime CalendarAstroStandardTime;

  typedef struct CalendarAstroLocalDateTime {
    double dateTime;
    CalendarEarthPos location;
  } CalendarAstroLocalDateTime;

  typedef struct CalendarAstroDynamicalDateTime {
    double dateTime;
  } CalendarAstroDynamicalDateTime;

  CalendarAstroUniversalDateTime calendar_astro_timezone_to_universal(CalendarAstroTimezoneDateTime timezoneTime);
  CalendarAstroUniversalDateTime calendar_astro_local_to_universal(CalendarAstroLocalDateTime localTime);
  CalendarAstroUniversalDateTime calendar_astro_dynamical_to_universal(CalendarAstroDynamicalDateTime localTime);

  CalendarAstroLocalDateTime calendar_astro_timezone_to_local(CalendarAstroTimezoneDateTime timezoneTime, CalendarEarthPos location);
  CalendarAstroLocalDateTime calendar_astro_universal_to_local(CalendarAstroUniversalDateTime universalTime, CalendarEarthPos location);
  CalendarAstroLocalDateTime calendar_astro_dynamical_to_local(CalendarAstroDynamicalDateTime dateTime, CalendarEarthPos location);

  CalendarAstroTimezoneDateTime calendar_astro_local_to_timezone(CalendarAstroLocalDateTime localTime, CalendarAstroTimezone timezone);
  CalendarAstroTimezoneDateTime calendar_astro_universal_to_timezone(CalendarAstroUniversalDateTime universalTime, CalendarAstroTimezone timezone);
  CalendarAstroTimezoneDateTime calendar_astro_dynamical_to_timezone(CalendarAstroDynamicalDateTime dateTime, CalendarAstroTimezone timezone);

  CalendarAstroDynamicalDateTime calendar_astro_local_to_dynamical(CalendarAstroLocalDateTime localTime);
  CalendarAstroDynamicalDateTime calendar_astro_universal_to_dynamical(CalendarAstroUniversalDateTime universalTime);
  CalendarAstroDynamicalDateTime calendar_astro_timezone_to_dynamical(CalendarAstroTimezoneDateTime dateTime);

  CalendarMoment calendar_astro_universal_to_moment(CalendarAstroUniversalDateTime dateTime);
  CalendarMoment calendar_astro_timezone_to_moment(CalendarAstroTimezoneDateTime dateTime);
  CalendarMoment calendar_astro_local_to_moment(CalendarAstroLocalDateTime dateTime);
  CalendarMoment calendar_astro_dynamical_to_moment(CalendarAstroDynamicalDateTime dateTime);

  CalendarAstroUniversalDateTime calendar_astro_universal_from_moment(CalendarMoment moment);
  CalendarAstroDynamicalDateTime calendar_astro_dynamical_from_moment(CalendarMoment moment);

  double calendar_astro_ephemeris_correction(CalendarRdDate date);
  double calendar_astro_ephemeris_correction_gregorian(CalendarGregorianDate date);

  double calendar_astro_equation_of_time(CalendarAstroUniversalDateTime t);

  double obliquity(CalendarAstroUniversalDateTime t);
#if __cplusplus
};
#endif

#endif //CALENDARS_TIME_H
