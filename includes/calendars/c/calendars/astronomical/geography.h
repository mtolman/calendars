#ifndef CALENDARS_GEOGRAPHY_H
#define CALENDARS_GEOGRAPHY_H

#if __cplusplus
extern "C" {
#endif

  typedef struct CalendarEarthPos {
    /** Latitude (degrees) */
    double latitude;
    /** Longitude (degrees) */
    double longitude;
    /** Elevation (meters) */
    double elevation;
    /** Zone Offset (fraction of a day) */
    double zoneOffset;
  } CalendarEarthPos;

  CalendarEarthPos calendar_earth_pos_create(double latitude, double longitude, double elevation, double zoneOffset);

  CalendarEarthPos calendar_earth_pos_urbana();
  CalendarEarthPos calendar_earth_pos_greenwhich();
  CalendarEarthPos calendar_earth_pos_mecca();
  CalendarEarthPos calendar_earth_pos_jerusalem();
  CalendarEarthPos calendar_earth_pos_acre();

  typedef struct LatLongUnit {
    double degrees;
    double minutes;
    double seconds;
  } CalendarGeoDegMinSec;

  double calendar_lat_lon_degrees_from_dms(CalendarGeoDegMinSec u);
  double calendar_pos_direction(CalendarEarthPos p1, CalendarEarthPos p2);

#if __cplusplus
};
#endif

#endif //CALENDARS_GEOGRAPHY_H
