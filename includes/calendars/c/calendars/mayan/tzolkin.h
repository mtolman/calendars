#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_TZOLKIN_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_TZOLKIN_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  IMIX = 1,
  IK,
  AKBAL,
  KAN,
  CHICCHAN,
  CIMI,
  MANIK,
  LAMAT,
  MULUC,
  OC,
  CHUEN,
  EB,
  BEN,
  IX,
  MEN,
  CIB,
  CABAN,
  ETZNAB,
  CAUAC,
  AHAU,
} CALENDAR_TZOLKIN_NAME;

typedef struct CalendarTzolkinDate {
  int16_t number;
  int16_t name;
} CalendarTzolkinDate;

CalendarTzolkinDate calendar_tzolkin_create(int16_t number, CALENDAR_TZOLKIN_NAME name);

void calendar_tzolkin_init(CalendarTzolkinDate* date, int16_t number, CALENDAR_TZOLKIN_NAME name);

int calendar_tzolkin_compare(const CalendarTzolkinDate* left, const CalendarTzolkinDate* right);

int64_t calendar_tzolkin_ordinal(const CalendarTzolkinDate* date);

CalendarTzolkinDate calendar_tzolkin_from_rd_date(CalendarRdDate rdDate);

const char8_t* const * calendar_tzolkin_names();
const char8_t* calendar_tzolkin_name(CALENDAR_TZOLKIN_NAME month);
extern const int calendar_tzolkin_num_names;

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_TZOLKIN_H
