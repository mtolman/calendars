#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_LONG_DATE_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_LONG_DATE_H

#include <calendars/c/calendars/common.h>

/**
 * @file
 * @defgroup c_mayan_long_date C - Mayan Long Date. Used for absolute dates and not just cyclic dates
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Mayan Long Date
 * @ingroup c_mayan_long_date
 */
typedef struct CalendarLongDate {
  double cycle0;
  double cycle1;
  double cycle2;
  double cycle3;
} CalendarLongDate;

extern const int64_t calendar_long_date_units_kin;
extern const int64_t calendar_long_date_units_uinal;
extern const int64_t calendar_long_date_units_tun;
extern const int64_t calendar_long_date_units_katun;
extern const int64_t calendar_long_date_units_baktun;
extern const int64_t calendar_long_date_units_pictun;
extern const int64_t calendar_long_date_units_calabtun;
extern const int64_t calendar_long_date_units_kinchiltun;
extern const int64_t calendar_long_date_units_alautun;

CalendarLongDate calendar_mayan_long_date_create(double cycle0, double cycle1, double cycle2, double cycle3);
void calendar_mayan_long_date_init(CalendarLongDate* date, double cycle0, double cycle1, double cycle2, double cycle3);

CalendarLongDate calendar_mayan_long_date_from_rd_date(CalendarRdDate rdDate);
CalendarRdDate calendar_mayan_long_date_to_rd_date(const CalendarLongDate* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_LONG_DATE_H
