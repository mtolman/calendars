#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_HAAB_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_HAAB_H

#include <calendars/c/calendars/common.h>

/**
 * @file
 * @defgroup c_mayan_haab C - Mayan Haab
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Represents a Mayan Haab month
 * @ingroup c_mayan_haab
 */
typedef enum {
  HAAB_MONTH_POP = 1,
  HAAB_MONTH_UO,
  HAAB_MONTH_ZIP,
  HAAB_MONTH_ZOTZ,
  HAAB_MONTH_TZEC,
  HAAB_MONTH_XUL,
  HAAB_MONTH_YAXKIN,
  HAAB_MONTH_MOL,
  HAAB_MONTH_CHEN,
  HAAB_MONTH_YAX,
  HAAB_MONTH_ZAC,
  HAAB_MONTH_CEH,
  HAAB_MONTH_MAC,
  HAAB_MONTH_KANKIN,
  HAAB_MONTH_MUAN,
  HAAB_MONTH_PAX,
  HAAB_MONTH_KAYAB,
  HAAB_MONTH_CUMKU,
  HAAB_MONTH_UAYEB,
} CALENDAR_HAAB_MONTH;

/**
 * Represents a Mayan Haab date
 * @ingroup c_mayan_haab
 */
typedef struct CalandarHaabDate {
  int16_t month;
  int16_t day;
} CalendarHaabDate;

/**
 * Creates a Mayan Haab date
 * @ingroup c_mayan_haab
 */
CalendarHaabDate calendar_haab_create(CALENDAR_HAAB_MONTH month, int16_t day);

/**
 * Initializes a Mayan Haab date
 * @ingroup c_mayan_haab
 */
void calendar_haab_init(CalendarHaabDate* date, CALENDAR_HAAB_MONTH month, int16_t day);

/**
 * Compares two Mayan Haab dates
 * @ingroup c_mayan_haab
 */
int calendar_haab_compare(const CalendarHaabDate* left, const CalendarHaabDate* right);

/**
 * Retrieves the Mayan Haab ordinal
 * @ingroup c_mayan_haab
 */
int64_t calendar_haab_ordinal(const CalendarHaabDate* date);

/**
 * Creates a Mayan Haab date from an RdDate
 * @ingroup c_mayan_haab
 */
CalendarHaabDate calendar_haab_from_rd_date(CalendarRdDate rdDate);

/**
 * Returns the mayan haab month names
 * @ingroup c_mayan_haab
 */
const char8_t* const * calendar_haab_month_names();

/**
 * The number of mayan haab month names
 * @ingroup c_mayan_haab
 */
extern const int calendar_haab_num_month_names;

/**
 * Returns the mayan haab month name for a given month
 * @ingroup c_mayan_haab
 */
const char8_t* calendar_haab_month_name(CALENDAR_HAAB_MONTH month);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_HAAB_H
