#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_COPTIC_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_COPTIC_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  COPTIC_THOOUT     = 1,
  COPTIC_PAOPE      = 2,
  COPTIC_ATHOR      = 3,
  COPTIC_KOIAK      = 4,
  COPTIC_TOBE       = 5,
  COPTIC_MESHIRE    = 6,
  COPTIC_PAREMOTEP  = 7,
  COPTIC_PARMOUTE   = 8,
  COPTIC_PASHONS    = 9,
  COPTIC_PAONE      = 10,
  COPTIC_EPEP       = 11,
  COPTIC_MESORE     = 12,
  COPTIC_EPAGOMENAE = 13
} CALENDAR_COPTIC_MONTH;

typedef struct CalendarCopticDate {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarCopticDate;

CalendarCopticDate calendar_coptic_create(int64_t year, CALENDAR_COPTIC_MONTH month, int16_t day);

void calendar_coptic_init(CalendarCopticDate* date, int64_t year, CALENDAR_COPTIC_MONTH month, int16_t day);

CalendarRdDate calendar_coptic_to_rd_date(const CalendarCopticDate* date);

CalendarCopticDate calendar_coptic_from_rd_date(CalendarRdDate rdDate);

int calendar_coptic_compare(const CalendarCopticDate* left, const CalendarCopticDate* right);

bool calendar_coptic_is_leap_year(int64_t year);

const char8_t* const * calendar_coptic_month_names();
const char8_t* const * calendar_coptic_month_names_sahidic();
const char8_t* const * calendar_coptic_month_names_bohairic();
extern const int calendar_coptic_num_month_names;
const char8_t* calendar_coptic_month_name(CALENDAR_COPTIC_MONTH month);
const char8_t* calendar_coptic_month_name_sahidic(CALENDAR_COPTIC_MONTH month);
const char8_t* calendar_coptic_month_name_bohairic(CALENDAR_COPTIC_MONTH month);

const char8_t* const * calendar_coptic_day_of_week_names();
const char8_t* calendar_coptic_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_COPTIC_H
