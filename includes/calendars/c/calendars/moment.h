#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_MOMENT_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_MOMENT_H

#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef double CalendarMoment;

double calendar_moment_to_time(CalendarMoment moment);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_MOMENT_H
