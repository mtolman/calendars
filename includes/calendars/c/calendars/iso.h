#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_ISO_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_ISO_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarIsoDate {
  int64_t year;
  int16_t week;
  int16_t day;
} CalendarIsoDate;

CalendarIsoDate calendar_iso_create(int64_t year, int16_t week, int16_t day);

void calendar_iso_init(CalendarIsoDate* date, int64_t year, int16_t week, int16_t day);

int calendar_iso_compare(const CalendarIsoDate* left, const CalendarIsoDate* right);

bool calendar_iso_is_long_year(int64_t year);

CalendarIsoDate calendar_iso_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_iso_to_rd_date(const CalendarIsoDate* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_ISO_H
