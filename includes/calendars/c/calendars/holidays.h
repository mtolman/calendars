#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_HOLIDAYS_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_HOLIDAYS_H

#include "holidays/common.h"

#include "holidays/armenian_church.h"
#include "holidays/christian.h"
#include "holidays/copts.h"
#include "holidays/eastern_orthodox.h"
#include "holidays/hebrew.h"
#include "holidays/usa.h"

#endif  // CALENDAR_LIBRARY_DATE_TYPE_HOLIDAYS_H
