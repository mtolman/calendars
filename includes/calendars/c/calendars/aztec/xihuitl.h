#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_XIHUITL_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_XIHUITL_H

#include <calendars/c/calendars/common.h>
#include "tonalpohualli.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  XIHUITL_IZCALI = 1,
  XIHUITL_ATLCAHUALO,
  XIHUITL_TLACAXIPEHUALIZTLI,
  XIHUITL_TOZOZTONTLI,
  XIHUITL_HUEI_TOZOZTLI,
  XIHUITL_TOXCATL,
  XIHUITL_ETZALCUALIZTLI,
  XIHUITL_TECUILHUITONTLI,
  XIHUITL_HUEI_TECUILHUITL,
  XIHUITL_TLAXOCHIMACO,
  XIHUITL_XOCOTLUETZI,
  XIHUITL_OCHPANIZTLI,
  XIHUITL_TEOTLECO,
  XIHUITL_TEPEIHUITL,
  XIHUITL_QUECHOLLI,
  XIHUITL_PANQUETZALIZTLI,
  XIHUITL_ATEMOZTLI,
  XIHUITL_TITITL,
  XIHUITL_NEMONTEMI
} CALENDAR_XIHUITL_MONTH;

typedef struct CalendarXihuitlDate {
  int16_t month;
  int16_t day;
} CalendarXihuitlDate;

CalendarXihuitlDate calendar_xihuitl_create(CALENDAR_XIHUITL_MONTH month, int16_t day);

void calendar_xihuitl_init(CalendarXihuitlDate* date, CALENDAR_XIHUITL_MONTH month, int16_t day);

int calendar_xihuitl_ordinal(const CalendarXihuitlDate* date);

int calendar_xihuitl_compare(const CalendarXihuitlDate* left, const CalendarXihuitlDate* right);

CalendarXihuitlDate calendar_xihuitl_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_xihuitl_on_or_before(CalendarRdDate rdDate, const CalendarXihuitlDate* date);

CalendarRdDateOptional xihuitl_tonalpohualli_on_or_before(CalendarRdDate date, const CalendarXihuitlDate* xihuitl, const CalendarTonalpohualliDate* tonalpohualli);

const char8_t* const* calendar_xihuitl_month_names();
const char8_t* calendar_xihuitl_month_name(CALENDAR_XIHUITL_MONTH month);
extern const int calendar_xihuitl_num_month_names;

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_XIHUITL_H
