#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_XIUHOMOLPILLI_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_XIUHOMOLPILLI_H

#include "tonalpohualli.h"
#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  XIUHOMOLPILLI_CALLI = 3,
  XIUHOMOLPILLI_TOCHTLI = 8,
  XIUHOMOLPILLI_ACATL = 13,
  XIUHOMOLPILLI_TECPATL = 18
} CALENDAR_XIUHOMOLPILLI_NAME;

/**
 * Represents a Xiuhomolpilli date
 */
typedef struct {
  int16_t number;
  int16_t name;
} CalendarXiuhomolpilliDate;

/**
 * Creates a Xiuhomolpilli date
 * @param number Xiuhomolpilli number
 * @param name Xiuhomolpilli name (one of CALLI, TOCHTLI, ACATL, TECPATL)
 * @return
 */
CalendarXiuhomolpilliDate calendar_xiuhomolpilli_create(int16_t number, CALENDAR_XIUHOMOLPILLI_NAME name);

/**
 * Initializes a Xiuhomolpilli date
 * @param date Date to initialize
 * @param number Xiuhomolpilli number
 * @param name Xiuhomolpilli name (one of CALLI, TOCHTLI, ACATL, TECPATL)
 */
void calendar_xiuhomolpilli_init(CalendarXiuhomolpilliDate* date, int16_t number, CALENDAR_XIUHOMOLPILLI_NAME name);

/**
 * Checks if a Xiuhomolpilli date is valid
 * @param date Date to check
 * @return
 */
bool calendar_xiuhomolpilli_is_valid(const CalendarXiuhomolpilliDate* date);

/**
 * Compares two Xiuhomolpilli dates
 * @param left left hand operand
 * @param right right hand operand
 * @return
 */
int calendar_xiuhomolpilli_compare(const CalendarXiuhomolpilliDate* left, const CalendarXiuhomolpilliDate* right);

/**
 * Represents a Xiuhomolpilli which may or may not exist
 * The Xiuhomolpilli is only considered valid (and initialized) if is_present is true
 */
typedef struct {
  bool is_present;
  CalendarXiuhomolpilliDate date;
} CalendarXiuhomolpilliDateOptional;

CalendarXiuhomolpilliDateOptional calendar_xiuhomolpilli_from_rd_date(CalendarRdDate date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_XIUHOMOLPILLI_H
