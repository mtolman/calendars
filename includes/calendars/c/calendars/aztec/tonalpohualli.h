#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_TONALPOHUALLI_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_TONALPOHUALLI_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Tonalpohualli names
 */
typedef enum {
  TONALPOHUALLI_CIPACTLI = 1,
  TONALPOHUALLI_EHECATL,
  TONALPOHUALLI_CALLI,
  TONALPOHUALLI_CUETZPALLIN,
  TONALPOHUALLI_COATL,
  TONALPOHUALLI_MIQUIZTLI,
  TONALPOHUALLI_MAZATL,
  TONALPOHUALLI_TOCHTLI,
  TONALPOHUALLI_ATL,
  TONALPOHUALLI_ITZCUINTLI,
  TONALPOHUALLI_OZOMATLI,
  TONALPOHUALLI_MALINALLI,
  TONALPOHUALLI_ACATL,
  TONALPOHUALLI_OCELOTL,
  TONALPOHUALLI_QUAUHTLI,
  TONALPOHUALLI_COZCAQUAUHTLI,
  TONALPOHUALLI_OLLIN,
  TONALPOHUALLI_TECPATL,
  TONALPOHUALLI_QUIAHUITL,
  TONALPOHUALLI_XOCHITL
} CALENDAR_TONALPOHUALLI_NAME;

/**
 * Represents an Aztec Tonalpohualli date.
 * These are cyclic dates and not absolute dates
 */
typedef struct CalendarTonalpohualliDate {
  int16_t number;
  int16_t name;
} CalendarTonalpohualliDate;

/**
 * Creates a Tonalpohualli date
 * @param number Tonalpohualli number
 * @param name Tonalpohualli nme
 * @return
 */
CalendarTonalpohualliDate calendar_tonalpohualli_create(int16_t number, CALENDAR_TONALPOHUALLI_NAME name);

/**
 * Initializes a Tonalpohualli date
 * @param date Tonalpohualli date to initialize
 * @param number Tonalpohualli number
 * @param name Tonalpohualli nme
 */
void calendar_tonalpohualli_init(CalendarTonalpohualliDate* date, int16_t number, CALENDAR_TONALPOHUALLI_NAME name);

/**
 * Gets the ordinal for a Tonalpohualli date
 * @param date Tonalpohualli date
 * @return
 */
int calendar_tonalpohualli_ordinal(const CalendarTonalpohualliDate* date);

/**
 * Compares two Tonalpohualli dates
 * @param left Left date to compare
 * @param right Right date to compare
 * @return
 */
int calendar_tonalpohualli_compare(const CalendarTonalpohualliDate* left, const CalendarTonalpohualliDate* right);

/**
 * Creates a Tonalpohualli date from an RdDate
 * @param rdDate RdDate to convert
 * @return
 */
CalendarTonalpohualliDate calendar_tonalpohualli_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_tonalpohualli_on_or_before(CalendarRdDate rdDate, const CalendarTonalpohualliDate* date);

const char8_t* const* calendar_tonalpohualli_names();

const char8_t* calendar_tonalpohualli_name(CALENDAR_TONALPOHUALLI_NAME name);

extern const int calendar_tonalpohualli_num_names;

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_TONALPOHUALLI_H
