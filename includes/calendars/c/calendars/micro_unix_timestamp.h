#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_MICRO_UNIX_TIMESTAMP_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_MICRO_UNIX_TIMESTAMP_H

#include <calendars/c/calendars/moment.h>
#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarMicroUnixTimestamp {
  double timestamp;
} CalendarMicroUnixTimestamp;

CalendarMicroUnixTimestamp calendar_micro_unix_timestamp_create(double seconds);

void calendar_micro_unix_timestamp_init(CalendarMicroUnixTimestamp* date, double seconds);

int calendar_micro_unix_timestamp_compare(const CalendarMicroUnixTimestamp* left, const CalendarMicroUnixTimestamp* right);

CalendarMicroUnixTimestamp calendar_micro_unix_timestamp_from_moment(CalendarMoment moment);

CalendarMoment calendar_micro_unix_timestamp_to_moment(const CalendarMicroUnixTimestamp* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_MICRO_UNIX_TIMESTAMP_H
