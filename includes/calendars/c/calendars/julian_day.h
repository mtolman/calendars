#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_JULIAN_DAY_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_JULIAN_DAY_H

#include <calendars/c/calendars/common.h>
#include <calendars/c/calendars/moment.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
  double datetime;
} CalendarJulianDay;

CalendarJulianDay calendar_julian_day_create(double datetime);
void calendar_julian_day_init(CalendarJulianDay* date, double datetime);
int calendar_julian_day_compare(const CalendarJulianDay* left, const CalendarJulianDay* right);
CalendarJulianDay calendar_julian_day_from_moment(CalendarMoment moment);
CalendarMoment calendar_julian_day_to_moment(const CalendarJulianDay* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_JULIAN_DAY_H
