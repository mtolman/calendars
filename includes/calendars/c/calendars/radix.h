#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_RADIX_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_RADIX_H

#include <calendars/c/calendars/moment.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarRadixTemplate {
  void* data;
} CalendarRadixTemplate;

typedef struct CalendarRadixDate {
  void* data;
} CalendarRadixDate;

/**
 * Initializes a Radix template with dynamically allocated memory
 * NOTE: This does NOT clear any previously allocated memory! You MUST call make call calendar_radix_template_deinit beforehand if you've previously called this method on the template object!
 * @see calendar_radix_template_deinit for de-initializing the dynamically allocated memory
 * @param date Date object to setup, does dynamically allocate memory (MUST DEALLOCATE using calendar_radix_template_deinit!)
 * @param num_day_cycles Number of day cycles being passed in
 * @param day_cycles The list of day cycles
 * @param num_time_cycles The number of time cycles being passed in
 * @param time_cycles The list of time cycles
 */
void calendar_radix_template_init(CalendarRadixTemplate* date, int num_day_cycles, const int day_cycles[], int num_time_cycles, const int time_cycles[]);

/**
 * De-initializes (frees) dynamically allocated memory tied to a radix template
 * @param date Template to free dynamically allocated memory for
 */
void calendar_radix_template_deinit(CalendarRadixTemplate* date);

/**
 * Initializes a Radix date with dynamically allocated memory
 * Uses an array of day cycle values and time cycle values to know what to initialize to
 * NOTE: This does NOT clear any previously allocated memory! You MUST call make call calendar_radix_date_deinit beforehand if you've previously called this method on the date object!
 * @see calendar_radix_date_deinit for de-initializing the dynamically allocated memory
 * @param date Date object to setup, does dynamically allocate memory (MUST DEALLOCATE using calendar_radix_date_deinit!)
 * @param dateTemplate Date template to use for initialization
 * @param num_day_cycle_values Number of day cycles values being passed in
 * @param day_cycle_values The list of day cycles values
 * @param num_time_cycle_values The number of time cycles values being passed in
 * @param time_cycle_values The list of time cycles values
 */
void calendar_radix_date_init(CalendarRadixDate* date, const CalendarRadixTemplate* dateTemplate, int num_day_cycle_values, const double day_cycle_values[], int num_time_cycle_values, const double time_cycle_values[]);

/**
 * Initializes a Radix date with dynamically allocated memory
 * Uses a Moment for knowing what to initialize the date to
 * NOTE: This does NOT clear any previously allocated memory! You MUST call make call calendar_radix_date_deinit beforehand if you've previously called this method on the date object!
 * @see calendar_radix_date_deinit for de-initializing the dynamically allocated memory
 * @param date Date object to setup
 * @param dateTemplate Date template to use for initialization
 * @param moment Moment to initialize to
 */
void calendar_radix_date_init_from_moment(CalendarRadixDate* date, const CalendarRadixTemplate* dateTemplate, CalendarMoment moment);

/**
 * De-initializes (frees) dynamically allocated emory tied to a radix date
 * @param date Date object to de initialize
 */
void calendar_radix_date_deinit(CalendarRadixDate* date);

/**
 * Converts a radix date into a moment
 * @param date Radix date to convert
 * @return
 */
CalendarMoment calendar_radix_date_to_moment(const CalendarRadixDate* date);

/**
 * Gets the size (number of radix elements) of a radix date
 * @param date Radix date to get the size of
 * @return
 */
int calendar_radix_date_size(const CalendarRadixDate* date);

/**
 * Gets the radix element at a specific location
 * returns NaN if the index is out of bounds
 * @param date Radix date to get the element from
 * @param index Radix element index
 * @return
 */
double calendar_radix_date_get(const CalendarRadixDate* date, int index);

/**
 * Sets the radix element at a specific location
 * returns false if the index is out of bounds, true if the index is in bounds
 * @param date Radix date to set the element for
 * @param index Radix element index
 * @param value Value to set radix element to
 * @return
 */
bool calendar_radix_date_set(const CalendarRadixDate* date, int index, double value);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_RADIX_H
