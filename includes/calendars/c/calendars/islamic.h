#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_ISLAMIC_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_ISLAMIC_H

#include <calendars/c/calendars/common.h>

#if __cplusplus
extern "C" {
#endif

// TODO: Add week day names
// TODO: Add month names

typedef enum {
  ISLAMIC_MUHARRAM = 1,
  ISLAMIC_SAFAR,
  ISLAMIC_RABI_I,
  ISLAMIC_RABI_II,
  ISLAMIC_JUMADA_I,
  ISLAMIC_JUMADA_II,
  ISLAMIC_RAJAB,
  ISLAMIC_SHABAN,
  ISLAMIC_RAMADAN,
  ISLAMIC_SHAWWAL,
  ISLAMIC_DHU_AL_QADA,
  ISLAMIC_DHU_AL_HIJJA
} CALENDAR_ISLAMIC_MONTH;

typedef struct CalendarIslamicDate {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarIslamicDate;

CalendarIslamicDate calendar_islamic_create(int64_t year, CALENDAR_ISLAMIC_MONTH month, int16_t day);

void calendar_islamic_init(CalendarIslamicDate* date, int64_t year, CALENDAR_ISLAMIC_MONTH month, int16_t day);

int calendar_islamic_compare(const CalendarIslamicDate* left, const CalendarIslamicDate* right);

bool calendar_islamic_is_leap_year(int64_t year);

CalendarIslamicDate calendar_islamic_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_islamic_to_rd_date(const CalendarIslamicDate* date);

const char8_t* const* calendar_islamic_days_of_week();
const char8_t* const* calendar_islamic_days_of_week_arabic();
const char8_t* calendar_islamic_day_of_week(CALENDAR_DAY_OF_WEEK dayOfWeek);
const char8_t* calendar_islamic_day_of_week_arabic(CALENDAR_DAY_OF_WEEK dayOfWeek);

const char8_t* const* calendar_islamic_month_names();
const char8_t* const* calendar_islamic_month_names_arabic();
extern const int calendar_islamic_num_month_names;
const char8_t* calendar_islamic_month_name(CALENDAR_ISLAMIC_MONTH month);
const char8_t* calendar_islamic_month_name_arabic(CALENDAR_ISLAMIC_MONTH month);

#if __cplusplus
};
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_ISLAMIC_H
