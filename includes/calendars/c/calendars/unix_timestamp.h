#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_UNIX_TIMESTAMP_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_UNIX_TIMESTAMP_H

#include <calendars/c/calendars/common.h>
#include <calendars/c/calendars/moment.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarUnixTimestamp {
  int64_t timestamp;
} CalendarUnixTimestamp;

CalendarUnixTimestamp calendar_unix_timestamp_create(int64_t seconds);

void calendar_unix_timestamp_init(CalendarUnixTimestamp* date, int64_t seconds);

int calendar_unix_timestamp_compare(const CalendarUnixTimestamp* left, const CalendarUnixTimestamp* right);

CalendarMoment calendar_unix_timestamp_to_moment(const CalendarUnixTimestamp* date);

CalendarUnixTimestamp calendar_unix_timestamp_from_moment(CalendarMoment moment);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_UNIX_TIMESTAMP_H
