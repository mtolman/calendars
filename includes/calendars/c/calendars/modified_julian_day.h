#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_MODIFIED_JULIAN_DAY_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_MODIFIED_JULIAN_DAY_H

#include <calendars/c/calendars/moment.h>
#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarModifiedJulianDay {
  double datetime;
} CalendarModifiedJulianDayDate;

CalendarModifiedJulianDayDate calendar_modified_julian_day_create(double seconds);

void calendar_modified_julian_day_init(CalendarModifiedJulianDayDate* date, double seconds);

int calendar_modified_julian_day_compare(const CalendarModifiedJulianDayDate* left, const CalendarModifiedJulianDayDate* right);

CalendarModifiedJulianDayDate calendar_modified_julian_day_from_moment(CalendarMoment moment);

CalendarMoment calendar_modified_julian_day_to_moment(const CalendarModifiedJulianDayDate* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_MODIFIED_JULIAN_DAY_H
