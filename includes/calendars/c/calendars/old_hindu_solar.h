#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_OLD_HINDU_SOLAR_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_OLD_HINDU_SOLAR_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

// TODO: Add day of week names
// TODO: Add samvatsaras (jovian year) names
// TODO: Add saura (month) names

typedef enum {
  OLD_HINDU_SOLAR_SAURA_ARIES,
  OLD_HINDU_SOLAR_SAURA_TAURUS,
  OLD_HINDU_SOLAR_SAURA_GEMINI,
  OLD_HINDU_SOLAR_SAURA_CANCER,
  OLD_HINDU_SOLAR_SAURA_LEO,
  OLD_HINDU_SOLAR_SAURA_VIRGO,
  OLD_HINDU_SOLAR_SAURA_LIBRA,
  OLD_HINDU_SOLAR_SAURA_SCORPIO,
  OLD_HINDU_SOLAR_SAURA_SAGITTARIUS,
  OLD_HINDU_SOLAR_SAURA_CAPRICORN,
  OLD_HINDU_SOLAR_SAURA_AQUARIUS,
  OLD_HINDU_SOLAR_SAURA_PISCES
} CALENDAR_OLD_HINDU_SOLAR_SAURA;

typedef struct CalendarOldHinduSolarDate {
  int64_t year;
  int16_t saura;
  int16_t day;
} CalendarOldHinduSolarDate;

CalendarOldHinduSolarDate calendar_old_hindu_solar_create(int64_t year, CALENDAR_OLD_HINDU_SOLAR_SAURA saura, int16_t day);

void calendar_old_hindu_solar_init(CalendarOldHinduSolarDate* date, int64_t year, CALENDAR_OLD_HINDU_SOLAR_SAURA saura, int16_t day);

int calendar_old_hindu_solar_compare(const CalendarOldHinduSolarDate* left, const CalendarOldHinduSolarDate* right);

int64_t calendar_old_hindu_solar_jovian_year(CalendarRdDate rdDate);

int64_t calendar_old_hindu_solar_day_count(CalendarRdDate rdDate);

CalendarOldHinduSolarDate calendar_old_hindu_solar_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_old_hindu_solar_to_rd_date(const CalendarOldHinduSolarDate* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_OLD_HINDU_SOLAR_H
