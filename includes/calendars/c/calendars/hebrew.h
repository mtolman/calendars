#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_HEBREW_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_HEBREW_H

#include <calendars/c/calendars/common.h>

#if __cplusplus
extern "C" {
#endif

typedef enum {
  HEBREW_NISAN = 1,
  HEBREW_IYYAR,
  HEBREW_SIVAN,
  HEBREW_TAMMUZ,
  HEBREW_AV,
  HEBREW_ELUL,
  HEBREW_TISHRI,
  HEBREW_MARHESHVAN,
  HEBREW_KISLEV,
  HEBREW_TEVET,
  HEBREW_SHEVAT,
  HEBREW_ADAR,
  HEBREW_ADAR_II
} CALENDAR_HEBREW_MONTH;

typedef struct CalendarHebrewDate {
  int64_t year;
  int16_t month;
  int16_t day;
} CalendarHebrewDate;

CalendarHebrewDate calendar_hebrew_create(int64_t year, CALENDAR_HEBREW_MONTH month, int16_t day);

void calendar_hebrew_init(CalendarHebrewDate* date, int64_t year, CALENDAR_HEBREW_MONTH month, int16_t day);

int calendar_hebrew_compare(const CalendarHebrewDate* left, const CalendarHebrewDate* right);

bool calendar_hebrew_is_leap_year(int64_t year);

CALENDAR_HEBREW_MONTH calendar_hebrew_last_month_of_year(int64_t year);

bool calendar_hebrew_is_sabbatical_year(int64_t year);

double calendar_hebrew_molad(int64_t year, CALENDAR_HEBREW_MONTH month);

int64_t calendar_hebrew_elapsed_days(int64_t year);

int64_t calendar_hebrew_year_length_correction(int64_t year);

CalendarRdDate calendar_hebrew_new_year(int64_t year);

int16_t calendar_hebrew_last_day_of_month(int64_t year, CALENDAR_HEBREW_MONTH month);

bool calendar_hebrew_long_marheshvan(int64_t year);

bool calendar_hebrew_short_kislev(int64_t year);

int64_t calendar_hebrew_days_in_year(int64_t year);

CalendarHebrewDate calendar_hebrew_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_hebrew_to_rd_date(const CalendarHebrewDate* date);

const char8_t* const * calendar_hebrew_day_of_week_names();
const char8_t* const * calendar_hebrew_day_of_week_names_hebrew();

const char8_t* calendar_hebrew_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek);
const char8_t* calendar_hebrew_day_of_week_name_hebrew(CALENDAR_DAY_OF_WEEK dayOfWeek);

const char8_t* const * calendar_hebrew_month_names();
const char8_t* const * calendar_hebrew_month_names_hebrew();
extern const int calendar_hebrew_num_month_names;

const char8_t* calendar_hebrew_month_name(CALENDAR_HEBREW_MONTH month);
const char8_t* calendar_hebrew_month_name_hebrew(CALENDAR_HEBREW_MONTH month);

#if __cplusplus
};
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_HEBREW_H
