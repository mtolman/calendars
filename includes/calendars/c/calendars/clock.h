#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_CLOCK_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_CLOCK_H

#include <calendars/c/calendars/moment.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CalendarClockDate {
  double day;
  double hour;
  double minute;
  double second;
} CalendarClockDate;

CalendarClockDate calendar_clock_create(double day, double hour, double minute, double second);
void calendar_clock_init(CalendarClockDate* date, double day, double hour, double minute, double second);

CalendarMoment calendar_clock_to_moment(const CalendarClockDate* date);

CalendarClockDate calendar_clock_from_moment(CalendarMoment moment);

int calendar_clock_compare(const CalendarClockDate* left, const CalendarClockDate* right);

double calendar_clock_time(const CalendarClockDate* date);

CalendarClockDate calendar_clock_nearest_second(const CalendarClockDate* date);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_CLOCK_H
