#ifndef C_CALENDAR_LIBRARY_DATE_TYPE_BALINESE_PAWUKON_DATE_H
#define C_CALENDAR_LIBRARY_DATE_TYPE_BALINESE_PAWUKON_DATE_H

#include <calendars/c/calendars/common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  DWIWARA_MENGA = 1, DWIWARA_PEPET
} CALENDAR_BALINESE_PAWUKON_DWIWARA;

typedef enum {
  TRIWARA_PASAH = 1, TRIWARA_BETENG, TRIWARA_KAJENG
} CALENDAR_BALINESE_PAWUKON_TRIWARA;

typedef enum {
  CATURWARA_SRI = 1, CATURWARA_LABA, CATURWARA_JAYA, CATURWARA_MENALA
} CALENDAR_BALINESE_PAWUKON_CATURWARA;

typedef enum {
  PANCAWARA_UMANIS = 1, PANCAWARA_PAING, PANCAWARA_PON, PANCAWARA_WAGE, PANCAWARA_KELIWON
} CALENDAR_BALINESE_PAWUKON_PANCAWARA;

typedef enum {
  SADWARA_TUNGLEH = 1, SADWARA_ARYANG, SADWARA_URUKUNG, SADWARA_PANIRON, SADWARA_WAS, SADWARA_MAULU
} CALENDAR_BALINESE_PAWUKON_SADWARA;

typedef enum {
  SAPTAWARA_REDITE = 1, SAPTAWARA_COMA, SAPTAWARA_ANGGARA, SAPTAWARA_BUDA, SAPTAWARA_WRASPATI, SAPTAWARA_SUKRA, SAPTAWARA_SANISCARA
} CALENDAR_BALINESE_PAWUKON_SAPTAWARA;

typedef enum {
  ASATAWARA_SRI = 1, ASATAWARA_INDRA, ASATAWARA_GURU, ASATAWARA_YAMA, ASATAWARA_LUDRA, ASATAWARA_BRAHMA, ASATAWARA_KALA, ASATAWARA_UMA
} CALENDAR_BALINESE_PAWUKON_ASATAWARA;

typedef enum {
  SANGAWARA_DANGU = 1, SANGAWARA_JANGUR, SANGAWARA_GIGIS, SANGAWARA_NOHAN, SANGAWARA_OGAN, SANGAWARA_ERANGAN, SANGAWARA_URUNGAN, SANGAWARA_TULUS, SANGAWARA_DADI
} CALENDAR_BALINESE_PAWUKON_SANGAWARA;

typedef enum {
  DASAWARA_PANDITA = 1, DASAWARA_PATI, DASAWARA_SUKA, DASAWARA_DUKA, DASAWARA_SRI, DASAWARA_MANUH, DASAWARA_MANUSA, DASAWARA_RAJA, DASAWARA_DEWA, DASAWARA_RAKSASA = 0
} CALENDAR_BALINESE_PAWUKON_DASAWARA;

typedef enum {
  EKAWARA_LUANG = 1
} CalendarBalinesePawukonEkawara;

typedef struct CalendarBalinesePawukonDate {
  int ekawara : 1;
  int dwiwara : 2;
  int triwara : 2;
  int caturwara : 3;
  int pancawara : 3;
  int sadwara : 3;
  int saptawara : 3;
  int asatawara : 4;
  int sangawara : 4;
  int dasawara : 4;
} CalendarBalinesePawukonDate;

CalendarBalinesePawukonDate calendar_balinese_pawukon_create(
    bool luang,
                                                             CALENDAR_BALINESE_PAWUKON_DWIWARA dwiwara,
                                                             CALENDAR_BALINESE_PAWUKON_TRIWARA triwara,
                                                             CALENDAR_BALINESE_PAWUKON_CATURWARA caturwara,
                                                             CALENDAR_BALINESE_PAWUKON_PANCAWARA pancawara,
                                                             CALENDAR_BALINESE_PAWUKON_SADWARA   sadwara,
                                                             CALENDAR_BALINESE_PAWUKON_SAPTAWARA saptawara,
                                                             CALENDAR_BALINESE_PAWUKON_ASATAWARA asatawara,
                                                             CALENDAR_BALINESE_PAWUKON_SANGAWARA sangawara,
                                                             CALENDAR_BALINESE_PAWUKON_DASAWARA  dasawara
);

void calendar_balinese_pawukon_init(
    CalendarBalinesePawukonDate * date,
    bool luang,
                                    CALENDAR_BALINESE_PAWUKON_DWIWARA dwiwara,
                                    CALENDAR_BALINESE_PAWUKON_TRIWARA triwara,
                                    CALENDAR_BALINESE_PAWUKON_CATURWARA caturwara,
                                    CALENDAR_BALINESE_PAWUKON_PANCAWARA pancawara,
                                    CALENDAR_BALINESE_PAWUKON_SADWARA   sadwara,
                                    CALENDAR_BALINESE_PAWUKON_SAPTAWARA saptawara,
                                    CALENDAR_BALINESE_PAWUKON_ASATAWARA asatawara,
                                    CALENDAR_BALINESE_PAWUKON_SANGAWARA sangawara,
                                    CALENDAR_BALINESE_PAWUKON_DASAWARA  dasawara
);

int calendar_balinese_pawukon_compare(const CalendarBalinesePawukonDate* left, const CalendarBalinesePawukonDate* right);

CalendarBalinesePawukonDate calendar_balinese_pawukon_from_rd_date(CalendarRdDate rdDate);

int calendar_balinese_pawukon_day_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_TRIWARA calendar_balinese_pawukon_triwara_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_SADWARA calendar_balinese_pawukon_sadawara_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_SAPTAWARA calendar_balinese_pawukon_saptawara_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_PANCAWARA calendar_balinese_pawukon_pancawara_from_rd_date(CalendarRdDate rdDate);

int calendar_balinese_pawukon_week_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_DASAWARA calendar_balinese_pawukon_dasawara_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_DWIWARA calendar_balinese_pawukon_dwiwara_from_rd_date(CalendarRdDate rdDate);

bool calendar_balinese_pawukon_luang_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_SANGAWARA calendar_balinese_pawukon_sangawara_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_ASATAWARA calendar_balinese_pawukon_asatawara_from_rd_date(CalendarRdDate rdDate);

CALENDAR_BALINESE_PAWUKON_CATURWARA calendar_balinese_pawukon_caturwara_from_rd_date(CalendarRdDate rdDate);

CalendarRdDate calendar_balinese_pawukon_on_or_before(CalendarRdDate rdDate, const CalendarBalinesePawukonDate* balinesePawukonDate);

const char8_t* const* calendar_balinese_pawukon_cycle_names();
extern const int calendar_balinese_pawukon_num_cycle_names;

const char8_t* const* calendar_balinese_pawukon_cycle_ekawara_names();
extern const int calendar_balinese_pawukon_cycle_num_ekawara_names;
const char8_t* calendar_balinese_pawukon_cycle_ekawara_name(bool ekawara);

const char8_t* const* calendar_balinese_pawukon_cycle_dwiwara_names();
extern const int calendar_balinese_pawukon_cycle_num_dwiwara_names;
const char8_t* calendar_balinese_pawukon_cycle_dwiwara_name(CALENDAR_BALINESE_PAWUKON_DWIWARA dwiwara);

const char8_t* const* calendar_balinese_pawukon_cycle_triwara_names();
extern const int calendar_balinese_pawukon_cycle_num_triwara_names;
const char8_t* calendar_balinese_pawukon_cycle_triwara_name(CALENDAR_BALINESE_PAWUKON_TRIWARA triwara);

const char8_t* const* calendar_balinese_pawukon_cycle_caturwara_names();
extern const int calendar_balinese_pawukon_cycle_num_caturwara_names;
const char8_t* calendar_balinese_pawukon_cycle_caturwara_name(CALENDAR_BALINESE_PAWUKON_CATURWARA caturwara);

const char8_t* const* calendar_balinese_pawukon_cycle_pancawara_names();
extern const int calendar_balinese_pawukon_cycle_num_pancawara_names;
const char8_t* calendar_balinese_pawukon_cycle_pancawara_name(CALENDAR_BALINESE_PAWUKON_PANCAWARA pancawara);

const char8_t* const* calendar_balinese_pawukon_cycle_sadwara_names();
extern const int calendar_balinese_pawukon_cycle_num_sadwara_names;
const char8_t* calendar_balinese_pawukon_cycle_sadwara_name(CALENDAR_BALINESE_PAWUKON_SADWARA sadwara);

const char8_t* const* calendar_balinese_pawukon_cycle_saptawara_names();
extern const int calendar_balinese_pawukon_cycle_num_saptawara_names;
const char8_t* calendar_balinese_pawukon_cycle_saptawara_name(CALENDAR_BALINESE_PAWUKON_SAPTAWARA saptawara);

const char8_t* const* calendar_balinese_pawukon_cycle_asatawara_names();
extern const int calendar_balinese_pawukon_cycle_num_asatawara_names;
const char8_t* calendar_balinese_pawukon_cycle_asatawara_name(CALENDAR_BALINESE_PAWUKON_ASATAWARA asatawara);

const char8_t* const* calendar_balinese_pawukon_cycle_sangawara_names();
extern const int calendar_balinese_pawukon_cycle_num_sangawara_names;
const char8_t* calendar_balinese_pawukon_cycle_sangawara_name(CALENDAR_BALINESE_PAWUKON_SANGAWARA sangawara);

const char8_t* const* calendar_balinese_pawukon_cycle_dasawara_names();
extern const int calendar_balinese_pawukon_cycle_num_dasawara_names;
const char8_t* calendar_balinese_pawukon_cycle_dasawara_name(CALENDAR_BALINESE_PAWUKON_DASAWARA dasawara);

const char8_t* const* calendar_balinese_pawukon_day_of_week_names();
const char8_t* calendar_balinese_pawukon_day_of_week_name(CALENDAR_DAY_OF_WEEK dayOfWeek);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_LIBRARY_DATE_TYPE_BALINESE_PAWUKON_DATE_H
