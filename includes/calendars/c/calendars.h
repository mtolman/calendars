#ifndef C_CALENDAR_LIBRARY_COMMON_H
#define C_CALENDAR_LIBRARY_COMMON_H

/** @file */

#include "calendars/common.h"

#include "calendars/akan.h"
#include "calendars/armenian.h"
#include "calendars/aztec.h"
#include "calendars/balinese_pawukon_date.h"
#include "calendars/clock.h"
#include "calendars/coptic.h"
#include "calendars/egyptian.h"
#include "calendars/gregorian.h"
#include "calendars/julian.h"
#include "calendars/moment.h"
#include "calendars/radix.h"

#endif  // C_CALENDAR_LIBRARY_COMMON_H
