#include <doctest/doctest.h>

#include <calendars/cxx/calendars/akan.h>

#include "test_utils.h"

TEST_SUITE("Akan Day") {
  TEST_CASE("Constructors") {
    auto akan = calendars::akan::Date{4, 5};
    CHECK_EQ(akan.prefix(), 4);
    CHECK_EQ(akan.stem(), 5);

    akan = calendars::akan::Date{6, 7};
    CHECK_EQ(akan.prefix(), 6);
    CHECK_EQ(akan.stem(), 7);

    akan = calendars::akan::Date{42};
    CHECK_EQ(akan.prefix(), 6);
    CHECK_EQ(akan.stem(), 7);

    akan = calendars::akan::Date{31};
    CHECK_EQ(akan.prefix(), 1);
    CHECK_EQ(akan.stem(), 7);

    akan = calendars::akan::Date{static_cast<int64_t>(42)};
    CHECK_EQ(akan.prefix(), 6);
    CHECK_EQ(akan.stem(), 7);
  }

  TEST_CASE("On or Before") {
    auto akan = calendars::akan::Date{1, 1};
    auto rd   = calendars::RdDate{38};

    CHECK_EQ(akan.rd_date_on_or_before(rd), rd);

    rd = calendars::RdDate{37};
    CHECK_EQ(akan.rd_date_on_or_before(rd), calendars::RdDate{-4});
  }

  TEST_CASE("Equality") {
    eq_tests_two_args<calendars::akan::Date>();
  }

  TEST_CASE("Sample Data") {
    using T = calendars::akan::Date;

    static std::vector<T> expected = {T{6, 5}, T{4, 1}, T{4, 1}, T{4, 5}, T{6, 1}, T{4, 6}, T{4, 4}, T{1, 5}, T{4, 5}, T{2, 3}, T{6, 4},
                                      T{5, 3}, T{4, 5}, T{3, 5}, T{1, 1}, T{1, 4}, T{3, 4}, T{4, 4}, T{5, 1}, T{1, 5}, T{3, 3}, T{1, 5},
                                      T{5, 6}, T{6, 1}, T{4, 5}, T{5, 5}, T{1, 6}, T{4, 6}, T{1, 2}, T{1, 7}, T{1, 5}, T{6, 1}, T{5, 5}};

    auto sampleRdDates = sample_rd_dates();

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
    }
  }
}
