#include <doctest/doctest.h>

#include <calendars/cxx/calendars/old_hindu_luni_solar.h>
#include "test_utils.h"

TEST_SUITE("Old Hindu Luni-Solar Date") {
  TEST_CASE("Comparison") {
    using CLASS = calendars::old_hindu_luni_solar::Date;

    CHECK(CLASS{0, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(0), 0, 0} == CLASS{0, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(0), 0, 0});
    CHECK(CLASS{58, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{58, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{58, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{5, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(5), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(2), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 6});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 0});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 0, 2} == CLASS{12, static_cast<CALENDAR_OLD_HINDU_LUNI_SOLAR_SANSKRIT>(3), 1, 0});

    CHECK_FALSE(CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{5, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(2), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 6});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 0});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} != CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 1, 0});

    CHECK_FALSE(CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0} < CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0});
    CHECK_FALSE(CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} < CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} < CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} < CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} < CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 6});
    CHECK(CLASS{15, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 0, 1} < CLASS{15, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 1, 1});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} < CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(2), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} < CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 0});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} < CLASS{5, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{15, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(1), 0, 2} < CLASS{13, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{15, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 0, 1} < CLASS{13, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});

    CHECK_FALSE(CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0} > CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0});
    CHECK_FALSE(CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} > CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} > CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} > CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} > CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 6});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} > CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(2), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} > CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 0});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} > CLASS{5, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});

    CHECK(CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0} <= CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0});
    CHECK(CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} <= CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} <= CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} <= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} <= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 6});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} <= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(2), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} <= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 0});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} <= CLASS{5, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});

    CHECK(CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0} >= CLASS{0, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(0), 0, 0});
    CHECK(CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} >= CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} >= CLASS{58, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} >= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(5), 0, 2});
    CHECK_FALSE(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} >= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 6});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} >= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(2), 0, 2});
    CHECK(CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} >= CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 0});
    CHECK((CLASS{12, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2} >= CLASS{5, static_cast<calendars::old_hindu_luni_solar::SANSKRIT>(3), 0, 2}));
  }

  TEST_CASE("Conversions") {
    calendars::old_hindu_luni_solar::Date date{};
    calendars::RdDate            rdate;

    rdate     = calendars::RdDate{8849};
    date      = rdate.to<decltype(date)>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::old_hindu_luni_solar::Date;
    using M = calendars::old_hindu_luni_solar::SANSKRIT;

    static std::vector<T> expected = {
        T{2515, static_cast<M>(6), false, 11}, T{2933, static_cast<M>(9), false, 26}, T{3171, static_cast<M>(8), false, 3},
        T{3236, static_cast<M>(8), false, 9}, T{3570, static_cast<M>(11), true, 19}, T{3677, static_cast<M>(3), false, 5},
        T{3795, static_cast<M>(9), false, 15}, T{4114, static_cast<M>(2), false, 7}, T{4197, static_cast<M>(2), false, 24},
        T{4291, static_cast<M>(1), false, 9}, T{4340, static_cast<M>(12), false, 9}, T{4389, static_cast<M>(1), false, 23},
        T{4399, static_cast<M>(2), false, 8}, T{4492, static_cast<M>(4), false, 2}, T{4536, static_cast<M>(11), false, 7},
        T{4593, static_cast<M>(1), false, 3}, T{4654, static_cast<M>(7), false, 2}, T{4660, static_cast<M>(11), false, 29},
        T{4749, static_cast<M>(3), false, 20}, T{4781, static_cast<M>(4), false, 4}, T{4817, static_cast<M>(5), false, 6},
        T{4869, static_cast<M>(4), false, 5}, T{4920, static_cast<M>(5), false, 12}, T{4940, static_cast<M>(1), true, 13},
        T{5004, static_cast<M>(1), false, 23}, T{5030, static_cast<M>(5), false, 21}, T{5042, static_cast<M>(7), false, 9},
        T{5044, static_cast<M>(1), false, 15}, T{5044, static_cast<M>(7), false, 9}, T{5092, static_cast<M>(12), false, 14},
        T{5096, static_cast<M>(12), false, 7}, T{5139, static_cast<M>(8), false, 14}, T{5195, static_cast<M>(4), false, 6},
    };

    const auto sampleRdDates = sample_rd_dates();
    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
