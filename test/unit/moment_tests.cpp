#include <doctest/doctest.h>

#include <calendars/cxx/calendars/common.h>
#include "test_utils.h"

TEST_SUITE("Moment") {
  TEST_CASE("Comparisons") {
    comp_test_one_arg<calendars::Moment>();
  }

  TEST_CASE("Conversions") {
    calendars::Moment moment{};
    calendars::RdDate rdate{};

    rdate       = calendars::RdDate{0};
    moment = calendars::Moment{0.5};
    CHECK_EQ(rdate, calendars::RdDate::from(moment));
    CHECK_EQ(rdate.to<calendars::Moment>().datetime(), 0);
    CHECK_EQ(calendars::Moment::from(moment).datetime(), moment.datetime());
    CHECK_EQ(calendars::Moment::from(moment).to<calendars::Moment>().datetime(), moment.datetime());

    rdate       = calendars::RdDate{-1};
    moment = calendars::Moment{-0.5};
    CHECK_EQ(rdate, calendars::RdDate::from(moment));

    moment = calendars::Moment{10.5};
    CHECK_IN_RANGE(0.5, moment.to_time(), 0.01)

    moment = calendars::Moment{12.7895};
    CHECK_IN_RANGE(0.7895, moment.to_time(), 0.01)

    moment = calendars::Moment{-12.7895};
    CHECK_IN_RANGE(0.210500, moment.to_time(), 0.01)
  }
}
