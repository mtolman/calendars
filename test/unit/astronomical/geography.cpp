#include <doctest/doctest.h>
#include <calendars/cxx/calendars/astronomical/geography.h>
#include <calendars/c/calendars/astronomical/geography.h>
#include "../test_utils.h"

TEST_SUITE("LatLong") {
  TEST_CASE("C") {
    double expectedLat = 21.423332833333333;
    double expectedLon = 39.82333283333334;
    CalendarGeoDegMinSec latIn = {.degrees=21, .minutes=25, .seconds=24};
    CalendarGeoDegMinSec lonIn = {.degrees=39, .minutes=49, .seconds=24};
    REQUIRE_IN_RANGE(calendar_lat_lon_degrees_from_dms(latIn), expectedLat);
    REQUIRE_IN_RANGE(calendar_lat_lon_degrees_from_dms(lonIn), expectedLon);
  }

  TEST_CASE("C++") {
    using namespace SI::Geo::literals;
    const auto expectedLat = 21.423332833333333_degree;
    const auto expectedLon = 39.82333283333334_degree;
    calendars::astronomical::DegMinSec latIn = {21_degree, 25_minutes, 24_seconds};
    calendars::astronomical::DegMinSec lonIn = {39_degree, 49_minutes, 24_seconds};
    REQUIRE_IN_RANGE(latIn.as_degrees().value(), expectedLat.value());
    REQUIRE_IN_RANGE(lonIn.as_degrees().value(), expectedLon.value());
  }
}
