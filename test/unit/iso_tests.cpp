#include <doctest/doctest.h>

#include <calendars/cxx/calendars/iso.h>
#include "test_utils.h"

TEST_SUITE("Iso Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::iso::Date>();
  }

  TEST_CASE("Conversions") {
    calendars::iso::Date date{};
    calendars::RdDate  rdate;

    rdate     = calendars::RdDate{8849};
    date      = rdate.to<calendars::iso::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::iso::Date;

    static std::vector<T> expected = {
        calendars::iso::Date{-586, 29, 7},
        calendars::iso::Date{-168, 49, 3},
        calendars::iso::Date{70, 39, 3},
        calendars::iso::Date{135, 39, 7},
        calendars::iso::Date{470, 2, 3},
        calendars::iso::Date{576, 21, 1},
        calendars::iso::Date{694, 45, 6},
        calendars::iso::Date{1013, 16, 7},
        calendars::iso::Date{1096, 21, 7},
        calendars::iso::Date{1190, 12, 5},
        calendars::iso::Date{1240, 10, 6},
        calendars::iso::Date{1288, 14, 5},
        calendars::iso::Date{1298, 17, 7},
        calendars::iso::Date{1391, 23, 7},
        calendars::iso::Date{1436, 5, 3},
        calendars::iso::Date{1492, 14, 6},
        calendars::iso::Date{1553, 38, 6},
        calendars::iso::Date{1560, 9, 6},
        calendars::iso::Date{1648, 24, 3},
        calendars::iso::Date{1680, 26, 7},
        calendars::iso::Date{1716, 30, 5},
        calendars::iso::Date{1768, 24, 7},
        calendars::iso::Date{1819, 31, 1},
        calendars::iso::Date{1839, 13, 3},
        calendars::iso::Date{1903, 16, 7},
        calendars::iso::Date{1929, 34, 7},
        calendars::iso::Date{1941, 40, 1},
        calendars::iso::Date{1943, 16, 1},
        calendars::iso::Date{1943, 40, 4},
        calendars::iso::Date{1992, 12, 2},
        calendars::iso::Date{1996, 8, 7},
        calendars::iso::Date{2038, 45, 3},
        calendars::iso::Date{2094, 28, 7}
    };

    const auto sampleRdDates = sample_rd_dates();
    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
