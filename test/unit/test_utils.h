#pragma once

#include <array>
#include <calendars/cxx/calendars/common.h>

template<typename CLASS>
void comp_test_one_arg() {
  CHECK(CLASS{0} == CLASS{0});
  CHECK(CLASS{58} == CLASS{58});
  CHECK_FALSE(CLASS{12} == CLASS{58});
  CHECK_FALSE(CLASS{12} == CLASS{5});

  CHECK(CLASS{58} != CLASS{12});
  CHECK(CLASS{12} != CLASS{58});
  CHECK_FALSE(CLASS{58} != CLASS{58});

  CHECK(CLASS{12} < CLASS{58});
  CHECK_FALSE(CLASS{58} < CLASS{58});
  CHECK_FALSE(CLASS{60} < CLASS{58});

  CHECK(CLASS{58} > CLASS{12});
  CHECK_FALSE(CLASS{58} > CLASS{58});
  CHECK_FALSE(CLASS{58} > CLASS{60});

  CHECK(CLASS{12} <= CLASS{58});
  CHECK(CLASS{58} <= CLASS{58});
  CHECK_FALSE(CLASS{60} <= CLASS{58});
  CHECK(CLASS{58} >= CLASS{12});
  CHECK(CLASS{58} >= CLASS{58});
  CHECK_FALSE(CLASS{58} >= CLASS{60});
}

template<typename CLASS, typename MONTH_CAST=int16_t>
void comp_tests_three_args() {
  CHECK(CLASS{0, static_cast<MONTH_CAST>(0), 0} == CLASS{0, static_cast<MONTH_CAST>(0), 0});
  CHECK(CLASS{58, static_cast<MONTH_CAST>(3), 2} == CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} == CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} == CLASS{5,  static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} == CLASS{12, static_cast<MONTH_CAST>(5), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} == CLASS{12, static_cast<MONTH_CAST>(2), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} == CLASS{12, static_cast<MONTH_CAST>(3), 6});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} == CLASS{12, static_cast<MONTH_CAST>(3), 0});

  CHECK_FALSE(CLASS{58, static_cast<MONTH_CAST>(3), 2} != CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} != CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} != CLASS{5, static_cast<MONTH_CAST>(3), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} != CLASS{12, static_cast<MONTH_CAST>(5), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} != CLASS{12, static_cast<MONTH_CAST>(2), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} != CLASS{12, static_cast<MONTH_CAST>(3), 6});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} != CLASS{12, static_cast<MONTH_CAST>(3), 0});

  CHECK_FALSE(CLASS{0, static_cast<MONTH_CAST>(0), 0} < CLASS{0, static_cast<MONTH_CAST>(0), 0});
  CHECK_FALSE(CLASS{58, static_cast<MONTH_CAST>(3), 2} < CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} < CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} < CLASS{12, static_cast<MONTH_CAST>(5), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} < CLASS{12, static_cast<MONTH_CAST>(3), 6});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} < CLASS{12, static_cast<MONTH_CAST>(2), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} < CLASS{12, static_cast<MONTH_CAST>(3), 0});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} < CLASS{5, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{15, static_cast<MONTH_CAST>(1), 2} < CLASS{13, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{15, static_cast<MONTH_CAST>(5), 1} < CLASS{13, static_cast<MONTH_CAST>(3), 2});

  CHECK_FALSE(CLASS{0, static_cast<MONTH_CAST>(0), 0} > CLASS{0, static_cast<MONTH_CAST>(0), 0});
  CHECK_FALSE(CLASS{58, static_cast<MONTH_CAST>(3), 2} > CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} > CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} > CLASS{12, static_cast<MONTH_CAST>(5), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} > CLASS{12, static_cast<MONTH_CAST>(3), 6});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} > CLASS{12, static_cast<MONTH_CAST>(2), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} > CLASS{12, static_cast<MONTH_CAST>(3), 0});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} > CLASS{5, static_cast<MONTH_CAST>(3), 2});

  CHECK(CLASS{0, static_cast<MONTH_CAST>(0), 0} <= CLASS{0, static_cast<MONTH_CAST>(0), 0});
  CHECK(CLASS{58, static_cast<MONTH_CAST>(3), 2} <= CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} <= CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} <= CLASS{12, static_cast<MONTH_CAST>(5), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} <= CLASS{12, static_cast<MONTH_CAST>(3), 6});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} <= CLASS{12, static_cast<MONTH_CAST>(2), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} <= CLASS{12, static_cast<MONTH_CAST>(3), 0});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} <= CLASS{5, static_cast<MONTH_CAST>(3), 2});

  CHECK(CLASS{0, static_cast<MONTH_CAST>(0), 0} >= CLASS{0, static_cast<MONTH_CAST>(0), 0});
  CHECK(CLASS{58, static_cast<MONTH_CAST>(3), 2} >= CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} >= CLASS{58, static_cast<MONTH_CAST>(3), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} >= CLASS{12, static_cast<MONTH_CAST>(5), 2});
  CHECK_FALSE(CLASS{12, static_cast<MONTH_CAST>(3), 2} >= CLASS{12, static_cast<MONTH_CAST>(3), 6});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} >= CLASS{12, static_cast<MONTH_CAST>(2), 2});
  CHECK(CLASS{12, static_cast<MONTH_CAST>(3), 2} >= CLASS{12, static_cast<MONTH_CAST>(3), 0});
  CHECK((CLASS{12, static_cast<MONTH_CAST>(3), 2} >= CLASS{5, static_cast<MONTH_CAST>(3), 2}));
}

template<typename CLASS>
void eq_tests_two_args() {
  CHECK(CLASS{0, 0} == CLASS{0, 0});
  CHECK(CLASS{58, 3} == CLASS{58, 3});
  CHECK_FALSE(CLASS{12, 3} == CLASS{58, 3});
  CHECK_FALSE(CLASS{12, 3} == CLASS{5, 3});
  CHECK_FALSE(CLASS{12, 3} == CLASS{12, 5});
  CHECK_FALSE(CLASS{12, 3} == CLASS{12, 2});

  CHECK_FALSE(CLASS{58, 3} != CLASS{58, 3});
  CHECK(CLASS{12, 3} != CLASS{58, 3});
  CHECK(CLASS{12, 3} != CLASS{5, 3});
  CHECK(CLASS{12, 3} != CLASS{12, 5});
  CHECK(CLASS{12, 3} != CLASS{12, 2});
}

template<typename CLASS, typename FIRST_CAST, typename SECOND_CAST>
void eq_tests_two_args_cast() {
  CHECK(CLASS{static_cast<FIRST_CAST>(0), static_cast<SECOND_CAST>(0)} == CLASS{static_cast<FIRST_CAST>(0), static_cast<SECOND_CAST>(0)});
  CHECK(CLASS{static_cast<FIRST_CAST>(58), static_cast<SECOND_CAST>(3)} == CLASS{static_cast<FIRST_CAST>(58), static_cast<SECOND_CAST>(3)});
  CHECK_FALSE(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} == CLASS{static_cast<FIRST_CAST>(58), static_cast<SECOND_CAST>(3)});
  CHECK_FALSE(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} == CLASS{static_cast<FIRST_CAST>(5), static_cast<SECOND_CAST>(3)});
  CHECK_FALSE(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} == CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(5)});
  CHECK_FALSE(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} == CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(2)});

  CHECK_FALSE(CLASS{static_cast<FIRST_CAST>(58), static_cast<SECOND_CAST>(3)} != CLASS{static_cast<FIRST_CAST>(58), static_cast<SECOND_CAST>(3)});
  CHECK(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} != CLASS{static_cast<FIRST_CAST>(58), static_cast<SECOND_CAST>(3)});
  CHECK(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} != CLASS{static_cast<FIRST_CAST>(5), static_cast<SECOND_CAST>(3)});
  CHECK(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} != CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(5)});
  CHECK(CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(3)} != CLASS{static_cast<FIRST_CAST>(12), static_cast<SECOND_CAST>(2)});
}

inline auto sample_rd_dates() {
  return std::array{// NOLINT(cert-err58-cpp)
                    calendars::RdDate{-214193}, calendars::RdDate{-61387}, calendars::RdDate{25469},  calendars::RdDate{49217},  calendars::RdDate{171307},
                    calendars::RdDate{210155},  calendars::RdDate{253427}, calendars::RdDate{369740}, calendars::RdDate{400085}, calendars::RdDate{434355},
                    calendars::RdDate{452605},  calendars::RdDate{470160}, calendars::RdDate{473837}, calendars::RdDate{507850}, calendars::RdDate{524156},
                    calendars::RdDate{544676},  calendars::RdDate{567118}, calendars::RdDate{569477}, calendars::RdDate{601716}, calendars::RdDate{613424},
                    calendars::RdDate{626596},  calendars::RdDate{645554}, calendars::RdDate{664224}, calendars::RdDate{671401}, calendars::RdDate{694799},
                    calendars::RdDate{704424},  calendars::RdDate{708842}, calendars::RdDate{709409}, calendars::RdDate{709580}, calendars::RdDate{727274},
                    calendars::RdDate{728714},  calendars::RdDate{744313}, calendars::RdDate{764652}};
}

#ifndef CHECK_IN_RANGE
#define EPSILON 0.0001
#define CHECK_IN_RANGE(D, V, E)                                                                                                                      \
  CHECK((D) <= (V) + (E));                                                                                                                           \
  CHECK((D) >= (V) - (E));
#define CHECK_IN_RANGE_DEF(D, V) CHECK_IN_RANGE(D, V, EPSILON)
#define CHECK_RET(V, N)                                                                                                                              \
  ([ & ] {                                                                                                                                           \
    const auto res = (V);                                                                                                                            \
    if (!res) {                                                                                                                                      \
      std::cout << "Failed input: " << (N) << "\n";                                                                                                  \
    }                                                                                                                                                \
    CHECK((V));                                                                                                                                      \
    return (V);                                                                                                                                      \
  })()
#define CHECK_RETB(V)                                                                                                                                \
  ([ & ] {                                                                                                                                           \
    const auto res = (V);                                                                                                                            \
    return (V);                                                                                                                                      \
  })()
#define REQUIRE_IN_RANGE(D, E)                                                                                                                       \
  {                                                                                                                                                  \
    const auto out      = (D);                                                                                                                       \
    const auto expected = (E);                                                                                                                       \
    REQUIRE_LE(out, expected + EPSILON);                                                                                                             \
    REQUIRE_GE(out, expected - EPSILON);                                                                                                             \
  }

#define REQUIRE_IN_RANGE_E(D, V, E)                                                                                                                  \
  ([ & ]() {                                                                                                                                         \
    const auto out = (D);                                                                                                                            \
    const auto in  = (V);                                                                                                                            \
    if (!(out <= in + (E) && out >= in - (E))) {                                                                                                     \
      std::cout << std::setprecision(25) << "Failed input: " << in << "\tFailed output:" << out << "\n";                                             \
    }                                                                                                                                                \
    return CHECK_RET((D) <= (V) + (E), V) && CHECK_RET((D) >= (V)-E, V);                                                                             \
  })();
#endif
