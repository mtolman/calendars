#include <doctest/doctest.h>

#include <calendars/cxx/calendars/comp_cyclic.h>
#include "test_utils.h"

TEST_SUITE("Templated Single Cycle") {
  TEST_CASE("Strictly Before") {
    // Recreate the coptic calendar using our generic single-cycle calendar
    using CopticCyclic = calendars::comp_cyclic::single_cycle::Date<calendars::comp_cyclic::single_cycle::Type::STRICTLY_BEFORE, 103605, (365 * 4) + 1, 4, 30, 1, 1, 4>;

    SUBCASE("Comparisons") { comp_tests_three_args<CopticCyclic, int32_t>(); }

    SUBCASE("Conversions") {
      CopticCyclic    date;
      calendars::RdDate rdate;

      rdate     = calendars::RdDate{8849};
      date      = rdate.to<CopticCyclic>();
      rdate     = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }

    SUBCASE("Sample Data") {
      using T = CopticCyclic;

      static std::vector<T> expected = {T{-870, 12, 6}, T{-451, 4, 12},  T{-213, 1, 29},  T{-148, 2, 5},   T{186, 5, 12},   T{292, 9, 23},
                                        T{411, 3, 11},  T{729, 8, 24},   T{812, 9, 23},   T{906, 7, 20},   T{956, 7, 7},    T{1004, 7, 30},
                                        T{1014, 8, 25}, T{1107, 10, 10}, T{1152, 5, 29},  T{1208, 8, 5},   T{1270, 1, 12},  T{1276, 6, 29},
                                        T{1364, 10, 6}, T{1396, 10, 26}, T{1432, 11, 19}, T{1484, 10, 14}, T{1535, 11, 27}, T{1555, 7, 19},
                                        T{1619, 8, 11}, T{1645, 12, 19}, T{1658, 1, 19},  T{1659, 8, 11},  T{1660, 1, 26},  T{1708, 7, 8},
                                        T{1712, 6, 17}, T{1755, 3, 1},   T{1810, 11, 11}};

      const auto sampleRdDates = sample_rd_dates();

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
        CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
      }
    }
    static_assert(CopticCyclic::epoch == 103605);
  }

  TEST_CASE("Up To And Including") {
    // Recreate the coptic calendar using our generic single-cycle calendar
    using CopticCyclic = calendars::comp_cyclic::single_cycle::Date<calendars::comp_cyclic::single_cycle::Type::UP_TO_AND_INCLUDING, 103605, (365 * 4) + 1, 4, 30, 1, 1, 2>;

    SUBCASE("Comparisons") { comp_tests_three_args<CopticCyclic, int32_t>(); }

    SUBCASE("Conversions") {
      CopticCyclic    date;
      calendars::RdDate rdate;

      rdate     = calendars::RdDate{8849};
      date      = rdate.to<CopticCyclic>();
      rdate     = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }

    SUBCASE("Sample Data") {
      using T = CopticCyclic;

      static std::vector<T> expected = {T{-870, 12, 6}, T{-451, 4, 12},  T{-213, 1, 29},  T{-148, 2, 5},   T{186, 5, 12},   T{292, 9, 23},
                                        T{411, 3, 11},  T{729, 8, 24},   T{812, 9, 23},   T{906, 7, 20},   T{956, 7, 7},    T{1004, 7, 30},
                                        T{1014, 8, 25}, T{1107, 10, 10}, T{1152, 5, 29},  T{1208, 8, 5},   T{1270, 1, 12},  T{1276, 6, 29},
                                        T{1364, 10, 6}, T{1396, 10, 26}, T{1432, 11, 19}, T{1484, 10, 14}, T{1535, 11, 27}, T{1555, 7, 19},
                                        T{1619, 8, 11}, T{1645, 12, 19}, T{1658, 1, 19},  T{1659, 8, 11},  T{1660, 1, 26},  T{1708, 7, 8},
                                        T{1712, 6, 17}, T{1755, 3, 1},   T{1810, 11, 11}};

      const auto sampleRdDates = sample_rd_dates();
      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
        CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
      }
    }
    static_assert(CopticCyclic::epoch == 103605);
  }

  TEST_CASE("Mean Month") {
    // Recreate the coptic calendar using our generic single-cycle calendar
    using Cyclic = calendars::comp_cyclic::single_cycle::Date<calendars::comp_cyclic::single_cycle::Type::MEAN_MONTH, 103605, 360, 4, 30, 1>;

    SUBCASE("Comparisons") { comp_tests_three_args<Cyclic, int32_t>(); }

    SUBCASE("Conversions") {
      Cyclic    date;
      calendars::RdDate rdate;

      rdate     = calendars::RdDate{8849};
      date      = rdate.to<Cyclic>();
      rdate     = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }
  }

  TEST_CASE("At or Before Mean Month") {
    // Recreate the coptic calendar using our generic single-cycle calendar
    using Cyclic = calendars::comp_cyclic::single_cycle::Date<calendars::comp_cyclic::single_cycle::Type::AT_OR_BEFORE_MEAN_MONTH, 103605, 360, 4, 30, 1>;

    SUBCASE("Comparisons") { comp_tests_three_args<Cyclic, int32_t>(); }

    SUBCASE("Conversions") {
      Cyclic    date;
      calendars::RdDate rdate;

      rdate     = calendars::RdDate{8849};
      date      = rdate.to<Cyclic>();
      rdate     = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }
  }
}

TEST_SUITE("Templated Double Cycle") {
  TEST_CASE("At or Before") {
    using Cyclic = calendars::comp_cyclic::double_cycle::Date<calendars::comp_cyclic::double_cycle::Type::AT_OR_BEFORE, 103605, 360, 4, 30, 1>;

    SUBCASE("Comparisons") { comp_tests_three_args<Cyclic>(); }

    SUBCASE("Conversions") {
      Cyclic    date;
      calendars::RdDate rdate;

      rdate     = calendars::RdDate{8849};
      date      = rdate.to<Cyclic>();
      rdate     = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }
  }
}
