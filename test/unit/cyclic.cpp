#include <calendars/cxx/calendars/cyclic.h>
#include <doctest/doctest.h>

#include "test_utils.h"

TEST_SUITE("Single Cycle") {
  TEST_CASE("Strictly Before") {
    // Recreate the coptic calendar using our generic single-cycle calendar
    const auto templ = calendars::cyclic::single_cycle::DateTemplate{calendars::cyclic::single_cycle::Type::STRICTLY_BEFORE,
                                                                     103605,
                                                                     calendars::math::int_to_pnum(365) + calendars::math::int_to_pnum(1) / calendars::math::int_to_pnum(4),
                                                                     calendars::math::int_to_pnum(30),
                                                                     calendars::math::int_to_pnum(1) / calendars::math::int_to_pnum(4),
                                                                     calendars::math::int_to_pnum(0)};

    SUBCASE("Comparisons") {
      using Date = calendars::cyclic::single_cycle::Date;
      CHECK(Date{0, (0), 0, templ} == Date{0, (0), 0, templ});
      CHECK(Date{58, (3), 2, templ} == Date{58, (3), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} == Date{58, (3), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} == Date{5,  (3), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} == Date{12, (5), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} == Date{12, (2), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} == Date{12, (3), 6, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} == Date{12, (3), 0, templ});
      
      CHECK_FALSE(Date{58, (3), 2, templ} != Date{58, (3), 2, templ});
      CHECK(Date{12, (3), 2, templ} != Date{58, (3), 2, templ});
      CHECK(Date{12, (3), 2, templ} != Date{5, (3), 2, templ});
      CHECK(Date{12, (3), 2, templ} != Date{12, (5), 2, templ});
      CHECK(Date{12, (3), 2, templ} != Date{12, (2), 2, templ});
      CHECK(Date{12, (3), 2, templ} != Date{12, (3), 6, templ});
      CHECK(Date{12, (3), 2, templ} != Date{12, (3), 0, templ});

      CHECK_FALSE(Date{0, (0), 0, templ} < Date{0, (0), 0, templ});
      CHECK_FALSE(Date{58, (3), 2, templ} < Date{58, (3), 2, templ});
      CHECK(Date{12, (3), 2, templ} < Date{58, (3), 2, templ});
      CHECK(Date{12, (3), 2, templ} < Date{12, (5), 2, templ});
      CHECK(Date{12, (3), 2, templ} < Date{12, (3), 6, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} < Date{12, (2), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} < Date{12, (3), 0, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} < Date{5, (3), 2, templ});
      CHECK_FALSE(Date{15, (1), 2, templ} < Date{13, (3), 2, templ});
      CHECK_FALSE(Date{15, (5), 1, templ} < Date{13, (3), 2, templ});

      CHECK_FALSE(Date{0, (0), 0, templ} > Date{0, (0), 0, templ});
      CHECK_FALSE(Date{58, (3), 2, templ} > Date{58, (3), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} > Date{58, (3), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} > Date{12, (5), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} > Date{12, (3), 6, templ});
      CHECK(Date{12, (3), 2, templ} > Date{12, (2), 2, templ});
      CHECK(Date{12, (3), 2, templ} > Date{12, (3), 0, templ});
      CHECK(Date{12, (3), 2, templ} > Date{5, (3), 2, templ});

      CHECK(Date{0, (0), 0, templ} <= Date{0, (0), 0, templ});
      CHECK(Date{58, (3), 2, templ} <= Date{58, (3), 2, templ});
      CHECK(Date{12, (3), 2, templ} <= Date{58, (3), 2, templ});
      CHECK(Date{12, (3), 2, templ} <= Date{12, (5), 2, templ});
      CHECK(Date{12, (3), 2, templ} <= Date{12, (3), 6, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} <= Date{12, (2), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} <= Date{12, (3), 0, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} <= Date{5, (3), 2, templ});

      CHECK(Date{0, (0), 0, templ} >= Date{0, (0), 0, templ});
      CHECK(Date{58, (3), 2, templ} >= Date{58, (3), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} >= Date{58, (3), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} >= Date{12, (5), 2, templ});
      CHECK_FALSE(Date{12, (3), 2, templ} >= Date{12, (3), 6, templ});
      CHECK(Date{12, (3), 2, templ} >= Date{12, (2), 2, templ});
      CHECK(Date{12, (3), 2, templ} >= Date{12, (3), 0, templ});
      CHECK((Date{12, (3), 2, templ} >= Date{5, (3), 2, templ}));
    }

    SUBCASE("Conversions") {
      using Date = calendars::cyclic::single_cycle::Date;
      Date      date;
      calendars::RdDate rdate;

      rdate = calendars::RdDate{8849};
      date  = Date::from_rd_date(rdate, templ);
      rdate = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }

    SUBCASE("Sample Data") {
      using T = calendars::cyclic::single_cycle::Date;

      static std::vector<T> expected = {T{-870, 12, 6, templ}, T{-451, 4, 12, templ},  T{-213, 1, 29, templ},  T{-148, 2, 5, templ},   T{186,  5, 12, templ},  T{292,  9, 23, templ},
                                        T{411,  3, 11, templ}, T{729,  8, 24, templ},  T{812,  9, 23, templ},  T{906,  7, 20, templ},  T{956,  7, 7, templ},   T{1004, 7, 30, templ},
                                        T{1014, 8, 25, templ}, T{1107, 10, 10, templ}, T{1152, 5, 29, templ},  T{1208, 8, 5, templ},   T{1270, 1, 12, templ},  T{1276, 6, 29, templ},
                                        T{1364, 10, 6, templ}, T{1396, 10, 26, templ}, T{1432, 11, 19, templ}, T{1484, 10, 14, templ}, T{1535,11, 27, templ},  T{1555, 7, 19, templ},
                                        T{1619, 8, 11, templ}, T{1645, 12, 19, templ}, T{1658,  1, 19, templ}, T{1659, 8, 11, templ},  T{1660, 1, 26, templ},  T{1708, 7,  8, templ},
                                        T{1712, 6, 17, templ}, T{1755,  3, 1, templ},  T{1810, 11, 11, templ}};

      const auto sampleRdDates = sample_rd_dates();

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(T::from_rd_date(sampleRdDates[ i ], templ), expected[ i ]);
        CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
      }
    }
  }

  TEST_CASE("Up To And Including") {
    const auto templ = calendars::cyclic::single_cycle::DateTemplate{calendars::cyclic::single_cycle::Type::UP_TO_AND_INCLUDING,
                                                                     103605,
                                                                     calendars::math::int_to_pnum(365 * 4 + 1) / calendars::math::int_to_pnum(4),
                                                                     calendars::math::int_to_pnum(30),
                                                                     calendars::math::int_to_pnum(1) / calendars::math::int_to_pnum(2)};
    SUBCASE("Conversions") {
      using Date = calendars::cyclic::single_cycle::Date;
      Date      date;
      calendars::RdDate rdate;

      rdate = calendars::RdDate{8849};
      date  = Date::from_rd_date(rdate, templ);
      rdate = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }

    SUBCASE("Sample Data") {
      using T = calendars::cyclic::single_cycle::Date;

      static std::vector<T> expected = {T{-870, 12, 6, templ}, T{-451, 4, 12, templ},  T{-213, 1, 29, templ},  T{-148, 2, 5, templ},   T{186,  5, 12, templ},  T{292,  9, 23, templ},
                                        T{411,  3, 11, templ}, T{729,  8, 24, templ},  T{812,  9, 23, templ},  T{906,  7, 20, templ},  T{956,  7, 7, templ},   T{1004, 7, 30, templ},
                                        T{1014, 8, 25, templ}, T{1107, 10, 10, templ}, T{1152, 5, 29, templ},  T{1208, 8, 5, templ},   T{1270, 1, 12, templ},  T{1276, 6, 29, templ},
                                        T{1364, 10, 6, templ}, T{1396, 10, 26, templ}, T{1432, 11, 19, templ}, T{1484, 10, 14, templ}, T{1535,11, 27, templ},  T{1555, 7, 19, templ},
                                        T{1619, 8, 11, templ}, T{1645, 12, 19, templ}, T{1658,  1, 19, templ}, T{1659, 8, 11, templ},  T{1660, 1, 26, templ},  T{1708, 7,  8, templ},
                                        T{1712, 6, 17, templ}, T{1755,  3, 1, templ},  T{1810, 11, 11, templ}};

      const auto sampleRdDates = sample_rd_dates();

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(T::from_rd_date(sampleRdDates[ i ], templ), expected[ i ]);
        CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
      }
    }
  }

  TEST_CASE("Mean Month") {
    const auto templ = calendars::cyclic::single_cycle::DateTemplate{calendars::cyclic::single_cycle::Type::MEAN_MONTH,
                                                                     103605,
                                                                     calendars::math::int_to_pnum(360) / calendars::math::int_to_pnum(4),
                                                                     calendars::math::int_to_pnum(30)};

    SUBCASE("Conversions") {
      using Date = calendars::cyclic::single_cycle::Date;
      Date      date;
      calendars::RdDate rdate;

      rdate = calendars::RdDate{8849};
      date  = Date::from_rd_date(rdate, templ);
      rdate = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }
  }

  TEST_CASE("At or Before Mean Month") {
    const auto templ = calendars::cyclic::single_cycle::DateTemplate{calendars::cyclic::single_cycle::Type::AT_OR_BEFORE_MEAN_MONTH,
                                                                     103605,
                                                                     calendars::math::int_to_pnum(360) / calendars::math::int_to_pnum(4),
                                                                     calendars::math::int_to_pnum(30)};

    SUBCASE("Conversions") {
      using Date = calendars::cyclic::single_cycle::Date;
      Date      date;
      calendars::RdDate rdate;

      rdate = calendars::RdDate{8849};
      date  = Date::from_rd_date(rdate, templ);
      rdate = calendars::RdDate::from(date);
      CHECK_EQ(rdate, calendars::RdDate{8849});
    }
  }
}

//TEST_SUITE("Double Cycle") {
//  TEST_CASE("At or Before") {
//    using Cyclic = calendars::cyclic::double_cycle::Date<calendars::cyclic::double_cycle::Type::AT_OR_BEFORE, 103605, 360, 30, 4, 1>;
//
//    SUBCASE("Comparisons") { tests_three_args<Cyclic>(); }
//
//    SUBCASE("Conversions") {
//      Cyclic            date;
//      calendars::RdDate rdate;
//
//      rdate = calendars::RdDate{8849};
//      date  = rdate.to<Cyclic>();
//      rdate = calendars::RdDate::from(date);
//      CHECK_EQ(rdate, calendars::RdDate{8849});
//    }
//  }
//}
