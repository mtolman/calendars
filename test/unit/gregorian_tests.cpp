#include <calendars/cxx/calendars/gregorian.h>
#include <doctest/doctest.h>

#include "test_utils.h"

TEST_SUITE("GregorianCalendar") {
  TEST_CASE("Comparisons") { comp_tests_three_args<calendars::gregorian::Date, calendars::gregorian::MONTH>(); }

  TEST_CASE("Comparisons") {
    auto dateBase      = calendars::gregorian::Date{2020, MONTH_DECEMBER, 14};
    auto dateCopy      = calendars::gregorian::Date(2020, MONTH_DECEMBER, 14);
    auto dateLessDay   = calendars::gregorian::Date(2020, MONTH_DECEMBER, 12);
    auto dateLessYear  = calendars::gregorian::Date(2018, MONTH_DECEMBER, 14);
    auto dateLessMonth = calendars::gregorian::Date(2020, MONTH_NOVEMBER, 14);

    CHECK_EQ(dateBase.year(), 2020);
    CHECK_EQ(dateBase.month(), calendars::gregorian::MONTH::DECEMBER);
    CHECK_EQ(dateBase.day(), 14);

    CHECK_EQ(dateBase, dateBase);

    CHECK_EQ(dateCopy, dateBase);
    CHECK_EQ(dateBase, dateCopy);

    CHECK_GT(dateBase, dateLessDay);
    CHECK_GT(dateBase, dateLessYear);
    CHECK_GT(dateBase, dateLessMonth);

    CHECK_GE(dateBase, dateCopy);
    CHECK_GE(dateBase, dateLessDay);
    CHECK_GE(dateBase, dateLessYear);
    CHECK_GE(dateBase, dateLessMonth);

    CHECK_LT(dateLessDay, dateBase);
    CHECK_LT(dateLessYear, dateBase);
    CHECK_LT(dateLessMonth, dateBase);

    CHECK_LE(dateBase, dateCopy);
    CHECK_LE(dateLessDay, dateBase);
    CHECK_LE(dateLessYear, dateBase);
    CHECK_LE(dateLessMonth, dateBase);
  }

  TEST_CASE("Ranges") {
    using MONTH = calendars::gregorian::MONTH;
    using G     = calendars::gregorian::Date;
    SUBCASE("Year Start") {
      CHECK_EQ(G{2020, MONTH::JANUARY, 1}, G::year_start(2020));
      CHECK_EQ(G{-2020, MONTH::JANUARY, 1}, G::year_start(-2020));
    }

    SUBCASE("Year End") {
      CHECK_EQ(G{2020, MONTH::DECEMBER, 31}, G::year_end(2020));
      CHECK_EQ(G{-2020, MONTH::DECEMBER, 31}, G::year_end(-2020));
    }

    SUBCASE("Year Boundaries") {
      CHECK_EQ(G{2020, MONTH::JANUARY, 1}, std::get<0>(G::year_boundaries(2020)));
      CHECK_EQ(G{2020, MONTH::DECEMBER, 31}, std::get<1>(G::year_boundaries(2020)));
    }

    SUBCASE("Year Vector") {
      SUBCASE("Non-leap year") {
        const auto year = 2019;
        REQUIRE(!G::is_leap_year(year));
        auto v = G::year_days(year);
        CHECK_EQ(365, v.size());
        CHECK_EQ(G{year, MONTH::JANUARY, 1}, v[ 0 ].to<G>());
        CHECK_EQ(G{year, MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

        for (size_t i = 1; i < v.size(); ++i) {
          CHECK_EQ(v[ i ].day_difference(v[ i - 1 ]), 1);
        }
      }

      SUBCASE("leap year") {
        const auto year = 2020;
        REQUIRE(G::is_leap_year(year));
        auto v = G::year_days(year);
        CHECK_EQ(366, v.size());
        CHECK_EQ(G{year, MONTH::JANUARY, 1}, v[ 0 ].to<G>());
        CHECK_EQ(G{year, MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

        for (size_t i = 1; i < v.size(); ++i) {
          CHECK_EQ(v[ i ].day_difference(v[ i - 1 ]), 1);
        }
      }

      SUBCASE("Days in Year") {
        auto check = [](int32_t year, int16_t month, uint16_t day) {
          auto       date     = G{year, static_cast<MONTH>(month), static_cast<int16_t>(day)};
          const auto expected = G::is_leap_year(year) ? 366 : 365;
          REQUIRE_EQ(date.day_number() + date.days_remaining(), expected);
          REQUIRE((date.day_number() != date.days_remaining() || (expected == 366 && date.day_number() == 366 / 2)));
        };

        check(2020, 12, 28);
        check(-256, 3, 2);
        check(4, 2, 29);
      }
    }
  }

  TEST_CASE("Conversions") {
    auto dateBase      = calendars::gregorian::Date{2020, MONTH_DECEMBER, 14};
    auto dateConverted = calendars::gregorian::Date::from_rd_date(dateBase.to_rd_date());

    CHECK_EQ(dateBase, dateConverted);
  }
}

TEST_SUITE("Gregorian - Date Range") {
  TEST_CASE("Between specific dates") {
    auto range = calendars::DateRange<calendars::gregorian::Date>{calendars::RdDate{10}.to<calendars::gregorian::Date>(),
                                                                  calendars::RdDate{23}.to<calendars::gregorian::Date>()};
    auto vec   = std::vector<CalendarRdDate>{};

    for (const auto& date : range) {
      vec.emplace_back(static_cast<CalendarRdDate>(date.to_rd_date()));
    }

    auto expected = std::vector<CalendarRdDate>{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
    REQUIRE_EQ(vec.size(), expected.size());
    REQUIRE_EQ(range.size(), expected.size());

    for (auto i = 0; i < vec.size(); ++i) {
      CHECK_EQ(vec[ i ], expected[ i ]);
    }
  }

  TEST_CASE("Reverse Between specific dates") {
    auto range = calendars::DateRange<calendars::gregorian::Date>{calendars::RdDate{10}.to<calendars::gregorian::Date>(),
                                                                  calendars::RdDate{23}.to<calendars::gregorian::Date>()};
    auto vec   = std::vector<CalendarRdDate>{};

    for (auto it = range.rbegin(); it != range.rend(); ++it) {
      vec.emplace_back(static_cast<CalendarRdDate>(it->to_rd_date()));
    }

    auto expected = std::vector<CalendarRdDate>{22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10};
    REQUIRE_EQ(vec.size(), expected.size());
    REQUIRE_EQ(range.size(), expected.size());

    for (auto i = 0; i < vec.size(); ++i) {
      CHECK_EQ(vec[ i ], expected[ i ]);
    }
  }

  TEST_CASE("Constant Between specific dates") {
    auto range = calendars::DateRange<calendars::gregorian::Date>{calendars::RdDate{10}.to<calendars::gregorian::Date>(),
                                                                  calendars::RdDate{23}.to<calendars::gregorian::Date>()};
    auto vec   = std::vector<CalendarRdDate>{};

    for (auto it = range.cbegin(); it != range.cend(); ++it) {
      vec.emplace_back(static_cast<CalendarRdDate>(it->to_rd_date()));
    }

    auto expected = std::vector<CalendarRdDate>{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
    REQUIRE_EQ(vec.size(), expected.size());
    REQUIRE_EQ(range.size(), expected.size());

    for (auto i = 0; i < vec.size(); ++i) {
      CHECK_EQ(vec[ i ], expected[ i ]);
    }
  }
  TEST_CASE("Sample Data") {
    using T = calendars::gregorian::Date;

    auto sampleRdDates = sample_rd_dates();

    static std::vector<T> expected = {
        T{-586, static_cast<calendars::gregorian::MONTH>(7), 24},  T{-168, static_cast<calendars::gregorian::MONTH>(12), 5},
        T{70, static_cast<calendars::gregorian::MONTH>(9), 24},    T{135, static_cast<calendars::gregorian::MONTH>(10), 2},
        T{470, static_cast<calendars::gregorian::MONTH>(1), 8},    T{576, static_cast<calendars::gregorian::MONTH>(5), 20},
        T{694, static_cast<calendars::gregorian::MONTH>(11), 10},  T{1013, static_cast<calendars::gregorian::MONTH>(4), 25},
        T{1096, static_cast<calendars::gregorian::MONTH>(5), 24},  T{1190, static_cast<calendars::gregorian::MONTH>(3), 23},
        T{1240, static_cast<calendars::gregorian::MONTH>(3), 10},  T{1288, static_cast<calendars::gregorian::MONTH>(4), 2},
        T{1298, static_cast<calendars::gregorian::MONTH>(4), 27},  T{1391, static_cast<calendars::gregorian::MONTH>(6), 12},
        T{1436, static_cast<calendars::gregorian::MONTH>(2), 3},   T{1492, static_cast<calendars::gregorian::MONTH>(4), 9},
        T{1553, static_cast<calendars::gregorian::MONTH>(9), 19},  T{1560, static_cast<calendars::gregorian::MONTH>(3), 5},
        T{1648, static_cast<calendars::gregorian::MONTH>(6), 10},  T{1680, static_cast<calendars::gregorian::MONTH>(6), 30},
        T{1716, static_cast<calendars::gregorian::MONTH>(7), 24},  T{1768, static_cast<calendars::gregorian::MONTH>(6), 19},
        T{1819, static_cast<calendars::gregorian::MONTH>(8), 2},   T{1839, static_cast<calendars::gregorian::MONTH>(3), 27},
        T{1903, static_cast<calendars::gregorian::MONTH>(4), 19},  T{1929, static_cast<calendars::gregorian::MONTH>(8), 25},
        T{1941, static_cast<calendars::gregorian::MONTH>(9), 29},  T{1943, static_cast<calendars::gregorian::MONTH>(4), 19},
        T{1943, static_cast<calendars::gregorian::MONTH>(10), 7}, T{1992, static_cast<calendars::gregorian::MONTH>(3), 17},
        T{1996, static_cast<calendars::gregorian::MONTH>(2), 25},  T{2038, static_cast<calendars::gregorian::MONTH>(11), 10},
        T{2094, static_cast<calendars::gregorian::MONTH>(7), 18}};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
