#include <doctest/doctest.h>

#include <calendars/cxx/calendars/julian_day.h>
#include "test_utils.h"

TEST_SUITE("Julian Day") {
  TEST_CASE("Comparisons") { comp_test_one_arg<calendars::julian_day::Date>(); }

  TEST_CASE("Conversions") {
    calendars::julian_day::Date date{};
    calendars::RdDate    rdate{};

    calendars::Moment expected{0.0};
    expected = calendars::Moment{0.0};
    date     = calendars::julian_day::Date{1721424.5};
    CHECK_IN_RANGE(calendars::Moment::from(date).datetime(), expected.datetime(), 0.01)
    CHECK_IN_RANGE(date.datetime(), expected.to<calendars::julian_day::Date>().datetime(), 0.01)

    expected = calendars::Moment{-9965.5};
    date     = calendars::julian_day::Date{1711459.0};
    CHECK_IN_RANGE(expected.datetime(), calendars::Moment::from(date).datetime(), 0.01)
    CHECK_EQ(calendars::julian_day::Date::from_moment(expected), date);
    CHECK_IN_RANGE(date.datetime(), expected.to<calendars::julian_day::Date>().datetime(), 0.01)
    CHECK_IN_RANGE(rdate.to<calendars::julian_day::Date>().datetime(), 1721424.5, 0.01);

    rdate     = calendars::RdDate{0};
    date = calendars::julian_day::Date{1721424.5};
    CHECK_EQ(rdate, calendars::RdDate::from(date));
    CHECK_IN_RANGE(date.datetime(), rdate.to<calendars::julian_day::Date>().datetime(), 0.01)

    rdate     = calendars::RdDate{811481951};
    date = calendars::julian_day::Date{813203375.75};
    CHECK_EQ(rdate, calendars::RdDate::from(date));
    CHECK_IN_RANGE(813203375.5, rdate.to<calendars::julian_day::Date>().datetime(), 0.01)
  }

  TEST_CASE("Sample Data") {
    using T = calendars::julian_day::Date;

    static std::vector<calendars::julian_day::Date> expected = {
        T{1507231.5}, T{1660037.5}, T{1746893.5}, T{1770641.5}, T{1892731.5}, T{1931579.5}, T{1974851.5}, T{2091164.5}, T{2121509.5},
        T{2155779.5}, T{2174029.5}, T{2191584.5}, T{2195261.5}, T{2229274.5}, T{2245580.5}, T{2266100.5}, T{2288542.5}, T{2290901.5},
        T{2323140.5}, T{2334848.5}, T{2348020.5}, T{2366978.5}, T{2385648.5}, T{2392825.5}, T{2416223.5}, T{2425848.5}, T{2430266.5},
        T{2430833.5}, T{2431004.5}, T{2448698.5}, T{2450138.5}, T{2465737.5}, T{2486076.5}};

    const auto sampleRdDates = sample_rd_dates();
    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<calendars::julian_day::Date>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
