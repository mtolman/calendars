#include <doctest/doctest.h>

#include <calendars/cxx/calendars/coptic.h>
#include "test_utils.h"

TEST_SUITE("Coptic Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::coptic::Date, calendars::coptic::MONTH>();
  }

  TEST_CASE("Conversions") {
    calendars::coptic::Date date;
    calendars::RdDate     rdate;

    rdate     = calendars::RdDate{8849};
    date      = rdate.to<calendars::coptic::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::coptic::Date;

    static std::vector<T> expected = {
        T{-870, static_cast<calendars::coptic::MONTH>(12), 6},
        T{-451, static_cast<calendars::coptic::MONTH>(4), 12},
        T{-213, static_cast<calendars::coptic::MONTH>(1), 29},
        T{-148, static_cast<calendars::coptic::MONTH>(2), 5},
        T{186, static_cast<calendars::coptic::MONTH>(5), 12},
        T{292, static_cast<calendars::coptic::MONTH>(9), 23},
        T{411, static_cast<calendars::coptic::MONTH>(3), 11},
        T{729, static_cast<calendars::coptic::MONTH>(8), 24},
        T{812, static_cast<calendars::coptic::MONTH>(9), 23},
        T{906, static_cast<calendars::coptic::MONTH>(7), 20},
        T{956, static_cast<calendars::coptic::MONTH>(7), 7},
        T{1004, static_cast<calendars::coptic::MONTH>(7), 30},
        T{1014, static_cast<calendars::coptic::MONTH>(8), 25},
        T{1107, static_cast<calendars::coptic::MONTH>(10), 10},
        T{1152, static_cast<calendars::coptic::MONTH>(5), 29},
        T{1208, static_cast<calendars::coptic::MONTH>(8), 5},
        T{1270, static_cast<calendars::coptic::MONTH>(1), 12},
        T{1276, static_cast<calendars::coptic::MONTH>(6), 29},
        T{1364, static_cast<calendars::coptic::MONTH>(10), 6},
        T{1396, static_cast<calendars::coptic::MONTH>(10), 26},
        T{1432, static_cast<calendars::coptic::MONTH>(11), 19},
        T{1484, static_cast<calendars::coptic::MONTH>(10), 14},
        T{1535, static_cast<calendars::coptic::MONTH>(11), 27},
        T{1555, static_cast<calendars::coptic::MONTH>(7), 19},
        T{1619, static_cast<calendars::coptic::MONTH>(8), 11},
        T{1645, static_cast<calendars::coptic::MONTH>(12), 19},
        T{1658, static_cast<calendars::coptic::MONTH>(1), 19},
        T{1659, static_cast<calendars::coptic::MONTH>(8), 11},
        T{1660, static_cast<calendars::coptic::MONTH>(1), 26},
        T{1708, static_cast<calendars::coptic::MONTH>(7), 8},
        T{1712, static_cast<calendars::coptic::MONTH>(6), 17},
        T{1755, static_cast<calendars::coptic::MONTH>(3), 1},
        T{1810, static_cast<calendars::coptic::MONTH>(11), 11}
    };

    auto sampleRdDates = sample_rd_dates();
    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
