#include <doctest/doctest.h>

#include <calendars/cxx/calendars/ethiopic.h>
#include "test_utils.h"

TEST_SUITE("Ethiopic Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::ethiopic::Date, calendars::ethiopic::MONTH>();
  }

  TEST_CASE("Conversions") {
    calendars::ethiopic::Date date;
    calendars::RdDate        rdate;

    rdate = calendars::RdDate{8849};
    date      = rdate.to<calendars::ethiopic::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::ethiopic::Date;

    auto sampleRdDates = sample_rd_dates();

    static std::vector<T> expected = {
        T{-594, static_cast<calendars::ethiopic::MONTH>(12) ,6},
        T{-175, static_cast<calendars::ethiopic::MONTH>(4), 12},
        T{63,   static_cast<calendars::ethiopic::MONTH>(1), 29},
        T{128,  static_cast<calendars::ethiopic::MONTH>(2), 5},
        T{462,  static_cast<calendars::ethiopic::MONTH>(5), 12},
        T{568,  static_cast<calendars::ethiopic::MONTH>(9), 23},
        T{687,  static_cast<calendars::ethiopic::MONTH>(3), 11},
        T{1005, static_cast<calendars::ethiopic::MONTH>(8), 24},
        T{1088, static_cast<calendars::ethiopic::MONTH>(9), 23},
        T{1182, static_cast<calendars::ethiopic::MONTH>(7), 20},
        T{1232, static_cast<calendars::ethiopic::MONTH>(7), 7},
        T{1280, static_cast<calendars::ethiopic::MONTH>(7), 30},
        T{1290, static_cast<calendars::ethiopic::MONTH>(8), 25},
        T{1383, static_cast<calendars::ethiopic::MONTH>(10), 10},
        T{1428, static_cast<calendars::ethiopic::MONTH>(5), 29},
        T{1484, static_cast<calendars::ethiopic::MONTH>(8), 5},
        T{1546, static_cast<calendars::ethiopic::MONTH>(1), 12},
        T{1552, static_cast<calendars::ethiopic::MONTH>(6), 29},
        T{1640, static_cast<calendars::ethiopic::MONTH>(10), 6},
        T{1672, static_cast<calendars::ethiopic::MONTH>(10), 26},
        T{1708, static_cast<calendars::ethiopic::MONTH>(11), 19},
        T{1760, static_cast<calendars::ethiopic::MONTH>(10), 14},
        T{1811, static_cast<calendars::ethiopic::MONTH>(11), 27},
        T{1831, static_cast<calendars::ethiopic::MONTH>(7), 19},
        T{1895, static_cast<calendars::ethiopic::MONTH>(8), 11},
        T{1921, static_cast<calendars::ethiopic::MONTH>(12), 19},
        T{1934, static_cast<calendars::ethiopic::MONTH>(1), 19},
        T{1935, static_cast<calendars::ethiopic::MONTH>(8), 11},
        T{1936, static_cast<calendars::ethiopic::MONTH>(1), 26},
        T{1984, static_cast<calendars::ethiopic::MONTH>(7), 8},
        T{1988, static_cast<calendars::ethiopic::MONTH>(6), 17},
        T{2031, static_cast<calendars::ethiopic::MONTH>(3), 1},
        T{2086, static_cast<calendars::ethiopic::MONTH>(11), 11}
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
