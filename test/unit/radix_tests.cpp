#include <calendars/cxx/calendars/radix.h>
#include <doctest/doctest.h>

#include "test_utils.h"

TEST_SUITE("Radix Date") {
  TEST_CASE("Basic") {
    auto templ = calendars::radix::DateTemplate{{7}, {24, 60, 60}};
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({4, 1, 12, 44, 2.88})).datetime(), 29.5305888889);

    auto result = templ.make_date(calendars::Moment{29.5305888889});
    CHECK_EQ(result[ 0 ], 4);
    CHECK_EQ(result[ 1 ], 1);
    CHECK_EQ(result[ 2 ], 12);
    CHECK_EQ(result[ 3 ], 44);
    CHECK_IN_RANGE(result[ 4 ], 2.88, 0.00001);
  }

  TEST_CASE("No Int Segment") {
    auto templ = calendars::radix::DateTemplate{{}, {24, 60, 60}};
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({1}, {12, 44, 2.88})).datetime(), 1.5305888889);

    auto result = templ.make_date(calendars::Moment{1.5305888889});
    CHECK_EQ(result[ 0 ], 1);
    CHECK_EQ(result[ 1 ], 12);
    CHECK_EQ(result[ 2 ], 44);
    CHECK_IN_RANGE(result[ 3 ], 2.88, 0.00001);
  }

  TEST_CASE("Frac Segment") {
    auto templ = calendars::radix::DateTemplate{{7}, {60, 60}};
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({4, 1}, {30, 15})).datetime(), 29.5041666667);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({1, 0, 30, 15})).datetime(), 7.5041666667);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({}, {30, 15})).datetime(), 0.5041666667);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({}, {15})).datetime(), 0.00416667);

    CHECK_EQ(templ.make_date({4, 1, 30, 15}).size(), 4);
    CHECK_EQ(templ.make_date({4, 1}, {30, 15}).size(), 4);
    CHECK_EQ(templ.make_date({4, 1}, {30, 15, 23, 23}).size(), 4);
    CHECK_EQ(templ.make_date({4, 1, 34, 34}, {30, 15, 23, 23}).size(), 4);
    CHECK_EQ(templ.make_date({4}, {30}).size(), 4);
    CHECK_EQ(templ.make_date({}, {}).size(), 4);
    CHECK_EQ(templ.make_date({}).size(), 4);
    CHECK_EQ(templ.make_date({4, 30}).size(), 4);
    CHECK_EQ(templ.make_date({4, 1, 30, 15, 34, 234, 324, 324, 2312}).size(), 4);
    CHECK_EQ(templ.make_date(calendars::Moment{29.5041666667}).size(), 4);

    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({4, 1, 30, 15, 34, 234, 324, 324, 2312})).datetime(), 29.5041666667);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({4, 1, 2432, 13223}, {30, 15, 34, 234, 324, 324, 2312})).datetime(), 29.5041666667);

    auto result = templ.make_date(calendars::Moment{29});
    CHECK_IN_RANGE_DEF(result[ 0 ], 4);
    CHECK_IN_RANGE_DEF(result[ 1 ], 1);
    CHECK_IN_RANGE_DEF(result[ 2 ], 0);
    CHECK_IN_RANGE_DEF(result[ 3 ], 0);
  }

  TEST_CASE("No Frac Segment") {
    auto templ = calendars::radix::DateTemplate{{7}, {}};
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({4, 1})).datetime(), 29);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({1, 0})).datetime(), 7);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(templ.make_date({0, 0})).datetime(), 0);

    auto result = templ.make_date(calendars::Moment{29});
    CHECK_EQ(result[ 0 ], 4);
    CHECK_EQ(result[ 1 ], 1);
  }

  TEST_CASE("C Conversions") {
    SUBCASE("Template") {
      auto  templ   = calendars::radix::DateTemplate{{7}, {24, 60, 60}};
      auto cTempl = templ.to_c_template();
      CalendarRadixDate cDate;
      double dayValues[] = {4, 1};
      double timeValues[] = {12, 44, 2.88};
      calendar_radix_date_init(&cDate, cTempl.get(), 2, dayValues, 3, timeValues);
      auto radix = calendars::radix::Date(cDate);
      calendar_radix_date_deinit(&cDate);
      CHECK_IN_RANGE_DEF(calendars::Moment::from(radix).datetime(), 29.5305888889);
    }

    SUBCASE("Date") {
      auto  templ   = calendars::radix::DateTemplate{{7}, {24, 60, 60}};
      auto date = templ.make_date({4, 1, 12, 44, 2.88});
      auto cDate = date.to_c_date();
      auto radix = calendars::radix::Date(*cDate);
      CHECK_IN_RANGE_DEF(calendars::Moment::from(radix).datetime(), 29.5305888889);
    }
  }
}
