#include <doctest/doctest.h>

#include <calendars/cxx/calendars/balinese_pawukon_date.h>
#include "test_utils.h"
#include <iostream>
#include <array>

TEST_SUITE("Balinese Pawukon") {
  TEST_CASE("Equals") {
    CHECK_EQ(calendars::RdDate{24}.to<calendars::balinese_pawukon::Date>(), calendars::RdDate{24}.to<calendars::balinese_pawukon::Date>());

    // Test that the rest of the 209 days in the 210-dat cycle don't equal the original day
    for (int i = 0; i < 209; ++i) {
      CHECK_NE(calendars::RdDate{24}.to<calendars::balinese_pawukon::Date>(), calendars::RdDate{25 + i}.to<calendars::balinese_pawukon::Date>());
    }
  }

  TEST_CASE("Sample Data") {
    using T = calendars::balinese_pawukon::Date;
    using DWIWARA = calendars::balinese_pawukon::DWIWARA;
    using TRIWARA = calendars::balinese_pawukon::TRIWARA;
    using CATURWARA = calendars::balinese_pawukon::CATURWARA;
    using PANCAWARA = calendars::balinese_pawukon::PANCAWARA;
    using SADWARA = calendars::balinese_pawukon::SADWARA;
    using SAPTAWARA = calendars::balinese_pawukon::SAPTAWARA;
    using ASATAWARA = calendars::balinese_pawukon::ASATAWARA;
    using SANGAWARA = calendars::balinese_pawukon::SANGAWARA;
    using DASAWARA = calendars::balinese_pawukon::DASAWARA;

    const auto sampleRdDates = sample_rd_dates();

    constexpr auto f = false;
    constexpr auto t = true;
    int line = __LINE__;
    static std::vector<T> expected = {
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(1), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(3), static_cast<SADWARA>(1), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(5), static_cast<SANGAWARA>(7), static_cast<DASAWARA>(3)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(4), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(4), static_cast<ASATAWARA>(5), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(2)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(5), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(4), static_cast<ASATAWARA>(1), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(6)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(3), static_cast<PANCAWARA>(3), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(3), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(3)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(1), static_cast<CATURWARA>(3), static_cast<PANCAWARA>(3), static_cast<SADWARA>(1), static_cast<SAPTAWARA>(4), static_cast<ASATAWARA>(3), static_cast<SANGAWARA>(1), static_cast<DASAWARA>(5)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(1), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(2), static_cast<ASATAWARA>(1), static_cast<SANGAWARA>(8), static_cast<DASAWARA>(0)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(3), static_cast<PANCAWARA>(3), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(7), static_cast<ASATAWARA>(3), static_cast<SANGAWARA>(2), static_cast<DASAWARA>(7)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(2), static_cast<PANCAWARA>(1), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(2), static_cast<SANGAWARA>(2), static_cast<DASAWARA>(1)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(1), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(1), static_cast<SANGAWARA>(8), static_cast<DASAWARA>(1)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(3), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(1), static_cast<SADWARA>(3), static_cast<SAPTAWARA>(6), static_cast<ASATAWARA>(1), static_cast<SANGAWARA>(3), static_cast<DASAWARA>(2)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(1), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(1), static_cast<SADWARA>(1), static_cast<SAPTAWARA>(7), static_cast<ASATAWARA>(5), static_cast<SANGAWARA>(1), static_cast<DASAWARA>(5)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(3), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(1), static_cast<SADWARA>(6), static_cast<SAPTAWARA>(6), static_cast<ASATAWARA>(8), static_cast<SANGAWARA>(6), static_cast<DASAWARA>(2)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(3), static_cast<PANCAWARA>(3), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(3), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(3)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(1), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(1), static_cast<SADWARA>(4), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(4), static_cast<SANGAWARA>(7), static_cast<DASAWARA>(1)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(2), static_cast<PANCAWARA>(2), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(4), static_cast<ASATAWARA>(2), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(7)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(2), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(7), static_cast<ASATAWARA>(8), static_cast<SANGAWARA>(8), static_cast<DASAWARA>(9)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(1), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(4), static_cast<SADWARA>(4), static_cast<SAPTAWARA>(7), static_cast<ASATAWARA>(4), static_cast<SANGAWARA>(7), static_cast<DASAWARA>(4)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(3), static_cast<PANCAWARA>(3), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(7), static_cast<ASATAWARA>(3), static_cast<SANGAWARA>(2), static_cast<DASAWARA>(7)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(3), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(2), static_cast<SADWARA>(6), static_cast<SAPTAWARA>(4), static_cast<ASATAWARA>(8), static_cast<SANGAWARA>(3), static_cast<DASAWARA>(7)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(5), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(4), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(4)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(1), static_cast<CATURWARA>(2), static_cast<PANCAWARA>(2), static_cast<SADWARA>(4), static_cast<SAPTAWARA>(6), static_cast<ASATAWARA>(2), static_cast<SANGAWARA>(1), static_cast<DASAWARA>(6)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(5), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(4), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(4)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(3), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(5), static_cast<SADWARA>(6), static_cast<SAPTAWARA>(2), static_cast<ASATAWARA>(8), static_cast<SANGAWARA>(3), static_cast<DASAWARA>(3)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(1), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(2), static_cast<SADWARA>(1), static_cast<SAPTAWARA>(4), static_cast<ASATAWARA>(5), static_cast<SANGAWARA>(4), static_cast<DASAWARA>(7)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(1), static_cast<PANCAWARA>(5), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(5), static_cast<SANGAWARA>(8), static_cast<DASAWARA>(4)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(3), static_cast<CATURWARA>(2), static_cast<PANCAWARA>(5), static_cast<SADWARA>(6), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(2), static_cast<SANGAWARA>(3), static_cast<DASAWARA>(4)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(2), static_cast<PANCAWARA>(3), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(2), static_cast<ASATAWARA>(2), static_cast<SANGAWARA>(1), static_cast<DASAWARA>(2)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(2), static_cast<CATURWARA>(3), static_cast<PANCAWARA>(5), static_cast<SADWARA>(5), static_cast<SAPTAWARA>(2), static_cast<ASATAWARA>(3), static_cast<SANGAWARA>(2), static_cast<DASAWARA>(3)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(1), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(5), static_cast<ASATAWARA>(4), static_cast<SANGAWARA>(8), static_cast<DASAWARA>(4)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(2), static_cast<PANCAWARA>(5), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(3), static_cast<ASATAWARA>(2), static_cast<SANGAWARA>(8), static_cast<DASAWARA>(2)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(2), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(5), static_cast<SADWARA>(2), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(4), static_cast<SANGAWARA>(5), static_cast<DASAWARA>(4)},
        T{t, static_cast<DWIWARA>(2), static_cast<TRIWARA>(1), static_cast<CATURWARA>(3), static_cast<PANCAWARA>(4), static_cast<SADWARA>(1), static_cast<SAPTAWARA>(4), static_cast<ASATAWARA>(7), static_cast<SANGAWARA>(1), static_cast<DASAWARA>(2)},
        T{f, static_cast<DWIWARA>(1), static_cast<TRIWARA>(3), static_cast<CATURWARA>(4), static_cast<PANCAWARA>(3), static_cast<SADWARA>(6), static_cast<SAPTAWARA>(1), static_cast<ASATAWARA>(8), static_cast<SANGAWARA>(6), static_cast<DASAWARA>(3)}
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      if (sampleRdDates[ i ].to<T>() != expected[ i ]) {
        const auto printBDate = [](const calendars::balinese_pawukon::Date& date) {
          const auto vals = std::array {
            static_cast<int>(date.dwiwara()), static_cast<int>(date.triwara()), static_cast<int>(date.caturwara()),
                static_cast<int>(date.pancawara()), static_cast<int>(date.sadwara()), static_cast<int>(date.saptawara()),
                static_cast<int>(date.asatawara()), static_cast<int>(date.sangawara()), static_cast<int>(date.dasawara()),
          };
          if (!date.is_luang()) {
            std::cout << "f ";
          }
          else {
            std::cout << "t ";
          }
          for (const auto& v : vals) {
            std::cout << v << " ";
          }
        };
        std::cout << "Error with line " << (line + 2 + i) << "! Expected: ";
        printBDate(expected[i]);
        std::cout << "Received: ";
        printBDate(sampleRdDates[ i ].to<T>());
        std::cout << "\n";
      }
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
    }
  }
}
