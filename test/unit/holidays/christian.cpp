#include <doctest/doctest.h>
#include "../test_utils.h"
#include <calendars/cxx/calendars.h>
#include <calendars/cxx/calendars/holidays.h>
#include "sample_data.h"

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("Christian Holidays") {
  TEST_CASE("Christmas Day") {
    auto year = 2018;
    auto d    = c::gregorian::Date{year, c::gregorian::MONTH::DECEMBER, 25};
    REQUIRE_EQ(d, h::christian::christmas(year));
  }

  TEST_CASE("Christmas Eve Day") {
    auto year = 2018;
    auto d    = (c::gregorian::Date{year, c::gregorian::MONTH::DECEMBER, 24});
    REQUIRE_EQ(d, h::christian::christmas_eve(year));
  }

  TEST_CASE("Advent Sunday") {
    auto year = 2018;
    auto d    = (c::gregorian::Date{year, c::gregorian::MONTH::NOVEMBER, 30});
    auto a    = h::christian::advent_sunday(year);
    REQUIRE_LE(std::abs(d.to_rd_date().day_difference(a.to_rd_date())), 3);
    REQUIRE_EQ(a.to_rd_date().day_of_week(), c::DAY_OF_WEEK::SUNDAY);
  }

  TEST_CASE("Epiphany") {
    auto year = 2018;
    auto d    = (c::gregorian::Date{year, c::gregorian::MONTH::JANUARY, 2});
    auto a    = h::christian::epiphany(year);
    REQUIRE_LE(d, a);
    REQUIRE_GE(d.to_rd_date().add_days(7), a.to_rd_date());
    REQUIRE_EQ(a.to_rd_date().day_of_week(), c::DAY_OF_WEEK::SUNDAY);
  }

  TEST_SUITE("Sample Data") {
    TEST_CASE("Christmas Day") {
      for (auto year : sampleYears) {
        REQUIRE_EQ(c::holidays::christian::christmas(year), c::gregorian::Date{year, c::gregorian::MONTH::DECEMBER, 25});
      }
    }

    TEST_CASE("Christmas Eve Day") {
      for (auto year : sampleYears) {
        REQUIRE_EQ(c::holidays::christian::christmas_eve(year), c::gregorian::Date{year, c::gregorian::MONTH::DECEMBER, 24});
      }
    }

    TEST_CASE("Advent Sunday") {
      using g                 = calendars::gregorian::Date;
      const auto expectedDays = std::vector<std::tuple<calendars::gregorian::MONTH, int16_t>>{
          std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 3),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 2),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 1),  std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 30),
          std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 28), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 27), std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 3),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 2),
          std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 30), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 29), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 28), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 27),
          std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 2),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 1),  std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 30), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 29),
          std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 27), std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 3),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 2),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 1),
          std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 29), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 28), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 27), std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 3),
          std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 1),  std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 30), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 29), std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 28),
          std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 3),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 2),  std::make_tuple(calendars::gregorian::MONTH::DECEMBER, 1),  std::make_tuple(calendars::gregorian::MONTH::NOVEMBER, 30),
      };
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::christian::advent_sunday(sampleYears[ i ]),
                   c::gregorian::Date{sampleYears[ i ], std::get<0>(expectedDays[ i ]), std::get<1>(expectedDays[ i ])});
      }
    }

    TEST_CASE("Epiphany") {
      using g                 = calendars::gregorian::Date;
      const auto expectedDays = std::vector<int16_t>{
          2, 7, 6, 5, 4, 2, 8, 7, 6, 4, 3, 2, 8, 6, 5, 4, 3, 8, 7, 6, 5, 3, 2, 8, 7, 5, 4, 3, 2, 7, 6, 5,
      };
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::christian::epiphany(sampleYears[ i ]),
                   c::gregorian::Date{sampleYears[ i ], c::gregorian::MONTH::JANUARY, expectedDays[ i ]});
      }
    }

    TEST_CASE("Easter") {
      using d = calendars::gregorian::Date;
      struct ExpectedEaster {
        calendars::gregorian::MONTH month;
        int16_t  day;

        ExpectedEaster(int month, int day) : month(static_cast<calendars::gregorian::MONTH>(month)), day(static_cast<int16_t>(day)) {}
      };
      const auto expectedDays = std::vector<ExpectedEaster>{
          {4, 23}, {4, 15}, {3, 31}, {4, 20}, {4, 11}, {3, 27}, {4, 16}, {4, 8},  {3, 23}, {4, 12}, {4, 4},  {4, 24}, {4, 8},  {3, 31},
          {4, 20}, {4, 5},  {3, 27}, {4, 16}, {4, 1},  {4, 21}, {4, 12}, {4, 4},  {4, 17}, {4, 9},  {3, 31}, {4, 20}, {4, 5},  {3, 28},
          {4, 16}, {4, 1},  {4, 21}, {4, 13}, {3, 28}, {4, 17}, {4, 9},  {3, 25}, {4, 13}, {4, 5},  {4, 25}, {4, 10}, {4, 1},  {4, 21},
          {4, 6},  {3, 29}, {4, 17}, {4, 9},  {3, 25}, {4, 14}, {4, 5},  {4, 18}, {4, 10}, {4, 2},  {4, 21}, {4, 6},  {3, 29}, {4, 18},
      };
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::christian::easter(sampleYears[ i ]),
                   d{sampleYears[ i ], expectedDays[ i ].month, expectedDays[ i ].day});
      }
    }
  }
}
