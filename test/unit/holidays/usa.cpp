#include <doctest/doctest.h>
#include <calendars/cxx/calendars/holidays/usa.h>
#include <calendars/cxx/calendars.h>
#include "sample_data.h"

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("USA Holidays") {
  TEST_CASE("Independence Day") {
    auto year = 2018;
    auto d    = c::gregorian::Date{year, c::gregorian::MONTH::JULY, 4};
    REQUIRE_EQ(d, h::usa::independence_day(year));
  }
  
  TEST_CASE("Labor Day") {
    auto year = 2018;
    auto d    = c::gregorian::Date{year, c::gregorian::MONTH::SEPTEMBER, 1};
    auto a    = h::usa::labor_day(year);
    REQUIRE_LE(d, a);
    REQUIRE_GE(d.to_rd_date().add_days(7), a.to_rd_date());
    REQUIRE_EQ(a.to_rd_date().day_of_week(), c::DAY_OF_WEEK::MONDAY);
  }

  TEST_CASE("Memorial Day") {
    auto year = 2018;
    auto d    = c::RdDate::from(c::gregorian::Date{year, c::gregorian::MONTH::MAY, 31});
    auto a    = h::usa::memorial_day(year);
    REQUIRE_GE(d, a.to_rd_date());
    REQUIRE_LE(d.sub_days(7), a.to_rd_date());
    REQUIRE_EQ(a.to_rd_date().day_of_week(), c::DAY_OF_WEEK::MONDAY);
  }

  TEST_CASE("Election Day") {
    auto year = 2018;
    auto d    = c::RdDate::from(c::gregorian::Date{year, c::gregorian::MONTH::NOVEMBER, 2});
    auto a    = h::usa::election_day(year);
    REQUIRE_LE(d, a.to_rd_date());
    REQUIRE_GE(d.add_days(7), a.to_rd_date());
    REQUIRE_EQ(a.to_rd_date().day_of_week(), c::DAY_OF_WEEK::TUESDAY);
  }

  TEST_CASE("Thanksgiving") {
    CHECK_EQ(h::usa::thanksgiving(2023), c::gregorian::Date{2023, c::gregorian::MONTH::NOVEMBER,23});
    CHECK_EQ(h::usa::thanksgiving(2024), c::gregorian::Date{2024, c::gregorian::MONTH::NOVEMBER,28});
    CHECK_EQ(h::usa::thanksgiving(2025), c::gregorian::Date{2025, c::gregorian::MONTH::NOVEMBER,27});
    CHECK_EQ(h::usa::thanksgiving(2026), c::gregorian::Date{2026, c::gregorian::MONTH::NOVEMBER,26});
    CHECK_EQ(h::usa::thanksgiving(1951), c::gregorian::Date{1951, c::gregorian::MONTH::NOVEMBER,22});
  }

  TEST_CASE("Black Friday") {
    CHECK_EQ(h::usa::black_friday(2023), c::gregorian::Date{2023, c::gregorian::MONTH::NOVEMBER,24});
    CHECK_EQ(h::usa::black_friday(2024), c::gregorian::Date{2024, c::gregorian::MONTH::NOVEMBER,29});
    CHECK_EQ(h::usa::black_friday(2025), c::gregorian::Date{2025, c::gregorian::MONTH::NOVEMBER,28});
    CHECK_EQ(h::usa::black_friday(2026), c::gregorian::Date{2026, c::gregorian::MONTH::NOVEMBER,27});
    CHECK_EQ(h::usa::black_friday(1951), c::gregorian::Date{1951, c::gregorian::MONTH::NOVEMBER,23});
  }

  TEST_CASE("New Years") {
    CHECK_EQ(h::usa::new_years(2023), c::gregorian::Date{2023, c::gregorian::MONTH::JANUARY,1});
    CHECK_EQ(h::usa::new_years(2024), c::gregorian::Date{2024, c::gregorian::MONTH::JANUARY,1});
    CHECK_EQ(h::usa::new_years(2025), c::gregorian::Date{2025, c::gregorian::MONTH::JANUARY,1});
    CHECK_EQ(h::usa::new_years(2026), c::gregorian::Date{2026, c::gregorian::MONTH::JANUARY,1});
    CHECK_EQ(h::usa::new_years(1951), c::gregorian::Date{1951, c::gregorian::MONTH::JANUARY,1});
  }

  TEST_CASE("New Years Eve") {
    CHECK_EQ(h::usa::new_years_eve(2023), c::gregorian::Date{2023, c::gregorian::MONTH::DECEMBER, 31});
    CHECK_EQ(h::usa::new_years_eve(2024), c::gregorian::Date{2024, c::gregorian::MONTH::DECEMBER, 31});
    CHECK_EQ(h::usa::new_years_eve(2025), c::gregorian::Date{2025, c::gregorian::MONTH::DECEMBER, 31});
    CHECK_EQ(h::usa::new_years_eve(2026), c::gregorian::Date{2026, c::gregorian::MONTH::DECEMBER, 31});
    CHECK_EQ(h::usa::new_years_eve(1951), c::gregorian::Date{1951, c::gregorian::MONTH::DECEMBER, 31});
  }

  TEST_CASE("Presidents day") {
    CHECK_EQ(h::usa::presidents_day(2023), c::gregorian::Date{2023, c::gregorian::MONTH::FEBRUARY, 20});
    CHECK_EQ(h::usa::presidents_day(2024), c::gregorian::Date{2024, c::gregorian::MONTH::FEBRUARY, 19});
    CHECK_EQ(h::usa::presidents_day(2025), c::gregorian::Date{2025, c::gregorian::MONTH::FEBRUARY, 17});
    CHECK_EQ(h::usa::presidents_day(2026), c::gregorian::Date{2026, c::gregorian::MONTH::FEBRUARY, 16});
  }

  TEST_CASE("Juneteenth day") {
    CHECK_EQ(h::usa::juneteenth(2023), c::gregorian::Date{2023, c::gregorian::MONTH::JUNE, 19});
    CHECK_EQ(h::usa::juneteenth(2024), c::gregorian::Date{2024, c::gregorian::MONTH::JUNE, 19});
    CHECK_EQ(h::usa::juneteenth(2025), c::gregorian::Date{2025, c::gregorian::MONTH::JUNE, 19});
    CHECK_EQ(h::usa::juneteenth(2026), c::gregorian::Date{2026, c::gregorian::MONTH::JUNE, 19});
  }

  TEST_CASE("Columbus day") {
    CHECK_EQ(h::usa::columbus_day(2023), c::gregorian::Date{2023, c::gregorian::MONTH::OCTOBER,  9});
    CHECK_EQ(h::usa::columbus_day(2024), c::gregorian::Date{2024, c::gregorian::MONTH::OCTOBER, 14});
    CHECK_EQ(h::usa::columbus_day(2025), c::gregorian::Date{2025, c::gregorian::MONTH::OCTOBER, 13});
    CHECK_EQ(h::usa::columbus_day(2026), c::gregorian::Date{2026, c::gregorian::MONTH::OCTOBER, 12});
  }

  TEST_CASE("Indigenous Peoples day") {
    CHECK_EQ(h::usa::indigenous_peoples_day(2023), c::gregorian::Date{2023, c::gregorian::MONTH::OCTOBER,  9});
    CHECK_EQ(h::usa::indigenous_peoples_day(2024), c::gregorian::Date{2024, c::gregorian::MONTH::OCTOBER, 14});
    CHECK_EQ(h::usa::indigenous_peoples_day(2025), c::gregorian::Date{2025, c::gregorian::MONTH::OCTOBER, 13});
    CHECK_EQ(h::usa::indigenous_peoples_day(2026), c::gregorian::Date{2026, c::gregorian::MONTH::OCTOBER, 12});
  }

  TEST_CASE("Veterans Day") {
    CHECK_EQ(h::usa::veterans_day(2023), c::gregorian::Date{2023, c::gregorian::MONTH::NOVEMBER, 11});
    CHECK_EQ(h::usa::veterans_day(2024), c::gregorian::Date{2024, c::gregorian::MONTH::NOVEMBER, 11});
    CHECK_EQ(h::usa::veterans_day(2025), c::gregorian::Date{2025, c::gregorian::MONTH::NOVEMBER, 11});
    CHECK_EQ(h::usa::veterans_day(2026), c::gregorian::Date{2026, c::gregorian::MONTH::NOVEMBER, 11});
  }

  TEST_CASE("Martin Luther King Jr Day") {
    CHECK_EQ(h::usa::martin_luther_king_jr_day(2023), c::gregorian::Date{2023, c::gregorian::MONTH::JANUARY, 16});
    CHECK_EQ(h::usa::martin_luther_king_jr_day(2024), c::gregorian::Date{2024, c::gregorian::MONTH::JANUARY, 15});
    CHECK_EQ(h::usa::martin_luther_king_jr_day(2025), c::gregorian::Date{2025, c::gregorian::MONTH::JANUARY, 20});
    CHECK_EQ(h::usa::martin_luther_king_jr_day(2026), c::gregorian::Date{2026, c::gregorian::MONTH::JANUARY, 19});
    CHECK_EQ(h::usa::martin_luther_king_jr_day(2027), c::gregorian::Date{2027, c::gregorian::MONTH::JANUARY, 18});
  }

  TEST_SUITE("Sample Data") {
    TEST_CASE("Independence Day") {
      for (auto year : sampleYears) {
        REQUIRE_EQ(c::holidays::usa::independence_day(year), c::gregorian::Date{year, c::gregorian::MONTH::JULY, 4});
      }
    }

    TEST_CASE("Election Day") {
      const auto expectedDays =
          std::vector<int16_t>{7, 6, 5, 4, 2, 8, 7, 6, 4, 3, 2, 8, 6, 5, 4, 3, 8, 7, 6, 5, 3, 2, 8, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 8, 7,
                               6, 4, 3, 2, 8, 6, 5, 4, 3, 8, 7, 6, 5, 3, 2, 8, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 8, 7, 6, 4, 3, 2, 8, 6, 5,
                               4, 3, 8, 7, 6, 5, 3, 2, 8, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 8, 7, 6, 4, 3, 2, 8, 6, 5, 4, 3, 2, 8, 7, 6};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::usa::election_day(sampleYears[ i ]),
                   c::gregorian::Date{sampleYears[ i ], c::gregorian::MONTH::NOVEMBER, expectedDays[ i ]});
      }
    }

    TEST_CASE("Labor Day") {
      const auto expectedDays =
          std::vector<int16_t>{4, 3, 2, 1, 6, 5, 4, 3, 1, 7, 6, 5, 3, 2, 1, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 1, 7, 6, 4, 3, 2, 1, 6, 5, 4,
                               3, 1, 7, 6, 5, 3, 2, 1, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 1, 7, 6, 4, 3, 2, 1, 6, 5, 4, 3, 1, 7, 6, 5, 3, 2,
                               1, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 1, 7, 6, 4, 3, 2, 1, 6, 5, 4, 3, 1, 7, 6, 5, 3, 2, 1, 7, 6, 5, 4, 3};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::usa::labor_day(sampleYears[ i ]), c::gregorian::Date{sampleYears[ i ], c::gregorian::MONTH::SEPTEMBER, expectedDays[ i ]});
      }
    }

    TEST_CASE("Memorial Day") {
      const auto expectedDays = std::vector<int16_t>{
          29, 28, 27, 26, 31, 30, 29, 28, 26, 25, 31, 30, 28, 27, 26, 25, 30, 29, 28, 27, 25, 31, 30, 29, 27, 26, 25, 31, 29, 28, 27, 26, 31, 30, 29,
          28, 26, 25, 31, 30, 28, 27, 26, 25, 30, 29, 28, 27, 25, 31, 30, 29, 27, 26, 25, 31, 29, 28, 27, 26, 31, 30, 29, 28, 26, 25, 31, 30, 28, 27,
          26, 25, 30, 29, 28, 27, 25, 31, 30, 29, 27, 26, 25, 31, 29, 28, 27, 26, 31, 30, 29, 28, 26, 25, 31, 30, 28, 27, 26, 25, 31, 30, 29, 28};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::usa::memorial_day(sampleYears[ i ]), c::gregorian::Date{sampleYears[ i ], c::gregorian::MONTH::MAY, expectedDays[ i ]});
      }
    }
  }
}
