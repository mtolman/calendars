#include <calendars/cxx/calendars/holidays.h>
#include <doctest/doctest.h>

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("Coptic Holidays") {
  TEST_CASE("Christmas") {
    const auto date = h::copts::christmas(2021);
    CHECK_EQ(date, c::coptic::Date{2021, c::coptic::MONTH::KOIAK, 29});
  }

  TEST_CASE("Building of the Cross") {
    const auto date = h::copts::building_of_the_cross(2021);
    CHECK_EQ(date, c::coptic::Date{2021, c::coptic::MONTH::THOOUT, 17});
  }

  TEST_CASE("Jesus Circumcision") {
    const auto date = h::copts::jesus_circumcision(2021);
    CHECK_EQ(date, c::coptic::Date{2021, c::coptic::MONTH::TOBE, 6});
  }

  TEST_CASE("Epiphany") {
    const auto date = h::copts::epiphany(2021);
    CHECK_EQ(date, c::coptic::Date{2021, c::coptic::MONTH::TOBE, 11});
  }

  TEST_CASE("Mary's Announcement") {
    const auto date = h::copts::marys_announcement(2021);
    CHECK_EQ(date, c::coptic::Date{2021, c::coptic::MONTH::PAREMOTEP, 29});
  }

  TEST_CASE("Jesus' Transfiguration") {
    const auto date = h::copts::jesus_transfiguration(2021);
    CHECK_EQ(date, c::coptic::Date{2021, c::coptic::MONTH::MESORE, 13});
  }
}
