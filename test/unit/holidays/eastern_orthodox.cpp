#include "../test_utils.h"
#include <doctest/doctest.h>
#include <calendars/cxx/calendars/holidays.h>
#include "sample_data.h"

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("Eastern Orthodox Holidays") {
  TEST_CASE("Christmas Day") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::christmas(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::DECEMBER, 25});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::christmas, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::DECEMBER);
        REQUIRE_EQ(julianDate.day(), 25);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("Nativity of the Virgin Mary") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::nativity_of_the_virgin_mary(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::SEPTEMBER, 8});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::nativity_of_the_virgin_mary, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::SEPTEMBER);
        REQUIRE_EQ(julianDate.day(), 8);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("Elevation of the Living Cross") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::elevation_of_the_life_giving_cross(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::SEPTEMBER, 14});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::elevation_of_the_life_giving_cross, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::SEPTEMBER);
        REQUIRE_EQ(julianDate.day(), 14);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("Presentation of the Virgin Mary in the temple") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::presentation_of_the_virgin_mary_in_the_temple(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::NOVEMBER, 21});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::presentation_of_the_virgin_mary_in_the_temple, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::NOVEMBER);
        REQUIRE_EQ(julianDate.day(), 21);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("Theophany") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::theophany(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::JANUARY, 6});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::theophany, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::JANUARY);
        REQUIRE_EQ(julianDate.day(), 6);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("Presentation of Christ in the Temple") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::presentation_of_christ_in_the_temple(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::FEBRUARY, 2});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::presentation_of_christ_in_the_temple, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::FEBRUARY);
        REQUIRE_EQ(julianDate.day(), 2);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("The Annunciation") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::the_annunciation(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::MARCH, 25});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::the_annunciation, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::MARCH);
        REQUIRE_EQ(julianDate.day(), 25);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("The Transfiguration") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::the_transfiguration(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::AUGUST, 6});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::the_transfiguration, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::AUGUST);
        REQUIRE_EQ(julianDate.day(), 6);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("The Repose of the Virgin Mary") {
    SUBCASE("Julian") {
      auto year    = 2018;
      auto holiday = h::eastern_orthodox::the_repose_of_the_virgin_mary(year);
      REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::AUGUST, 15});
    }

    SUBCASE("Gregorian") {
      auto year    = 2018;
      auto holiday = h::holiday_in_gregorian_year(h::eastern_orthodox::the_repose_of_the_virgin_mary, year);

      for (decltype(auto) date : holiday) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::AUGUST);
        REQUIRE_EQ(julianDate.day(), 15);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("The Fast of the Repose of the Virgin Mary") {
    SUBCASE("Julian") {
      auto year     = 2018;
      auto holidays = h::eastern_orthodox::the_fast_of_the_repose_of_the_virgin_mary(year);

      REQUIRE_EQ(holidays.size(), 14);
      REQUIRE_EQ(holidays[ 0 ], c::julian::Date{year, c::julian::MONTH::AUGUST, 1});
      REQUIRE_EQ(holidays[ holidays.size() - 1 ], c::julian::Date{year, c::julian::MONTH::AUGUST, 14});

      for (int i = 0; i < holidays.size(); ++i) {
        REQUIRE_EQ(holidays.at(i), c::julian::Date{year, c::julian::MONTH::AUGUST, static_cast<int16_t>(i + 1)});
      }
    }

    SUBCASE("Gregorian") {
      auto year     = 2018;
      auto holidays = h::holidays_in_gregorian_year(h::eastern_orthodox::the_fast_of_the_repose_of_the_virgin_mary, year);

      for (decltype(auto) date : holidays) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE_EQ(julianDate.month(), c::julian::MONTH::AUGUST);
        REQUIRE_GE(julianDate.day(), 1);
        REQUIRE_LE(julianDate.day(), 14);
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_CASE("The 40 Day Christmas Fast") {
    SUBCASE("Julian") {
      auto year     = 2018;
      auto holidays = h::eastern_orthodox::the_40_day_christmas_fast(year);

      REQUIRE_EQ(holidays.size(), 40);
      REQUIRE_EQ(holidays[ 0 ], c::julian::Date{year, c::julian::MONTH::NOVEMBER, 15});
      REQUIRE_EQ(holidays[ holidays.size() - 1 ], c::julian::Date{year, c::julian::MONTH::DECEMBER, 24});

      for (int i = 0; i < holidays.size(); ++i) {
        if (i <= 15) {
          REQUIRE_EQ(holidays.at(i), c::julian::Date{year, c::julian::MONTH::NOVEMBER, static_cast<int16_t>(i + 15)});
        }
        else {
          REQUIRE_EQ(holidays.at(i), c::julian::Date{year, c::julian::MONTH::DECEMBER, static_cast<int16_t>(i - 15)});
        }
      }
    }

    SUBCASE("Gregorian") {
      auto year     = 2018;
      auto holidays = h::holidays_in_gregorian_year(h::eastern_orthodox::the_40_day_christmas_fast, year);

      for (decltype(auto) date : holidays) {
        auto julianDate = date.to_rd_date().to<c::julian::Date>();
        REQUIRE((julianDate.month() == c::julian::MONTH::NOVEMBER || julianDate.month() == c::julian::MONTH::DECEMBER));
        if (julianDate.month() == c::julian::MONTH::NOVEMBER) {
          REQUIRE_GE(julianDate.day(), 15);
          REQUIRE_LE(julianDate.day(), 30);
        }
        else {
          REQUIRE_GE(julianDate.day(), 1);
          REQUIRE_LE(julianDate.day(), 24);
        }
        REQUIRE_EQ(date.year(), year);
      }
    }
  }

  TEST_SUITE("Sample Data") {
    TEST_CASE("Easter (Orthodox)") {
      using d = calendars::julian::Date;
      struct ExpectedEaster {
        calendars::julian::MONTH month;
        int16_t  day;

        ExpectedEaster(int month, int day) : month(static_cast<calendars::julian::MONTH>(month)), day(static_cast<int16_t>(day)) {}
      };
      const auto expectedDays = std::vector<ExpectedEaster>{
          {4, 30},
          {4, 15},
          {5, 5},
          {4, 27},
          {4, 11},
          {5, 1},
          {4, 23},
          {4, 8},
      };
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::eastern_orthodox::easter(sampleYears[ i ]).to_rd_date(),
                   (calendars::gregorian::Date{sampleYears[ i ], static_cast<calendars::gregorian::MONTH>(expectedDays[ i ].month), expectedDays[ i ].day}).to_rd_date());
      }
    }
  }
}
