#include <doctest/doctest.h>

#include <calendars/cxx/calendars.h>
#include <calendars/cxx/calendars/holidays.h>

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("Armenian Church Holidays") {
  TEST_CASE("Christmas Day (Jerusalem)") {
    auto year    = 2018;
    auto holiday = h::armenian_church::christmas_jerusalem(year);
    REQUIRE_EQ(holiday, c::julian::Date{year, c::julian::MONTH::JANUARY, 6});

    auto holidays = h::holiday_in_gregorian_year(h::armenian_church::christmas_jerusalem, year);

    for (const auto& gregorianDate : holidays) {
      auto julianDate = gregorianDate.to_rd_date().to<c::julian::Date>();
      REQUIRE_EQ(julianDate.month(), c::julian::MONTH::JANUARY);
      REQUIRE_EQ(julianDate.day(), 6);
      REQUIRE_EQ(gregorianDate.year(), year);
    }
  }

  TEST_CASE("Christmas Day (General)") {
    auto year    = 2018;
    auto holiday = h::armenian_church::christmas(year);
    REQUIRE_EQ(holiday, c::gregorian::Date{year, c::gregorian::MONTH::JANUARY, 6});
  }
}
