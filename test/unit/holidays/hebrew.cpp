#include <calendars/cxx/calendars/holidays.h>
#include <calendars/cxx/calendars/gregorian.h>
#include <doctest/doctest.h>
#include <tuple>
#include <vector>
#include "sample_data.h"

TEST_SUITE("Hebrew Holidays") {
  TEST_CASE("Yom Kippur") {
    const auto expectedMonthDays =
      std::vector<std::tuple<int16_t, int16_t>>{
        {10, 9}, {9, 27}, {9, 16}, {10, 6}, {9, 25}, {10, 13}, {10, 2}, {9, 22},
        {10, 9}, {9, 28}, {9, 18}, {10, 8}, {9, 26}, {9, 14}, {10, 4}, {9, 23},
      };
    for (size_t i = 0; i < expectedMonthDays.size(); ++i) {
      const auto hebrewYear = calendars::hebrew::Date::year_from_gregorian(sampleYears[i]);
      const auto expectedRd = calendars::gregorian::Date{sampleYears[i], static_cast<calendars::gregorian::MONTH>(std::get<0>(expectedMonthDays[i])), std::get<1>(expectedMonthDays[i])}.to_rd_date();
      REQUIRE_EQ(
        calendars::holidays::hebrew::yom_kippur(hebrewYear).to_rd_date().to<calendars::gregorian::Date>(),
        expectedRd.to<calendars::gregorian::Date>()
      );
    }
  }

  TEST_CASE("Rosh Ha Shanah") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::rosh_ha_shanah(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::TISHRI);
      REQUIRE_EQ(date.day(), 1);
    }
  }

  TEST_CASE("Sukkot") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::sukkot(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::TISHRI);
      REQUIRE_EQ(date.day(), 15);
    }
  }

  TEST_CASE("Hoshana Rabba") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::hoshana_rabba(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::TISHRI);
      REQUIRE_EQ(date.day(), 21);
    }
  }

  TEST_CASE("Shemini Azeret") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::shemini_azeret(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::TISHRI);
      REQUIRE_EQ(date.day(), 22);
    }
  }

  TEST_CASE("Shemini Torah") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::simhat_torah(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::TISHRI);
      REQUIRE_EQ(date.day(), 23);
    }
  }

  TEST_CASE("Passover") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::passover(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::NISAN);
      REQUIRE_EQ(date.day(), 15);
    }
  }

  TEST_CASE("Passover End") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::passover_end(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::NISAN);
      REQUIRE_EQ(date.day(), 21);
    }
  }

  TEST_CASE("Shavout") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::shavout(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::SIVAN);
      REQUIRE_EQ(date.day(), 6);
    }
  }

  TEST_CASE("Tu B Shevat") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::tu_b_shevat(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::SHEVAT);
      REQUIRE_EQ(date.day(), 15);
    }
  }

  TEST_CASE("Ta Anit Esther") {
    SUBCASE("Not Sunday") {
      const auto year = 5861;
      auto       date = calendars::holidays::hebrew::ta_anit_esther(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::ADAR);
      REQUIRE_EQ(date.day(), 13);
    }

    SUBCASE("Not Sunday") {
      const auto year = 5862;
      auto       date = calendars::holidays::hebrew::ta_anit_esther(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::MONTH::ADAR);
      REQUIRE_EQ(date.day(), 11);
    }
  }

  TEST_CASE("Purim") {
    for (const auto& year : sampleYears) {
      auto date = calendars::holidays::hebrew::purim(year);
      REQUIRE_EQ(date.year(), year);
      REQUIRE_EQ(date.month(), calendars::hebrew::Date::last_month_of_year(year));
      REQUIRE_EQ(date.day(), 14);
    }
  }

  TEST_CASE("Sukkot Intermediate Days") {
    SUBCASE("C") {
      CalendarHolidayArray* array;
      calendar_holidays_hebrew_sukkot_intermediate_days(&array, 5671);
      CHECK_EQ(array->size, 6);
      CHECK_EQ(array->dates[0], 697539);
      CHECK_EQ(array->dates[1], 697540);
      CHECK_EQ(array->dates[2], 697541);
      CHECK_EQ(array->dates[3], 697542);
      CHECK_EQ(array->dates[4], 697543);
      CHECK_EQ(array->dates[5], 697544);
      calendar_holiday_array_free(&array);
    }

    SUBCASE("CXX") {
      auto dates = calendars::holidays::hebrew::sukkot_intermediate_days(5671);
      REQUIRE_EQ(dates.size(), 6);
      REQUIRE_EQ(dates[0].to_rd_date(), calendars::RdDate{697539});
      REQUIRE_EQ(dates[1].to_rd_date(), calendars::RdDate{697540});
      REQUIRE_EQ(dates[2].to_rd_date(), calendars::RdDate{697541});
      REQUIRE_EQ(dates[3].to_rd_date(), calendars::RdDate{697542});
      REQUIRE_EQ(dates[4].to_rd_date(), calendars::RdDate{697543});
      REQUIRE_EQ(dates[5].to_rd_date(), calendars::RdDate{697544});
    }
  }

  TEST_CASE("Passover Days") {
    SUBCASE("C") {
      CalendarHolidayArray* array;
      calendar_holidays_hebrew_passover_days(&array, 5671);
      CHECK_EQ(array->size, 5);
      CHECK_EQ(array->dates[0], 697716);
      CHECK_EQ(array->dates[1], 697717);
      CHECK_EQ(array->dates[2], 697718);
      CHECK_EQ(array->dates[3], 697719);
      CHECK_EQ(array->dates[4], 697720);
      calendar_holiday_array_free(&array);
    }

    SUBCASE("CXX") {
      auto dates = calendars::holidays::hebrew::passover_days(5671);
      REQUIRE_EQ(dates.size(), 5);
      REQUIRE_EQ(dates[0].to_rd_date(), calendars::RdDate{697716});
      REQUIRE_EQ(dates[1].to_rd_date(), calendars::RdDate{697717});
      REQUIRE_EQ(dates[2].to_rd_date(), calendars::RdDate{697718});
      REQUIRE_EQ(dates[3].to_rd_date(), calendars::RdDate{697719});
      REQUIRE_EQ(dates[4].to_rd_date(), calendars::RdDate{697720});
    }
  }

  TEST_CASE("Hanukkah Days") {
    SUBCASE("C") {
      CalendarHolidayArray* array;
      calendar_holidays_hebrew_hanukkah_days(&array, 5671);
      CHECK_EQ(array->size, 8);
      CHECK_EQ(array->dates[0], 697607);
      CHECK_EQ(array->dates[1], 697608);
      CHECK_EQ(array->dates[2], 697609);
      CHECK_EQ(array->dates[3], 697610);
      CHECK_EQ(array->dates[4], 697611);
      CHECK_EQ(array->dates[5], 697612);
      CHECK_EQ(array->dates[6], 697613);
      CHECK_EQ(array->dates[7], 697614);
      calendar_holiday_array_free(&array);
    }

    SUBCASE("CXX") {
      auto dates = calendars::holidays::hebrew::hanukkah(5671);
      REQUIRE_EQ(dates.size(), 8);
      REQUIRE_EQ(dates[0].to_rd_date(), calendars::RdDate{697607});
      REQUIRE_EQ(dates[1].to_rd_date(), calendars::RdDate{697608});
      REQUIRE_EQ(dates[2].to_rd_date(), calendars::RdDate{697609});
      REQUIRE_EQ(dates[3].to_rd_date(), calendars::RdDate{697610});
      REQUIRE_EQ(dates[4].to_rd_date(), calendars::RdDate{697611});
      REQUIRE_EQ(dates[5].to_rd_date(), calendars::RdDate{697612});
      REQUIRE_EQ(dates[6].to_rd_date(), calendars::RdDate{697613});
      REQUIRE_EQ(dates[7].to_rd_date(), calendars::RdDate{697614});
    }
  }
}
