#include <doctest/doctest.h>

#include <calendars/cxx/calendars/icelandic.h>
#include "test_utils.h"

TEST_SUITE("Icelandic Dates") {
  TEST_CASE("Comparisons") {
    using CLASS = calendars::icelandic::Date;

    CHECK(CLASS{0, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{0, static_cast<calendars::icelandic::SEASON>(0), 0, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK(CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{58, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{58, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{5, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{12, static_cast<calendars::icelandic::SEASON>(180), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{12, static_cast<calendars::icelandic::SEASON>(0), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 6, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 0, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} == CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(1)});

    CHECK_FALSE(CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{5, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(180), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 6, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} != CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(1)});

    CHECK_FALSE(CLASS{0, static_cast<calendars::icelandic::SEASON>(0), 0, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{0, static_cast<calendars::icelandic::SEASON>(0), 0, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{58, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{58, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{58, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{12, static_cast<calendars::icelandic::SEASON>(180), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 6, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(1)});
    CHECK_FALSE(CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{12, static_cast<calendars::icelandic::SEASON>(0), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 0, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{5, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{15, static_cast<calendars::icelandic::SEASON>(0), 2, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{13, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{15, static_cast<calendars::icelandic::SEASON>(180), 1, static_cast<calendars::DAY_OF_WEEK>(0)} < CLASS{13, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{13, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(1)} < CLASS{13, static_cast<calendars::icelandic::SEASON>(90), 2, static_cast<calendars::DAY_OF_WEEK>(0)});

    CHECK_FALSE(CLASS{0, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{0, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(180), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 6, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} > CLASS{5, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});

    CHECK(CLASS{0, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{0, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(180), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 6, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} <= CLASS{5, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});

    CHECK(CLASS{0, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{0, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{58, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(180), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK_FALSE(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 6, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(0), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 0, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
    CHECK(CLASS{12, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)} >= CLASS{5, static_cast<CALENDAR_ICELANDIC_SEASON>(90), 2, static_cast<CALENDAR_DAY_OF_WEEK>(0)});
  }

  TEST_CASE("Conversions") {
    calendars::icelandic::Date date{};
    calendars::RdDate        rdate;

    rdate     = calendars::RdDate{8849};
    date      = rdate.to<calendars::icelandic::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::icelandic::Date;
    using S = calendars::icelandic::SEASON;
    using D = calendars::DAY_OF_WEEK;
    const auto sampleRdDates = sample_rd_dates();

    static std::vector<T> expected = {
        T{-587, S::WINTER, 22, D::SUNDAY},   T{-168, S::SUMMER, 15, D::WEDNESDAY}, T{70, S::SUMMER, 22, D::WEDNESDAY},
        T{135, S::SUMMER, 24, D::SUNDAY},    T{469, S::WINTER, 11, D::WEDNESDAY},  T{576, S::SUMMER, 4, D::MONDAY},
        T{694, S::WINTER, 3, D::SATURDAY},   T{1013, S::SUMMER, 1, D::SUNDAY},     T{1096, S::SUMMER, 5, D::SUNDAY},
        T{1189, S::WINTER, 22, D::FRIDAY},   T{1239, S::WINTER, 21, D::SATURDAY},  T{1287, S::WINTER, 23, D::FRIDAY},
        T{1298, S::SUMMER, 1, D::SUNDAY},    T{1391, S::SUMMER, 8, D::SUNDAY},     T{1435, S::WINTER, 15, D::WEDNESDAY},
        T{1491, S::WINTER, 25, D::SATURDAY}, T{1553, S::SUMMER, 22, D::SATURDAY},  T{1559, S::WINTER, 20, D::SATURDAY},
        T{1648, S::SUMMER, 7, D::WEDNESDAY}, T{1680, S::SUMMER, 10, D::SUNDAY},    T{1716, S::SUMMER, 14, D::FRIDAY},
        T{1768, S::SUMMER, 9, D::SUNDAY},    T{1819, S::SUMMER, 15, D::MONDAY},    T{1838, S::WINTER, 22, D::WEDNESDAY},
        T{1902, S::WINTER, 26, D::SUNDAY},   T{1929, S::SUMMER, 18, D::SUNDAY},    T{1941, S::SUMMER, 23, D::MONDAY},
        T{1942, S::WINTER, 26, D::MONDAY},   T{1943, S::SUMMER, 25, D::THURSDAY},  T{1991, S::WINTER, 21, D::TUESDAY},
        T{1995, S::WINTER, 18, D::SUNDAY},   T{2038, S::WINTER, 3, D::WEDNESDAY},  T{2094, S::SUMMER, 13, D::SUNDAY},
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      auto actual = sampleRdDates[ i ].to<T>();
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
