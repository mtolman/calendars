#include <doctest/doctest.h>

#include <calendars/cxx/calendars/julian.h>
#include "test_utils.h"

TEST_SUITE("Julian Date") {
  TEST_CASE("Comparisons") { comp_tests_three_args<calendars::julian::Date, calendars::julian::MONTH>(); }

  TEST_CASE("Conversions") {
    calendars::julian::Date date;
    calendars::RdDate     rdate;

    rdate     = calendars::RdDate{8849};
    date      = rdate.to<calendars::julian::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::julian::Date;

    auto sampleRdDates = sample_rd_dates();

    static std::vector<T> expected = {T{-587, static_cast<calendars::julian::MONTH>(7), 30}, T{-169, static_cast<calendars::julian::MONTH>(12), 8}, T{70, static_cast<calendars::julian::MONTH>(9), 26},   T{135, static_cast<calendars::julian::MONTH>(10), 3},   T{470, static_cast<calendars::julian::MONTH>(1), 7},   T{576, static_cast<calendars::julian::MONTH>(5), 18},  T{694, static_cast<calendars::julian::MONTH>(11), 7},
                                      T{1013, static_cast<calendars::julian::MONTH>(4), 19}, T{1096, static_cast<calendars::julian::MONTH>(5), 18}, T{1190, static_cast<calendars::julian::MONTH>(3), 16}, T{1240, static_cast<calendars::julian::MONTH>(3), 3},   T{1288, static_cast<calendars::julian::MONTH>(3), 26}, T{1298, static_cast<calendars::julian::MONTH>(4), 20}, T{1391, static_cast<calendars::julian::MONTH>(6), 4},
                                      T{1436, static_cast<calendars::julian::MONTH>(1), 25}, T{1492, static_cast<calendars::julian::MONTH>(3), 31}, T{1553, static_cast<calendars::julian::MONTH>(9), 9},  T{1560, static_cast<calendars::julian::MONTH>(2), 24},  T{1648, static_cast<calendars::julian::MONTH>(5), 31}, T{1680, static_cast<calendars::julian::MONTH>(6), 20}, T{1716, static_cast<calendars::julian::MONTH>(7), 13},
                                      T{1768, static_cast<calendars::julian::MONTH>(6), 8},  T{1819, static_cast<calendars::julian::MONTH>(7), 21}, T{1839, static_cast<calendars::julian::MONTH>(3), 15}, T{1903, static_cast<calendars::julian::MONTH>(4), 6},   T{1929, static_cast<calendars::julian::MONTH>(8), 12}, T{1941, static_cast<calendars::julian::MONTH>(9), 16}, T{1943, static_cast<calendars::julian::MONTH>(4), 6},
                                      T{1943, static_cast<calendars::julian::MONTH>(9), 24}, T{1992, static_cast<calendars::julian::MONTH>(3), 4},  T{1996, static_cast<calendars::julian::MONTH>(2), 12}, T{2038, static_cast<calendars::julian::MONTH>(10), 28}, T{2094, static_cast<calendars::julian::MONTH>(7), 5}};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
