#include <calendars/cxx/calendars/common.h>
#include <doctest/doctest.h>

#include <algorithm>
#include <array>
#include <vector>

#include "test_utils.h"

TEST_SUITE("Common - RdDate") {
  TEST_CASE("Day of week") {
    CHECK_EQ(calendars::RdDate{0}.day_of_week(), calendars::DAY_OF_WEEK::SUNDAY);
    CHECK_EQ(calendars::RdDate{1}.day_of_week(), calendars::DAY_OF_WEEK::MONDAY);
    CHECK_EQ(calendars::RdDate{2}.day_of_week(), calendars::DAY_OF_WEEK::TUESDAY);
    CHECK_EQ(calendars::RdDate{3}.day_of_week(), calendars::DAY_OF_WEEK::WEDNESDAY);
    CHECK_EQ(calendars::RdDate{4}.day_of_week(), calendars::DAY_OF_WEEK::THURSDAY);
    CHECK_EQ(calendars::RdDate{5}.day_of_week(), calendars::DAY_OF_WEEK::FRIDAY);
    CHECK_EQ(calendars::RdDate{6}.day_of_week(), calendars::DAY_OF_WEEK::SATURDAY);

    CHECK_EQ(calendars::RdDate{7}.day_of_week(), calendars::DAY_OF_WEEK::SUNDAY);
    CHECK_EQ(calendars::RdDate{8}.day_of_week(), calendars::DAY_OF_WEEK::MONDAY);
    CHECK_EQ(calendars::RdDate{9}.day_of_week(), calendars::DAY_OF_WEEK::TUESDAY);
    CHECK_EQ(calendars::RdDate{10}.day_of_week(), calendars::DAY_OF_WEEK::WEDNESDAY);
    CHECK_EQ(calendars::RdDate{11}.day_of_week(), calendars::DAY_OF_WEEK::THURSDAY);
    CHECK_EQ(calendars::RdDate{12}.day_of_week(), calendars::DAY_OF_WEEK::FRIDAY);
    CHECK_EQ(calendars::RdDate{13}.day_of_week(), calendars::DAY_OF_WEEK::SATURDAY);
  }

  TEST_CASE("Date") {
    auto baseDate = calendars::RdDate{234};
    SUBCASE("Add days") {
      CHECK_EQ(static_cast<CalendarRdDate>(baseDate.add_days(34)), 268);
    }

    SUBCASE("Add days") {
      CHECK_EQ(static_cast<CalendarRdDate>(baseDate.sub_days(34)), 200);
    }

    SUBCASE("Diff Days") {
      CHECK_EQ(baseDate.day_difference(calendars::RdDate{434}), -200);
    }
  }

  TEST_CASE("positions_in_range") {
    auto result = calendars::RdDate{12}.positions_in_range(calendars::RdDate{33}, 3, 7, 2);
    CHECK_EQ(result.size(), 3);

    CHECK_EQ(result[ 0 ], calendars::RdDate{15});
    CHECK_EQ(result[ 1 ], calendars::RdDate{22});
    CHECK_EQ(result[ 2 ], calendars::RdDate{29});

    result = calendars::RdDate{12}.positions_in_range(calendars::RdDate{33}, 3, 7, 0);
    CHECK_EQ(result.size(), 3);

    CHECK_EQ(result[ 0 ], calendars::RdDate{17});
    CHECK_EQ(result[ 1 ], calendars::RdDate{24});
    CHECK_EQ(result[ 2 ], calendars::RdDate{31});
  }

  TEST_CASE("M Cycle") {
    CHECK_EQ(calendars::RdDate{0}.day_of_m_cycle(5, 0), 0);
    CHECK_EQ(calendars::RdDate{1}.day_of_m_cycle(5, 0), 1);
    CHECK_EQ(calendars::RdDate{2}.day_of_m_cycle(5, 0), 2);
    CHECK_EQ(calendars::RdDate{3}.day_of_m_cycle(5, 0), 3);
    CHECK_EQ(calendars::RdDate{4}.day_of_m_cycle(5, 0), 4);
    CHECK_EQ(calendars::RdDate{5}.day_of_m_cycle(5, 0), 0);
    CHECK_EQ(calendars::RdDate{6}.day_of_m_cycle(5, 0), 1);

    CHECK_EQ(calendars::RdDate{0}.day_of_m_cycle(5, 1), 4);
    CHECK_EQ(calendars::RdDate{1}.day_of_m_cycle(5, 1), 0);
    CHECK_EQ(calendars::RdDate{2}.day_of_m_cycle(5, 1), 1);
    CHECK_EQ(calendars::RdDate{3}.day_of_m_cycle(5, 1), 2);
    CHECK_EQ(calendars::RdDate{4}.day_of_m_cycle(5, 1), 3);
    CHECK_EQ(calendars::RdDate{5}.day_of_m_cycle(5, 1), 4);
    CHECK_EQ(calendars::RdDate{6}.day_of_m_cycle(5, 1), 0);
  }

  TEST_CASE("Day of week on or before") {
    CHECK_EQ(calendars::RdDate{0}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{1}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{2}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{3}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{4}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{5}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{6}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
  }

  TEST_CASE("Day of week before") {
    CHECK_EQ(calendars::RdDate{0}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{-7});
    CHECK_EQ(calendars::RdDate{1}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{2}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{3}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{4}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{5}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{6}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{-7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
  }

  TEST_CASE("Day of week on or after") {
    CHECK_EQ(calendars::RdDate{0}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{1}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{2}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{3}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{4}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{5}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{6}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
  }

  TEST_CASE("Day of week after") {
    CHECK_EQ(calendars::RdDate{0}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{1}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{2}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{3}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{4}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{5}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{6}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
  }

  TEST_CASE("Day of week nearest") {
    CHECK_EQ(calendars::RdDate{0}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{1}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{2}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{3}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{0});
    CHECK_EQ(calendars::RdDate{4}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{5}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});
    CHECK_EQ(calendars::RdDate{6}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{7});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{0}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::RdDate{calendars::RdDate{7}});
  }

  TEST_CASE("K-day of M Cycle on or before") {
    CHECK_EQ(calendars::RdDate{-12}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{0}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{1}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{2}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{3}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{4}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{5}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{6}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{-12});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{-12}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
  }

  TEST_CASE("K-day of M Cycle before") {
    CHECK_EQ(calendars::RdDate{-12}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-32});
    CHECK_EQ(calendars::RdDate{0}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{1}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{2}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{3}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{4}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{5}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{6}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{-12});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{-12}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-32}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
  }

  TEST_CASE("K-day of M Cycle after") {
    CHECK_EQ(calendars::RdDate{-12}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{0}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{1}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{2}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{3}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{4}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{5}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{6}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{8});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{-12}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
  }

  TEST_CASE("K-day of M Cycle on or after") {
    CHECK_EQ(calendars::RdDate{-12}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{0}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{1}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{2}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{3}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{4}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{5}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{6}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{8});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{-12}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
  }

  TEST_CASE("K-day of M Cycle nearest") {
    CHECK_EQ(calendars::RdDate{-12}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{-12});
    CHECK_EQ(calendars::RdDate{0}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{1}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{2}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{3}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{4}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{5}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{8});
    CHECK_EQ(calendars::RdDate{6}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{8});

    CHECK_EQ(calendars::RdDate{calendars::RdDate{-12}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{-12}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{0}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{1}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{2}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{3}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{4}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{5}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
    CHECK_EQ(calendars::RdDate{calendars::RdDate{6}}.kth_day_of_m_cycle_nearest(5, 20, 3), calendars::RdDate{calendars::RdDate{8}});
  }

  TEST_CASE("Nth Weekday") {
    auto rd   = calendars::RdDate{38};
    CHECK_EQ(rd, rd.nth_week_day(0, calendars::DAY_OF_WEEK::WEDNESDAY));
    CHECK_EQ(rd.add_days(1), rd.nth_week_day(1, calendars::DAY_OF_WEEK::THURSDAY));
    CHECK_EQ(rd.sub_days(1), rd.nth_week_day(-1, calendars::DAY_OF_WEEK::TUESDAY));
  }

  TEST_CASE("Day of Week - Sample Data") {
    using T = calendars::DAY_OF_WEEK;

    auto sampleRdDates = sample_rd_dates();

    static std::vector<calendars::DAY_OF_WEEK> expected = {
        T::SUNDAY,    T::WEDNESDAY, T::WEDNESDAY, T::SUNDAY, T::WEDNESDAY, T::MONDAY,    T::SATURDAY, T::SUNDAY,   T::SUNDAY,
        T::FRIDAY,    T::SATURDAY,  T::FRIDAY,    T::SUNDAY, T::SUNDAY,    T::WEDNESDAY, T::SATURDAY, T::SATURDAY, T::SATURDAY,
        T::WEDNESDAY, T::SUNDAY,    T::FRIDAY,    T::SUNDAY, T::MONDAY,    T::WEDNESDAY, T::SUNDAY,   T::SUNDAY,   T::MONDAY,
        T::MONDAY,    T::THURSDAY,  T::TUESDAY,   T::SUNDAY, T::WEDNESDAY, T::SUNDAY};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].day_of_week(), expected[ i ]);
    }
  }
}

TEST_SUITE("Common - Date Range") {
  TEST_CASE("Between specific dates") {
    auto range = calendars::DateRange(calendars::RdDate{10}, calendars::RdDate{23});
    auto vec = std::vector<CalendarRdDate>{};

    for(const auto& date : range) {
      vec.emplace_back(static_cast<CalendarRdDate>(date));
    }

    auto expected = std::vector<CalendarRdDate>{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
    REQUIRE_EQ(vec.size(), expected.size());
    REQUIRE_EQ(range.size(), expected.size());

    for (auto i = 0; i < vec.size(); ++i) {
      CHECK_EQ(vec[i], expected[i]);
    }
  }

  TEST_CASE("Reverse Between specific dates") {
    auto range = calendars::DateRange(calendars::RdDate{10}, calendars::RdDate{23});
    auto vec = std::vector<CalendarRdDate>{};

    for(auto it = range.rbegin(); it != range.rend(); ++it) {
      vec.emplace_back(static_cast<CalendarRdDate>(*it));
    }

    auto expected = std::vector<CalendarRdDate>{22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10};
    REQUIRE_EQ(vec.size(), expected.size());
    REQUIRE_EQ(range.size(), expected.size());

    for (auto i = 0; i < vec.size(); ++i) {
      CHECK_EQ(vec[i], expected[i]);
    }
  }

  TEST_CASE("Constant Between specific dates") {
    auto range = calendars::DateRange(calendars::RdDate{10}, calendars::RdDate{23});
    auto vec = std::vector<CalendarRdDate>{};

    for(auto it = range.cbegin(); it != range.cend(); ++it) {
      vec.emplace_back(static_cast<CalendarRdDate>(*it));
    }

    auto expected = std::vector<CalendarRdDate>{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
    REQUIRE_EQ(vec.size(), expected.size());
    REQUIRE_EQ(range.size(), expected.size());

    for (auto i = 0; i < vec.size(); ++i) {
      CHECK_EQ(vec[i], expected[i]);
    }
  }
}

#include <calendars/cxx/calendars/holidays.h>
#include <calendars/cxx/calendars/hebrew.h>
#include <calendars/cxx/calendars/holidays/copts.h>

TEST_SUITE("Holidays common") {
  TEST_CASE("To Gregorian Single Date") {
    SUBCASE("Lambda") {
      auto holidays = calendars::holidays::holiday_lambda_in_gregorian_year<calendars::hebrew::Date>([](int64_t year){
        return calendars::hebrew::Date{year, calendars::hebrew::MONTH::NISAN, 1};
      }, 1244);

      for (const auto& gregorianDate : holidays) {
        auto hebrewDate = gregorianDate.to_rd_date().to<calendars::hebrew::Date>();
        REQUIRE_EQ(hebrewDate.month(), calendars::hebrew::MONTH::NISAN);
        REQUIRE_EQ(hebrewDate.day(), 1);
        REQUIRE_EQ(gregorianDate.year(), 1244);
      }
    }

    SUBCASE("C function") {
      auto holidays = calendars::holidays::holiday_in_gregorian_year(calendars::holidays::copts::christmas, 1244);

      for (const auto& gregorianDate : holidays) {
        auto hebrewDate = gregorianDate.to_rd_date().to<calendars::coptic::Date>();
        REQUIRE_EQ(hebrewDate.month(), calendars::coptic::MONTH::KOIAK);
        REQUIRE_EQ(hebrewDate.day(), 29);
        REQUIRE_EQ(gregorianDate.year(), 1244);
      }
    }
  }
}
