#include <doctest/doctest.h>

#include <calendars/cxx/calendars/old_hindu_solar.h>
#include "test_utils.h"

TEST_SUITE("Old Hindu Solar Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::old_hindu_solar::Date, calendars::old_hindu_solar::SAURA>();
  }

  TEST_CASE("Conversions") {
    calendars::old_hindu_solar::Date date{};
    calendars::RdDate            rdate;

    rdate = calendars::RdDate{8849};
    date      = rdate.to<decltype(date)>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::old_hindu_solar::Date;
    using M = calendars::old_hindu_solar::SAURA;

    static std::vector<T> expected = {
        T{2515, static_cast<M>(5), 19},  T{2933, static_cast<M>(9), 26},  T{3171, static_cast<M>(7), 11},  T{3236, static_cast<M>(7), 17},
        T{3570, static_cast<M>(10), 19}, T{3677, static_cast<M>(2), 28},  T{3795, static_cast<M>(8), 17},  T{4114, static_cast<M>(1), 26},
        T{4197, static_cast<M>(2), 24},  T{4290, static_cast<M>(12), 20}, T{4340, static_cast<M>(12), 7},  T{4388, static_cast<M>(12), 30},
        T{4399, static_cast<M>(1), 24},  T{4492, static_cast<M>(3), 7},   T{4536, static_cast<M>(10), 28}, T{4593, static_cast<M>(1), 3},
        T{4654, static_cast<M>(6), 12},  T{4660, static_cast<M>(11), 27}, T{4749, static_cast<M>(3), 1},   T{4781, static_cast<M>(3), 21},
        T{4817, static_cast<M>(4), 13},  T{4869, static_cast<M>(3), 8},   T{4920, static_cast<M>(4), 20},  T{4939, static_cast<M>(12), 13},
        T{5004, static_cast<M>(1), 4},   T{5030, static_cast<M>(5), 11},  T{5042, static_cast<M>(6), 15},  T{5044, static_cast<M>(1), 4},
        T{5044, static_cast<M>(6), 23},  T{5092, static_cast<M>(12), 2},  T{5096, static_cast<M>(11), 11}, T{5139, static_cast<M>(7), 26},
        T{5195, static_cast<M>(4), 2}};

    const auto sampleRdDates = sample_rd_dates();
    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);

      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
