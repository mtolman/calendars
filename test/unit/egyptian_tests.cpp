#include <calendars/cxx/calendars/egyptian.h>
#include <doctest/doctest.h>

#include "test_utils.h"

TEST_SUITE("Egyptian Date") {
  TEST_CASE("Comparison") {
    comp_tests_three_args<calendars::egyptian::Date>();
  }

  TEST_CASE("Conversions") {
    calendars::egyptian::Date date;
    calendars::RdDate       rdate;

    rdate     = calendars::RdDate{8961};
    date      = rdate.to<calendars::egyptian::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8961});
    CHECK_EQ(date.year(), 772);
    CHECK_EQ(date.month(), 12);
    CHECK_EQ(date.day(), 4);

    rdate     = calendars::RdDate{8958};
    date      = rdate.to<calendars::egyptian::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8958});
    CHECK_EQ(date.year(), 772);
    CHECK_EQ(date.month(), 12);
    CHECK_EQ(date.day(), 1);

    rdate     = calendars::RdDate{8988};
    date      = rdate.to<calendars::egyptian::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8988});
    CHECK_EQ(date.year(), 772);
    CHECK_EQ(date.month(), 13);
    CHECK_EQ(date.day(), 1);

    rdate = calendars::RdDate{8849};
    date      = rdate.to<calendars::egyptian::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
    CHECK_EQ(date.year(), 772);
    CHECK_EQ(date.month(), 8);
    CHECK_EQ(date.day(), 12);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::egyptian::Date;

    static std::vector<T> expected = {
        T{161, 7, 15},  T{580, 3, 6},    T{818, 2, 22},   T{883, 3, 15},  T{1217, 9, 15},  T{1324, 2, 18},  T{1442, 9, 10},
        T{1761, 5, 8},  T{1844, 6, 28},  T{1938, 5, 18},  T{1988, 5, 18}, T{2036, 6, 23},  T{2046, 7, 20},  T{2139, 9, 28},
        T{2184, 5, 29}, T{2240, 8, 19},  T{2302, 2, 11},  T{2308, 7, 30}, T{2396, 11, 29}, T{2428, 12, 27}, T{2465, 1, 24},
        T{2517, 1, 2},  T{2568, 2, 27},  T{2587, 10, 29}, T{2651, 12, 7}, T{2678, 4, 17},  T{2690, 5, 25},  T{2691, 12, 17},
        T{2692, 6, 3},  T{2740, 11, 27}, T{2744, 11, 7},  T{2787, 8, 1},  T{2843, 4, 20},
    };

    auto sampleRdDates = sample_rd_dates();

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
