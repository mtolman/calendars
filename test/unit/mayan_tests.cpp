#include <doctest/doctest.h>

#include <calendars/cxx/calendars/mayan.h>
#include <calendars/cxx/calendars/julian_day.h>
#include "test_utils.h"
#include "epochs.h"

TEST_SUITE("Mayan") {
  TEST_CASE("Long Date") {
    SUBCASE("Conversions") {
      CHECK_EQ(mayan_long_date_epoch, static_cast<CalendarRdDate>(calendars::RdDate::from(calendars::julian_day::Date{584283})));
      CHECK_EQ(mayan_haab_epoch, mayan_long_date_epoch - calendars::mayan::haab::Date{calendars::mayan::haab::MONTH::CUMKU, 8}.ordinal());

      SUBCASE("mayan::LongDate") {
        calendars::mayan::long_date::Date date{};
        calendars::RdDate            rdate;

        rdate     = calendars::RdDate{8849};
        date      = rdate.to<decltype(date)>();
        rdate     = calendars::RdDate::from(date);
        CHECK_EQ(rdate, calendars::RdDate{8849});
      }
    }

    SUBCASE("Sample Data") {
      using T = calendars::mayan::long_date::Date;

      static std::vector<T> expected = {
          T{{6, 8, 3, 13, 9}}, T{{7, 9, 8, 3, 15}}, T{{8, 1, 9, 8, 11}}, T{{8, 4, 15, 7, 19}},
          T{{9, 1, 14, 10, 9}}, T{{9, 7, 2, 8, 17}}, T{{9, 13, 2, 12, 9}}, T{{10, 9, 5, 14, 2}},
          T{{10, 13, 10, 1, 7}}, T{{10, 18, 5, 4, 17}}, T{{11, 0, 15, 17, 7}}, T{{11, 3, 4, 13, 2}},
          T{{11, 3, 14, 16, 19}}, T{{11, 8, 9, 7, 12}}, T{{11, 10, 14, 12, 18}}, T{{11, 13, 11, 12, 18}},
          T{{11, 16, 14, 1, 0}}, T{{11, 17, 0, 10, 19}}, T{{12, 1, 10, 2, 18}}, T{{12, 3, 2, 12, 6}},
          T{{12, 4, 19, 4, 18}}, T{{12, 7, 11, 16, 16}}, T{{12, 10, 3, 14, 6}}, T{{12, 11, 3, 13, 3}},
          T{{12, 14, 8, 13, 1}}, T{{12, 15, 15, 8, 6}}, T{{12, 16, 7, 13, 4}}, T{{12, 16, 9, 5, 11}},
          T{{12, 16, 9, 14, 2}}, T{{12, 18, 18, 16, 16}}, T{{12, 19, 2, 16, 16}}, T{{13, 1, 6, 4, 15}},
          T{{13, 4, 2, 13, 14}}
      };

      const auto sampleRdDates = sample_rd_dates();
      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
        CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
      }
    }
  }

  TEST_CASE("Haab") {
    SUBCASE("Sample Data") {
      using T = calendars::mayan::haab::Date;
      using MONTH = calendars::mayan::haab::MONTH;

      static std::vector<T> expected = {
          T{static_cast<MONTH>(11), 12}, T{static_cast<MONTH>(5), 3}, T{static_cast<MONTH>(4), 9},
          T{static_cast<MONTH>(5), 12}, T{static_cast<MONTH>(14), 12}, T{static_cast<MONTH>(4), 5},
          T{static_cast<MONTH>(14), 7}, T{static_cast<MONTH>(8), 5}, T{static_cast<MONTH>(10), 15},
          T{static_cast<MONTH>(8), 15}, T{static_cast<MONTH>(8), 15}, T{static_cast<MONTH>(10), 10},
          T{static_cast<MONTH>(11), 17}, T{static_cast<MONTH>(15), 5}, T{static_cast<MONTH>(9), 6},
          T{static_cast<MONTH>(13), 6}, T{static_cast<MONTH>(3), 18}, T{static_cast<MONTH>(12), 7},
          T{static_cast<MONTH>(18), 6}, T{static_cast<MONTH>(1), 9}, T{static_cast<MONTH>(3), 1},
          T{static_cast<MONTH>(1), 19}, T{static_cast<MONTH>(4), 14}, T{static_cast<MONTH>(16), 16},
          T{static_cast<MONTH>(18), 14}, T{static_cast<MONTH>(7), 4}, T{static_cast<MONTH>(9), 2},
          T{static_cast<MONTH>(19), 4}, T{static_cast<MONTH>(9), 10}, T{static_cast<MONTH>(18), 4},
          T{static_cast<MONTH>(17), 4}, T{static_cast<MONTH>(12), 8}, T{static_cast<MONTH>(7), 7}
      };

      const auto sampleRdDates = sample_rd_dates();
      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      }
    }
  }

  TEST_CASE("Tzolkin") {
    SUBCASE("Sample Data") {

      using T = calendars::mayan::tzolkin::Date;
      using NAME = calendars::mayan::tzolkin::NAME;

      static std::vector<T> expected = {
          T{5, static_cast<NAME>(9)}, T{9, static_cast<NAME>(15)}, T{12, static_cast<NAME>(11)},
          T{9, static_cast<NAME>(19)}, T{3, static_cast<NAME>(9)}, T{7, static_cast<NAME>(17)},
          T{2, static_cast<NAME>(9)}, T{4, static_cast<NAME>(2)}, T{7, static_cast<NAME>(7)},
          T{9, static_cast<NAME>(17)}, T{7, static_cast<NAME>(7)}, T{12, static_cast<NAME>(2)},
          T{10, static_cast<NAME>(19)}, T{2, static_cast<NAME>(12)}, T{6, static_cast<NAME>(18)},
          T{12, static_cast<NAME>(18)}, T{3, static_cast<NAME>(20)}, T{9, static_cast<NAME>(19)},
          T{8, static_cast<NAME>(18)}, T{3, static_cast<NAME>(6)}, T{6, static_cast<NAME>(18)},
          T{10, static_cast<NAME>(16)}, T{12, static_cast<NAME>(6)}, T{13, static_cast<NAME>(3)},
          T{11, static_cast<NAME>(1)}, T{3, static_cast<NAME>(6)}, T{1, static_cast<NAME>(4)},
          T{9, static_cast<NAME>(11)}, T{11, static_cast<NAME>(2)}, T{12, static_cast<NAME>(16)},
          T{9, static_cast<NAME>(16)}, T{8, static_cast<NAME>(15)}, T{2, static_cast<NAME>(14)},
      };

      const auto sampleRdDates = sample_rd_dates();
      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      }
    }
  }
}
