#include <calendars/cxx/math/common.h>
#include <doctest/doctest.h>
#include <array>
#include "../test_utils.h"

TEST_SUITE("Math - Common") {
  TEST_CASE("[ceil] [floor]") {
    const static auto testData = std::array{
        /* num, denom, floorRes, ceilRes */
        std::array{-11, 10, -2, -1},
        std::array{-10, 10, -1, -1},
        std::array{-9, 10, -1, 0},
        std::array{-1, 10, -1, 0},
        std::array{0, 10, 0, 0},
        std::array{1, 10, 0, 1},
        std::array{9, 10, 0, 1},
        std::array{10, 10, 1, 1},
        std::array{11, 10, 1, 2}
    };
    for (unsigned int i = 0; i < testData.size(); i++) {
      auto c = calendars::math::int_to_pnum(testData.at(i).at(0));
      c /= calendars::math::int_to_pnum(testData.at(i).at(1));
      CHECK_EQ(calendars::math::floor(c), testData.at(i).at(2));
      CHECK_EQ(calendars::math::ceil(c), testData.at(i).at(3));
    }
  }

  TEST_CASE("arctan") {
    using namespace SI::literals;
    auto x = 36;
    auto y = 47;

    REQUIRE_IN_RANGE(
      (std::atan2(y, x)),
      (calendars::math::arctan(y, x).value())
    );
  }
}
