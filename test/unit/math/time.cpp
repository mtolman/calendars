#include <doctest/doctest.h>
#include <calendars/cxx/calendars/astronomical/time.h>
#include <calendars/c/calendars/astronomical/time.h>
#include "../test_utils.h"
#include "calendars/c/calendars/astronomical/time.h"
#include "calendars/cxx/calendars/astronomical/time.h"



TEST_SUITE("Time") {
  TEST_CASE("HH:MM") {
    SUBCASE("C") {
      auto t = 5.25;
      auto res = calendar_time_from_day_fraction(t);
      CHECK_EQ(res.hours, 6);
      CHECK_EQ(res.minutes, 0);
      REQUIRE_IN_RANGE(calendar_time_from_hours_minutes(res), calendars::math::mod(t, 1.));

      t = 0;
      res = calendar_time_from_day_fraction(t);
      CHECK_EQ(res.hours, 0);
      CHECK_EQ(res.minutes, 0);
      REQUIRE_IN_RANGE(calendar_time_from_hours_minutes(res), calendars::math::mod(t, 1.));

      t = 3.125;
      res = calendar_time_from_day_fraction(t);
      CHECK_EQ(res.hours, 3);
      CHECK_EQ(res.minutes, 0);
      REQUIRE_IN_RANGE(calendar_time_from_hours_minutes(res), calendars::math::mod(t, 1.));

      t = 0.0625;
      res = calendar_time_from_day_fraction(t);
      CHECK_EQ(res.hours, 1);
      CHECK_EQ(res.minutes, 30);
      REQUIRE_IN_RANGE(calendar_time_from_hours_minutes(res), calendars::math::mod(t, 1.));

      t = 0.03125;
      res = calendar_time_from_day_fraction(t);
      CHECK_EQ(res.hours, 0);
      CHECK_EQ(res.minutes, 45);

      t = 0.28125;
      res = calendar_time_from_day_fraction(t);
      CHECK_EQ(res.hours, 6);
      CHECK_EQ(res.minutes, 45);
      REQUIRE_IN_RANGE(calendar_time_from_hours_minutes(res), calendars::math::mod(t, 1.));

      t = 0.53125;
      res = calendar_time_from_day_fraction(t);
      CHECK_EQ(res.hours, 12);
      CHECK_EQ(res.minutes, 45);
      REQUIRE_IN_RANGE(calendar_time_from_hours_minutes(res), calendars::math::mod(t, 1.));
    }

    SUBCASE("C++") {
      using namespace SI::literals;
      auto t = 5.25_days;
      auto res = calendars::math::TimeHM(t);
      CHECK_EQ(res.hours(), 6._h);
      CHECK_EQ(res.minutes(), 0._min);
      REQUIRE_IN_RANGE(res.dayFraction().value(), calendars::math::mod(t.value(), 1.));

      t = 0._days;
      res = calendars::math::TimeHM(t);
      CHECK_EQ(res.hours(), 0._h);
      CHECK_EQ(res.minutes(), 0._min);
      REQUIRE_IN_RANGE(res.dayFraction().value(), calendars::math::mod(t.value(), 1.));

      t = 3.125_days;
      res = calendars::math::TimeHM(t);
      CHECK_EQ(res.hours(), 3._h);
      CHECK_EQ(res.minutes(), 0._min);
      REQUIRE_IN_RANGE(res.dayFraction().value(), calendars::math::mod(t.value(), 1.));

      t = 0.0625_days;
      res = calendars::math::TimeHM(t);
      CHECK_EQ(res.hours(), 1._h);
      CHECK_EQ(res.minutes(), 30._min);
      REQUIRE_IN_RANGE(res.dayFraction().value(), calendars::math::mod(t.value(), 1.));

      t = 0.03125_days;
      res = calendars::math::TimeHM(t);
      CHECK_EQ(res.hours(), 0._h);
      CHECK_EQ(res.minutes(), 45._min);
      REQUIRE_IN_RANGE(res.dayFraction().value(), calendars::math::mod(t.value(), 1.));

      t = 0.28125_days;
      res = calendars::math::TimeHM(t);
      CHECK_EQ(res.hours(), 6._h);
      CHECK_EQ(res.minutes(), 45._min);
      REQUIRE_IN_RANGE(res.dayFraction().value(), calendars::math::mod(t.value(), 1.));

      t = 0.53125_days;
      res = calendars::math::TimeHM(t);
      CHECK_EQ(res.hours(), 12._h);
      CHECK_EQ(res.minutes(), 45._min);
      REQUIRE_IN_RANGE(res.dayFraction().value(), calendars::math::mod(t.value(), 1.));
    }
  }
}
