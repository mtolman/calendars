#include <doctest/doctest.h>

#include <calendars/cxx/calendars/unix_timestamp.h>
#include "test_utils.h"

TEST_SUITE("Unix Timestamp") {
  TEST_CASE("Comparisons") {
    comp_test_one_arg<calendars::unix_timestamp::Date>();
  }

  TEST_CASE("Conversions") {
    calendars::unix_timestamp::Date date{-62135683200};

    calendars::Moment expected{0.0};
    CHECK_IN_RANGE(expected.datetime(), calendars::Moment::from(date).datetime(), 0.01)
    CHECK_EQ(date.timestamp(), expected.to<calendars::unix_timestamp::Date>().timestamp());

    CHECK_EQ(calendars::RdDate::from(calendars::RdDate{85747}.to<calendars::unix_timestamp::Date>()), calendars::RdDate{85747});
  }

  TEST_CASE("Sample Data") {
    using T = calendars::unix_timestamp::Date;

    static std::vector<T> expected = {
        T{-80641958400}, T{-67439520000}, T{-59935161600}, T{-57883334400}, T{-47334758400}, T{-43978291200}, T{-40239590400},
        T{-30190147200}, T{-27568339200}, T{-24607411200}, T{-23030611200}, T{-21513859200}, T{-21196166400}, T{-18257443200},
        T{-16848604800}, T{-15075676800}, T{-13136688000}, T{-12932870400}, T{-10147420800}, T{-9135849600},  T{-7997788800},
        T{-6359817600},  T{-4746729600},  T{-4126636800},  T{-2105049600},  T{-1273449600},  T{-891734400},   T{-842745600},
        T{-827971200},   T{700790400},    T{825206400},    T{2172960000},   T{3930249600},
    };

    const auto sampleRdDates = sample_rd_dates();
    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::RdDate::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
