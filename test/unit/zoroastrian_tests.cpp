#include <doctest/doctest.h>

#include <calendars/cxx/calendars/zoroastrian.h>
#include "test_utils.h"

TEST_SUITE("Zoroastrian Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::zoroastrian::Date>();
  }

  TEST_CASE("Conversions") {
    calendars::zoroastrian::Date date;
    calendars::RdDate            rdate;

    rdate     = calendars::RdDate{8849};
    date      = rdate.to<calendars::zoroastrian::Date>();
    rdate     = calendars::RdDate::from(date);
    CHECK_EQ(rdate, calendars::RdDate{8849});
  }
}
