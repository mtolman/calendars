#include <doctest/doctest.h>

#include <calendars/cxx/calendars/clock.h>
#include "test_utils.h"

TEST_SUITE("Clock") {
  TEST_CASE("Clock time") {
    auto time = calendars::Moment{4.500009}.to<calendars::clock::Date>().time();
    CHECK_IN_RANGE_DEF(time, 0.500009);
  }

  TEST_CASE("Clock to_moment") {
    auto time = calendars::Moment::from(calendars::clock::Date{12, 0, 0}).datetime();
    CHECK_IN_RANGE(time, 0.5, 0.001);
  }

  TEST_CASE("Clock pieces") {
    auto time = calendars::clock::Date{11, 34, 22};
    CHECK_EQ(11, time.hour());
    CHECK_EQ(34, time.minute());
    CHECK_EQ(22, time.second());
  }

  TEST_CASE("Clock from_moment") {
    auto clock = calendars::Moment{0.5}.to<calendars::clock::Date>();
    CHECK_EQ(clock.day(), 0);
    CHECK_EQ(clock.hour(), 12);
    CHECK_EQ(clock.minute(), 0);
    CHECK_EQ(clock.second(), 0);

    clock = calendars::Moment{4.5}.to<calendars::clock::Date>();
    CHECK_EQ(clock.day(), 4);
    CHECK_EQ(clock.hour(), 12);
    CHECK_EQ(clock.minute(), 0);
    CHECK_EQ(clock.second(), 0);
  }

  TEST_CASE("Clock nearest_second") {
    auto clock = calendars::Moment{4.5000000000000001}.to<calendars::clock::Date>().nearest_second();
    CHECK_EQ(clock.day(), 0);
    CHECK_EQ(clock.hour(), 12);
    CHECK_EQ(clock.minute(), 0);
    CHECK_EQ(clock.second(), 0);

    clock = calendars::Moment{4.500009}.to<calendars::clock::Date>().nearest_second();
    CHECK_EQ(clock.day(), 0);
    CHECK_EQ(clock.hour(), 12);
    CHECK_EQ(clock.minute(), 0);
    CHECK_EQ(clock.second(), 1);
  }
}
