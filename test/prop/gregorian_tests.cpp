#include <doctest/doctest.h>
#include <calendars/cxx/math/common.h>
#include <calendars/cxx/calendars/gregorian.h>
#include <test-jam-cxx/integrations/doctest.h>

namespace n = test_jam::gens::numbers;

TEST_SUITE("Gregorian") {
  TEST_JAM_CASE("Conversions", n::int32(), n::int16(), n::int16())
  (int32_t year, int16_t month, int16_t day) {
    month = calendars::math::mod(month, 12) + 1;
    day = calendars::math::mod(month, 28) + 1;
    auto start = calendars::gregorian::Date{year, static_cast<CALENDAR_GREGORIAN_MONTH>(month), day};
    auto converted = calendars::RdDate{start.to_rd_date()}.to<calendars::gregorian::Date>();
    TEST_JAM_CHECK_EQ(start, converted);
  };
}
