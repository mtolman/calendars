#pragma once

#include <iomanip>
#include <iostream>

#ifdef RC_ASSERT

#ifndef CHECK_IN_RANGE
#define EPSILON 0.00001
#define CHECK_IN_RANGE(D, V, E)                                                                                                                      \
  CHECK((D) <= (V) + (E));                                                                                                                           \
  CHECK((D) >= (V) - (E));
#define CHECK_IN_RANGE_DEF(D, V) CHECK_IN_RANGE(D, V, EPSILON)
#define CHECK_RET(V, N)                                                                                                                              \
  ([ & ] {                                                                                                                                           \
    const auto res = (V);                                                                                                                            \
    if (!res) {                                                                                                                                      \
      std::cout << "Failed input: " << (N) << "\n";                                                                                                  \
    }                                                                                                                                                \
    CHECK((V));                                                                                                                                      \
    RC_ASSERT((V));                                                                                                                                  \
    return (V);                                                                                                                                      \
  })()
#define CHECK_RETB(V)                                                                                                                                \
  ([ & ] {                                                                                                                                           \
    const auto res = (V);                                                                                                                            \
    RC_ASSERT((V));                                                                                                                                  \
    return (V);                                                                                                                                      \
  })()
#define REQUIRE_IN_RANGE(D, V)                                                                                                                       \
  ([ & ]() {                                                                                                                                         \
    const auto out = (D);                                                                                                                            \
    const auto in  = (V);                                                                                                                            \
    if (!(out <= in + EPSILON && out >= in - EPSILON)) {                                                                                             \
      std::cout << std::setprecision(25) << "Failed input: " << in << "\tFailed output:" << out << "\n";                                             \
    }                                                                                                                                                \
    return CHECK_RET(out <= in + EPSILON, V) && CHECK_RET(out >= in - EPSILON, V);                                                                   \
  })();

#define REQUIRE_IN_RANGE_E(D, V, E)                                                                                                                  \
  ([ & ]() {                                                                                                                                         \
    const auto out = (D);                                                                                                                            \
    const auto in  = (V);                                                                                                                            \
    if (!(out <= in + (E) && out >= in - (E))) {                                                                                                     \
      std::cout << std::setprecision(25) << "Failed input: " << in << "\tFailed output:" << out << "\n";                                             \
    }                                                                                                                                                \
    return CHECK_RET((D) <= (V) + (E), V) && CHECK_RET((D) >= (V)-E, V);                                                                             \
  })();

#endif
#else

#ifndef CHECK_IN_RANGE
#define EPSILON 0.0001
#define CHECK_IN_RANGE(D, V, E)                                                                                                                      \
  CHECK((D) <= (V) + (E));                                                                                                                           \
  CHECK((D) >= (V) - (E));
#define CHECK_IN_RANGE_DEF(D, V) CHECK_IN_RANGE(D, V, EPSILON)
#define CHECK_RET(V, N)                                                                                                                              \
  ([ & ] {                                                                                                                                           \
    const auto res = (V);                                                                                                                            \
    if (!res) {                                                                                                                                      \
      std::cout << "Failed input: " << (N) << "\n";                                                                                                  \
    }                                                                                                                                                \
    CHECK((V));                                                                                                                                      \
    return (V);                                                                                                                                      \
  })()
#define CHECK_RETB(V)                                                                                                                                \
  ([ & ] {                                                                                                                                           \
    const auto res = (V);                                                                                                                            \
    return (V);                                                                                                                                      \
  })()
#define REQUIRE_IN_RANGE(D, E)                                                                                                                       \
  {                                                                                                                                                  \
    const auto out      = (D);                                                                                                                       \
    const auto expected = (E);                                                                                                                       \
    REQUIRE_LE(out, expected + EPSILON);                                                                                                             \
    REQUIRE_GE(out, expected - EPSILON);                                                                                                             \
  }

#define REQUIRE_IN_RANGE_E(D, V, E)                                                                                                                  \
  ([ & ]() {                                                                                                                                         \
    const auto out = (D);                                                                                                                            \
    const auto in  = (V);                                                                                                                            \
    if (!(out <= in + (E) && out >= in - (E))) {                                                                                                     \
      std::cout << std::setprecision(25) << "Failed input: " << in << "\tFailed output:" << out << "\n";                                             \
    }                                                                                                                                                \
    return CHECK_RET((D) <= (V) + (E), V) && CHECK_RET((D) >= (V)-E, V);                                                                             \
  })();
#endif

#endif
