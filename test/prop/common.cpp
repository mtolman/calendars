#include <doctest/doctest.h>
#include <calendars/cxx/calendars/common.h>
#include "common_test_headers.h"
#include <test-jam-cxx/integrations/doctest.h>

namespace n = test_jam::gens::numbers;

TEST_SUITE("Common") {
  TEST_JAM_CASE("Day of week in range",
                n::int32())
    (int32_t day) {
      auto d = calendars::RdDate{day}.day_of_week();
      TEST_JAM_CHECK_GE(static_cast<int32_t>(d), 0);
      TEST_JAM_CHECK_LE(static_cast<int32_t>(d), 6);
    };
}
