#include <calendars/c/calendars/gregorian.h>
#include <tau/tau.h>

TEST(GregorianCalendar, Comparisons) {
  CalendarGregorianDate dateBase      = calendar_gregorian_create(2020, 12, 14);
  CalendarGregorianDate dateCopy      = calendar_gregorian_create(2020, 12, 14);
  CalendarGregorianDate dateLessDay   = calendar_gregorian_create(2020, 12, 12);
  CalendarGregorianDate dateLessYear  = calendar_gregorian_create(2018, 12, 14);
  CalendarGregorianDate dateLessMonth = calendar_gregorian_create(2020, 11, 14);

  CHECK_EQ(calendar_gregorian_compare(&dateBase, &dateBase), 0);

  CHECK_EQ(calendar_gregorian_compare(&dateCopy, &dateBase), 0);
  CHECK_EQ(calendar_gregorian_compare(&dateBase, &dateCopy), 0);

  CHECK_EQ(calendar_gregorian_compare(&dateBase, &dateLessDay), 1);
  CHECK_EQ(calendar_gregorian_compare(&dateBase, &dateLessYear), 1);
  CHECK_EQ(calendar_gregorian_compare(&dateBase, &dateLessMonth), 1);
  CHECK_EQ(calendar_gregorian_compare(&dateLessDay, &dateBase), -1);
  CHECK_EQ(calendar_gregorian_compare(&dateLessYear, &dateBase), -1);
  CHECK_EQ(calendar_gregorian_compare(&dateLessMonth, &dateBase), -1);
}

TEST(GregorianCalendar, Conversions) {
  CalendarGregorianDate dateBase = calendar_gregorian_create(2020, 12, 14);
  CalendarGregorianDate dateConverted = calendar_gregorian_from_rd_date(calendar_gregorian_to_rd_date(&dateBase));

  CHECK_EQ(calendar_gregorian_compare(&dateBase, &dateConverted), 0);
}

TEST(GregorianCalendar, DaysInMonth) {
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_JANUARY), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_JANUARY), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_FEBRUARY), 28);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_FEBRUARY), 29);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_MARCH), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_MARCH), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_APRIL), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_APRIL), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_MAY), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_MAY), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_JUNE), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_JUNE), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_JULY), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_JULY), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_AUGUST), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_AUGUST), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_SEPTEMBER), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_SEPTEMBER), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_OCTOBER), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_OCTOBER), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_NOVEMBER), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_NOVEMBER), 30);
  CHECK_EQ(calendar_gregorian_days_in_month(2011, MONTH_DECEMBER), 31);
  CHECK_EQ(calendar_gregorian_days_in_month(2012, MONTH_DECEMBER), 31);
}
