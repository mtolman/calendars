#include <calendars/c/calendars/common.h>
#include <calendars/c/calendars/holidays/common.h>
#include <tau/tau.h>

TEST(CommonCalendar, DayOfWeek) {
  CHECK_EQ(calendar_day_of_week(0), DAY_OF_WEEK_SUNDAY);
  CHECK_EQ(calendar_day_of_week(1), DAY_OF_WEEK_MONDAY);
  CHECK_EQ(calendar_day_of_week(2), DAY_OF_WEEK_TUESDAY);
  CHECK_EQ(calendar_day_of_week(3), DAY_OF_WEEK_WEDNESDAY);
  CHECK_EQ(calendar_day_of_week(4), DAY_OF_WEEK_THURSDAY);
  CHECK_EQ(calendar_day_of_week(5), DAY_OF_WEEK_FRIDAY);
  CHECK_EQ(calendar_day_of_week(6), DAY_OF_WEEK_SATURDAY);
  
  CHECK_EQ(calendar_day_of_week(7), DAY_OF_WEEK_SUNDAY);
  CHECK_EQ(calendar_day_of_week(8), DAY_OF_WEEK_MONDAY);
  CHECK_EQ(calendar_day_of_week(9), DAY_OF_WEEK_TUESDAY);
  CHECK_EQ(calendar_day_of_week(10), DAY_OF_WEEK_WEDNESDAY);
  CHECK_EQ(calendar_day_of_week(11), DAY_OF_WEEK_THURSDAY);
  CHECK_EQ(calendar_day_of_week(12), DAY_OF_WEEK_FRIDAY);
  CHECK_EQ(calendar_day_of_week(13), DAY_OF_WEEK_SATURDAY);
}


TEST(CommonCalendar, DayOfMCycle) {
  CHECK_EQ(calendar_day_of_m_cycle(0, 5, 0), 0);
  CHECK_EQ(calendar_day_of_m_cycle(1, 5, 0), 1);
  CHECK_EQ(calendar_day_of_m_cycle(2, 5, 0), 2);
  CHECK_EQ(calendar_day_of_m_cycle(3, 5, 0), 3);
  CHECK_EQ(calendar_day_of_m_cycle(4, 5, 0), 4);
  CHECK_EQ(calendar_day_of_m_cycle(5, 5, 0), 0);
  CHECK_EQ(calendar_day_of_m_cycle(6, 5, 0), 1);

  CHECK_EQ(calendar_day_of_m_cycle(0, 5, 1), 4);
  CHECK_EQ(calendar_day_of_m_cycle(1, 5, 1), 0);
  CHECK_EQ(calendar_day_of_m_cycle(2, 5, 1), 1);
  CHECK_EQ(calendar_day_of_m_cycle(3, 5, 1), 2);
  CHECK_EQ(calendar_day_of_m_cycle(4, 5, 1), 3);
  CHECK_EQ(calendar_day_of_m_cycle(5, 5, 1), 4);
  CHECK_EQ(calendar_day_of_m_cycle(6, 5, 1), 0);
}
