#ifndef CALENDAR_CONVERTER_TEST_UTILS_H
#define CALENDAR_CONVERTER_TEST_UTILS_H

#include <tau/tau.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CHECK_RANGE(actual, expected, epsilon) CHECK_GE(actual, expected - epsilon); CHECK_LE(actual, expected + epsilon);

#ifdef __cplusplus
}
#endif

#endif  // CALENDAR_CONVERTER_TEST_UTILS_H
