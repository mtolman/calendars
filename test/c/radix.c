#include <calendars/c/calendars/radix.h>
#include "test_utils.h"

TEST(CalendarRadixDate, BasicOperations) {
  CalendarRadixTemplate radixTemplate;
  const int dayTemplate[] = {7}; // NOLINT
  const int timeTemplate[] = {24, 60, 60}; // NOLINT
  calendar_radix_template_init(&radixTemplate, 1, dayTemplate, 3, timeTemplate); // NOLINT

  const double dayValues[] = {4, 1}; // NOLINT
  const double timeValues[] = {12, 44, 2.88}; // NOLINT
  CalendarRadixDate date;
  calendar_radix_date_init(&date, &radixTemplate, 2, dayValues, 3, timeValues); // NOLINT

  // Template should be free-able as soon as we're done with it
  calendar_radix_template_deinit(&radixTemplate);

  CalendarMoment moment = calendar_radix_date_to_moment(&date);

  CHECK_RANGE(moment, 29.5305888889, 0.0001);

  CHECK_EQ(calendar_radix_date_get(&date, 0), 4);
  CHECK_EQ(calendar_radix_date_get(&date, 1), 1);
  CHECK_EQ(calendar_radix_date_get(&date, 2), 12);
  CHECK_EQ(calendar_radix_date_get(&date, 3), 44);
  CHECK_RANGE(calendar_radix_date_get(&date, 4 ), 2.88, 0.00001);

  calendar_radix_date_deinit(&date);

  // Double free should be safe
  calendar_radix_date_deinit(&date);
  calendar_radix_template_deinit(&radixTemplate);
}
