## Contributor Setup

Install the following software:
* clang-format
* Doxygen (optional, recommended)
  * Latex (MikTex, TexLive, etc)
  * Graphviz (diagrams)
* gitleaks (optional, downloaded automatically)